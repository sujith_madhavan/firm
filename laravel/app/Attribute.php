<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function AttributeValues()
    {
        return $this->hasMany('App\AttributeValue');
    }
}
