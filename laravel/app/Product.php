<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
use App\Product;

class Product extends Model
{

	protected $appends = ['image_path','thumb_image_path','product_category','product_average','two_star','three_star','four_star','five_star' ,'sort_positions','offer_valid'];

    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function productGalleries()
    {
        return $this->hasMany('App\ProductGallery');
    }

    public function productAttributes()
    {
        return $this->hasMany('App\ProductAttribute');
    }

    public function productAttributeValues()
    {
        return $this->hasMany('App\ProductAttributeValue');
    }

    public function productCategories()
    {
        return $this->hasMany('App\ProductCategory');
    }

    public function relatedCategories()
    {
        return $this->hasMany('App\RelatedProduct');
    }

    public function productReviews()
    {
        return $this->hasMany('App\ProductReview');
    }
    public function productGroup()
    {
        return $this->belongsTo('App\ProductGroup');
    }

    public function brandLogo()
    {
        return $this->belongsTo('App\BrandLogo');
    }

    public function getProductCategoryAttribute()
    {
        return $this->productCategories->first();
    }
     public function getSortPositionsAttribute()
    {
        return $this->productAttributes->sortBy('position');
    }

    public function getImagePathAttribute() {
        if(count($this->productGalleries->where('type','IMAGE'))){
            $product_image = $this->productGalleries->first();
            return asset('laravel/public/products').'/'.$product_image->image;           
        }
        else
            return asset('laravel/public/products/default_image.png');

    }

    public function getThumbImagePathAttribute() {
        if(count($this->productGalleries->where('type','IMAGE'))){
            $product_image = $this->productGalleries->first();
            return asset('laravel/public/product-thumbs').'/'.$product_image->image;           
        }
        else
            return asset('laravel/public/product-thumbs/default_image.png');

    }

    public function getProductAverageAttribute() {
        $average = 0;
        if(count($this->productReviews)){
            $total_product_average = 0;
            foreach ( $this->productReviews as $product_review) {
                $product_average = ($product_review->quality + $product_review->value + $product_review->price )/3;
                $total_product_average += $product_average;
            }

            $average = $total_product_average/count($this->productReviews);
        }
        
        return number_format((float)$average, 1, '.', '');
    }

    public function getTwoStarAttribute() {
        $two_star = $this->productReviews->where('quality',2);

        return count($two_star);
    }

    public function getThreeStarAttribute() {
        $three_star = $this->productReviews->where('quality',3);

        return count($three_star);
    }

    public function getFourStarAttribute() {
        $four_star = $this->productReviews->where('quality',4);

        return count($four_star);
    }

    public function getFiveStarAttribute() {
        $five_star = $this->productReviews->where('quality',5);

        return count($five_star);
    }


    public function getOfferValidAttribute() {
        
        $datetime = new DateTime();

        $offer_avail = Product::where('id',$this->id)
                                    ->where('start_date','<=',$datetime)
                                    ->where('end_date','>=',$datetime)
                                    ->first();

        if($offer_avail)                            
            return true;
        else
            return false;
    }


}
