<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandLogo extends Model
{
    protected $appends = ['image_path']; 

	public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function getImagePathAttribute() {
        if(empty($this->image) || $this->image == "")
            return asset('laravel/public/brand_logos/default.jpg');
        else
            return asset('laravel/public/brand_logos').'/'.$this->image;

    }
}
