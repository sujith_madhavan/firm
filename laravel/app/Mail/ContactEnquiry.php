<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEnquiry extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$content)
    {
        $this->name = $name;        
        $this->email = $email; 
        $this->content = $content; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Firmer Contact Enquiry')->view('emails.contact_enquiry');
        //return $this->view('view.name');
    }
}
