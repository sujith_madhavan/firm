<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductEnquiry extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $email;
    public $phone;
    public $type;
    public $content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$phone,$type,$content)
    {
        $this->name = $name;        
         $this->email = $email; 
         $this->phone = $phone; 
         $this->type = $type; 
         $this->content = $content; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Firmer Product Enquiry')->view('emails.product_enquiry');
    }
}
