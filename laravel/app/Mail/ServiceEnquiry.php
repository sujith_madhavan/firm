<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ServiceEnquiry extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $email;
    public $phone;
    public $service;
    public $content;
     public $product_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$phone,$service,$content,$product_name)
    {
         $this->name = $name;        
         $this->email = $email; 
         $this->phone = $phone; 
         $this->service = $service; 
         $this->content = $content; 
          $this->product_name = $product_name; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Firmer Services Enquiry')->view('emails.service_enquiry');
    }
}
