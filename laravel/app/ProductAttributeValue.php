<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeValue extends Model
{

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function productAttribute()
    {
        return $this->belongsTo('App\ProductAttribute');
    }

    public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }
    
    public function attributeValue()
    {
        return $this->belongsTo('App\AttributeValue');
    }
}
