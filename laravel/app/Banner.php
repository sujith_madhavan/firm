<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	protected $appends = ['image_path']; 

	public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function getImagePathAttribute() {
        if(empty($this->image) || $this->image == "")
            return asset('laravel/public/banners/default.jpg');
        else
            return asset('laravel/public/banners').'/'.$this->image;

    }
}
