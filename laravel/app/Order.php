<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
     public function coupon()
    {
        return $this->belongsTo('App\Coupon');
    }

    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function shippingDetail()
    {
        return $this->hasOne('App\ShippingDetail');
    }

    public function billingDetail()
    {
        return $this->hasOne('App\BillingDetail');
    }
}
