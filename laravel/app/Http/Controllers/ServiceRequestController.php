<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\ServiceRequest;
use DB;
use Log;
use Auth;

class ServiceRequestController extends Controller
{
    public function getServiceRequests(){
    	$service_requests = ServiceRequest::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('SERVICE_REQUEST');

        return view('admin.service_requests')->with('service_requests',$service_requests)
        							->with('allowed_operations',$allowed_operations);
    }

    public function viewServiceRequest($service_request_id){

    	$service_request = ServiceRequest::find($service_request_id);

        return view('admin.view_service_request')->with('service_request',$service_request);
    }

    public function deleteServiceRequest($service_request_id){
        $service_request = ServiceRequest::destroy($service_request_id);

        return response()->json(['status'=>'success','msg'=>'Request Deleted']);

    }
}
