<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Manual_billing_product;
use App\Manual_billing;
use DB;
use Log;
use Auth;
class ManualBillingController extends Controller
{
    public function getManualBilling(){

    	$manual_billings = Manual_billing::orderBy('created_at', 'desc')->get();

        return view('admin.manual_billing_list')->with('manual_billings',$manual_billings);
    }
    public function addManualBilling(){

        return view('admin.add_manual_billing');
    }

    public function postManualBilling(Request $request){
        $this->validate($request, [
            'address' => 'required',
            'ref_no' => 'required',
            'subject' => 'required',
            'lists'=> 'required',
        ]);

        //return $request->all();

        $manual_billing = new Manual_billing;
        $manual_billing->address = trim($request->address);
        $manual_billing->ref_no = $request->ref_no;
        $manual_billing->kind_attention = $request->kind_attention;
        $manual_billing->subject = $request->subject;
        $manual_billing->shipping_amount = $request->shipping_amount;
        $manual_billing->net_total = $request->net_total;
        $manual_billing->gst_percentage = $request->gst_percentage;
        $manual_billing->gst_amount = $request->gst_amount;
        // $manual_billing->sgst_percentage = $request->sgst_percentage;
        // $manual_billing->sgst_amount = $request->sgst_amount;
        $manual_billing->net_total = $request->net_total;
        $manual_billing->save();
        foreach ($request->lists as $list){
            $manual_billing_product = new Manual_billing_product;
            $manual_billing_product->manual_billing_id =  $manual_billing->id;
            $manual_billing_product->description = trim($list['description']);
            $manual_billing_product->qty =$list['qty'];
            $manual_billing_product->price =$list['price'];
            // $manual_billing_product->gst =$list['gst'];
            $manual_billing_product->amount =$list['amount'];
            $manual_billing_product->save();
        }
        

        return redirect('admin/manual-billing')->with('message','manual 
        	billing Created')
        ->with('status','success'); 

    }
    public function editManualBilling($id){

        $manual_billing = Manual_billing::find($id);

        $manual_billing_products = Manual_billing_product::where('manual_billing_id',$manual_billing->id)->get();

        return view('admin.edit_manual_billing')->with('manual_billing',$manual_billing)->with('manual_billing_products',$manual_billing_products);
    }

    public function updateManualBilling(Request $request,$id){
        //return $request->all();
        $manual_billing =Manual_billing::find($id);
        $manual_billing->address = trim($request->address);
        $manual_billing->ref_no = $request->ref_no;
        $manual_billing->kind_attention = $request->kind_attention;
        $manual_billing->subject = $request->subject;
        $manual_billing->shipping_amount = $request->shipping_amount;
        $manual_billing->net_total = $request->net_total;
        $manual_billing->gst_percentage = $request->gst_percentage;
        $manual_billing->gst_amount = $request->gst_amount;
        // $manual_billing->sgst_percentage = $request->sgst_percentage;
        // $manual_billing->sgst_amount = $request->sgst_amount;
        $manual_billing->net_total = $request->net_total;
        $manual_billing->save();

        $manual_billing_ids = $request->manual_billing_ids;
        $i = 0;
        foreach ($request->lists as $list){
            if(!empty($list)){
                if(empty($manual_billing_ids[$i]))
                    $manual_billing_product = new Manual_billing_product;
                else
                    $manual_billing_product = Manual_billing_product::find($manual_billing_ids[$i]);

            // $manual_billing_product = new Manual_billing_product;
                $manual_billing_product->manual_billing_id =  $manual_billing->id;
                $manual_billing_product->description = trim($list['description']);
                $manual_billing_product->qty =$list['qty'];
                $manual_billing_product->price =$list['price'];
                // $manual_billing_product->gst =$list['gst'];
                $manual_billing_product->amount =$list['amount'];
                $manual_billing_product->save();
            }
            $i++;
        }

        return redirect('admin/manual-billing')->with('message','manual billing Updated')
        ->with('status','success'); 

    }
    public function printOrderDetail($id)
    {
        $manual_billing = Manual_billing::find($id);

        $manual_billing_products = Manual_billing_product::where('manual_billing_id',$manual_billing->id)->get();
        return view('admin.manual_billing_print')->with('manual_billing',$manual_billing)->with('manual_billing_products',$manual_billing_products);
    }

public function deleteManualBilling($id){

        $manual_billing = Manual_billing::find($id);
        $manual_billing_products = Manual_billing_product::where('manual_billing_id',$manual_billing->id)->delete();
        $manual_billing->delete();
        return response()->json(['status'=>'success','msg'=>'manual billing Deleted']);

    }
    public function deleteManualBillingProduct($id){

        $manual_billing = Manual_billing_product::find($id);
        $manual_billing->delete();

        return Redirect::back()->with('status','success')->with('message','Successfully delete');

    }
}
