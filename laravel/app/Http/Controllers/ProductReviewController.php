<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductReview;
use DB;
use Log;
use Auth;
class ProductReviewController extends Controller
{
    public function getProductReview(){
    	$product_reviews = ProductReview::orderBy('created_at', 'desc')->get();
        return view('admin.product_review')->with('product_reviews',$product_reviews);
    }
     public function updateProductReviewStatus($id,$status){
        $product_review = ProductReview::find($id);
        $product_review->status = $status;
        $product_review->save();

        if($product_review)
            return redirect('admin/product-review')
                        ->with('message','Product Review Status Updated')
                        ->with('status','success');

    }
    public function deleteProductReview($id){
    //$brand_logo = BrandLogo::find($brand_logo_id);

    $product_review = ProductReview::destroy($id);

    return response()->json(['status'=>'success','msg'=>'Product Review Deleted']);

}
}
