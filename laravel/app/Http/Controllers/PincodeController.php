<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Pincode;
use DB;
use Log;
use Auth;
class PincodeController extends Controller
{
    public function getPincodes(){
    	$pincodes = Pincode::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('PINCODE');

        return view('admin.pincodes')->with('pincodes',$pincodes)
        							->with('allowed_operations',$allowed_operations);
    }

    public function postPincode(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'pincode' => 'required|digits:6',
        ]);	

        $pincode = new Pincode;
        $pincode->area = ucwords($request->name);
        $pincode->pincode = $request->pincode;
        $pincode->modified_by = Auth::user()->id;
        $pincode->save();

        return redirect('admin/pincodes')->with('message','Pincode Created')
                        ->with('status','success'); 

    }

    public function updatePincode(Request $request,$pincode_id){
        $this->validate($request, [
            'name' => 'required',
            'pincode' => 'required|digits:6',
        ]);	

        $pincode = Pincode::find($pincode_id);
        $pincode->area = ucwords($request->name);
        $pincode->pincode = $request->pincode;
        $pincode->modified_by = Auth::user()->id;
        $pincode->save();

        return redirect('admin/pincodes')->with('message','Pincode Updated')
                        ->with('status','success'); 

    }

    public function updatePincodeStatus($pincode_id,$status){
        $pincode = Pincode::find($pincode_id);
        $pincode->status = $status;
        $pincode->save();

        if($pincode)
            return redirect('admin/pincodes')
                        ->with('message','Pincode Status Updated')
                        ->with('status','success');

    }

    public function deletePincode($pincode_id){
        $pincode = Pincode::destroy($pincode_id);

        return response()->json(['status'=>'success','msg'=>'Pincode Deleted']);

    }
}
