<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use App\Category;
use App\ProductCategory;
use App\ProductAttributeValue;
use App\CustomerAddress;
use App\Product;
use App\ProductReview;
use App\Customer;
use App\ShippingDetail;
use App\BillingDetail;
use App\Coupon;
use App\ServiceRequest;
use App\Order;
use App\OrderDetail;
use App\Cart;
use App\RelatedProduct;
use App\Mail\ContactEnquiry;
use App\Mail\ContactReplayEnquiry;
use App\Mail\ServiceEnquiry;
use App\Mail\CustomerServiceEnquiry;
use App\Mail\ProductEnquiry;
use App\Mail\ProductReplayEnquiry;
use DateTime;
use DB;
use Log;
use Auth;
use Validator;


class FrontendController extends Controller
{
    public function getCategoryProducts($slug){
        $category = Category::where('slug',$slug)->first();

        $category_image='';

        if(empty($category->parentCategory))
        {
            $category_image = $category->image_path;
        }
        else
        {
            $active_category =$category->parentCategory;
            if(empty($active_category->parentCategory))
            {
                $category_image = $active_category->image_path;
            }
            else
            {
                $parent_category = $active_category->parentCategory;

                $category_image = $parent_category->image_path;
            }
        }

        $categories = array();

        if(empty($category->parentCategory))
        {
            $categories[]= $category->id;

            foreach ($category->childCategories as $childCategory) 
            {
                $categories[] = $childCategory->id;

                foreach ($childCategory->childCategories as $childCategory) 
                {
                    $categories[] = $childCategory->id;
                }
            }

        }
        else
        {
            $active_category =$category->parentCategory;
            //return $active_category;
            if(empty($active_category->parentCategory))
            {
                //$parent_category = $active_category->parentCategory;
                $categories[] = $category->id;
                foreach ($category->childCategories as $childCategory) 
                {
                    $categories[] = $childCategory->id;
                }

            }

            else
            {
                $categories[] = $category->id;

                foreach ($active_category->childCategories as $childCategory) 
                {
                //$categories[] = $childCategory->name;
                    foreach ($childCategory->childCategories as $childCategory) 
                    {
                        $categories[] = $childCategory->id;
                    }
                }
            }
        }

//return  $categories;


        $category_products = ProductCategory::whereIn('category_id',$categories)
        ->orderBy('id','desc')->groupBy('product_id')->paginate(12);

        $selected_attribute_value_ids = array();
        $selected_attribute_ids = array(); 

        session(['attribute_values' => ""]);
        session(['category_id' => $category->id]);
        session(['sort_by' => ""]);                                        

        return view('category_products')->with(['category'=>$category,'category_image'=>$category_image,'category_products'=>$category_products,'selected_attribute_ids'=>$selected_attribute_ids,'selected_attribute_value_ids'=>$selected_attribute_value_ids,'sort_by'=>""]);						
    }

    public function getProduct($slug)
    {
        $product = Product::where('slug',$slug)->first();


        // return $product; $category_image='';

        if(empty($product->product_category->category->parentCategory))
        {
            $category_image = $product->product_category->category->image_path;
        }
        else
        {
            $active_category =$product->product_category->category->parentCategory;

            if(empty($active_category->parentCategory))
            {
                $category_image = $active_category->image_path;
            }
            else
            {
                $parent_category = $active_category->parentCategory;

                $category_image = $parent_category->image_path;
            }
        }
        
        $group_product_attributes = array();
        foreach ($product->productCategories as $productCategory) 
        {
            foreach ($productCategory->category->group->groupAttributes as $groupAttribute)
            {

                $group_product_attributes[] = array('attribute_name'=>$groupAttribute->attribute->common_name,'position'=>$groupAttribute->position,'attribute_id'=>$groupAttribute->attribute->id);
            } 
        }
        $group_product_attributes = array_values(array_sort($group_product_attributes, function ($value) {
            return $value['position'];
        }));
       //Log::info($group_product_attributes);         
        $sorted_attributes = array();         
        foreach ($group_product_attributes as $group_product_attribute) 
        {
            foreach ($product->productAttributes as $product_attribute) {

                if($product_attribute->attribute_id == $group_product_attribute['attribute_id']){
                    $attribute_values = "";
                    foreach ($product_attribute->productAttributeValues as $product_attribute_value){
                        $attribute_values .= $product_attribute_value->attributeValue->value;
                    }

                    $sorted_attributes[] = array('arrtibute_name' => $group_product_attribute['attribute_name'],'arrtibute_values' => $attribute_values );
                }

            }
        }

        //return $sorted_attributes;
        $related_products=RelatedProduct::where('product_id',$product->id)->get();

        $categories = array();

        foreach ($related_products as $related_product) {
            $categories[] = $related_product->category_id;

        }

        $product_categories = ProductCategory::whereIn('category_id', $categories)->limit(6)->get();

        $product_categories = $product_categories->unique('product_id');                                        

    //return $product_categories;
        $product_review_lists = ProductReview::where('product_id',$product->id)->where('status',1)->get();

        $customer = Auth::guard('customers')->user();
        if(Auth::guard('customers')->check())
            {
                $product_review=ProductReview::where('product_id',$product->id)->where('customer_id',$customer->id)->first();
                return view('productdetail')->with('product',$product)->with('product_review',$product_review)->with('product_categories',$product_categories)->with('category_image',$category_image)->with('sorted_attributes',$sorted_attributes)->with('product_review_lists',$product_review_lists);
            }
            else{
                return view('productdetail')->with('product',$product)->with('category_image',$category_image)->with('product_categories',$product_categories)->with('sorted_attributes',$sorted_attributes)->with('product_review_lists',$product_review_lists); 
            } 

        }
        public function reviewProduct(Request $request)
        {
            $this->validate($request, [
                'name' => 'required',
                'quality' => 'required',
                'review_summary' => 'required',
                'review' => 'required',
                'value' => 'required',
                'price' => 'required',
            ]);
//dd($request->product_id);
            $customer = Auth::guard('customers')->user();

            $product_review = ProductReview::where('product_id',$request->product_id)->where('customer_id',$customer->id)->first();

            if(empty($product_review))
                $product_review = new ProductReview;



            $product_review->product_id = $request->product_id;
            $product_review->customer_id = $request->customer_id;
            $product_review->quality = $request->quality;
            $product_review->value = $request->value;
            $product_review->price = $request->price;
            $product_review->name = $request->name;
            $product_review->review_summary = $request->review_summary;
            $product_review->review  = $request->review;
//dd($product_view);
            $product_review->save();

            return Redirect::back()->with('status','success')->with('message','Successfully Review Placed');
        }

        public function getCustomerProductReviews()
        {
            $customer = Auth::guard('customers')->user();

            $product_reviews =ProductReview::where('customer_id',$customer->id)
            ->paginate(10);
//return $product_reviews;
            return view('product_review')->with('product_reviews',$product_reviews);
        }
        public function getCustomerMyorder()
        {
            $customer = Auth::guard('customers')->user();

            $orders =Order::where('customer_id',$customer->id)->where('transaction_status','!=','INITIATED')->orderBy('created_at', 'desc')->get();
//return $orders;
            return view('myorder')->with('orders',$orders);
        }
        public function getOrderDetail($id)
        {

            $customer = Auth::guard('customers')->user();
            $order_details =OrderDetail::where('order_id',$id)->get();
            $order =Order::where('id',$id)->where('customer_id',$customer->id)->first();
//dd($order_detail);
            return view('order_detail')->with('order_details',$order_details)->with('order',$order);
        }


        public function getProductReviewDetail($id)
        {
//dd($id);
            $customer = Auth::guard('customers')->user();

            $product_review_detail =ProductReview::where('id',$id)->first();
//return $product_reviews;
            return view('product_review_detail')->with('product_review_detail',$product_review_detail);
        }

        public function ServiceEnquirySent(Request $request)
        {

            $this->validate($request, [
                'email' => 'required',
                'name' => 'required',
                'phone' =>'required',
                'service'=>'required',
                'message'=>'required',
            ]);
            //return $request->all();

            $name=$request->name;
            $email=$request->email;
            $phone=$request->phone;
            $service=$request->service;
            $content=$request->message;
            $product_name=$request->product_name;

            $service_request = new ServiceRequest;
            $service_request->name = $name;
            $service_request->mobile = $phone;
            $service_request->email = $email;
            $service_request->service = $service;
            $service_request->message = $content;
            $service_request->save();

            Mail::to('enquiry@firmer.in')->send(new ServiceEnquiry($name,$email,$product_name,$phone,$service,$content));
            Mail::to($email)->send(new CustomerServiceEnquiry($name));

            return Redirect::back()->with('status','success')->with('message','Successfuly Sent');
        }

        public function getCartDetail()
        {
            $customer = Auth::guard('customers')->user();

            $carts =Cart::where('customer_id',$customer->id)->get();
//dd($carts);
            return view('cart')->with('carts',$carts);
        }

        public function postAddToCart(Request $request)
        {

            $this->validate($request, [
                'quantity' => 'required',

            ]);
//dd($request->quantity);
            $customer = Auth::guard('customers')->user();

            $cart = Cart::where('product_id',$request->product_id)
            ->where('customer_id',$customer->id)->first();

            if($cart){
                $cart->quantity = $cart->quantity + $request->quantity;
                $cart->save();
                return response()->json(['status'=>'success','msg'=>'Add to Cart']);
            }                

            $cart = new Cart;
            $cart->product_id = $request->product_id;
            $cart->customer_id = $customer->id;
            $cart->quantity = $request->quantity;

            $cart->save();


            return response()->json(['status'=>'success','msg'=>'Add to Cart']);
//return Redirect::back()->with('message','Add to Cart')->with('status','success'); 
        }
        public function postCartAdd(Request $request)
        {

            $this->validate($request, [
                'quantity' => 'required',

            ]);
//dd($request->quantity);
            $customer = Auth::guard('customers')->user();

            DB::table('carts')->where('customer_id', $customer->id)->delete();             
            $cart = new Cart;
            $cart->product_id = $request->product_id;
            $cart->customer_id = $customer->id;
            $cart->quantity = $request->quantity;
            $cart->save();



            $carts =Cart::where('customer_id',$customer->id)->get();

            $totals = 0;
            foreach($carts as $cart)
            {
                if($cart->product->offer_valid)
                    $price = $cart->product->slashed_price;
                else
                    $price = $cart->product->price;

                $product_price = ($cart->quantity*$price) + (($cart->quantity*$price)/100)*$cart->product->gst; 
                $totals += $product_price;

                // $product_price = ($cart->quantity*$price); 
                // $totals += $product_price;
            }


            $order = new Order;
            $order->amount = $totals;
            $order->customer_id=$customer->id;
            $order->transaction_id="";

            $order->transaction_status="INITIATED";
            $order->order_status="INITIATED";


            $order->save();

            $carts =Cart::where('customer_id',$customer->id)->get();
//dd($carts);
            foreach($carts as $cart)
            {
                $product = Product::find($cart->product_id);
//dd($product);
                $order_detail = new OrderDetail;
                $order_detail->order_id = $order->id;
                $order_detail->product_id=$cart->product_id;
                if($product->offer_valid)
                    $price=$product->slashed_price;
                else
                    $price=$product->price;

                $order_detail->price=$price;
                $order_detail->quantity=$cart->quantity;

                $order_detail->gst=$product->gst;
                $gst_amount=(($cart->quantity*$price)/100)*$product->gst; 

                $order_detail->gst_amount=$gst_amount;
                $order_detail->save();
            }
            $order =Order::where('id',$order->id)->where('customer_id',$customer->id)->first();

            $order_details =OrderDetail::where('order_id',$order->id)->get();
            $customer_addresses = CustomerAddress::where('customer_id',$customer->id)->get();
            $billing_addres = ShippingDetail::where('order_id',$order->id)->first();

            return view('shipping_address_edit')->with('customer_addresses',$customer_addresses)->with('order_id',$order->id)->with('order',$order)->with('billing_addres',$billing_addres)->with('order_details',$order_details);

//return Redirect::back()->with('message','Add to Cart')->with('status','success'); 
        }
        public function getCartAdd($id)
        {

//dd($request->quantity);
            $customer = Auth::guard('customers')->user();
            $product =Product::where('id',$id)->first();

            $cart = Cart::where('product_id',$product->id)
            ->where('customer_id',$customer->id)->first();

            if($cart){
                $cart->quantity = $cart->quantity + $product->minimum_quantity;
                $cart->save();
                return Redirect::back()->with('message','Add to Cart')->with('status','success');
            } 

            $cart = new Cart;
            $cart->product_id = $id;
            $cart->customer_id = $customer->id;
            $cart->quantity = $product->minimum_quantity;
            $cart->save();

            return Redirect::back()->with('message','Add to Cart')->with('status','success'); 
        }
        public function deleteCart($id)
        {
            $cart = Cart::find($id);
            $cart->delete();
            return response()->json(['status'=>'success','msg'=>'product Deleted']);
        }

        public function clearCart()
        {
            $customer = Auth::guard('customers')->user();
            DB::table('carts')->where('customer_id', $customer->id)->delete();  
            return response()->json(['status'=>'success','msg'=>'Clear Cart Deleted']);

        }

        public function cartUpdate(Request $request)
        {
            $i=0;
            foreach($request->carts as $cart) {
                $cart = Cart::find($cart);
                $cart->quantity = $request->quantities[$i]; 
                $cart->save();
                $i++;
            }
            return Redirect::back()->with('message','Updated Cart')->with('status','success');
        }


        public function getFilterCategoryProducts(Request $request){

            if ($request->has(['attribute_values', 'category_id'])) {
                $attribute_values = $request->attribute_values;
                $category_id = $request->category_id;
                session(['attribute_values' => $request->attribute_values]);
                session(['category_id' => $request->category_id]);
                session(['sort_by' => $request->sort_by]);
            }
            else{
                $attribute_values = session('attribute_values', '');
                $category_id = session('category_id', '');
                $sort_by = session('sort_by', '');
            }

            //return $request->all();

            // Log::info($attribute_values);
            // Log::info($category_id);


            $sort_by = $request->sort_by;

            //return $sort_by;

            if(empty($category_id))
                return redirect('/');

            $category = Category::find($category_id);

            $categories = array();

            if(empty($category->parentCategory))
            {
                $categories[]= $category->id;

                foreach ($category->childCategories as $childCategory) 
                {
                    $categories[] = $childCategory->id;

                    foreach ($childCategory->childCategories as $childCategory) 
                    {
                        $categories[] = $childCategory->id;
                    }
                }

            }
            else
            {
                $active_category =$category->parentCategory;
                //return $active_category;
                if(empty($active_category->parentCategory))
                {
                    //$parent_category = $active_category->parentCategory;
                    $categories[] = $category->id;
                    foreach ($category->childCategories as $childCategory) 
                    {
                        $categories[] = $childCategory->id;
                    }

                }

                else
                {
                    $categories[] = $category->id;

                    foreach ($active_category->childCategories as $childCategory) 
                    {
                    //$categories[] = $childCategory->name;
                        foreach ($childCategory->childCategories as $childCategory) 
                        {
                            $categories[] = $childCategory->id;
                        }
                    }
                }
            }

            $category_image='';

            if(empty($category->parentCategory))
            {
                $category_image = $category->image_path;
            }
            else
            {
                $active_category =$category->parentCategory;
                if(empty($active_category->parentCategory))
                {
                    $category_image = $active_category->image_path;
                }
                else
                {
                    $parent_category = $active_category->parentCategory;

                    $category_image = $parent_category->image_path;
                }

            }


            if(empty($attribute_values)){
                $category_products = ProductCategory::join('products', 'products.id', '=', 'product_categories.product_id')->whereIn('product_categories.category_id',$categories);
                if(!empty($sort_by))
                //return $sort_by;
                    $category_products = $category_products->orderBy('products.price',$sort_by);

                $category_products = $category_products->paginate(12);

        //return $category_products;

                $selected_attribute_value_ids = array();
                $selected_attribute_ids = array(); 

                return view('category_products')->with(['category'=>$category,'category_image'=>$category_image,'category_products'=>$category_products,'selected_attribute_ids'=>$selected_attribute_ids,'selected_attribute_value_ids'=>$selected_attribute_value_ids,'sort_by'=>$sort_by]);
            }


            $product_attribute_values = ProductAttributeValue::select('product_attribute_values.*')
            ->join('product_attributes', 'product_attributes.id', '=', 'product_attribute_values.product_attribute_id')
            ->join('products', 'products.id', '=', 'product_attributes.product_id')
            ->whereIn('product_attributes.category_id',$categories)
            ->whereIn('product_attribute_values.attribute_value_id', $attribute_values)
            ->groupBy('product_attribute_values.product_id');
            if(!empty($sort_by))
                $product_attribute_values = $product_attribute_values->orderBy('products.price',$sort_by);

            $product_attribute_values = $product_attribute_values->paginate(12);                            


// $product_attribute_values = $product_attribute_values->unique('product_id');

            $selected_attribute_ids = array(); 


            return view('category_products_filter')->with(['category'=>$category,'category_image'=>$category_image,'product_attribute_values'=>$product_attribute_values,'selected_attribute_ids'=>$selected_attribute_ids,'selected_attribute_value_ids'=>$attribute_values,'sort_by'=>$sort_by]);


        }


// public function getFilterProducts(Request $request){
//     // return $request->all();
//     $attribute_values = session('attribute_values', '');
//     $category_id = session('category_id', '');
//     if(empty($attribute_values) || empty($category_id))
//         return redirect('/');


//     $category = Category::find($category_id);

//     $product_attribute_values = ProductAttributeValue::select('product_attribute_values.*')
//                                 ->join('product_attributes', 'product_attributes.id', '=', 'product_attribute_values.product_attribute_id')
//                                 ->where('product_attributes.category_id',$category_id)
//                                 ->whereIn('product_attribute_values.attribute_value_id', $attribute_values)
//                                 ->groupBy('product_attribute_values.product_id')
//                                 ->paginate(1);


//     // $product_attribute_values = $product_attribute_values->unique('product_id');

//     $selected_attribute_ids = array(); 

//     return view('category_products_filter')->with(['category'=>$category,'product_attribute_values'=>$product_attribute_values,'selected_attribute_ids'=>$selected_attribute_ids,'selected_attribute_value_ids'=>$attribute_values]);

// } 
        public function getCouponCode(Request $request)
        {

            $customer = Auth::guard('customers')->user();

            $code =$request->code;
            $amount =$request->amount;
//Log::info($amount);
            $coupon_code = Coupon::where('code',$code)->where('status',1)->first();
            if(!empty($coupon_code))
            {
                $datetime = new datetime();
                $date=$datetime->format('Y-m-d');
//Log::info($date);
                $coupon_date = Coupon::where('code',$code)->where('valid_end_on', '>=',$date)->first();
                if(count($coupon_date)==1)
                {
                    $coupon_star_date = Coupon::where('code',$code)->where('valid_start_on', '<=',$date)->first();

                    if(count($coupon_star_date)==1)
                    {
                        $order_coupon = Order::where('coupon_id',$coupon_code->id)->get();
//Log::info($order_coupon);
                        $usage_limt = Coupon::where('code',$code)->where('usage_limt', '>=',count($order_coupon))->first();


                        if(count($usage_limt)==1)
                        {

                            if($coupon_code->minimum_spent <= $amount)
                            {
                                $carts =Cart::where('customer_id',$customer->id)->get();

                                $totals = 0;
                                foreach($carts as $cart)
                                {
                                    $product_price = ($cart->quantity*$cart->product->price) + (($cart->quantity*$cart->product->price)/100)*$cart->product->gst; 
                                    $totals += $product_price;
                                }
                                $totalamount=$totals-$coupon_code->amount;
                                return response()->json(['status'=>'success','msg'=>'coupon Add Success','amount'=>$coupon_code->amount,'id'=>$coupon_code->id ,'totals'=>$totalamount]);
                            }
                            else
                            {
                                return response()->json(['status'=>'error','msg'=>'You have large amount Purchasing']);
                            }
                        }
                        else
                        {
                            return response()->json(['status'=>'error','msg'=>'coupon code already used']);
                        }
                    }
                    else
                    {
                        return response()->json(['status'=>'error','msg'=>'Coupon Code comming soon']);
                    }

                }
                else
                {
                    return response()->json(['status'=>'error','msg'=>'Coupon Code valid date expire']);
                }
            }
            else
            {
                return response()->json(['status'=>'error','msg'=>'Coupon Code Not Match']);
            }    
        }
        public function getEstimateShipping(Request $request)
        {

            $customer = Auth::guard('customers')->user();
            $postal_code =$request->postal_code;
//Log::info( $postal_code);


            $lat = 1.0; 
            $lng = 1.0;
//$address = str_replace(" ", "+", $request->address);
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$postal_code."&sensor=false&key=AIzaSyBqYNrJVs23qat6nJBU_a-1ElVNHHo6nyM";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($response);
            if($response->status == "OK"){
                $lat = $response->results[0]->geometry->location->lat;
                $lng = $response->results[0]->geometry->location->lng;

            }

            $standard_lat = 13.0625012;
            $standard_long = 80.2102927;

            $theta = $lng - $standard_long;
            $dist = sin(deg2rad($lat)) * sin(deg2rad($standard_lat)) +  cos(deg2rad($lat)) * cos(deg2rad($standard_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $kilometer = $dist * 60 * 1.1515 * 1.609344;

            $per_kilometer = 19;

            $shipping_price = round(($per_kilometer * $kilometer), 2);

            return response()->json(['status'=>'success','msg'=>'Estimate Shipping Pincode','shipping_price'=>$shipping_price,'kilometer'=>$kilometer]);

        }

        public function getCustomerShippingAddress(Request $request)
        {
            $customer = Auth::guard('customers')->user();

            // Log::info($request->all());
            $order = new Order;
            $order->amount = $request->total;
            $order->coupon_id = $request->coupon_id;
            $order->coupon_amount = $request->coupon_amount ;
            $order->customer_id=$customer->id;
            $order->transaction_id="";
            $order->transaction_status="INITIATED";
            $order->order_status="INITIATED";
//$order->created_at    =$transactionDate;

            $order->save();

            $carts =Cart::where('customer_id',$customer->id)->get();
//dd($carts);
            foreach($carts as $cart)
            {
                $product = Product::find($cart->product_id);
//dd($product);
                $order_detail = new OrderDetail;
                $order_detail->order_id = $order->id;
                $order_detail->product_id=$cart->product_id;
                $order_detail->prodcut_name=$product->name;
                $order_detail->product_code=$product->code;
                if($product->offer_valid)
                    $price=$product->slashed_price;
                else
                    $price=$product->price;

                $order_detail->price=$price;

                $order_detail->quantity=$cart->quantity;

                $order_detail->gst=$product->gst;
                $gst_amount=(($cart->quantity*$price)/100)*$product->gst; 

                $order_detail->gst_amount=$gst_amount;


                $order_detail->save();
            }
            $order =Order::where('id',$order->id)->where('customer_id',$customer->id)->first();
            $order_details =OrderDetail::where('order_id',$order->id)->get();
            $customer_addresses = CustomerAddress::where('customer_id',$customer->id)->get();
            $billing_addres = ShippingDetail::where('order_id',$order->id)->first();

            return view('shipping_address_edit')->with('customer_addresses',$customer_addresses)->with('order_id',$order->id)->with('billing_addres',$billing_addres)->with('order_details',$order_details)->with('order',$order);

        }


        public function billingaddress(Request $request)
        {
//Log::info($request->all());

            $billing_detail = new BillingDetail;
            $billing_detail->order_id = $request->order_id;
            $billing_detail->first_name = $request->first_name;
            $billing_detail->last_name = $request->last_name;
            $billing_detail->company = $request->company;
            $billing_detail->mobile = $request->mobile;
            $billing_detail->fax = $request->fax;
            $billing_detail->address = $request->address;
            $billing_detail->address1 = $request->address1;
            $billing_detail->address2 = $request->address2;
            $billing_detail->city = $request->city;
            $billing_detail->state = $request->state;
            $billing_detail->pincode = $request->pincode;
            $billing_detail->country = $request->country;
            $billing_detail->save();

            $customer = Auth::guard('customers')->user();

            $customer_address = new CustomerAddress;
            $customer_address->customer_id = $customer->id;
            $customer_address->first_name = $request->first_name;
            $customer_address->last_name = $request->last_name;
            $customer_address->company = $request->company;
            $customer_address->mobile = $request->mobile;
            $customer_address->fax = $request->fax;
            $customer_address->address = $request->address;
            $customer_address->address1 = $request->address1;
            $customer_address->address2 = $request->address2;
            $customer_address->city = $request->city;
            $customer_address->state = $request->state;
            $customer_address->pincode = $request->pincode;
            $customer_address->country = $request->country;
            $customer_address->save();

            $content = $billing_detail->first_name." ".$billing_detail->last_name."<br>".$billing_detail->company.", ".$billing_detail->mobile."<br>".$billing_detail->address." <br>".$billing_detail->address1." ".$billing_detail->city." ".$billing_detail->state."<br>".$billing_detail->pincode." ".$billing_detail->country; 

            return response()->json(['status'=>'success','msg'=>'billing address Add','billing_address'=>$content,'billing_id'=>$billing_detail->id]);

        }
        public function billingaddressCustomer(Request $request)
        {
//Log::info($request->all());
            $customer = Auth::guard('customers')->user();
            $customer_addresses = CustomerAddress::where('id',$request->billing_address_id)->first();

//Log::info($customer_addresses);

            $billing_detail = new BillingDetail;
            $billing_detail->order_id = $request->order_id;
            $billing_detail->first_name = $customer_addresses->first_name;
            $billing_detail->last_name = $customer_addresses->last_name;
            $billing_detail->company = $customer_addresses->company;
            $billing_detail->mobile = $customer_addresses->mobile;
            $billing_detail->fax = $customer_addresses->fax;
            $billing_detail->address = $customer_addresses->address;
            $billing_detail->address1 = $customer_addresses->address1;
            $billing_detail->address2 = $customer_addresses->address2;
            $billing_detail->city = $customer_addresses->city;
            $billing_detail->state = $customer_addresses->state;
            $billing_detail->pincode = $customer_addresses->pincode;
            $billing_detail->country = $customer_addresses->country;
            $billing_detail->save();


            $content = $billing_detail->first_name." ".$billing_detail->last_name."<br>".$billing_detail->company.", ".$billing_detail->mobile."<br>".$billing_detail->address." <br>".$billing_detail->address1." ".$billing_detail->city." ".$billing_detail->state."<br>".$billing_detail->pincode." ".$billing_detail->country; 

            return response()->json(['status'=>'success','msg'=>'billing address Add','billing_address'=>$content,'billing_id'=>$billing_detail->id]);
        }
        public function shippingaddress(Request $request)
        {
            Log::info($request->all());

            $shipping_detail = new ShippingDetail;
            $shipping_detail->order_id = $request->order_id;
            $shipping_detail->first_name = $request->first_name;
            $shipping_detail->last_name = $request->last_name;
            $shipping_detail->company = $request->company;
            $shipping_detail->mobile = $request->mobile;
            $shipping_detail->fax = $request->fax;
            $shipping_detail->address = $request->address;
            $shipping_detail->address1 = $request->address1;
            $shipping_detail->address2 = $request->address2;
            $shipping_detail->city = $request->city;
            $shipping_detail->state = $request->state;
            $shipping_detail->pincode = $request->pincode;
            $shipping_detail->country = $request->country;
            $shipping_detail->save();

            $customer = Auth::guard('customers')->user();

            $customer_address = new CustomerAddress;
            $customer_address->customer_id = $customer->id;
            $customer_address->first_name = $request->first_name;
            $customer_address->last_name = $request->last_name;
            $customer_address->company = $request->company;
            $customer_address->mobile = $request->mobile;
            $customer_address->fax = $request->fax;
            $customer_address->address = $request->address;
            $customer_address->address1 = $request->address1;
            $customer_address->address2 = $request->address2;
            $customer_address->city = $request->city;
            $customer_address->state = $request->state;
            $customer_address->pincode = $request->pincode;
            $customer_address->country = $request->country;
            $customer_address->save();


            $postal_code =$request->pincode;
            //Log::info( $postal_code);


            $lat = 1.0; 
            $lng = 1.0;
            //$address = str_replace(" ", "+", $request->address);
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$postal_code."&sensor=false&key=AIzaSyBqYNrJVs23qat6nJBU_a-1ElVNHHo6nyM";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($response);
            if($response->status == "OK"){
                $lat = $response->results[0]->geometry->location->lat;
                $lng = $response->results[0]->geometry->location->lng;

            }

            $standard_lat = 13.0625012;
            $standard_long = 80.2102927;

            $theta = $lng - $standard_long;
            $dist = sin(deg2rad($lat)) * sin(deg2rad($standard_lat)) +  cos(deg2rad($lat)) * cos(deg2rad($standard_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $kilometer = $dist * 60 * 1.1515 * 1.609344;

            $per_kilometer = 19;
            $order = Order::find($request->order_id);
            $shipping_price = round(($per_kilometer * $kilometer), 2);

            $order->estimated_shipping =$shipping_price;
            $order->save();

            $order_details =OrderDetail::where('order_id',$order->id)->get();

            $total_amount=0;
            $totals = 0;
            foreach($order_details as $order_detail){

                $product_price = ($order_detail->quantity*$order_detail->price) + (($order_detail->quantity*$order_detail->price)/100)*$order_detail->product->gst; 
                $totals += $product_price;
            }
            $total_amounts = $totals-$order->coupon_amount;
            $total_amount = round(($total_amounts+$shipping_price), 2);

            $content = $shipping_detail->first_name." ".$shipping_detail->last_name."<br>".$shipping_detail->company.", ".$shipping_detail->mobile."<br>".$shipping_detail->address." <br>".$shipping_detail->address1." ".$shipping_detail->city." ".$shipping_detail->state."<br>".$shipping_detail->pincode." ".$shipping_detail->country; 

            return response()->json(['status'=>'success','msg'=>'billing address Add','shipping_address'=>$content,'shipping_price'=>$shipping_price,'total_amount'=>$total_amount]);
        }
        public function shippingaddressCustomer(Request $request)
        {
            Log::info($request->all());
            $customer = Auth::guard('customers')->user();
            $customer_addresses = CustomerAddress::where('id',$request->shipping_address_id)->first();

            $shipping_detail = new ShippingDetail;
            $shipping_detail->order_id = $request->order_id;
            $shipping_detail->first_name = $customer_addresses->first_name;
            $shipping_detail->last_name = $customer_addresses->last_name;
            $shipping_detail->company = $customer_addresses->company;
            $shipping_detail->mobile = $customer_addresses->mobile;
            $shipping_detail->fax = $customer_addresses->fax;
            $shipping_detail->address = $customer_addresses->address;
            $shipping_detail->address1 = $customer_addresses->address1;
            $shipping_detail->address2 = $customer_addresses->address2;
            $shipping_detail->city = $customer_addresses->city;
            $shipping_detail->state = $customer_addresses->state;
            $shipping_detail->pincode = $customer_addresses->pincode;
            $shipping_detail->country = $customer_addresses->country;
            $shipping_detail->save();

            $postal_code =$customer_addresses->pincode;
            //Log::info( $postal_code);


            $lat = 1.0; 
            $lng = 1.0;
            //$address = str_replace(" ", "+", $request->address);
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$postal_code."&sensor=false&key=AIzaSyBqYNrJVs23qat6nJBU_a-1ElVNHHo6nyM";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($response);
            if($response->status == "OK"){
                $lat = $response->results[0]->geometry->location->lat;
                $lng = $response->results[0]->geometry->location->lng;

            }

            $standard_lat = 13.0625012;
            $standard_long = 80.2102927;

            $theta = $lng - $standard_long;
            $dist = sin(deg2rad($lat)) * sin(deg2rad($standard_lat)) +  cos(deg2rad($lat)) * cos(deg2rad($standard_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $kilometer = $dist * 60 * 1.1515 * 1.609344;

            $per_kilometer = 19;
            $order = Order::find($request->order_id);

            $shipping_price = round(($per_kilometer * $kilometer), 2);
            $order->estimated_shipping =$shipping_price;
            $order->save();
            

            $order_details =OrderDetail::where('order_id',$order->id)->get();

            $total_amount=0;
            $totals = 0;
            foreach($order_details as $order_detail){

                $product_price = ($order_detail->quantity*$order_detail->price) + (($order_detail->quantity*$order_detail->price)/100)*$order_detail->product->gst; 
                $totals += $product_price;
            }
            $total_amounts = $totals-$order->coupon_amount;
            $total_amount =  round(($total_amounts+$shipping_price), 2);

            $content = $shipping_detail->first_name." ".$shipping_detail->last_name."<br>".$shipping_detail->company.", ".$shipping_detail->mobile."<br>".$shipping_detail->address." <br>".$shipping_detail->address1." ".$shipping_detail->city." ".$shipping_detail->state."<br>".$shipping_detail->pincode." ".$shipping_detail->country; 

            return response()->json(['status'=>'success','msg'=>'billing address Add','shipping_address'=>$content,'shipping_price'=>$shipping_price,'total_amount'=>$total_amount]);
        }
        public function shippingBillingAddress(Request $request)
        {
            Log::info($request->all());
            $customer = Auth::guard('customers')->user();
            $customer_addresses = BillingDetail::where('id',$request->billing_id)->first();

            $shipping_detail = new ShippingDetail;
            $shipping_detail->order_id = $request->order_id;
            $shipping_detail->first_name = $customer_addresses->first_name;
            $shipping_detail->last_name = $customer_addresses->last_name;
            $shipping_detail->company = $customer_addresses->company;
            $shipping_detail->mobile = $customer_addresses->mobile;
            $shipping_detail->fax = $customer_addresses->fax;
            $shipping_detail->address = $customer_addresses->address;
            $shipping_detail->address1 = $customer_addresses->address1;
            $shipping_detail->address2 = $customer_addresses->address2;
            $shipping_detail->city = $customer_addresses->city;
            $shipping_detail->state = $customer_addresses->state;
            $shipping_detail->pincode = $customer_addresses->pincode;
            $shipping_detail->country = $customer_addresses->country;
            $shipping_detail->save();


            //Log::info( $postal_code);
            $content = $shipping_detail->first_name." ".$shipping_detail->last_name."<br>".$shipping_detail->company.", ".$shipping_detail->mobile."<br>".$shipping_detail->address." <br>".$shipping_detail->address1." ".$shipping_detail->city." ".$shipping_detail->state."<br>".$shipping_detail->pincode." ".$shipping_detail->country; 

            return response()->json(['status'=>'success','msg'=>'billing address Add','shipping_address'=>$content]);
        }
        public function contactEnquirySent(Request $request)
        {
            // Log::info($request->all);
            $this->validate($request, [
                'email' => 'required',
                'name' => 'required',
                'message'=>'required',
            ]);

            $name=$request->name;
            $email=$request->email;
            $content=$request->message;

            Mail::to('enquiry@firmer.in')->send(new ContactEnquiry($name,$email,$content));
            Mail::to($email)->send(new ContactReplayEnquiry($name));

            return Redirect::back()->with('status','success')->with('message','Successfuly Sent');
        }

        public function getSearchedProducts(Request $request){
            $search_name = $request->search_name;
            $category = $request->category;

            if(empty($category) && empty($search_name)){
                $search_name = session('search_key','');
                $category = session('search_category','0');
            }
            else
            {
                session(['search_key' => $search_name]);
                session(['search_category' => $category]);
            }

            if(empty($search_name))
            {
             $category1 = Category::where('slug',$category)->first();


             $categories = array();

             if(empty($category1->parentCategory))
             {
                $categories[]= $category1->id;

                foreach ($category1->childCategories as $childCategory) 
                {
                    $categories[] = $childCategory->id;

                    foreach ($childCategory->childCategories as $childCategory) 
                    {
                        $categories[] = $childCategory->id;
                    }
                }

            }
            else
            {
                $active_category =$category1->parentCategory;
            //return $active_category;
                if(empty($active_category->parentCategory))
                {
                //$parent_category = $active_category->parentCategory;
                    $categories[] = $category1->id;
                    foreach ($category1->childCategories as $childCategory) 
                    {
                        $categories[] = $childCategory->id;
                    }

                }

                else
                {
                    $categories[] = $category1->id;

                    foreach ($active_category->childCategories as $childCategory) 
                    {
                //$categories[] = $childCategory->name;
                        foreach ($childCategory->childCategories as $childCategory) 
                        {
                            $categories[] = $childCategory->id;
                        }
                    }
                }
            }

            $products = Product::select('products.*')
            ->join('product_categories', 'product_categories.product_id', '=', 'products.id');

            if(!empty($categories)) {
                $products = $products->whereIn('product_categories.category_id',$categories)
                ->where('products.status',1)
                ->orderby('products.created_at','desc')
                ->groupBy('products.id')
                ->paginate(12);
            }                        
            $categories = Category::whereIn('id', $categories)->limit(10)->get();
        }
        else
        {



            $category_array = array();
            $search_categories = Category::where('name','like','%'.$search_name.'%')
            ->where('status',1)->get();

            foreach ($search_categories as $search_category) {
                $category_array[] = $search_category->id;
            }                                
            array_push($category_array,$category);

            $products = Product::select('products.*')
            ->join('product_categories', 'product_categories.product_id', '=', 'products.id');


            if(!empty($category_array)) {
                $products = $products->whereIn('product_categories.category_id',$category_array);
            }                        

            $products = $products->orwhere('products.name','like','%'.$search_name.'%')
            ->where('products.status',1)
            ->orderby('products.created_at','desc')
            ->groupBy('products.id')
            ->paginate(12);                       

        // return $products;                        
            $categories = array();

            foreach ($products as $product) {
                foreach ($product->productCategories as $product_category) {
                    $categories[] = $product_category->category_id;
                }
            }                        

            $categories = array_unique($categories);

            $categories = Category::whereIn('id', $categories)->get();
        }
        return view('searched_products')->with('products',$products)
        ->with('categories',$categories)
        ->with('search_name',$search_name);                        


        
    }

    public function getProductNames(){

        $product_autocomplete = array();

        $products = Product::where('status',1)->get();


        foreach ($products as $product) {
            $product_autocomplete[] = $product->name;

        }

        return $product_autocomplete;

    }

    public function printOrderDetail($order_id)
    {

        $order = Order::find($order_id);
        $date = date('Y-m-d H:i:s');
        $month = date('m');
        if($month < 4)
        {
            $to =date('y');
            $from = date('y', strtotime('-1 years'));
        }
        else
        {
            $from =date('y');
            $to = date('y', strtotime('+1 years'));
        }
        //return $order;
        return view('print_order')->with('order',$order)->with('from',$from)->with('to',$to);
    } 

    public function ProductEnquirySent(Request $request)
    {

        $this->validate($request,[
            'email' => 'required',
            'name' => 'required',
            'phone' =>'required',
            'type'=>'required',
            'message'=>'required',
        ]);

            //return $request->all();

        $name=$request->name;
        $email=$request->email;
        $phone=$request->phone;
        $type=$request->type;
        $content=$request->message;

        $service_request = new ServiceRequest;
        $service_request->name = $name;
        $service_request->mobile = $phone;
        $service_request->email = $email;
        $service_request->service = $type;
        $service_request->message = $content;
        $service_request->save();


        Mail::to('enquiry@firmer.in')->send(new ProductEnquiry($name,$email,$phone,$type,$content));
        Mail::to($email)->send(new ProductReplayEnquiry($name));

        return Redirect::back()->with('status','success')->with('message','Enquiry Successfuly Placed');
    }


}
