<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Menu;
use App\Category;
use DB;
use Log;
use Auth;

class MenuController extends Controller
{
    public function getMenus(){
    	$menus = Menu::orderBy('created_at', 'desc')->get();
        $categories = Category::whereNull('parent_id')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('MENU');

        return view('admin.menus')->with('menus',$menus)
                                    ->with('categories',$categories)
        							->with('allowed_operations',$allowed_operations);
    }

    public function postMenu(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'position' => 'required|numeric',
            'category' => 'required',
        ]);

        DB::table('menus')->where('position','>=',$request->position)->increment('position');	

        $menu = new Menu;
        $menu->name = strtoupper($request->name);
        $menu->position = $request->position;
        $menu->category_id = $request->category;
        $menu->modified_by = Auth::user()->id;
        $menu->save();

        return redirect('admin/menus')->with('message','Menu Created')
                        ->with('status','success'); 

    }

    public function updateMenu(Request $request,$menu_id){
        $this->validate($request, [
            'name' => 'required',
            'position' => 'required|numeric',
            'category_id' => 'required',
        ]);

        DB::table('menus')->where('position','>=',$request->position)->increment('position');	

        $menu = Menu::find($menu_id);
        $menu->name = strtoupper($request->name);
        $menu->position = $request->position;
        $menu->category_id = $request->category_id;
        $menu->modified_by = Auth::user()->id;
        $menu->save();

        return redirect('admin/menus')->with('message','Menu Updated')
                        ->with('status','success'); 

    }

    public function updateMenuStatus($menu_id,$status){
        $menu = Menu::find($menu_id);
        $menu->status = $status;
        $menu->save();

        if($menu)
            return redirect('admin/menus')
                        ->with('message','Menu Status Updated')
                        ->with('status','success');

    }

    public function deleteMenu($menu_id){
        $menu = Menu::destroy($menu_id);

        return response()->json(['status'=>'success','msg'=>'Menu Deleted']);

    }
}
