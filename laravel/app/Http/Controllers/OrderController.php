<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Order;
use App\Vendor;
use App\OrderTracking;
use App\ShippingDetail;
use App\Mail\VendorAssign;
use Mail;

class OrderController extends Controller
{
    public function getOrders(){
    	$orders = Order::where('order_status','confirmed')->where('transaction_status','success')->orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('ORDER');

        $order_vendor_confirmeds = Order::orderBy('created_at','desc')->where('order_status','vendor assigned')->get();
        $order_shipped_details = Order::orderBy('created_at','desc')->where('order_status','shipped')->get();
        $order_delivered_details = Order::orderBy('created_at','desc')->where('order_status','delivered')->get();

        $order_cancels = Order::orderBy('created_at','desc')->where('order_status','cancelled')->get();

        return view('admin.orders')->with('orders',$orders)->with('order_vendor_confirmeds',$order_vendor_confirmeds)->with('order_shipped_details',$order_shipped_details)->with('order_delivered_details',$order_delivered_details)->with('order_cancels',$order_cancels)
        ->with('allowed_operations',$allowed_operations);

    }

    public function viewOrder($order_id){
    	$order = Order::find($order_id);
        $vendors = Vendor::all();
        $order_trackings = OrderTracking::where('order_id',$order_id)->get();
        return view('admin.view_order')->with('order',$order)->with('vendors',$vendors)->with('order_trackings',$order_trackings);

    }

    public function getShippingDetail($order_id){
    	$shipping_detail = ShippingDetail::where('order_id',$order_id)->first();

        return view('admin.edit_shipping_detail')->with('shipping_detail',$shipping_detail);

    }

    public function updateShippingDetail(Request $request,$order_id){
        $this->validate($request, [
            'first_name' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'pincode' => 'required',
            'country' => 'required',
        ]);

        $shipping_detail = ShippingDetail::where('order_id',$order_id)->first();
        $shipping_detail->order_id = $order_id;
        $shipping_detail->first_name = $request->first_name;
        $shipping_detail->last_name = $request->last_name;
        $shipping_detail->company = $request->company;
        $shipping_detail->mobile = $request->mobile;
        $shipping_detail->fax = $request->fax;
        $shipping_detail->address = $request->address;
        $shipping_detail->address1 = $request->address1;
        $shipping_detail->address2 = $request->address2;
        $shipping_detail->city = $request->city;
        $shipping_detail->state = $request->state;
        $shipping_detail->pincode = $request->pincode;
        $shipping_detail->country = $request->country;
        $shipping_detail->save();

        return redirect('admin/orders')
        ->with('message','Shipping Address Updated')
        ->with('status','success');
    } 


    public function updateOrderStatus(Request $request,$order_id){
        $this->validate($request, [
            'order_status' => 'required',

        ]);

        $order = Order::find($order_id);
        $order->order_status = $request->order_status;
        $order->comments = $request->comments;
        $order->save();

        return redirect('admin/orders')
        ->with('message','Order Status Updated')
        ->with('status','success');
    }   

    public function vendorAssignStatus(Request $request,$id)
    {
        //return $request->all();
      $order = Order::find($id);

      $vendor = Vendor::where('id',$request->vendor_id)->first();

        //Mail::to($vendor->email)->send(new VendorAssign($order));

      if ($request->has('vendor_id'))
        $order->vendor_id = $request->vendor_id;

    if ($request->has('tracking_no'))
       $order->tracking_no = $request->tracking_no;

   if ($request->has('start_date'))
       $order->estimated_shipping = $request->start_date;

   $order->order_status = $request->status;
   $order->save();

   $order_tracking = new OrderTracking;
   $order_tracking->order_id = $order->id;
   $order_tracking->status = $request->status;
   $order_tracking->comments = $request->comments;
   $order_tracking->save();



   return redirect('admin/orders')
   ->with('message','Order Status Updated')
   ->with('status','success');

}
public function printOrderDetail($order_id)
        {
            // $order = Order::find($order_id);
            // return view('print_order')->with('order',$order);
            $order = Order::find($order_id);
            $date = date('Y-m-d H:i:s');
            $month = date('m');
            if($month < 4)
            {
                $to =date('y');
                $from = date('y', strtotime('-1 years'));
            }
            else
            {
                $from =date('y');
                $to = date('y', strtotime('+1 years'));
            }
            return view('print_order')->with('order',$order)->with('from',$from)->with('to',$to);
        }    
}
