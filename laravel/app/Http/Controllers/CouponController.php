<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Coupon;
use DB;
use Log;
use Auth;

class CouponController extends Controller
{
    public function getCoupons(){
    	$coupons = Coupon::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('COUPON');

        return view('admin.coupons')->with('coupons',$coupons)
        							->with('allowed_operations',$allowed_operations);
    }

    public function addCoupon(){

        return view('admin.add_coupon');
    }

    public function postCoupon(Request $request){
        $this->validate($request, [
            'code' => 'required|unique:coupons,code',
            'description' => 'required',
        ]);

        $coupon = new Coupon;
        $coupon->code = $request->code;
        $coupon->description = $request->description;
        $coupon->valid_on = $request->valid_on;
        $coupon->valid_for = $request->valid_for;
        $coupon->valid_start_on = $request->valid_start_on;
        $coupon->valid_end_on = $request->valid_end_on;
        $coupon->discount_type = $request->discount_type;
        $coupon->amount = $request->amount;
        $coupon->minimum_spent = $request->minimum_spent;
        $coupon->maximum_spent = $request->maximum_spent;
        $coupon->usage_limt = $request->usage_limt;
        $coupon->usage_limt_per_user = $request->usage_limt_per_user;
        $coupon->modified_by = Auth::user()->id;
        $coupon->save();

        return redirect('admin/coupons')->with('message','Coupon Created')
                        ->with('status','success'); 

    }

    public function viewCoupon($coupon_id){

    	$coupon = Coupon::find($coupon_id);

        return view('admin.view_coupon')->with('coupon',$coupon);
    }

    public function editCoupon($coupon_id){
        $coupon = Coupon::find($coupon_id);

        return view('admin.edit_coupon')->with('coupon',$coupon);
    }

    public function updateCoupon(Request $request,$coupon_id){
        $this->validate($request, [
            'code' => 'required|unique:coupons,code,'.$coupon_id,
            'description' => 'required',
        ]);

        $coupon = Coupon::find($coupon_id);
        $coupon->code = $request->code;
        $coupon->description = $request->description;
        $coupon->valid_on = $request->valid_on;
        $coupon->valid_for = $request->valid_for;
        $coupon->valid_start_on = $request->valid_start_on;
        $coupon->valid_end_on = $request->valid_end_on;
        $coupon->discount_type = $request->discount_type;
        $coupon->amount = $request->amount;
        $coupon->minimum_spent = $request->minimum_spent;
        $coupon->maximum_spent = $request->maximum_spent;
        $coupon->usage_limt = $request->usage_limt;
        $coupon->usage_limt_per_user = $request->usage_limt_per_user;
        $coupon->modified_by = Auth::user()->id;
        $coupon->save();

        return redirect('admin/coupons')->with('message','Coupon Updated')
                        ->with('status','success'); 

    }

    public function updateCouponStatus($coupon_id,$status){
        $coupon = Coupon::find($coupon_id);
        $coupon->status = $status;
        $coupon->save();

        if($coupon)
            return redirect('admin/coupons')
                        ->with('message','Coupon Status Updated')
                        ->with('status','success');

    }

}
