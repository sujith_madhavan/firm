<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use App\RoleModule;
use Auth;
use Log;

class OperationController extends Controller
{
    public function checkOperations($module_code){

        $add = false;
        $view = false;
        $edit = false;
        $status = false;
        $remove = false;

        $role_id = Auth::user()->role_id;
        if(Auth::user()->role->code == 'SUPER_ADMIN')
        	return array('add' => !$add, 'view' => !$view, 'edit' => !$edit, 'status' => !$status, 'remove' => !$remove);


        $module = Module::where('code',$module_code)->first();

        $role_module = RoleModule::where('role_id',$role_id)->where('module_id',$module->id)->first();
        if(!$role_module)
            return redirect('admin/home')
        				->with('message','Unauthorized Role Module Error')
                        ->with('status','error');

        if(!empty($role_module->roleModuleOperations)){
            foreach ($role_module->roleModuleOperations as $roleModuleOperation) {
               $type =  $roleModuleOperation->operation->type;
               switch ($type) {
                    case 'ADD':
                        $add = true;
                        break;

                    case 'VIEW':
                        $view = true;
                        break;
                   
                    case 'EDIT':
                        $edit = true;
                        break;
                   
                    case 'STATUS':
                       $status = true;
                       break;

                    case 'REMOVE':
                       $remove = true;
                       break;         

                    default:
                        # code...
                        break;
               }
            }
        }

        return array('add' => $add, 'view' => $view, 'edit' => $edit, 'status' => $status, 'remove' => $remove); 

    }
}
