<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\User;
use App\Role;
use Log;
use Hash;
use Auth;


class UserController extends Controller
{
    public function getUsers(){
    	$users = User::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('USER');

        return view('admin.users')->with('users',$users)
                                    ->with('allowed_operations',$allowed_operations);
    }

    public function addUser(){
        $roles = Role::where('code','!=','SUPER_ADMIN')->where('status',1)->get();

        return view('admin.add_user')->with('roles',$roles);
    }

    public function postUser(Request $request){
        $this->validate($request, [
            'email' => 'required|unique:users,email',
            'name' => 'required',
            'password' => 'required',
            'role' => 'required',
        ]);

        $hashed_password = Hash::make($request->password);

        if ($request->hasFile('image')){
	        $this->validate($request, [
	        	'image' => 'required|image',
			]);


			// Start of saving Image to server 
			$photo = $request->file('image');
			// Creating Names for Image
		    $imagename = 'user-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();

		    // Saving to server without resizing
		    $image = $request->file('image')->move(public_path('/users/'),$imagename);	
		    // End of Image saving  
		}

        $user = new User;
        $user->name = ucwords($request->name);
        $user->email = $request->email;
        $user->password = $hashed_password;
        if ($request->hasFile('image'))
        	$user->image = $imagename;

        $user->modified_by = Auth::user()->id;
        $user->role_id = $request->role;
        $user->save();

        return redirect('admin/home')->with('message','User Created')
                        ->with('status','success'); 

    }

    public function viewUser($user_id){

    	$user = User::find($user_id);

        return view('admin.view_user')->with('user',$user);
    }

    public function editUser($user_id){
        $roles = Role::where('code','!=','SUPER_ADMIN')->get();
        $user = User::find($user_id);

        return view('admin.edit_user')->with('user',$user)
                                    ->with('roles',$roles);
    }

    public function updateUser(Request $request,$user_id){
        $this->validate($request, [
            'email' => 'required|unique:users,email,'.$user_id,
            'name' => 'required',
            'role' => 'required',
        ]);

        if ($request->hasFile('image')){
            $this->validate($request, [
                'image' => 'required|image',
            ]);


            // Start of saving Image to server 
            $photo = $request->file('image');
            // Creating Names for Image
            $imagename = 'user-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();

            // Saving to server without resizing
            $image = $request->file('image')->move(public_path('/users/'),$imagename);  
            // End of Image saving  
        }

        $user = User::find($user_id);
        $user->name = ucwords($request->name);
        $user->email = $request->email;

        if ($request->hasFile('image'))
            $user->image = $imagename;

        $user->modified_by = Auth::user()->id;
        $user->role_id = $request->role;
        $user->save();

        return redirect('admin/users')->with('message','User Updated')
                        ->with('status','success'); 

    }

    public function updateUserStatus($user_id,$status){
        $user = User::find($user_id);
        $user->status = $status;
        $user->save();

        if($user)
            return redirect('admin/users')
                        ->with('message','User Status Updated')
                        ->with('status','success');

    }


}
