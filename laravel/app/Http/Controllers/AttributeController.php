<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Attribute;
use App\AttributeValue;
use App\ProductAttributeValue;
use DB;
use Log;
use Auth;

class AttributeController extends Controller
{
    public function getAttributes(){
    	$attributes = Attribute::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('ATTRIBUTE');

        return view('admin.attributes')->with('attributes',$attributes)
        ->with('allowed_operations',$allowed_operations);
    }

    public function addAttribute(){
        return view('admin.add_attribute');
    }


    public function postAttribute(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'values' => 'required',
            'common_name' => 'required',
        ]);	

        $attribute = new Attribute;
        $attribute->name = ucwords($request->name);
        $attribute->common_name = ucwords($request->common_name);
        $attribute->modified_by = Auth::user()->id;
        $attribute->save();

        foreach ($request->values as $value) {
            if(!empty($value)){
                $attribute_value = new AttributeValue;
                $attribute_value->attribute_id = $attribute->id;
                $attribute_value->value = strtoupper($value);
                $attribute_value->save();
            }    
        }

        return redirect('admin/attributes')->with('message','Attribute Created')
        ->with('status','success'); 

    }

    public function editAttribute($attribute_id){
        $attribute = Attribute::find($attribute_id);

        return view('admin.edit_attribute')->with('attribute',$attribute);
    }

    public function updateAttribute(Request $request,$attribute_id){
        $this->validate($request, [
            'name' => 'required',
            'values' => 'required',
            'common_name' => 'required',
        ]);	


        $attribute = Attribute::find($attribute_id);
        $attribute->name = ucwords($request->name);
        $attribute->common_name = ucwords($request->common_name);
        $attribute->modified_by = Auth::user()->id;
        $attribute->save();

        // DB::table('attribute_values')->where('attribute_id', $attribute_id)->delete();
        $attribute_ids = $request->attribute_ids;
        $i = 0;
        foreach ($request->values as $value) {
            if(!empty($value)){
                if(empty($attribute_ids[$i]))
                    $attribute_value = new AttributeValue;
                else
                    $attribute_value = AttributeValue::find($attribute_ids[$i]);

                $attribute_value->attribute_id = $attribute->id;
                $attribute_value->value = strtoupper($value);
                $attribute_value->save();
            } 
            $i++;   
        }

        return redirect('admin/attributes')->with('message','Attribute Updated')
        ->with('status','success'); 

    }

    public function updateAttributeStatus($attribute_id,$status){
        $attribute = Attribute::find($attribute_id);
        $attribute->status = $status;
        $attribute->save();

        if($attribute)
            return redirect('admin/attributes')
        ->with('message','Attribute Status Updated')
        ->with('status','success');

    }
    public function deleteAttribute($id){

    //return $id;

        $attribute_value = AttributeValue::find($id);


        $attribute_value1 =ProductAttributeValue::where('attribute_value_id', $attribute_value->id)->first();
        //return $attribute_value1;

        if(!empty($attribute_value1))
        {
           return response()->json(['status'=>'error','message'=>'Product Not Delete']);
        }
        else
        {
             $attribute_value->delete();
            return response()->json(['status'=>'success','message'=>'Product Delete successfully']);
            
        }

    }
}
