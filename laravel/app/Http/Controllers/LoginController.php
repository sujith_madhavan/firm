<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendOtp;
use App\Mail\SendUserPassword;
use App\User;
use App\BlockedIp;
use Validator;
use Auth;
use DB;
use Log;
use Hash;
use Mail;

class LoginController extends Controller
{
    public function checkUser(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $blocked_ip = BlockedIp::where('ip_address',$request->ip())->first();
        if(!$blocked_ip){
            $blocked_ip = new BlockedIp;
            $blocked_ip->ip_address = $request->ip();
            $blocked_ip->attempts = 0;
        }

        $blocked_ip->attempts = $blocked_ip->attempts + 1;
        $blocked_ip->save();

        if($blocked_ip->attempts >= 3)
            return redirect('admin/login')
                        ->with('message','Your Ip Blocked Please contact Admin')
                        ->with('status','error');

        $user = User::where('email',$request->email)->first();
        if(!$user)
            return redirect('admin/login')->with('message','Invalid Email')
                        ->with('status','error');

        if (!Hash::check($request->password, $user->password))
            return redirect('admin/login')->with('message','Invalid Password')
                        ->with('status','error');
               

        $otp = rand(1000,999999);
        $user->otp = $otp;
        $user->last_accessed_ip = $request->ip();
        $user->save();

        $blocked_ip->delete();

        Mail::to($user->email)->send(new SendOtp($user->name,$otp));               

        return view('admin.auth.otp_login')->with('email',$request->email)
                                ->with('password',$request->password);
    }

    public function checkUserOtp(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
            'otp' => 'required',
        ]);


        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'otp' => $request->otp, 'status' => 1])){
            $user = Auth::user();
            $user->otp = 0;
            $user->last_accessed_ip = $request->ip();
            $user->save();
            return redirect('admin/home');
        }
        else
            return redirect('admin/login')->with('message','Invalid Otp')
                        ->with('status','error'); 
  
    }

    public function logoutUser(Request $request){
        Auth::logout();
        $request->session()->flush();
        return redirect('admin/login');
    }

    public function forgotPassword(Request $request){
        $this->validate($request, [
            'email' => 'required',
        ]);


        $user = User::where('email',$request->email)->first();
        if(!$user)
            return redirect('admin/forgot-password')->with('message','Invalid Email')
                        ->with('status','error');

               
        $password = str_random(7);
        $hashedPassword = Hash::make($password);

        $user->password = $hashedPassword;
        $user->last_accessed_ip = $request->ip();
        $user->save();

        Mail::to($user->email)->send(new SendUserPassword($user->name,$password));               
        return redirect('admin/login');
    }

    public function getBlockedIps(){
        $blocked_ips = BlockedIp::all();

        return view('admin.blocked_ips')->with('blocked_ips',$blocked_ips);
                                    
    }

    public function removeBlockedIp($blocked_ip_id){
        $blocked_ip = BlockedIp::find($blocked_ip_id);
        $blocked_ip->delete();


        return redirect('admin/blocked-ips')
                        ->with('message','Ip Unblocked')
                        ->with('status','success');

    }
}
