<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Product;
use App\ProductGallery;
use App\Group;
use App\Attribute;
use App\GroupAttribute;
use App\Category;
use App\ProductAttribute;
use App\ProductCategory;
use App\RelatedProduct;
use App\ProductAttributeValue;
use App\ProductGroup;
use App\GroupProduct;
use App\AttributeValue;
use App\BrandLogo;
use App\OrderDetail;
use App\ProductReview;
use App\Cart;
use DB;
use Log;
use Auth;
use Image;
use File;
use Excel;
use Validator;
class ProductController extends Controller
{
    public function getproduct(){

        $products = Product::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('PRODUCT');

        $categories = Category::where('status',1)->get();
        
        return view('admin.productlist')->with(['products'=>$products,'allowed_operations'=>$allowed_operations,'categories'=>$categories]);
    }

    public function addProduct(){
        $categories = Category::where('status',1)->whereNull('parent_id')->get();
        $product_groups = ProductGroup::where('status',1)->get();
        $brand_logos = BrandLogo::where('status',1)->get();

        return view('admin.add_product')->with(['categories'=>$categories,'product_groups'=>$product_groups,'brand_logos'=>$brand_logos]);
    }

    public function getAttributes(Request $request)
    {

        $category_ids = $request->category_ids;
        if(empty($category_ids))
            return response()->json(['status'=>'error','msg'=>'No Category Id']);


        $attributes = array();
        $attribute_ids = array();
        $content = '';
        foreach ($category_ids as $category_id) {
            $category = Category::find($category_id);
            if($category){
                $group_attributes = GroupAttribute::where('group_id',$category->group_id)
                ->get();


                foreach ($group_attributes as $group_attribute) 
                {
                    if(!in_array($group_attribute->attribute->id, $attribute_ids)){
                        $attribute_ids[] = $group_attribute->attribute->id;

                        $attribute = '';
                        foreach ($group_attribute->attribute->AttributeValues as $attribute_value) {
                            $attribute .= '<div class="checkbox checkbox-danger checkbox-circle" style="display: inline;">
                            <input type="checkbox" name="'.$group_attribute->attribute->name.'[]" value="'.$attribute_value->id.'">
                            <label for="">'.$attribute_value->value.'</label></div>&nbsp';

                        }


                        $content .= '<div class="form-group">
                        <label class="control-label mb-10 text-left">'.$group_attribute->attribute->name.'</label><br>
                        '.$attribute.'</div>';

                    }   

                } 
            }
        }
        
        return response()->json(['status'=>'success','msg'=>'Attributes','content'=>$content]);
    }

    public function checkProductDetails(Request $request)
    {
        $product_name = $request->product_name;
        $product_code = $request->product_code;

        if(empty($product_name) || empty($product_code))
            return response()->json(['status'=>'error','msg'=>'Empty Values']);

        $product_name = Product::where('name',$product_name)->first();
        if($product_name)
            return response()->json(['status'=>'error','msg'=>'Product Name Already Exists']);

        $product_code = Product::where('code',$product_code)->first();
        if($product_code)
            return response()->json(['status'=>'error','msg'=>'Product Code Already Exists']);

        return response()->json(['status'=>'success','msg'=>'No Errors']);

    }

    public function checkUpdateProductDetails(Request $request)
    {
        $product_name = $request->name;
        $product_code = $request->code;
        $product_id = $request->product_id;

        if(empty($product_name) || empty($product_code) || empty($product_id))
            return response()->json(['status'=>'error','msg'=>'Empty Values']);

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:products,name,'.$product_id,
            'code' => 'required|unique:products,code,'.$product_id,
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>'error','msg'=>'Product Code/Name Already Exists']);
        }

        return response()->json(['status'=>'success','msg'=>'No Errors']);

    }

    public function postProduct(Request $request){

        $this->validate($request, [
            'categories' => 'required',
            'name' => 'required|unique:products,name',
            'code' => 'required|unique:products,code',
            'old_price' => 'required',
            'stock' => 'required',
            'minimum_quantity' => 'required',
            'related_categories' => 'required',
            'description' => 'required',
            'description' => 'required',
            'gst' => 'required',
        ]);


  


        $video1 = $request->video1;
        if(!$request->hasFile('image1') && empty($video1))
            return redirect()->back()->with('message','Select Atleat One Image or Video')
        ->with('status','success');

        if ($request->hasFile('image1')) {
            $this->validate($request, [
                'image1' => 'required|image',
            ]);

            $image1 = $this->saveProductImage($request->file('image1'));
        }    

        if ($request->hasFile('image2')) {
            $this->validate($request, [
                'image2' => 'required|image',
            ]);

            $image2 = $this->saveProductImage($request->file('image2'));
        }  

        if ($request->hasFile('image3')) {
            $this->validate($request, [
                'image3' => 'required|image',
            ]);

            $image3 = $this->saveProductImage($request->file('image3'));
        }  
        if ($request->hasFile('image4')) {
            $this->validate($request, [
                'image4' => 'required|image',
            ]);

            $image4 = $this->saveProductImage($request->file('image4'));
        }  

        $product = new Product;
        $product->name = $request->name;
        $product->code = $request->code;
        if(empty($request->hsc_code))
            $product->hsc_code = "";
        else
            $product->hsc_code = $request->hsc_code;

        $product->description = $request->description;
        $product->slug = str_slug($request->name, '-');
        $product->slashed_price = floatval($request->new_price);
        $product->price = floatval($request->old_price);
        $product->gst = floatval($request->gst);
        $product->price_for = $request->price_for;
        if ($request->has('discount'))
            $product->discount = $request->discount;

        if ($request->has('start_date'))
            $product->start_date = date("Y-m-d", strtotime($request->start_date));

        if ($request->has('end_date'))
            $product->end_date = date("Y-m-d", strtotime($request->end_date));

        if ($request->has('box_quantity'))
            $product->box_quantity =  $request->box_quantity;

        $product->stock = intval($request->stock);
        $product->minimum_quantity = intval($request->minimum_quantity);
        if ($request->has('featured'))
            $product->featured = 1;

        if ($request->has('new_product'))
            $product->new_product = 1;

        if ($request->has('top_selling'))
            $product->top_selling = 1;

        if ($request->has('product_group') && !empty($request->product_group))
            $product->product_group_id = $request->product_group;

        if ($request->has('brand_id'))
            $product->brand_logo_id = $request->brand_id;

        $product->modified_by = Auth::user()->id;
        $product->save();

        if ($request->has('product_group') && !empty($request->product_group)){
            $group_product = new GroupProduct;
            $group_product->product_group_id = $request->product_group;
            $group_product->product_id = $product->id;
            $group_product->save();

        }

        $category_ids = $request->categories;
        foreach ($category_ids as $category_id) {
            $category = Category::find($category_id);
            if($category){
                $product_category = new ProductCategory;
                $product_category->product_id = $product->id;
                $product_category->category_id = $category_id;
                $product_category->save();

                $group_attributes = GroupAttribute::where('group_id',$category->group_id)->get();
                    //print_r($groupid);die;

                foreach ($group_attributes as $group_attribute) 
                {
                    if ($request->has($group_attribute->attribute->name)){
                        $product_attribute = new ProductAttribute;
                        $product_attribute->category_id = $category_id;
                        $product_attribute->product_id = $product->id;
                        $product_attribute->attribute_id = $group_attribute->attribute->id;

                        $product_attribute->save();


                        foreach ($request->input($group_attribute->attribute->name) as $value_id) {
                            $product_attribute_value = new ProductAttributeValue;
                            $product_attribute_value->product_id = $product->id;
                            $product_attribute_value->product_attribute_id = $product_attribute->id;
                            $product_attribute_value->attribute_id = $group_attribute->attribute->id;
                            $product_attribute_value->attribute_value_id = $value_id;
                            $product_attribute_value->save();
                        }
                    }

                }
            } 

        }


        if ($request->hasFile('image1')) {
            $product_gallery = new ProductGallery;
            $product_gallery->product_id = $product->id;
            $product_gallery->image = $image1;
            $product_gallery->type = 'IMAGE';
            $product_gallery->save();
        }
        else{
            $product_gallery = new ProductGallery;
            $product_gallery->product_id = $product->id;
            $product_gallery->video = $request->video1;
            $product_gallery->type = 'VIDEO';
            $product_gallery->save();
        }    

        if ($request->hasFile('image2')) {
            $product_gallery = new ProductGallery;
            $product_gallery->product_id = $product->id;
            $product_gallery->image = $image2;
            $product_gallery->type = 'IMAGE';
            $product_gallery->save();
        }
        elseif ($request->has('video2')) {
            $product_gallery = new ProductGallery;
            $product_gallery->product_id = $product->id;
            $product_gallery->video = $request->video2;
            $product_gallery->type = 'VIDEO';
            $product_gallery->save();
        }  

        if ($request->hasFile('image3')) {
            $product_gallery = new ProductGallery;
            $product_gallery->product_id = $product->id;
            $product_gallery->image = $image3;
            $product_gallery->type = 'IMAGE';
            $product_gallery->save();
        }
        elseif ($request->has('video3')) {
            $product_gallery = new ProductGallery;
            $product_gallery->product_id = $product->id;
            $product_gallery->video = $request->video3;
            $product_gallery->type = 'VIDEO';
            $product_gallery->save();
        }  
        if ($request->hasFile('image4')) {
            $product_gallery = new ProductGallery;
            $product_gallery->product_id = $product->id;
            $product_gallery->image = $image3;
            $product_gallery->type = 'IMAGE';
            $product_gallery->save();
        }
        elseif ($request->has('video4')) {
            $product_gallery = new ProductGallery;
            $product_gallery->product_id = $product->id;
            $product_gallery->video = $request->video3;
            $product_gallery->type = 'VIDEO';
            $product_gallery->save();
        }  

        $related_category_ids = $request->related_categories;
        foreach ($related_category_ids as $related_category_id) {
            $related_product = new RelatedProduct;
            $related_product->product_id = $product->id;
            $related_product->category_id = $related_category_id;
            $related_product->save();
        }


        return redirect('admin/products')->with('message','Product Created')
        ->with('status','success'); 


    }


    public function saveProductImage($image){

        $extension = $image->getClientOriginalExtension();
        $imagename =  'Product-'.date("Ymdgis").rand(0,100). '.' . $image->getClientOriginalExtension();

        $destinationPath = public_path('/product-thumbs');
        $thumb_img = Image::make($image->getRealPath())->resize(233, 172);
        $thumb_img->save($destinationPath.'/'.$imagename,90);


        $image_path = public_path('/products');
        $image = Image::make($image->getRealPath())->resize(425, 400);
        $image->save($image_path.'/'.$imagename,90);

        return $imagename;
    }



    public function editProduct($product_id){
        $product = Product::find($product_id);
        $categories = Category::where('status',1)->whereNull('parent_id')->get();
        $product_groups = ProductGroup::where('status',1)->get();
        $brand_logos = BrandLogo::where('status',1)->get();

        $product_categories = ProductCategory::where('product_id',$product_id)
        ->get();

        $selected_categories = array();                                   
        foreach ($product_categories as $product_category) {
            $selected_categories[] = $product_category->category_id;
        }

        $related_products = RelatedProduct::where('product_id',$product_id)
        ->get();

        $related_categories = array();                                   
        foreach ($related_products as $related_product) {
            $related_categories[] = $related_product->category_id;
        }

        return view('admin.edit_product')->with(['product'=>$product,'categories'=>$categories,'selected_categories'=>$selected_categories,'related_categories'=>$related_categories,'product_groups'=>$product_groups,'brand_logos'=>$brand_logos]);

    }

    public function updateProduct(Request $request,$product_id){
        $this->validate($request, [
            'categories' => 'required',
            'name' => 'required|unique:products,name,'.$product_id,
            'code' => 'required|unique:products,code,'.$product_id,
            'old_price' => 'required',
            'stock' => 'required',
            'minimum_quantity' => 'required',
            'related_categories' => 'required',
            'description' => 'required',
            'gst' => 'required',
        ]);



        log::info($request->all());
    //return $request->all();

        if ($request->hasFile('image1')) {
            $this->validate($request, [
                'image1' => 'required|image|',
            ]);

            $image1 = $this->saveProductImage($request->file('image1'));
        }    

        if ($request->hasFile('image2')) {
            $this->validate($request, [
                'image2' => 'required|image|',
            ]);

            $image2 = $this->saveProductImage($request->file('image2'));
        }  

        if ($request->hasFile('image3')) {
            $this->validate($request, [
                'image3' => 'required|image|',
            ]);

            $image3 = $this->saveProductImage($request->file('image3'));
        } 
        if ($request->hasFile('image4')) {
            $this->validate($request, [
                'image4' => 'required|image',
            ]);

            $image4 = $this->saveProductImage($request->file('image4'));
        }  

        DB::table('product_attribute_values')->where('product_id', $product_id)->delete();
        DB::table('product_attributes')->where('product_id', $product_id)->delete();
        DB::table('product_categories')->where('product_id', $product_id)->delete();
        DB::table('related_products')->where('product_id', $product_id)->delete();  
        DB::table('group_products')->where('product_id', $product_id)->delete();  

        $product = Product::find($product_id);
        $product->name = $request->name;
        $product->code = $request->code;
        if(empty($request->hsc_code))
            $product->hsc_code = "";
        else
            $product->hsc_code = $request->hsc_code;

        $product->description = $request->description;
        $product->slug = str_slug($request->name, '-');
        $product->price = floatval($request->old_price);
        $product->slashed_price = floatval($request->new_price);
        $product->gst = floatval($request->gst);
        $product->price_for = $request->price_for;
        $product->stock = intval($request->stock);
        $product->minimum_quantity = intval($request->minimum_quantity);

        if ($request->has('featured'))
            $product->featured = 1;
        else
         $product->featured = 0;

     if(empty($request->new_price))
     {
        $product->discount = 0;
        $product->start_date = NULL;
        $product->end_date = NULL;
    }
    else
    {
        if ($request->has('discount'))
        $product->discount = $request->discount;

    if ($request->has('start_date'))
        $product->start_date = date("Y-m-d", strtotime($request->start_date));

    if ($request->has('end_date'))
        $product->end_date = date("Y-m-d", strtotime($request->end_date));
    }

  

    if ($request->has('new_product'))
        $product->new_product = 1;
    else
     $product->new_product = 0;

 if ($request->has('top_selling'))
    $product->top_selling = 1;
else
 $product->top_selling = 0;

if ($request->has('box_quantity'))
    $product->box_quantity =  $request->box_quantity;

if ($request->has('brand_id'))
{
    if ($request->brand_id=='00')
        $product->brand_logo_id = NULL;          
    else
        $product->brand_logo_id = $request->brand_id;
}

if ($request->has('product_group') && !empty($request->product_group))
    $product->product_group_id = $request->product_group;

$product->modified_by = Auth::user()->id;
$product->save();

if ($request->has('product_group') && !empty($request->product_group)){
    $group_product = new GroupProduct;
    $group_product->product_group_id = $request->product_group;
    $group_product->product_id = $product->id;
    $group_product->save();

}


$category_ids = $request->categories;

//return $category_ids;
foreach ($category_ids as $category_id) {
 $category = Category::find($category_id);
 if($category){
     $product_category = new ProductCategory;
     $product_category->product_id = $product->id;
     $product_category->category_id = $category_id;
     $product_category->save();

     $group_attributes = GroupAttribute::where('group_id',$category->group_id)->get();
                   //print_r($groupid);die;
 
     foreach ($group_attributes as $group_attribute) 
     {
        //return  $group_attribute;
         if ($request->has(str_replace(" ","_",$group_attribute->attribute->name))){
             $product_attribute = new ProductAttribute;
             $product_attribute->category_id = $category_id;
             $product_attribute->product_id = $product->id;
             $product_attribute->attribute_id = $group_attribute->attribute->id;

             $product_attribute->save();


             foreach ($request->input(str_replace(" ","_",$group_attribute->attribute->name)) as $value_id) {
                 $product_attribute_value = new ProductAttributeValue;
                 $product_attribute_value->product_id = $product->id;
                 $product_attribute_value->product_attribute_id = $product_attribute->id;
                 $product_attribute_value->attribute_id = $group_attribute->attribute->id;
                 $product_attribute_value->attribute_value_id = $value_id;
                 $product_attribute_value->save();
             }
         }

     }
 } 

}


if($request->has('gallery1')){

    if(empty($request->gallery1))
        $product_gallery = new ProductGallery;
    else
     $product_gallery = ProductGallery::find($request->gallery1); 

 if ($request->hasFile('image1')) {
    $product_gallery->product_id = $product->id;
    $product_gallery->image = $image1;
    $product_gallery->type = 'IMAGE';
    $product_gallery->save();
}
elseif ($request->has('video1')){
    $product_gallery->product_id = $product->id;
    $product_gallery->video = $request->video1;
    $product_gallery->image = '';
    $product_gallery->type = 'VIDEO';
    $product_gallery->save();
}   
}



if($request->has('gallery2')){

    if(empty($request->gallery2))
        $product_gallery = new ProductGallery;
    else
     $product_gallery = ProductGallery::find($request->gallery2); 

 if ($request->hasFile('image2')) {
     $product_gallery->product_id = $product->id;
     $product_gallery->image = $image2;
     $product_gallery->type = 'IMAGE';
     $product_gallery->save();
 }
 elseif ($request->has('video2')) {
     $product_gallery->product_id = $product->id;
     $product_gallery->video = $request->video2;
     $product_gallery->image = '';
     $product_gallery->type = 'VIDEO';
     $product_gallery->save();
 }   
}

if($request->has('gallery3')){

    if(empty($request->gallery3))
        $product_gallery = new ProductGallery;
    else
     $product_gallery = ProductGallery::find($request->gallery3); 

 if ($request->hasFile('image3')) {
     $product_gallery->product_id = $product->id;
     $product_gallery->image = $image3;
     $product_gallery->type = 'IMAGE';
     $product_gallery->save();
 }
 elseif ($request->has('video3')) {
     $product_gallery->product_id = $product->id;
     $product_gallery->video = $request->video3;
     $product_gallery->image = '';
     $product_gallery->type = 'VIDEO';
     $product_gallery->save();
 }   
}

if($request->has('gallery4')){

    if(empty($request->gallery4))
        $product_gallery = new ProductGallery;
    else
     $product_gallery = ProductGallery::find($request->gallery4); 

 if ($request->hasFile('image4')) {
     $product_gallery->product_id = $product->id;
     $product_gallery->image = $image4;
     $product_gallery->type = 'IMAGE';
     $product_gallery->save();
 }
 elseif ($request->has('video4')) {
     $product_gallery->product_id = $product->id;
     $product_gallery->video = $request->video4;
     $product_gallery->image = '';
     $product_gallery->type = 'VIDEO';
     $product_gallery->save();
 }   
}



$related_category_ids = $request->related_categories;
foreach ($related_category_ids as $related_category_id) {
    $related_product = new RelatedProduct;
    $related_product->product_id = $product->id;
    $related_product->category_id = $related_category_id;
    $related_product->save();
}


return redirect('admin/products')->with('message','Product Updated')
->with('status','success'); 
}



public function viewProduct($product_id){
    $product = Product::find($product_id);
        //print_r($attributes);die;
    return view('admin.view_product')->with(['product'=>$product]);
}



public function updateProductStatus($product_id,$status){
    $product = Product::find($product_id);
    $product->status = $status;
    $product->save();

    if($product)
        return redirect('admin/products')
    ->with('message','Product Status Changed')
    ->with('status','success');

}

public function exportProducts()
{


    $products = Product::where('status',1)->get();

    $data = array();
    foreach ($products as $product)
    {
        $categories = array();
        foreach ($product->productCategories as $product_category) {
            $categories[] = $product_category->category->name;
        }
        $category = implode(",",$categories);

        $attribute_names = array();
        $attribute_values = array();
        foreach ($product->productAttributeValues as $product_attribute_value) {
            $attribute_names[] = $product_attribute_value->attribute->name;
            $attribute_values[] = $product_attribute_value->attributeValue->value;
        }

        $attribute_name = implode(",",$attribute_names);
        $attribute_value = implode(",",$attribute_values);

        $group = "";
        if(!empty($product->productGroup))
            $group = $product->productGroup->name;

        $data[] = array('Name'=>$product->name,'Categories'=>$category,'Group'=>$group,'Old Price'=>$product->slashed_price,'Price'=>$product->price,'Price For'=>$product->price_for,'Stock'=>$product->stock,'Attributes'=>$attribute_name,'Attributes value'=>$attribute_value,'Description'=>$product->description);
    }


    return Excel::create('Product', function($excel) use ($data) {
        $excel->sheet('mySheet', function($sheet) use ($data)
        {
            $sheet->fromArray($data);
        });
    })->download('xlsx');
}



public function importProductExcel(Request $request)
{
    $validator = Validator::make($request->all(), [
        'category_id' => 'required',
        'product_file' => 'required|mimes:csv,CSV,txt',
    ]);

    if ($validator->fails())
        return redirect('admin/products')->withErrors($validator)->withInput();


    $path = $request->file('product_file')->getRealPath();
    $datas = Excel::load($path, function($reader) {

    })->get();

    $error_products = array();

    if(!empty($datas) && $datas->count())
    {
        foreach ($datas as $data) 
        {

            $product_name = Product::where('name',$data->product_name)->first();
            $product_code = Product::where('code',$data->product_code)->first();
            $attribute_names = explode(',',$data->attributes);
            $attribute_values = explode(',',$data->attribute_values);   

            if(empty($data->category) || $product_name || $product_code || empty($data->old_price) || empty($data->new_price)  || empty($data->price_for) || empty($data->stock) || empty($data->minimum_quantity)){

                $error_products[] = array('Name'=>$data->product_name,'Code'=>$data->product_code,'Hsc code'=>$data->hsc_code,'Old Price'=>$data->old_price,'New Price'=>$data->new_price,'Price For'=>$data->price_for,'Stock'=>$data->stock,'Gst'=>$data->gst,'Minimum Quantity'=>$data->minimum_quantity,'Attributes'=>$data->attributes,'Attribute Values'=>$data->attribute_values);
            }
            else{

                $product = new Product;
                $product->name = ucwords($data->product_name);
                $product->code = $data->product_code;
                $product->hsc_code = $data->hsc_code;
                $product->description = $data->description;
                $product->slug = str_slug($data->product_name, '-');
                $product->slashed_price = floatval($request->new_price);
                $product->price = floatval($request->old_price);
                $product->gst = floatval($data->gst);
                $product->price_for = $data->price_for;
                $product->stock = intval($data->stock);
                $product->minimum_quantity = intval($data->minimum_quantity);
                $product->modified_by = Auth::user()->id;
                $product->save();

                $category = Category::find($request->category_id);
                if($category){
                    $product_category = new ProductCategory;
                    $product_category->product_id = $product->id;
                    $product_category->category_id = $category->id;
                    $product_category->save(); 
                    
                    
                    $i = 0;
                    foreach ($attribute_names as $attribute_name) {
                        $attribute = Attribute::where('name',$attribute_name)->first();
                        if($attribute){
                            $group_attribute = GroupAttribute::where('group_id',$category->group_id)
                            ->where('attribute_id',$attribute->id)->first();
                            if($group_attribute){
                                $product_attribute = new ProductAttribute;
                                $product_attribute->category_id = $category->id;
                                $product_attribute->product_id = $product->id;
                                $product_attribute->attribute_id = $attribute->id;
                                $product_attribute->save();

                                $values = explode('-',$attribute_values[$i]);

                                foreach ($values as $value) {

                                    $attribute_value = AttributeValue::where('attribute_id',$attribute->id)->where('value',$value)->first();
                                    if($attribute_value){
                                        $product_attribute_value = new ProductAttributeValue;
                                        $product_attribute_value->product_id = $product->id;
                                        $product_attribute_value->product_attribute_id = $product_attribute->id;
                                        $product_attribute_value->attribute_id = $attribute->id;
                                        $product_attribute_value->attribute_value_id = $attribute_value->id;
                                        $product_attribute_value->save();
                                    }

                                } 

                            }    
                        }
                        $i++;
                    }

                }

                if(!empty($data->image1)){
                    $product_gallery = new ProductGallery;
                    $product_gallery->product_id = $product->id;
                    $product_gallery->image = $data->image1;
                    $product_gallery->type = 'IMAGE';
                    $product_gallery->save();
                }    

                if(!empty($data->image2)){
                    $product_gallery = new ProductGallery;
                    $product_gallery->product_id = $product->id;
                    $product_gallery->image = $data->image2;
                    $product_gallery->type = 'IMAGE';
                    $product_gallery->save();
                }

                if(!empty($data->image3)){
                    $product_gallery = new ProductGallery;
                    $product_gallery->product_id = $product->id;
                    $product_gallery->image = $data->image3;
                    $product_gallery->type = 'IMAGE';
                    $product_gallery->save();
                }

                    // $group_attributes = GroupAttribute::where('group_id',$category->group_id)->get();
                    //     //print_r($groupid);die;
                    // foreach ($group_attributes as $group_attribute) 
                    // {

                    //     if ($data->has($group_attribute->attribute->name)){
                    //         $product_attribute = new ProductAttribute;
                    //         $product_attribute->category_id = $category_id;
                    //         $product_attribute->product_id = $product->id;
                    //         $product_attribute->attribute_id = $group_attribute->attribute->id;
                    //         // $product_attribute->save();


                    //         foreach ($request->input($group_attribute->attribute->name) as $value_id) {
                    //             $product_attribute_value = new ProductAttributeValue;
                    //             $product_attribute_value->product_id = $product->id;
                    //             $product_attribute_value->product_attribute_id = $product_attribute->id;
                    //             $product_attribute_value->attribute_id = $group_attribute->attribute->id;
                    //             $product_attribute_value->attribute_value_id = $value_id;
                    //             // $product_attribute_value->save();
                    //         }
                    //     }
                    // }    

            }

        }
        if(empty($error_products))
            return redirect('admin/products')
        ->with('message','Product Inserted')
        ->with('status','success');
        else{
            return Excel::create('Product', function($excel) use ($error_products) {
                $excel->sheet('mySheet', function($sheet) use ($error_products)
                {
                    $sheet->fromArray($error_products);
                });
            })->download('xlsx');
        }                    

    }
    return redirect('admin/products')
    ->with('message','Product Upload Failed')
    ->with('status','error');

}

public function getSampleProduct(Request $request)
{

    $this->validate($request, [
        'category_id' => 'required',
    ]);

    $category = Category::find($request->category_id);

    $group_attributes = $category->group->groupAttributes;
    $data = array('product_name','product_code','category','old_price','new_price','price_for','stock','minimum_quantity','description','hsc_code','gst','image1','image2','image3','attributes','attribute_values');

    $category_name = "category - ".$category->name;
    $attribute_values = array($category_name);
    foreach ($group_attributes as $group_attribute) {
        $values = array();
        foreach ($group_attribute->attribute->AttributeValues as $attribute_value) {
            $values[] = $attribute_value->value;
        }
        $implode_values = implode(",",$values);
        $attribute_values[] = $group_attribute->attribute->name." - ".$implode_values;


            // $data[] = strtolower($group_attribute->attribute->name);
    }

    $data[] = '';

    $data = array_merge($data,$attribute_values);



    return Excel::create('Product', function($excel) use ($data,$attribute_values) {

        $excel->sheet('Products', function($sheet) use ($data) {
            $sheet->fromArray($data);
        });

            // // Our second sheet
            // $excel->sheet('Values', function($sheet) use ($attribute_values) {
            //     $sheet->fromArray($attribute_values);
            // });

    })->download('csv');
}

public function duplicateProduct(Request $request,$product_id){


    $old_product = Product::find($product_id);

    $product_name = 'dublicate-'.$old_product->name;
    $product_code ='dublicate-'. $old_product->code;
    $product = new Product;
    $product->name = $product_name;
    $product->code = $product_code;
    if(empty($old_product->hsc_code))
        $product->hsc_code = "";
    else
        $product->hsc_code = $old_product->hsc_code;

    $product->description = $old_product->description;
    $product->slug = str_slug($product_name, '-');
    $product->slashed_price = floatval($old_product->slashed_price);
    $product->price = floatval($old_product->price);
    $product->gst = floatval($old_product->gst);
    $product->price_for = $old_product->price_for;
    $product->stock = intval($old_product->stock);
    $product->minimum_quantity = intval($old_product->minimum_quantity);
    if ($old_product->featured)
        $product->featured = 1;

    if ($old_product->new_product)
        $product->new_product = 1;

    if ($old_product->top_selling)
        $product->top_selling = 1;

    if ($old_product->product_group_id)
        $product->product_group_id = $old_product->product_group_id;

    if ($old_product->brand_logo_id)
        $product->brand_logo_id = $old_product->brand_logo_id;

    $product->modified_by = Auth::user()->id;
    $product->save();

    if ($old_product->product_group_id){
        $group_product = new GroupProduct;
        $group_product->product_group_id = $old_product->product_group_id;
        $group_product->product_id = $product->id;
        $group_product->save();

    }

    $old_product_categories = $old_product->productCategories;

    foreach ($old_product_categories as $old_product_category) {

        $product_category = new ProductCategory;
        $product_category->product_id = $product->id;
        $product_category->category_id = $old_product_category->category_id;
        $product_category->save();

    }

    $old_product_attributes = $old_product->productAttributes;

    foreach ($old_product_attributes as $old_product_attribut) {

     $product_attribute = new ProductAttribute;
     $product_attribute->category_id = $old_product_attribut->category_id;
     $product_attribute->product_id = $product->id;
     $product_attribute->attribute_id = $old_product_attribut->attribute_id;
     $product_attribute->save();
 }

 $old_product_attributevalues = $old_product->productAttributeValues;

 foreach ($old_product_attributevalues as $old_product_attributevalue) {
     $product_attribute_value = new ProductAttributeValue;
     $product_attribute_value->product_id = $product->id;
     $product_attribute_value->product_attribute_id = $product_attribute->id;
     $product_attribute_value->attribute_id = $old_product_attributevalue->attribute_id;
     $product_attribute_value->attribute_value_id = $old_product_attributevalue->attribute_value_id;
     $product_attribute_value->save();
 }

 $old_product_productGalleries = $old_product->productGalleries;

 foreach ($old_product_productGalleries as $old_product_productGallery) {
   $product_gallery = new ProductGallery;
   $product_gallery->product_id = $product->id;
   $product_gallery->image = $old_product_productGallery->image;
   $product_gallery->type = $old_product_productGallery->type;
   $product_gallery->save();
}

$related_relate_categories = $old_product->relatedCategories;
foreach ($related_relate_categories as $related_relate_category) {
    $related_product = new RelatedProduct;
    $related_product->product_id = $product->id;
    $related_product->category_id = $related_relate_category->category_id;
    $related_product->save();
}


return redirect('admin/products')->with('message','Product Created')
->with('status','success'); 


}
public function deleteProductImage($id){

    //return $id;
    $product_gallery = ProductGallery::find($id);

    $product_id = $product_gallery->product_id;
//return  $product_id;
    $url =url('admin/products',[$product_id])."/edit";

    $product_gallery = ProductGallery::destroy($id);

    return response()->json(['status'=>'success','msg'=>'Product Deleted','url'=>$url]);

}

public function deleteProduct($id){

    //return $id;

    $product = Product::find($id);
    //return $product;

    DB::table('carts')->where('product_id', $product->id)->delete();

    DB::table('order_details')->where('product_id', $product->id)->update(['product_id' =>NULL,'prodcut_name' =>$product->name,'product_code' =>$product->code]);

    DB::table('related_products')->where('product_id', $product->id)->delete();

    DB::table('product_reviews')->where('product_id', $product->id)->delete();

    DB::table('group_products')->where('product_id', $product->id)->delete();

    DB::table('product_categories')->where('product_id', $product->id)->delete();

    DB::table('product_attribute_values')->where('product_id', $product->id)->delete();

    DB::table('product_attributes')->where('product_id', $product->id)->delete();

    $product_images =DB::table('product_galleries')->where('product_id', $product->id)->get();

    foreach ($product_images as $product_image) {
        $image_path = public_path('/products').'/'.$product_image->image;
            //unlink($image_path);
        $thumb_image_path = public_path('/product-thumbs').'/'.$product_image->image;
            //if (File::exists($thumb_image_path))
                //unlink($thumb_image_path);    
    }
    DB::table('product_galleries')->where('product_id', $product->id)->delete();


    $product->delete();

    return response()->json(['status'=>'success','message'=>'Product Delete successfully']);

// return redirect('admin/products')->with('message','Product Delete successfully')
// ->with('status','success');
}

public function getProductSearch(Request $request){

    $code=$request->code;
    $codes=Product::where('status',1)->where('name','LIKE',$code.'%')->get();
    
    $content='';
    foreach($codes as $code){
        $content .="<li data-id='$code->name'>$code->name</li>";
    }
    return $content;
}


}
