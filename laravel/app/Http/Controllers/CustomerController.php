<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Customer;
use DB;
use Log;
use Auth;
use Hash;

class CustomerController extends Controller
{
    public function getCustomers(){
    	$customers = Customer::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('CUSTOMER');

        return view('admin.customers')->with('customers',$customers)
        							->with('allowed_operations',$allowed_operations);
    }

    public function updateCustomerStatus($customer_id,$status){
        $customer = Customer::find($customer_id);
        $customer->status = $status;
        $customer->save();

        if($customer)
            return redirect('admin/customers')
                        ->with('message','Group Status Changed')
                        ->with('status','success');

    }

    public function viewCustomer($customer_id){

        $customer = Customer::find($customer_id);

        return view('admin.view_customer')->with('customer',$customer);
    }

    public function updateCustomerPassword(Request $request){
        $this->validate($request, [
            'customer_id' => 'required',
            'password' => 'required',
        ]);

        $hashed_password = Hash::make($request->password);

        $customer = Customer::find($request->customer_id);
        $customer->password = $hashed_password;
        $customer->save();

        if($customer)
            return redirect('admin/customers')
                        ->with('message','Password Changed')
                        ->with('status','success');

    }
}
