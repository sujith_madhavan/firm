<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\ProductGroup;
use DB;
use Log;
use Auth;

class ProductGroupController extends Controller
{
    public function getProductGroups(){
    	$product_groups = ProductGroup::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('PRODUCT_GROUP');

        return view('admin.product_groups')->with('product_groups',$product_groups)
        							->with('allowed_operations',$allowed_operations);
    }

    public function postProductGroup(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:product_groups,name',
        ]);	

        $product_group = new ProductGroup;
        $product_group->name = ucwords($request->name);
        $product_group->modified_by = Auth::user()->id;
        $product_group->save();

        return redirect('admin/product-groups')->with('message','Product Group Created')
                        ->with('status','success'); 

    }

    public function updateProductGroup(Request $request,$product_group_id){
        $this->validate($request, [
            'name' => 'required|unique:product_groups,name,'.$product_group_id,
        ]);	

        $product_group = ProductGroup::find($product_group_id);
        $product_group->name = ucwords($request->name);
        $product_group->modified_by = Auth::user()->id;
        $product_group->save();

        return redirect('admin/product-groups')->with('message','Product Group Updated')
                        ->with('status','success'); 

    }

    public function updateProductGroupStatus($product_group_id,$status){
        $product_group = ProductGroup::find($product_group_id);
        $product_group->status = $status;
        $product_group->save();

        if($product_group)
            return redirect('admin/product-groups')
                        ->with('message','Product Group Status Updated')
                        ->with('status','success');

    }
}
