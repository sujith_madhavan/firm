<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Banner;

use DB;
use Log;
use Auth;
use Image;
use File;

class BannerController extends Controller
{
    public function getBanners(){
    	$banners = Banner::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('BANNER');
        
        return view('admin.banners')->with(['banners'=>$banners,'allowed_operations'=>$allowed_operations]);
    }

    public function addBanner(){
        return view('admin.add_banner');
    }

    public function postBanner(Request $request){
        $this->validate($request, [
            'image' => 'required|image|',
            'position' => 'required|numeric',
            'content_small' => 'required',
        ]);



		// Start of saving Image to server 
		$photo = $request->file('image');
		// Creating Names for Image
	    $imagename = 'banner-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();
	    // Resizing image and saving to server
	    $destinationPath = public_path('/banners');
		$image = Image::make($photo->getRealPath()); 


		// $image->resize(1300, null, function ($constraint) {
		//     $constraint->aspectRatio();
		//     $constraint->upsize();
		// });
        $image->resize(1300, 650);

		$image->save($destinationPath.'/'.$imagename,90);

        $banner_check = Banner::where('position',$request->position)->first();
        if($banner_check)
		  DB::table('banners')->where('position','>=',$request->position)->increment('position');

        $banner = new Banner;
        $banner->image = $imagename;
        $banner->content = "";
        $banner->content_medium = "";
        $banner->content_small =$request->content_small; 
    //  dd($banner->content_small);die;
 
        $banner->position = $request->position;
        $banner->modified_by = Auth::user()->id;
        $banner->save();

        return redirect('admin/banners')->with('message','Banner Created')
                        ->with('status','success'); 

    }

    public function editBanner($banner_id){
    	$banner = Banner::find($banner_id);

        return view('admin.edit_banner')->with(['banner'=>$banner]);
    }

    public function updateBanner(Request $request,$banner_id){
        $this->validate($request, [
            'position' => 'required|numeric',
            
        ]);

        $banner = Banner::find($banner_id);

        if ($request->hasFile('image')){
	        $this->validate($request, [
	        	'image' => 'required|image|dimensions:min_width=1300,min_height=650',
			]);

			// Start of saving Image to server 
			$photo = $request->file('image');
			// Creating Names for Image
		    $imagename = 'banner-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();
		    // Resizing image and saving to server
		    $destinationPath = public_path('/banners');
			$image = Image::make($photo->getRealPath());


            $image->resize(1300, 650);

			$image->save($destinationPath.'/'.$imagename,90);

            // $image_path = public_path('/banners')."/".$banner->image;
            // unlink($image_path); 
		}

        if($request->position != $banner->position){
            $banner_check = Banner::where('position',$request->position)->first();
            if($banner_check)
              DB::table('banners')->where('position','>=',$request->position)->increment('position');
        }

        
        if ($request->hasFile('image'))
        	$banner->image = $imagename;

        $banner->content = "";
        $banner->content_medium ="";
        $banner->content_small = $request->content_small;
        if($request->position != $banner->position)
            $banner->position = $request->position;
        
        $banner->modified_by = Auth::user()->id;
        $banner->save();

        return redirect('admin/banners')->with('message','Banner Updated')
                        ->with('status','success'); 

    }

    public function updateBannerStatus($banner_id,$status){
        $banner = Banner::find($banner_id);
        $banner->status = $status;
        $banner->save();

        if($banner)
            return redirect('admin/banners')
                        ->with('message','Banner Status Updated')
                        ->with('status','success');

    }

    public function deleteBanner($banner_id){
        $banner = Banner::find($banner_id);

        $image_path = public_path('/banners')."/".$banner->image;
        unlink($image_path);
        $banner = Banner::destroy($banner_id);

        return response()->json(['status'=>'success','msg'=>'Pincode Deleted']);

    }
}
