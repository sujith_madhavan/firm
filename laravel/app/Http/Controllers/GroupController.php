<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Group;
use App\Attribute;
use App\GroupAttribute;
use DB;
use Log;
use Auth;

class GroupController extends Controller
{
    public function getGroups(){
    	$groups = Group::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('GROUP');

        return view('admin.groups')->with('groups',$groups)
        ->with('allowed_operations',$allowed_operations);
    }

    public function addGroup(){
        $attributes = Attribute::where('status',1)->get();

        return view('admin.add_group')->with('attributes',$attributes);
    }

    public function postGroup(Request $request){
    	$this->validate($request, [
            'attribute_ids' => 'required',
            'name' => 'required',
        ]);

    	// return $request->all();
    	$group = new Group;
    	$group->name = ucwords($request->name);
    	$group->modified_by = Auth::user()->id;
        $group->save();
        // $position = $request->position;
        $i=0;
        foreach ($request->attribute_ids as $attribute) {


         $group_attribute = new GroupAttribute;
         $group_attribute->group_id = $group->id;
         $group_attribute->attribute_id = $attribute;
         $position = "position".$attribute;
         $group_attribute->position = $request->input($position);
         $group_attribute->save();
         
     }

     return redirect('admin/groups')->with('message','Group Created')
     ->with('status','success'); 
 }

 public function viewGroup($group_id){
    $group = Group::find($group_id);

    return view('admin.view_group')->with('group',$group);
}

public function editGroup($group_id){
    $group = Group::find($group_id);
    $attributes = Attribute::all();
    return view('admin.edit_group')->with('group',$group)
    ->with('attributes',$attributes);
}

public function updateGroup(Request $request,$group_id){
 $this->validate($request, [
    'attribute_ids' => 'required',
    'name' => 'required',
]);

 $group = Group::find($group_id);
 $group->name = ucwords($request->name);
 $group->modified_by = Auth::user()->id;
 $group->save();

 $operation = DB::table('group_attributes')->where('group_id', $group_id)->delete();

 foreach ($request->attribute_ids as $attribute) {
     $group_attribute = new GroupAttribute;
     $group_attribute->group_id = $group->id;
     $group_attribute->attribute_id = $attribute;
     $position = "position".$attribute;
     $group_attribute->position = $request->input($position);
     $group_attribute->save();
 }

 return redirect('admin/groups')->with('message','Group Updated')
 ->with('status','success'); 
}

public function updateGroupStatus($group_id,$status){
    $group = Group::find($group_id);
    $group->status = $status;
    $group->save();

    if($group)
        return redirect('admin/groups')
    ->with('message','Group Status Changed')
    ->with('status','success');

}
}
