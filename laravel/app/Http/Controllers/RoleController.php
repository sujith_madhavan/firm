<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Role;
use App\Module;
use App\RoleModule;
use App\Operation;
use App\RoleModuleOperation;
use Log;
use Auth;
use DB;
class RoleController extends Controller
{
    public function getRoles(){
    	$roles = Role::where('code','!=','SUPER_ADMIN')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('ROLE');

        return view('admin.roles')->with('roles',$roles)
                                    ->with('allowed_operations',$allowed_operations);
    }

    public function viewRole($role_id){
        $role = Role::find($role_id);

        return view('admin.view_role')->with('role',$role);
    }

    public function addRole(){
        $modules = Module::all();
        return view('admin.add_role')->with('modules',$modules);
    }

    public function postRole(Request $request){
    	$this->validate($request, [
            'name' => 'required|unique:roles,name',
        ]);

        $user = Auth::user();
        if(!$user)
        	return redirect('admin/login');

        $code = str_replace(' ', '_', $request->name);
        $code = strtoupper($code);

        $role = new Role;
        $role->name = ucwords($request->name);
        $role->code = $code;
        $role->modified_by = $user->id;
        $role->save();

        $modules = Module::all();
        foreach ($modules as $module) {
            if ($request->has($module->code)) {
                // Log::info("operations".print_r($request->input($module->code),true));
                $list_operation = Operation::where('module_id',$module->id)->where('type','LIST')->first();

                $role_module = new RoleModule;
                $role_module->role_id = $role->id;
                $role_module->module_id = $module->id;
                $role_module->save();

                if($list_operation){
                    $role_module_operation = new RoleModuleOperation;
                    $role_module_operation->role_id = $role->id;
                    $role_module_operation->role_module_id = $role_module->id;
                    $role_module_operation->operation_id = $list_operation->id;
                    $role_module_operation->save();
                }

                foreach ($request->input($module->code) as $operation){
                    $role_module_operation = new RoleModuleOperation;
                    $role_module_operation->role_id = $role->id;
                    $role_module_operation->role_module_id = $role_module->id;
                    $role_module_operation->operation_id = $operation;
                    $role_module_operation->save();
                }
            }
        }

        return redirect('admin/roles')->with('message','Role Created')
                        ->with('status','success'); 

    }

    public function editRole($role_id){
        $role = Role::find($role_id);
        $module_operations = RoleModuleOperation::where('role_id',$role_id)->get();
        $modules = Module::all();
        return view('admin.edit_role')->with('module_operations',$module_operations)
                    ->with('role',$role)
                    ->with('modules',$modules);
    }

    public function updateRole(Request $request,$role_id){
        $this->validate($request, [
            'name' => 'required|unique:roles,name,'.$role_id,
            // 'code' => 'required|unique:roles,code,'.$role_id,
        ]);

        $user = Auth::user();
        if(!$user)
            return redirect('admin/login');

        $code = str_replace(' ', '_', $request->name);
        $code = strtoupper($code);

        $role = Role::find($role_id);
        $role->name = $request->name;
        $role->code = $code;
        $role->modified_by = $user->id;
        $role->save();

        $operation = DB::table('role_module_operations')->where('role_id', $role_id)->delete();
        $operation = DB::table('role_modules')->where('role_id', $role_id)->delete();


        $modules = Module::all();
        foreach ($modules as $module) {
            if ($request->has($module->code)) {
                // Log::info("operations".print_r($request->input($module->code),true));
                $list_operation = Operation::where('module_id',$module->id)->where('type','LIST')->first();

                $role_module = new RoleModule;
                $role_module->role_id = $role_id;
                $role_module->module_id = $module->id;
                $role_module->save();

                if($list_operation){
                    $role_module_operation = new RoleModuleOperation;
                    $role_module_operation->role_id = $role->id;
                    $role_module_operation->role_module_id = $role_module->id;
                    $role_module_operation->operation_id = $list_operation->id;
                    $role_module_operation->save();
                }
                
                foreach ($request->input($module->code) as $operation){
                    $role_module_operation = new RoleModuleOperation;
                    $role_module_operation->role_id = $role_id;
                    $role_module_operation->role_module_id = $role_module->id;
                    $role_module_operation->operation_id = $operation;
                    $role_module_operation->save();
                }
            }
        }


        return redirect('admin/roles')
                        ->with('message','Successfully Updated')
                        ->with('status','success');


    }

    public function updateRoleStatus($role_id,$status){
        $role = Role::find($role_id);
        $role->status = $status;
        $role->save();

        if($role)
            return redirect('admin/roles')
                        ->with('message','Role Status Changed')
                        ->with('status','success');

    }



}
