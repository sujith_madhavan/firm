<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\OperationController;
use App\Mail\CustomerForgotPassword;
use App\Mail\CustomerRegister;
use App\Product;
use App\Productimg;
use App\Group;
use App\Attribute;
use App\GroupAttribute;
use App\Category;
use App\Product_attribute;
use App\Customer;
use App\Vendor;
use App\VendorBankDetail;
use App\CustomerAddress;
use App\NewsLetter;
use App\Banner;
use App\BrandLogo;
use DateTime;
use DB;
use Log;
use Auth;
use Validator;
use Mail;

class HomeController extends Controller
{
    public function getHome(){
        $banners = Banner::where('status',1)->orderBy('position','asc')->get();
        $brand_logos = BrandLogo::where('status',1)->orderBy('position','asc')->get();
        $featured_products = Product::where('featured',1)
        ->orderBy('updated_at','desc')->take(4)->get();
        $latest_products = Product::where('new_product',1)
        ->orderBy('updated_at','desc')->take(6)->get();
        $top_selling_products = Product::where('top_selling',1)
        ->orderBy('updated_at','desc')->take(4)->get();                            

        return view('index')->with(['featured_products'=>$featured_products,'latest_products'=>$latest_products,'top_selling_products'=>$top_selling_products,'banners'=>$banners,'brand_logos'=>$brand_logos]);
    }

    public function getRegister()
    {
        return view('registration');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'email_id' => 'required|unique:customers,email',
            'mobile' => 'required|unique:customers,mobile',
            'password' => 'required|min:6|confirmed',
            'first_name' => 'required',
            'last_name' => 'required',
            'user_type' => 'required',
        ]);

        $random_number = rand(100009,999999);

        $customer = new Customer;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->mobile = $request->mobile;
        $customer->email = $request->email_id;
        $customer->password = bcrypt($request->password);
        $customer->user_type = $request->user_type;
        $customer->gst = $request->gst;
        if ($request->has('news_letter'))
            $customer->news_letter = 1;

        $customer->otp = $random_number;


        $number=$request->mobile;

//return  $number;

        $customer->save();


        $url = 'http://pay4sms.in';
        $key = 'A40eb53cfec3525dd53185e55623430f3';
        $sender = 'FIRMER';

        $message="".$request->first_name." ".$request->last_name." Thanks For registering with firmer Your OTP is ".$random_number." Enjoy Shopping ";

      //return $message;

        $mysms = urlencode($message);
        $smsurl = 'http://trans.kapsystem.com/api/web2sms.php?workingkey='.$key.'&to='.$number.'&sender='.$sender.'&message=HI '.$mysms;

        //return $smsurl;
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$smsurl);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_HEADER,false);
        $result = curl_exec($curl);
        curl_close($curl);
        //Log::info(print_r($result));
        //return $result;



        // Auth::guard('customers')->login($customer);

        // $link = url('/login');
        Mail::to($request->email_id)->send(new CustomerRegister($request->first_name,$request->email_id,$request->password)); 

        return view('otp')->with(['customer_id'=>$customer->id]);

    }

    public function postOtpLogin(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required',
            'otp' => 'required',
        ]);

        $customer = Customer::find($request->customer_id);
        if(!$customer)
            return view('otp')->with(['customer_id'=>$customer->id, 'error_message'=>"Invalid Customer"]);

        if($customer->otp != $request->otp)
            return view('otp')->with(['customer_id'=>$customer->id, 'error_message'=>"Invalid Otp"]);

        $customer->otp = 0;
        $customer->otp_status = 1;
        $customer->save();
        Auth::guard('customers')->login($customer);

        $previous_url = session('previous_url', '');
        if(!empty($previous_url))
            return Redirect::to($previous_url);
        else
            return redirect('/customer/dashboard');
    }    

    public function getLogin()
    {
        if(str_contains(url()->previous(), ['product', 'cart']))
            session(['previous_url' => url()->previous()]);

        return view('login');
    }

    public function postLogin(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);


        if(Auth::guard('customers')->attempt(['email' => $request->email, 'password' => $request->password])){
            $customer = Auth::guard('customers')->user();
            if($customer->otp_status == 0){
                Auth::guard('customers')->logout();

                $random_number = rand(100009,999999);

                $customer->otp = $random_number;
                $customer->save();

                return view('otp')->with(['customer_id'=>$customer->id]);

            }

            $previous_url = session('previous_url', '');
            if(!empty($previous_url))
                return Redirect::to($previous_url);
            else
                return redirect('/customer/dashboard');
        }
        else{
            $validator ='Email/Password Invalid';
            return back()->withErrors($validator)->withInput(); 
        }

    }

    public function getForgotPassword()
    {
        return view('forgot_password');
    }

    public function postForgotPassword(Request $request)
    {
        $this->validate($request, [
            'email_id' => 'required|email',
        ]);

        $customer = Customer::where('email', $request->email_id)->first();
        if(!$customer){
            $validator ='Email not registered';
            return back()->withErrors($validator)->withInput(); 
        }

        $email_token = str_random(30);
        $customer->email_token = $email_token;
        $customer->save();

        Mail::to($request->email_id)->send(new CustomerForgotPassword($customer->first_name,$email_token));

        $content = "Verification link sent to your email, You can reset your password with that link.";
        $title = "Forgot Password";

        return view('display_information')->with(['content'=>$content,'title'=>$title]);
    }


    public function resetCustomerPassword($token)
    {
        $customer = Customer::where('email_token', $token)->first();
        if(!$customer){
            $content = "Invalid Link, Please try again.";
            $title = "Forgot Password";

            return view('display_information')->with(['content'=>$content,'title'=>$title]);
        }

        $customer->email_token = "";
        $customer->save();

        return view('reset_password')->with(['customer_id'=>$customer->id]);
    }

    public function postRestPassword(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        $customer = Customer::find($request->customer_id);
        if(!$customer)
            return view('reset_password')->with(['customer_id'=>$customer->id, 'error_message'=>"Invalid Customer"]);


        $customer->password = bcrypt($request->password);
        $customer->save();

        return redirect('/login');
    }

    public function getLogout(){
        Auth::guard('customers')->logout();

        return redirect('/login');
    }


    public function getVendorRegister()
    {
        return view('vendor_registration');
    }

    public function postVendorRegister(Request $request)
    {
        $this->validate($request, [
            'email_id' => 'required|unique:customers,email',
            'mobile' => 'required|unique:customers,mobile',
            'password' => 'required|min:6|confirmed',
            'business_name' => 'required',
            'business_type' => 'required',
            'contact_person' => 'required',
            'address' => 'required',
            'state' => 'required',
            'district' => 'required',
            'pincode' => 'required',
        ]);

        if ($request->hasFile('image')){
            $this->validate($request, [
                'image' => 'required|image',
            ]);


            // Start of saving Image to server 
            $photo = $request->file('image');
            // Creating Names for Image
            $imagename = 'vendor-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();

            // Saving to server without resizing
            $image = $request->file('image')->move(public_path('/vendors/'),$imagename);  
            // End of Image saving  
        }

        $vendor = new Vendor;
        $vendor->business_name = ucwords($request->business_name);
        $vendor->email = $request->email_id;
        $vendor->password = bcrypt($request->password);
        $vendor->mobile = $request->mobile;
        $vendor->address = $request->address;
        $vendor->state = $request->state;
        $vendor->district = $request->district;
        $vendor->pincode = $request->pincode;
        $vendor->business_type = $request->business_type;
        $vendor->gst = $request->gst;
        $vendor->pan = $request->pan;
        $vendor->contact_person = $request->contact_person;
        $vendor->designation = $request->designation;
        $vendor->phone = $request->phone;
        $vendor->distance = $request->distance;
        if ($request->hasFile('image'))
            $vendor->image = $imagename;

        $vendor->save();

        if(!empty($request->account_number) && !empty($request->ifsc)){
            $vendor_bank_detail = new VendorBankDetail;
            $vendor_bank_detail->vendor_id = $vendor->id;
            $vendor_bank_detail->bank_name = $request->bank_name;
            $vendor_bank_detail->branch = $request->branch;
            $vendor_bank_detail->account_number = $request->account_number;
            $vendor_bank_detail->account_name = $request->account_name;
            $vendor_bank_detail->ifsc = $request->ifsc;
            $vendor_bank_detail->save();

        }

        $url = 'http://pay4sms.in';
        $key = 'A40eb53cfec3525dd53185e55623430f3';
        $sender = 'FIRMER';
        
        $message="".ucwords($request->business_name)."Thanks For registering AS VENDOR WITH FIRMER WILL CALL YOU SHORTLY ";

      //return $message;

        $mysms = urlencode($message);
        $smsurl = 'http://trans.kapsystem.com/api/web2sms.php?workingkey='.$key.'&to='.$number.'&sender='.$sender.'&message=HI '.$mysms;

        //return $smsurl;
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$smsurl);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_HEADER,false);
        $result = curl_exec($curl);
        curl_close($curl);

        $content = "Registration is success. After Verified you can Login in vendor CRM";
        $title = "Vendor Registration";

        return view('display_information')->with(['content'=>$content,'title'=>$title]);        

    }

    public function getCustomerAccountInfo()
    {
        $customer = Auth::guard('customers')->user();
        if(!$customer)
            return redirect('/login');

        return view('account_information')->with(['customer'=>$customer]);
    }

    public function postCustomerAccountInfo(Request $request)
    {

        $customer = Auth::guard('customers')->user();
        if(!$customer)
            return redirect('/login');

        $this->validate($request, [
            'email_id' => 'required|unique:customers,email,'.$customer->id,
            'mobile' => 'required|unique:customers,mobile,'.$customer->id,
            'first_name' => 'required',
            'last_name' => 'required',
            'user_type' => 'required',
        ]);

        $old_mobile = $customer->mobile;
        if($old_mobile != $request->mobile){
            $random_number = rand(100009,999999);
            $customer->otp = $random_number; 
            $customer->otp_status = 0;        
        }

        if ($request->has('current_password') && $request->has('new_password')){
            if (!Hash::check($request->current_password, $customer->password)){
                $validator ='Invalid Current Password';
                return back()->withErrors($validator)->withInput(); 
            }

            $customer->password = bcrypt($request->new_password);
        }


        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->mobile = $request->mobile;
        $customer->email = $request->email_id;   
        $customer->user_type = $request->user_type;
        $customer->gst = $request->gst;
        if ($request->has('news_letter'))
            $customer->news_letter = 1;
        else
            $customer->news_letter = 0;

        
        $customer->save();
        
        if($old_mobile != $request->mobile){
            Auth::guard('customers')->logout();
            return view('otp')->with(['customer_id'=>$customer->id]);
        }

        return redirect('customer/account-info')->with('message','Account Updated')
        ->with('status','success'); 
    }


    public function getCustomerAddress()
    {
        $customer = Auth::guard('customers')->user();
        if(!$customer)
            return redirect('/login');

        return view('add_customer_address');
    }

    public function getCustomerAddressList()
    {
        $customer = Auth::guard('customers')->user();
        if(!$customer)
            return redirect('/login');
        $customer_addresses = CustomerAddress::where('customer_id',$customer->id)->where('is_billing_address',0)->where('is_shipping_address',0)->get();
        $customer_address = $customer->addresses;
        $count=$customer_address->count();

        if ($count==0) 
           return view('add_customer_address');

       $billing_addres = CustomerAddress::where('customer_id',$customer->id)->where('is_billing_address',1)->first();
       $shipping_addres = CustomerAddress::where('customer_id',$customer->id)->where('is_shipping_address',1)->first();

       return view('address_list')->with('customer_addresses',$customer_addresses)->with('billing_addres',$billing_addres)->with('shipping_addres',$shipping_addres);
   }


   public function CustomerAddressEdit($id)
   {
    $customer_address = CustomerAddress::find($id);
    return view('customer_address_edit')->with('customer_address',$customer_address);
}

public function postCustomerAddress(Request $request)
{

    $customer = Auth::guard('customers')->user();
    if(!$customer)
        return redirect('/login');

    $this->validate($request, [
        'first_name' => 'required',
        'mobile' => 'required',
        'address' => 'required',
        'city' => 'required',
        'state' => 'required',
        'pincode' => 'required',
        'country' => 'required',
    ]);

    $customer_address = new CustomerAddress;
    $customer_address->customer_id = $customer->id;
    $customer_address->first_name = $request->first_name;
    $customer_address->last_name = $request->last_name;
    $customer_address->company = $request->company;
    $customer_address->mobile = $request->mobile;
    $customer_address->fax = $request->fax;
    $customer_address->address = $request->address;
    $customer_address->address1 = $request->address1;
    $customer_address->address2 = $request->address2;
    $customer_address->city = $request->city;
    $customer_address->state = $request->state;
    $customer_address->pincode = $request->pincode;
    $customer_address->country = $request->country;
    $customer_addresses = $customer->addresses;
    $count=$customer_addresses->count();

    if ($count==0) 
    {
       $customer_address->is_billing_address = 1;
       $customer_address->is_shipping_address = 1;
   }

   if ($request->has('is_billing'))
        {          //$address = CustomerAddress::($customer->id)
            DB::table('customer_addresses')->where('customer_id',$customer->id)->update(['is_billing_address' => 0]);
            $customer_address->is_billing_address = 1;
        }
        if ($request->has('is_shipping'))
        {
          DB::table('customer_addresses')->where('customer_id',$customer->id)->update(['is_shipping_address' => 0]);
          $customer_address->is_shipping_address = 1;
      }

      $customer_address->save();

      return redirect('customer/address-list')->with('message','Address Added')
      ->with('status','success'); 
  }

  public function CustomerAddressUpdate(Request $request,$id)
  {

//return $request->all();
    $customer = Auth::guard('customers')->user();
    if(!$customer)
        return redirect('/login');

    $this->validate($request, [
        'first_name' => 'required',
        'mobile' => 'required',
        'address' => 'required',
        'city' => 'required',
        'state' => 'required',
        'pincode' => 'required',
        'country' => 'required',
    ]);

    if ($request->has('is_billing'))
        DB::table('customer_addresses')->where('customer_id',$customer->id) ->update(['is_billing_address' => 0]);

    if ($request->has('is_shipping'))
        DB::table('customer_addresses')->where('customer_id',$customer->id) ->update(['is_shipping_address' => 0]);

    $customer_address =CustomerAddress::find($id);
    $customer_address->customer_id = $customer->id;
    $customer_address->first_name = $request->first_name;
    $customer_address->last_name = $request->last_name;
    $customer_address->company = $request->company;
    $customer_address->mobile = $request->mobile;
    $customer_address->fax = $request->fax;
    $customer_address->address = $request->address;
    $customer_address->address1 = $request->address1;
    $customer_address->address2 = $request->address2;
    $customer_address->city = $request->city;
    $customer_address->state = $request->state;
    $customer_address->pincode = $request->pincode;
    $customer_address->country = $request->country;

    if ($request->has('is_billing'))
    {
        $customer_address->is_billing_address = 1;
    }
    else
    {
        $customer_address->is_billing_address = 0;
    }
    

    if ($request->has('is_shipping'))
    {    
      $customer_address->is_shipping_address = 1;
  }
  else
  {
    $customer_address->is_shipping_address = 0;
}


$customer_address->save();

return redirect('customer/address-list')->with('message','Address Update')
->with('status','success'); 
}

public function deleteAddress($id)
{
        //Log::info($id);
    $CustomerAddress = CustomerAddress::find($id);

    $CustomerAddress->delete();

    return response()->json(['status'=>'success','msg'=>'Pincode Deleted']);

}

public function getCustomerNewsLetters()
{
    $customer = Auth::guard('customers')->user();
    if(!$customer)
        return redirect('/login');

    $datetime = new DateTime();
    $news_letters = NewsLetter::where('expires_at', '>=',$datetime)->get();

    return view('news_letters')->with(['news_letters'=>$news_letters]);
}

public function dashboard()
{
    $customer = Auth::guard('customers')->user();
    if(!$customer)
        return redirect('/login');

    $datetime = new DateTime();

    $news_letters = NewsLetter::where('expires_at', '>=',$datetime)->get();
    $count = $news_letters->count();

    $billing_address = CustomerAddress::where('customer_id',$customer->id)->where('is_billing_address',1)->first();

    $shipping_address = CustomerAddress::where('customer_id',$customer->id)->where('is_shipping_address',1)->first();

    return view('dashboard')->with('billing_address',$billing_address)->with('shipping_address',$shipping_address)->with('customer',$customer)->with('count',$count);
        //return view('dashboard');
}
public function postsearchAddressCode(Request $request)
{
    //return $request->all();
    session(['address' => $request->address]);

    return redirect('/');

}

public function getBrandPageList($slug){

    $brand_name = BrandLogo::where('slug',$slug)->first();  
 
    $products = Product::select('products.*')
    ->join('brand_logos', 'brand_logos.id', '=', 'products.brand_logo_id')
    ->where('products.status',1)
    ->orderby('products.created_at','desc')
    ->where('brand_logos.slug',$slug)
    // ->groupBy('products.id')
    ->paginate(12);
    
    // return $products;                        
    $categories = array();

    foreach ($products as $product) {
        foreach ($product->productCategories as $product_category) {
            $categories[] = $product_category->category_id;
        }
    }                        

    $categories = array_unique($categories);

    $categories = Category::whereIn('id', $categories)->get();

    return view('searched_products')->with('products',$products)
    ->with('categories',$categories)
    ->with('search_name',$brand_name->name);                        

}
public function autoslug(){

        //$portfolio=$this->db->select("*")->from('brand_logos')->get()->result();

 $portfolio = BrandLogo::all();
        //print_r($portfolio);die;
 foreach($portfolio as $port){
            //$string=str_replace(' ',"-",strtolower(((rtrim($port->product_name)))));
             //$string = str_replace(' ', '-', $port->product_name); 
    $string = str_replace(array('[\', \']'), '', $port->name);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
    $slug= strtolower(trim($string, '-'));

    $portfolios = BrandLogo::find($port->id);  
    $portfolios->slug = $slug;

    $portfolios->save();
            //$this->db->where('id',$port->id)->update('products',$data);
}


}
}
