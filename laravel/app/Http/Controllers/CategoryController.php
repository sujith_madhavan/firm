<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Category;
use App\Group;
use Log;
use Auth;
use Image;
use File;

class CategoryController extends Controller
{
    public function getCategories(){
    	$categories = Category::orderBy('created_at', 'desc')->get();

        //return $categories;
        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('CATEGORY');

        return view('admin.categories')->with('categories',$categories)
        ->with('allowed_operations',$allowed_operations);

    }

    public function addCategory(){
        $categories = Category::where('status',1)->whereNull('parent_id')->get();
        $groups = Group::where('status',1)->get();

        return view('admin.add_category')->with('categories',$categories)->with('groups',$groups);
    }

    public function postCategory(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:categories,name',
            'title' => 'required',
            'group_id' => 'required',
        ]);

        $category = new Category;


        if(empty($request->category))
        {
            $this->validate($request, [
                'image' => 'required|image',
            ]);



            if ($request->hasFile('image')){
        // Start of saving Image to server 
                $photo = $request->file('image');
        // Creating Names for Image
                $imagename = 'category-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();
        // Resizing image and saving to server
                $destinationPath = public_path('/categories');
                $image = Image::make($photo->getRealPath()); 

                $image->resize(1300, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $image->save($destinationPath.'/'.$imagename,90);
                $category->image = $imagename;

            }
        }


        $category->group_id = $request->group_id;
        $category->name = ucwords($request->name);
        $category->slug = str_slug($request->name, '-');
        if(!empty($request->category))
        	$category->parent_id = $request->category;

        
        $category->banner_title = $request->title;
        $category->modified_by = Auth::user()->id;
        $category->save();

        return redirect('admin/categories')->with('message','Category Created')
        ->with('status','success'); 

    }

    public function editCategory($category_id){
        $active_categories = Category::whereNull('parent_id')->get();
        $category = Category::find($category_id);
        $groups = Group::where('status',1)->get();
        return view('admin.edit_category')->with('category',$category)
        ->with('groups',$groups)
        ->with('active_categories',$active_categories);
    }

    public function updateCategory(Request $request,$category_id){
        $this->validate($request, [
            'name' => 'required|unique:categories,name,'.$category_id,
            'title' => 'required',
            'group_id' => 'required',
        ]);

        $category = Category::find($category_id);
       
            if ($request->hasFile('image'))
            {
                $this->validate($request, [
                    'image' => 'required|image',
                ]);

                 // Start of saving Image to server 
                $photo = $request->file('image');
            // Creating Names for Image
                $imagename = 'category-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();
            // Resizing image and saving to server
                $destinationPath = public_path('/categories');
                $image = Image::make($photo->getRealPath()); 

                $image->resize(1300, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $image->save($destinationPath.'/'.$imagename,90);

                if(!empty($category->image)){
                    $image_path = public_path('/categories')."/".$category->image;
                    if (File::exists($image_path))
                        unlink($image_path);
                }
            }


        $category->group_id = $request->group_id;
        $category->name = ucwords($request->name);
        $category->slug = str_slug($request->name, '-');
        if(empty($request->category))
        	$category->parent_id = NULL;
        else
        	$category->parent_id = $request->category;

        if ($request->hasFile('image'))
            $category->image = $imagename;

        $category->banner_title = $request->title;
        $category->modified_by = Auth::user()->id;
        $category->save();

        return redirect('admin/categories')->with('message','Category Updated')
        ->with('status','success'); 

    }

    public function updateCategoryStatus($category_id,$status){
        $category = Category::find($category_id);
        $category->status = $status;
        $category->save();

        if($category)
            return redirect('admin/categories')
        ->with('message','Category Status Updated')
        ->with('status','success');

    }
}
