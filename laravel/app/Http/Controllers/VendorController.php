<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Vendor;
use DB;
use Log;
use Auth;

class VendorController extends Controller
{
    public function getVendors(){
    	$vendors = Vendor::orderBy('status', 'ASC')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('VENDOR');

        return view('admin.vendors')->with('vendors',$vendors)
        							->with('allowed_operations',$allowed_operations);
    }

    public function viewVendor($vendor_id){

        $vendor = Vendor::find($vendor_id);

        return view('admin.view_vendor')->with('vendor',$vendor);
    }

    public function updateVendorStatus($vendor_id,$status){
        $vendor = Vendor::find($vendor_id);
        $vendor->status = $status;
        $vendor->modified_by = Auth::user()->id;
        $vendor->save();

        if($vendor)
            return redirect('admin/vendors')
                        ->with('message','Vendor Status Changed')
                        ->with('status','success');

    }
}
