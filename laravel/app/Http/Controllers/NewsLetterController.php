<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\Mail\SendNewsLetter;
use App\NewsLetter;
use App\Customer;
use DB;
use Log;
use Auth;
use Mail;

class NewsLetterController extends Controller
{
    public function getNewsLetters(){
    	$news_letters = NewsLetter::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('NEWS_LETTER');

        return view('admin.news_letters')->with('news_letters',$news_letters)
        							->with('allowed_operations',$allowed_operations);
    }

    public function addNewsLetter(){

    	return view('admin.add_news_letter');
    }

    public function postNewsLetter(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'expires_at' => 'required',
            'content' => 'required',
        ]);

        if ($request->hasFile('attachment')){
	        $this->validate($request, [
	        	'attachment' => "required|mimes:pdf|max:10000"
			]);


			// Start of saving Image to server 
			$attachment = $request->file('attachment');
			// Creating Names for Image
		    $filename = 'news-letter-'.date("Ymdgis") . '.' . $attachment->getClientOriginalExtension();

		    // Saving to server without resizing
		    $file_save = $request->file('attachment')->move(public_path('/news_letters/'),$filename);	
		    // End of Image saving  
		}

		$customers = Customer::where('news_letter',1)->get();
        foreach ($customers as $customer) {
        	Mail::to($customer->email)->send(new SendNewsLetter($request->title,$request->content));
        }
         
        $news_letter = new NewsLetter;
        $news_letter->title = $request->title;
        $news_letter->content = $request->content;
        $news_letter->attachment = $filename;
        $news_letter->expires_at = $request->expires_at;
        $news_letter->modified_by = Auth::user()->id;
        $news_letter->save();

        return redirect('admin/news-letters')->with('message','News Letter Created')
                        ->with('status','success'); 

    }

    public function deleteNewsLetter($news_letter_id){
        $news_letter = NewsLetter::destroy($news_letter_id);

        return response()->json(['status'=>'success','msg'=>'News Letter Deleted']);

    }


}
