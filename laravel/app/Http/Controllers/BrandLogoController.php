<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OperationController;
use App\BrandLogo;

use DB;
use Log;
use Auth;
use Image;
use File;


class BrandLogoController extends Controller
{
    public function getBrandLogos(){
    	$brand_logos = BrandLogo::orderBy('created_at', 'desc')->get();

        $operation = new OperationController();
        $allowed_operations = $operation->checkOperations('BRAND_LOGO');
        
        return view('admin.brand_logos')->with(['brand_logos'=>$brand_logos,'allowed_operations'=>$allowed_operations]);
    }

    public function postBrandLogo(Request $request){
        $this->validate($request, [
            'image' => 'required|image',
            'position' => 'required|numeric',
            'name' => 'required|unique:brand_logos,name',
        ]);



		// Start of saving Image to server 
        $photo = $request->file('image');
		// Creating Names for Image
        $imagename = 'brand-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();
	    // Resizing image and saving to server
        $destinationPath = public_path('/brand_logos');
        $image = Image::make($photo->getRealPath()); 

        $image->resize(150, null, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
      });

        $image->save($destinationPath.'/'.$imagename,90);

        $brand_logo_check = BrandLogo::where('position',$request->position)->first();
        if($brand_logo_check)
            DB::table('brand_logos')->where('position','>=',$request->position)->increment('position');

        $string = str_replace(array('[\', \']'), '', $request->name);
        $string = preg_replace('/\[.*\]/U', '', $string);
        $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
        $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
        $slug= strtolower(trim($string, '-'));

        $brand_logo = new BrandLogo;
        $brand_logo->name = $request->name;
        $brand_logo->slug = $slug;
        $brand_logo->image = $imagename;
        $brand_logo->position = $request->position;
        $brand_logo->modified_by = Auth::user()->id;
        $brand_logo->save();

        return redirect('admin/brand-logos')->with('message','Brand Logo Created')
        ->with('status','success'); 

    }

    public function updateBrandLogo(Request $request,$brand_logo_id){
        $this->validate($request, [
            'position' => 'required|numeric',
            'name' => 'required',
        ]);

        $brand_logo = BrandLogo::find($brand_logo_id);

        if ($request->hasFile('image')){
         $this->validate($request, [
          'image' => 'required|image',
      ]);


			// Start of saving Image to server 
         $photo = $request->file('image');
			// Creating Names for Image
         $imagename = 'brand-'.date("Ymdgis") . '.' . $photo->getClientOriginalExtension();
		    // Resizing image and saving to server
         $destinationPath = public_path('/brand_logos');
         $image = Image::make($photo->getRealPath()); 

         $image->resize(150, null, function ($constraint) {
           $constraint->aspectRatio();
           $constraint->upsize();
       });

         $image->save($destinationPath.'/'.$imagename,90);

         $image_path = public_path('/brand_logos')."/".$brand_logo->image;
         if(file_exists($image_path)) 
         {
            unlink($image_path); 
        }

    }

    if($request->position != $brand_logo->position){
        $brand_logo_check = BrandLogo::where('position',$request->position)->first();
        if($brand_logo_check)
            DB::table('brand_logos')->where('position','>=',$request->position)->increment('position');
    }

    $string = str_replace(array('[\', \']'), '', $request->name);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
    $slug= strtolower(trim($string, '-'));
    $brand_logo->slug = $slug;

    if ($request->hasFile('image'))
     $brand_logo->image = $imagename;

    $brand_logo->name = $request->name;
 if($request->position != $brand_logo->position)
    $brand_logo->position = $request->position;

$brand_logo->modified_by = Auth::user()->id;
$brand_logo->save();

return redirect('admin/brand-logos')->with('message','Brand Logo Updated')
->with('status','success'); 

}

public function updateBrandLogoStatus($brand_logo_id,$status){
    $brand_logo = BrandLogo::find($brand_logo_id);
    $brand_logo->status = $status;
    $brand_logo->save();

    if($brand_logo)
        return redirect('admin/brand-logos')
    ->with('message','Brand Logo Status Updated')
    ->with('status','success');

} 




public function deleteBrandLogo($brand_logo_id){
    $brand_logo = BrandLogo::find($brand_logo_id);

    $image_path = public_path('/brand_logos')."/".$brand_logo->image;
    if(file_exists($image_path)) 
    {
        unlink($image_path); 
    }
    
    DB::table('products')->where('brand_logo_id', $brand_logo_id)->update(['brand_logo_id' =>NULL]);

    $brand_logo = BrandLogo::destroy($brand_logo_id);

    return response()->json(['status'=>'success','msg'=>'Pincode Deleted']);

}
}
