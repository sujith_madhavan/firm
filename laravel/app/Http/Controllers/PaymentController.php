<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Mail\OrderSuccess;
use App\Http\Controllers\TransactionRequestController;
use App\Product;
use App\Customer;
use App\Order;
use App\OrderDetail;
use App\Cart;
use DateTime;
use DB;
use Log;
use Auth;
use Validator;
use Mail;
class PaymentController extends Controller
{
	public function sendPaymentGateway(Request $request)
	{
		//return ($request->all());
		$customer = Auth::guard('customers')->user();

		//date_default_timezone_set('Asia/Calcutta');
		//$datenow = date("d/m/Y h:m:s");
		//$transactionDate = date('Y-m-d h:i:s');

		$transactionId = mt_rand(999999999,9999999999);;

		$order = Order::where('id',$request->order_id)->first();
		$order->transaction_id=$transactionId;
		$order->save();
    //     if(!empty($order->coupon_amount))
    //     {
		  // $amount = floatval($order->amount+$order->estimated_shipping) - floatval($order->coupon_amount);
    //     }
    //     else
    //     {
    //         $amount = floatval($order->amount+$order->estimated_shipping);
    //     }


        $amount = floatval($order->amount);

		$datenow = date("d/m/Y h:m:s");
		$transactionDate = str_replace(" ", "%20", $datenow);	
		//require_once 'TransactionRequest.php';

		$merchant_data='';
        $working_key='F2BC43032E12A07FB1D8F89D83418E1E';//Shared by CCAVENUES
        $access_code='AVCI78FF67BL25ICLB';//Shared by CCAVENUES
        $return_url = route('hdfc.return');  
        $merchant_id = '180486';      

        $merchant_data .= 'tid='.urlencode($transactionId).'&';
        $merchant_data .= 'merchant_id='.urlencode($merchant_id).'&';
        $merchant_data .= 'order_id='.urlencode($order->id).'&';
        $merchant_data .= 'amount='.urlencode($amount).'&';
        $merchant_data .= 'currency='.urlencode("INR").'&';
        $merchant_data .= 'redirect_url='.urlencode($return_url).'&';
        $merchant_data .= 'cancel_url='.urlencode($return_url).'&';
        $merchant_data .= 'language='.urlencode("EN").'&';
        $merchant_data .= 'delivery_name='.urlencode($order->shippingDetail->first_name).'&';
        $merchant_data .= 'delivery_address='.urlencode($order->shippingDetail->address).'&';
        $merchant_data .= 'delivery_city='.urlencode($order->shippingDetail->city).'&';
        $merchant_data .= 'delivery_state='.urlencode($order->shippingDetail->state).'&';
        $merchant_data .= 'delivery_zip='.urlencode($order->shippingDetail->pincode).'&';
        $merchant_data .= 'delivery_country='.urlencode($order->shippingDetail->country).'&';
        $merchant_data .= 'delivery_tel='.urlencode($order->shippingDetail->mobile).'&';


        $encrypted_data = $this->encrypt($merchant_data,$working_key);

        return view('hdfc_payment')->with('encrypted_data',$encrypted_data)
                                    ->with('access_code',$access_code);

		//header("Location: $url");
	}
	public function paymentResponse(Request $request)
	{


		$workingKey='F2BC43032E12A07FB1D8F89D83418E1E';     //Working Key should be provided here.
        $encResponse=$request->encResp;         //This is the response sent by the CCAvenue Server
        $rcvdString=$this->decrypt($encResponse,$workingKey);      //Crypto Decryption used as per the specified working key.
        $order_status="";
        $order_id = 0;
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);

        $status="error";
        $customer = Auth::guard('customers')->user();

        for($i = 0; $i < $dataSize; $i++) 
        {
            $information = explode('=',$decryptValues[$i]);
            
            if($i==0)   
            	$order_id=$information[1];

            if($i==3)   
            	$order_status=$information[1];
        }


        $order = Order::find($order_id);

        if($order && $order_status==="Success")
        {
			$status="success";
			$status_error="success";

			 //$cart =Cart::where('customer_id',$customer->id);
			DB::table('carts')->where('customer_id', $customer->id)->delete();

			$url = 'http://pay4sms.in';
			$key = 'A40eb53cfec3525dd53185e55623430f3';
			$sender = 'FIRMER';

			$message="Dear Customer ".$customer->first_name." ".$customer->last_name." we  received your order,you will get a call from our executive shortly,Thank You.";

			$number=$customer->mobile;
			$mysms = urlencode($message);
			$smsurl = 'http://trans.kapsystem.com/api/web2sms.php?workingkey='.$key.'&to='.$number.'&sender='.$sender.'&message=HI '.$mysms;


			$curl = curl_init();
			curl_setopt($curl,CURLOPT_URL,$smsurl);
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl,CURLOPT_HEADER,false);
			$result = curl_exec($curl);
			curl_close($curl);


			$order->transaction_status=$status;
            $order->order_status="confirmed";
			$order->save();

			Mail::to($order->customer->email)->send(new OrderSuccess($order));
			
			return redirect('/')->with('status',$status_error)->with('message',$status);
            
        }
        else if($order_status==="Aborted")
        {
            $order->transaction_status="Aborted";
            $order->save();

            return redirect('/')->with('status',$order_status)->with('message',$status);
        
        }
        else if($order_status==="Failure")
        {
            $order->transaction_status="Failure";
            $order->save();

            return redirect('/')->with('status',$order_status)->with('message',$status);
        }
		else
		{
            $order->transaction_status="Error";
            $order->save();

			return redirect('/')->with('status','error')->with('message','Transaction Id Failed');
		}


	}


	function encrypt($plainText,$key)
    {
        $key = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
        $encryptedText = bin2hex($openMode);
        return $encryptedText;
    }

    function decrypt($encryptedText,$key)
    {
        $key = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = $this->hextobin($encryptedText);
        $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
        return $decryptedText;
        
    }
    //*********** Padding Function *********************

     function pkcs5_pad ($plainText, $blockSize)
    {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }

    //********** Hexadecimal to Binary function for php 4.0 version ********

    function hextobin($hexString) 
     { 
        $length = strlen($hexString); 
        $binString="";   
        $count=0; 
        while($count<$length) 
        {       
            $subString =substr($hexString,$count,2);           
            $packedString = pack("H*",$subString); 
            if ($count==0)
        {
            $binString=$packedString;
        } 
            
        else 
        {
            $binString.=$packedString;
        } 
            
        $count+=2; 
        } 
        return $binString; 
    } 


}
