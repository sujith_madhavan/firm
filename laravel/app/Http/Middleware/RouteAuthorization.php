<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class RouteAuthorization
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }



    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $routeName = $request->route()->getName();

        if( $this->auth->user()->role->code == "SUPER_ADMIN" || $request->ajax() ){
            return $next($request);
        }

        if ($request->isMethod('post')) {
            return $next($request);
        }

        if (!empty($this->auth->user()->role->roleModules)) {
            foreach ($this->auth->user()->role->roleModules as $roleModule) {
                if (!empty($roleModule->roleModuleOperations)) {
                    foreach ($roleModule->roleModuleOperations as $roleModuleOperation) {
                        if ($roleModuleOperation->operation->route == $routeName )
                            return $next($request);
                    }
                }
            }
        }

        return redirect('admin/home')->with('message','Unauthorized Access')
                        ->with('status','error');

    }
}
