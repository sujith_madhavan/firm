<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CustomerLogged
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('customers')->check()) {
            return $next($request);
        }
        else{
            Auth::logout();
            return redirect('login');
        }
    }
}
