<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model
{

	protected $appends = ['thumb_image_path','image_path'];	
    
	
	public function getThumbImagePathAttribute() {
    	if(empty($this->image) || $this->image == "")
        	return asset('laravel/public/product-thumbs/default_image.png');
        else
        	return asset('laravel/public/product-thumbs').'/'.$this->image;

    }

    public function getImagePathAttribute() {
        if(empty($this->image) || $this->image == "")
            return asset('laravel/public/products/default_image.png');
        else
            return asset('laravel/public/products').'/'.$this->image;

    }
	

    
}
