<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function groupProducts()
    {
        return $this->hasMany('App\GroupProduct');
    }
}
