<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }
}
