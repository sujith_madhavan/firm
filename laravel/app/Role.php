<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    
    public function roleModules(){
        return $this->hasMany('App\RoleModule');
    }

    public function roleModuleOperations(){
        return $this->hasMany('App\RoleModuleOperation');
    }

    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }
}
