<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function operations()
    {
        return $this->hasMany('App\Operation');
    }
}
