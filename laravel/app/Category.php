<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $appends = ['category_name', 'image_path'];

    public function group()
    {
        return $this->belongsTo('App\Group');
    } 

    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function parentCategory()
    {
        return $this->belongsTo('App\Category','parent_id');
    }
     public function childCategories()
    {
        return $this->hasMany('App\Category','parent_id');
    }


    public function getCategoryNameAttribute() {
        if(empty($this->parentCategory))
            return "";
        else
            return $this->parentCategory->name;

    }

    public function getImagePathAttribute() {
        if(empty($this->image) || $this->image == "")
            return asset('laravel/public/categories/default.jpg');
        else
            return asset('laravel/public/categories').'/'.$this->image;

    }


}
