<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
