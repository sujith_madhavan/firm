<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleModule extends Model
{
    protected $table = 'role_modules';

    public function roleModuleOperations(){
        return $this->hasMany('App\RoleModuleOperation');
    }

    public function module()
    {
        return $this->belongsTo('App\Module');
    }
}
