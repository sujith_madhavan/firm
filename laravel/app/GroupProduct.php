<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupProduct extends Model
{
    public function productGroup()
    {
        return $this->belongsTo('App\ProductGroup');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
