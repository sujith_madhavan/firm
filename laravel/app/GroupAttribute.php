<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupAttribute extends Model
{
    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }
}
