<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleModuleOperation extends Model
{
    protected $table = 'role_module_operations';

    public function roleModule()
    {
        return $this->belongsTo('App\RoleModule');
    }

    public function operation()
    {
        return $this->belongsTo('App\Operation');
    }
}
