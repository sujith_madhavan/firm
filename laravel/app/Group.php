<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function groupAttributes(){
        return $this->hasMany('App\GroupAttribute');
    }
}
