<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $appends = ['image_path']; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getImagePathAttribute() {
        if(empty($this->image) || $this->image == "")
            return asset('laravel/public/users/default.png');
        else
            return asset('laravel/public/users').'/'.$this->image;

    }

    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }
}
