<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{

	protected $appends = ['image_path']; 


    public function modifiedBy()
    {
        return $this->belongsTo('App\User','modified_by');
    }

    public function bankDetails()
    {
        return $this->hasMany('App\VendorBankDetail');
    }

    public function getImagePathAttribute() {
        if(empty($this->image) || $this->image == "")
            return asset('laravel/public/users/default.png');
        else
            return asset('laravel/public/vendors').'/'.$this->image;

    }
}
