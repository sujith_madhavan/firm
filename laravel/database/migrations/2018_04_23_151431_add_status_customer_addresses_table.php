<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusCustomerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_addresses', function (Blueprint $table) {

            $table->boolean('is_shipping_address')->after('is_default')->default(0)->comment('1 = Default, 0 = Not Default');
            $table->renameColumn('is_default', 'is_billing_address');
        

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_addresses', function (Blueprint $table) {
            $table->dropColumn(['is_shipping_address']);
            $table->renameColumn('is_billing_address', 'is_default');
        });
    }
}
