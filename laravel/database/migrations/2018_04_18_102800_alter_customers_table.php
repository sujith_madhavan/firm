<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('user_type')->after('password');
            $table->string('gst')->after('user_type')->nullable();
            $table->boolean('news_letter')->default(0)->comment('1 = Active, 0 = Inactive')->after('gst');
            $table->integer('otp')->default(0)->after('news_letter');
            $table->boolean('otp_status')->default(0)->comment('1 = Verified, 0 = Not Verified')->after('otp');
            $table->string('email_token')->after('otp_status')->nullable();
            $table->boolean('email_status')->default(0)->comment('1 = Verified, 0 = Not Verified')->after('email_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn(['user_type', 'gst', 'news_letter', 'otp', 'otp_status', 'email_token', 'email_status']);
        });
    }
}
