<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('image')->nullable();
            $table->string('mobile')->unique();
            $table->longText('address');
            $table->string('state');
            $table->string('district');
            $table->string('pincode');
            $table->string('business_type');
            $table->string('gst')->nullable();
            $table->string('pan')->nullable();
            $table->string('contact_person');
            $table->string('designation')->nullable();
            $table->string('phone')->nullable();
            $table->string('distance')->nullable();
            $table->boolean('status')->default(0)->comment('1 = Verified, 0 = Not Verified');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
