<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('description');
            $table->date('valid_on')->nullable();
            $table->date('valid_for')->nullable();
            $table->date('valid_start_on')->nullable();
            $table->date('valid_end_on')->nullable();
            $table->string('discount_type')->nullable();
            $table->double('amount')->nullable();
            $table->string('minimum_spent')->nullable();
            $table->string('maximum_spent')->nullable();
            $table->string('usage_limt')->nullable();
            $table->string('usage_limt_per_user')->nullable();
            $table->integer('modified_by')->unsigned();
            $table->boolean('status')->default(1)->comment('1 = Active, 0 = Inactive');
            $table->timestamps();

            $table->foreign('modified_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
