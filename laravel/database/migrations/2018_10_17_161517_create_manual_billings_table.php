<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_billings', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('address');
            $table->string('ref_no')->nullable();
            $table->string('kind_attention')->nullable();
            $table->string('subject')->nullable();
            $table->string('gst_percentage')->nullable();
            $table->string('gst_amount')->nullable();
            $table->string('shipping_amount')->nullable();
            $table->string('net_total');
            $table->date('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual_billings');
    }
}
