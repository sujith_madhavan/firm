<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->string('name');
            $table->longText('description');
            $table->string('slug')->unique();
            $table->float('slashed_price', 8, 2);
            $table->float('price', 8, 2);
            $table->string('price_for');
            $table->integer('stock');
            $table->integer('minimum_quantity')->nullable();
            $table->integer('modified_by')->unsigned();
            $table->boolean('featured')->default(0)->comment('1 = Active, 0 = Inactive');
            $table->boolean('new_product')->default(0)->comment('1 = Active, 0 = Inactive');
            $table->boolean('top_selling')->default(0)->comment('1 = Active, 0 = Inactive');
            $table->boolean('status')->default(1)->comment('1 = Active, 0 = Inactive');
            $table->timestamps();

            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('modified_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
