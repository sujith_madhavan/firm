<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualBillingProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_billing_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manual_billing_id')->unsigned();
            $table->string('description')->nullable();
            $table->string('qty')->nullable();
            $table->string('price')->nullable();
            $table->string('amount')->nullable();
            $table->timestamps();

            $table->foreign('manual_billing_id')->references('id')->on('manual_billings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual_billing_products');
    }
}
