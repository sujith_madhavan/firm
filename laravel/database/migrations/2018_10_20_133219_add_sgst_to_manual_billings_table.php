<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSgstToManualBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_billings', function (Blueprint $table) {
            $table->string('sgst_percentage')->nullable()->after('gst_amount');
            $table->string('sgst_amount')->nullable()->after('sgst_percentage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manual_billings', function (Blueprint $table) {
            //
        });
    }
}
