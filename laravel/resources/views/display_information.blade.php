<?php $page="forgot";?>  
@extends('layouts.app') 
@section('content')
<div class="login-banner"></div>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="login-title text-center">
				<h2> {{ $title }} </h2>
			</div>
		</div>

		<div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">

			<div class="login-border">
				<p>{{ $content }}</p>
			</div>
		</div>
	</div>
</div>


@endsection
