@php ($page = "registration")
@extends('layouts.app') 
@section('styles')
    <style type="text/css">
        .container {
          margin-top: 20px;
        }

        .panel-heading {
          font-size: larger;
        }

        .alert {
          display: none;
        }


        /**
         * Error color for the validation plugin
         */

        .error {
          color: #e74c3c;
        }
    </style>
@endsection
@section('content')
<div class="login-banner log-img-new"></div>

<div class="container">
    <div class="row">
        <div class="reg-head">
            <h2>Vendor Registration</h2>
        </div>

        <div class="log-border">
            <form id="vendor_register" action="{{url('/vendor/register')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field()}}
                <div class="row"> 
                    @if ($errors->any())
                        <div class="alert alert-danger" style="display: block;">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group col-md-12">
                        <h3>Vendor Information</h3>
                    </div>    
                    <div class="form-group col-md-6">
                        <label> Business Name*	</label>
                        <input type="text" class="form-control" value="{{ old('business_name') }}" name="business_name" id="business_name" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Business Type* </label>
                        <input type="text" class="form-control" value="{{ old('business_type')}}" name="business_type" id="business_type" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Email*</label>
                        
                        <input type="email" class="form-control" placeholder="" name="email_id" id="email_id" value="{{ old('email_id')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Mobile<span>*</span></label>
                        <input type="text" maxlength="10" pattern="[9|8|7]\d{9}$" class="form-control" name="mobile" id="mobile"  value="{{ old('mobile') }}" required><span id="err_mobile" class="text-danger"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Password <span>*</span></label>
                        <input type="password" class="form-control" name="password" id="password" required><span id="err_pwd" class="text-danger"></span>
                    </div>
                    <div class="form-group con-top col-md-6">
                        <label>Confirm Password<span>*</span></label>
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required><span id="err_confirmpwd" class="text-danger"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label>GST Number</label>
                        <input type="text" class="form-control" name="gst" id="gst" >
                    </div>
                    <div class="form-group col-md-6">
                        <label>Pan Number</label>
                        <input type="text" class="form-control" name="pan" id="pan" >
                    </div>
                    <div class="form-group col-md-6">
                        <label>Upload GST Image</label>
                        <input type="file" class="form-control" name="image" id="image" >
                    </div>
                    <div class="form-group col-md-12">
                        <h3>Contact Details</h3>
                    </div>    
                    <div class="form-group col-md-6">
                        <label> Contact Person*  </label>
                        <input type="text" class="form-control" value="{{ old('contact_person') }}" name="contact_person" id="contact_person" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Contact Designation</label>
                        <input type="text" class="form-control" value="{{ old('contact_designation')}}" name="contact_designation" id="contact_designation">
                    </div>
                    <div class="form-group col-md-6">
                        <label> Address*</label>
                        <input type="text" class="form-control"  name="address" id="address" value="{{ old('address')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label> State*</label>
                        <input type="text" class="form-control"  name="state" id="state" value="{{ old('state')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label> District*</label>
                        <input type="text" class="form-control"  name="district" id="district" value="{{ old('district')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Pincode*</label>
                        <input type="number" class="form-control"  name="pincode" id="pincode" value="{{ old('pincode')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Phone</label>
                        <input type="text" class="form-control"  name="phone" id="phone" value="{{ old('phone')}}">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Service Distance(In Km)</label>
                        <input type="text" class="form-control" name="distance" id="distance" >
                    </div>
                    <div class="form-group col-md-12">
                        <h3>Bank Details</h3>
                    </div>    
                    <div class="form-group col-md-6">
                        <label> Bank Name</label>
                        <input type="text" class="form-control" value="{{ old('bank_name') }}" name="bank_name" id="bank_name">
                    </div>
                    <div class="form-group col-md-6">
                        <label> Branch</label>
                        <input type="text" class="form-control" value="{{ old('branch') }}" name="branch" id="branch">
                    </div>
                    <div class="form-group col-md-6">
                        <label> Account Number</label>
                        <input type="text" class="form-control" value="{{ old('account_number') }}" name="account_number" id="account_number">
                    </div>
                    <div class="form-group col-md-6">
                        <label> Account Name</label>
                        <input type="text" class="form-control" value="{{ old('account_name') }}" name="account_name" id="account_name">
                    </div>
                    <div class="form-group col-md-6">
                        <label> IFSC Code</label>
                        <input type="text" class="form-control" value="{{ old('ifsc') }}" name="ifsc" id="ifsc">
                    </div>
                </div>

                <div class="log1-back">
                    <button type="submit" id="submit" class="btn btn-login pull-right"> Register</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection
@section('scripts') 
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#vendor_register").submit(function(e){ 
                var password = $("#password").val();
                var confirm_password = $("#password_confirmation").val();
                var length = password.length;
                //alert(length1);
                if(password.length < 6)
                {
                    $('#register').attr('disabled',true);
                    $("#err_pwd").text("* Password length should be atleast six");
                    $("#password").focus();
                    e.preventDefault();
                }
                if(password != confirm_password)
                {
                    $('#register').attr('disabled',true);
                    $("#err_confirmpwd").text("* Password mismatch");
                    $("#password_confirmation").focus();
                    e.preventDefault();
                   
                }             
            });
        });

        // function chk_pwd()
        // {
        //     //alert();
        //     var password = $("#password").val();
        //     var confirm_password = $("#confirm_password").val();
        //     var length1 = password.length;
        //     //alert(length1);
        //     if(password != confirm_password)
        //     {
        //         //$("#confirm_password").val('');
        //         //$("#confirm_password").focus();
        //         $('#register').attr('disabled',true);
        //         $("#err_confirmpwd").text("* Password mismatch!");
        //         return false;
               
        //     }
        //     else if(length1 < 6)
        //     {
        //         $('#register').attr('disabled',true);
        //         $("#err_confirmpwd").text("* Password length should be atleast 6!");
        //         return false;
        //     }
        //     else
        //     {
        //          $("#err_confirmpwd").text(""); 
        //     }

        // }

      
         // $('document').ready(function(){      
         //    //alert();
         //    $("#first_name").focusout(function(){
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                //$('#register').attr('disabled',true);
         //                 $("#err_name").text("* You have to enter the name!");
         //        }
                
         //        else
         //        {

         //            $(this).css("border-color", "#2eb82e");
         //            //$('#register').attr('disabled',false);
         //            $("#err_name").text("");

         //        }
         //    });
            
         //    $("#email").focusout(function(){
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                $('#register').attr('disabled',true);
         //                 $("#err_email").text("* You have to enter the email!");
         //        }
         //        else
         //        {
         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            $("#err_email").text("");

         //        }
         //    });
         //    $("#last_name").focusout(function(){            
                
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                $('#register').attr('disabled',true);
         //                 $("#err_lname").text("* You have to enter the lastname!");
         //        }
         //        else
         //        {
         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            $("#err_lname").text("");                
         //        }
         //    }); 
         //    $("#mobile").focusout(function(){            
                
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                $('#register').attr('disabled',true);
         //                 $("#err_mobile").text("* You have to enter the mobile no!");
         //        }
         //        else
         //        {
         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            $("#err_mobile").text("");                
         //        }
         //    }); 
         //    $("#password").focusout(function(){
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                $('#register').attr('disabled',true);
         //                 $("#err_pwd").text("* You have to enter the password!");
         //        }
                
         //        else
         //        {

         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            $("#err_pwd").text("");

         //        }
         //    });
         //    $("#confirm_pwd").focusout(function(){
         //        if(($(this).val()=='')){
         //            $(this).css("border-color", "#FF0000");
         //            $('#register').attr('disabled',true);
         //            $("#err_confirmpwd").text("* You have to enter the confirm password!");
         //        }
         //        else
         //        {
         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            //$("#err_confirmpwd").text("");

         //        }
         //    });

        // });

     </script>
@endsection

