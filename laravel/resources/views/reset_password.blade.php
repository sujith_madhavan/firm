<?php $page="reset_password";?>  
@extends('layouts.app') 
@section('content')
<div class="login-banner"></div>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="login-title text-center">
				<h2>RETRIEVE YOUR PASSWORD HERE</h2>
				
			</div>
		</div>

		<div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">

			<div class="login-border">
				<form id="reset_password" action="{{ url('/customer/reset-password') }}" method="post">
                	{{ csrf_field() }}
                	@if ($errors->any())
                        <div class="alert alert-danger" style="display: block;">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @isset($error_message)
					    <div class="alert alert-danger" style="display: block;">
                            <ul>
                                <li>{{ $error_message }}</li>
                            </ul>
                        </div>
					@endisset
                	<input type="hidden" name="customer_id" id="customer_id" value="{{ $customer_id }}" >
					<div class="form-group">
						<label> Password <span>*</span></label>
                        <input type="password" class="form-control" name="password" id="password" required><span id="err_pwd" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label>Confirm Password<span>*</span></label>
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required><span id="err_confirmpwd" class="text-danger"></span>
					</div>
					<button type="submit" class="btn btn-login">Submit</button>
				</form>	
			</div>
		</div>
	</div>
</div>


@endsection
@section('scripts') 
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#reset_password").submit(function(e){ 
                var password = $("#password").val();
                var confirm_password = $("#password_confirmation").val();
                var length = password.length;
                //alert(length1);
                if(password.length < 6)
                {
                    $('#register').attr('disabled',true);
                    $("#err_pwd").text("* Password length should be atleast six");
                    e.preventDefault();
                }
                if(password != confirm_password)
                {

                    $('#register').attr('disabled',true);
                    $("#err_confirmpwd").text("* Password mismatch");
                    e.preventDefault();
                   
                }             
            });
        });
    </script>
@endsection   

