@php ($page = "home")
@extends('layouts.app') 
@section('content') 
<div class="login-banner"></div>


<style>

</style>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="check-head">
                <h1>Checkout</h1>
            </div>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">1 Billing Information</a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p> <label> Select a billing address from your address book or enter a new address. </label></p>
                            
                            <div class="form-group">

                                <select class="form-control billingaddress"  name="billing_address_id" id="my-dropdown">
                                   <option value="" >--Select Billing Address--</option>
                                   @if(!empty($customer_addresses))
                                   @php ($i = 1) 
                                   @foreach ($customer_addresses as $customer_address)
                                   <option value="{{$customer_address->id}}">{{ ucwords($customer_address->first_name) }} {{ $customer_address->last_name }},{{ ucwords($customer_address->address) }},  @if(!empty($customer_address->address1)) 
                                       {{ $customer_address->address1 }}@endif,{{ ucwords($customer_address->city) }}, {{ ucwords($customer_address->state) }} {{ $customer_address->pincode }}, {{ ucwords($customer_address->country) }}
                                   </option>
                                   @php ($i++) 
                                   @endforeach
                                   <option value="new" >New Address</option>
                                   @else
                                   <option value="new" >New Address</option>
                                   @endif
                               </select>

                           </div>
                           <input type="hidden" class="form-control" name="order_id" id="order_id" value="{{ $order_id}}" >
                           <p id="shipaddress"><input  type="radio" name="gender" value="" checked><label>&nbsp;&nbsp; Ship to this address</label> </p>
                           <p id="shipaddressdifferent" ><input  type="radio" name="gender"  value="new"><label>&nbsp;&nbsp; Ship to different address</label> </p>

                           <div class="button-set">
                            <button type="button" id="billingcontinue" class="btn-acc go-btn">Continue</button>
                        </div>
                        <form id="billing_address_form" action="" method="post" >
                            {{ csrf_field() }}
                            <div class="log-border displaybillingaddress" style="display: none;">
                               <input type="hidden" class="form-control" name="order_id" id="order_id" value="{{ $order_id}}" >
                               <div class="row">
                                <div class="reg-head3">
                                    <h3> CONTACT INFORMATION </h3>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="name"> First Name* </label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('first_name') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="name"> Last Name </label>
                                    <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" id="last_name">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for=""> Company </label>
                                    <input type="text" class="form-control" name="company" value="{{ old('company') }}" id="company">
                                </div>

                                <div class="form-group col-md-6">
                                    <label> Mobile number<span>*</span></label>
                                    <input type="text" maxlength="10" pattern="[9|8|7]\d{9}$" class="form-control" name="mobile" id="mobile" value="{{ old('mobile') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label> Fax </label>
                                    <input type="text" class="form-control" name="fax" id="fax" value="{{ old('fax') }}">
                                </div>
                                <div class="reg-head3">
                                    <h3> ADDRESS</h3>
                                </div>
                                <div class="form-group col-md-12">
                                    <label> Street Address<span>*</span></label>
                                    <input type="text" class="form-control" name="address" value="{{ old('address') }}" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" name="address1" value="{{ old('address1') }}">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" name="address2" value="{{ old('address2') }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label> City<span>*</span></label>
                                    <input type="text" class="form-control" name="city" value="{{ old('city') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label> State<span>*</span> </label>
                                    <input type="text" class="form-control" name="state" value="{{ old('state') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label> Postal code<span>*</span></label>
                                    <input type="text" class="form-control" name="pincode" value="{{ old('pincode') }}" maxlength="6" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label> Country<span>*</span></label>
                                    <input type="text" class="form-control" name="country" value="{{ old('country') }}" required>
                                </div>                              
                            </div> 
                            <div class="log2-back">
                                <button type="submit" class="btn btn-login2 pull-right"> Continue </button>
                            </div>  
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" >2 Shipping Information</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <p> <label> Select a shipping address from your address book or enter a new address. </label></p>
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="order_id" id="order_id" value="{{ $order_id}}" >
                        <select class="form-control shippingaddress"  name="shipping_address_id" id="shippingaddress">
                          <option value="" >--Select Billing Address--</option>
                          @if(!empty($customer_addresses))
                          @php ($i = 1) 
                          @foreach ($customer_addresses as $customer_address)
                          <option value="{{$customer_address->id}}">{{ ucwords($customer_address->first_name) }} {{ $customer_address->last_name }},{{ ucwords($customer_address->address) }},  @if(!empty($customer_address->address1)) 
                           {{ $customer_address->address1 }}@endif,{{ ucwords($customer_address->city) }}, {{ ucwords($customer_address->state) }} {{ $customer_address->pincode }}, {{ ucwords($customer_address->country) }}
                       </option>
                       @php ($i++) 
                       @endforeach
                       <option value="new" >New Address</option>
                       @else
                       <option value="new" >New Address</option>
                       @endif
                   </select>
                   <div class="check-bt" id="shipping">
                    <input type="checkbox"  name="" value=""><label>&nbsp; Use Billing Address </label>
                </div>

                <div class="button-set">
                    <button type="button" id="shippingcontinue" class="btn-acc go-btn1">Continue</button>
                </div>
                <form id="shipping_address_form" action="" method="post" >
                    {{ csrf_field() }}
                    <div class="log-border displayshippingaddress" style="display: none;">
                       <input type="hidden" class="form-control" name="order_id" id="order_id" value="{{ $order_id}}" >
                       <div class="row">
                        <div class="reg-head3">
                            <h3> CONTACT INFORMATION </h3>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name"> First Name* </label>
                            <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('first_name') }}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name"> Last Name </label>
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" id="last_name">
                        </div>
                        <div class="form-group col-md-12">
                            <label for=""> Company </label>
                            <input type="text" class="form-control" name="company" value="{{ old('company') }}" id="company">
                        </div>

                        <div class="form-group col-md-6">
                            <label> Mobile number<span>*</span></label>
                            <input type="text" maxlength="10" pattern="[9|8|7]\d{9}$" class="form-control" name="mobile" id="mobile" value="{{ old('mobile') }}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label> Fax </label>
                            <input type="text" class="form-control" name="fax" id="fax" value="{{ old('fax') }}">
                        </div>
                        <div class="reg-head3">
                            <h3> ADDRESS</h3>
                        </div>
                        <div class="form-group col-md-12">
                            <label> Street Address<span>*</span></label>
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}" required>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control" name="address1" value="{{ old('address1') }}">
                        </div>
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control" name="address2" value="{{ old('address2') }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label> City<span>*</span></label>
                            <input type="text" class="form-control" name="city" value="{{ old('city') }}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label> State<span>*</span> </label>
                            <input type="text" class="form-control" name="state" value="{{ old('state') }}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label> Postal code<span>*</span></label>
                            <input type="text" class="form-control" name="pincode" value="{{ old('pincode') }}" maxlength="6" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label> Country<span>*</span></label>
                            <input type="text" class="form-control" name="country" value="{{ old('country') }}" required>
                        </div>                              
                    </div> 
                    <div class="log2-back">
                        <button type="submit" class="btn btn-login2 pull-right"> Continue </button>
                    </div>  
                </div>
            </form>
        </div>
    </div>
</div>
</div>
              <!--  <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3 Shipping Method</a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>build2303</p>
                        <p><input type="radio" name="gender" value=""><label>&nbsp;&nbsp;Product Flatrate Shipping <span> ₹11,880.00</span> </label> </p>
                        <div class="button-set">
                            <p><a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Back</a></p>
                            <a href="#"><button type="button" class="btn-acc">Continue</button></a>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" >3 Payment Information</a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <form>
                            <p><input type="radio" name="gender" value="" checked><label>&nbsp;&nbsp; Credit/Debit Card or Internet Banking</label> </p>
                            <p><input type="radio" name="gender" value=""><label>&nbsp;&nbsp; CC Avenue MCPG Payment</label></p>
                        </form>
                        <div class="button-set">
                            <p><a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Back</a></p>
                            <a href="#"><button type="button" class="btn-acc">Continue</button></a>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" >3 Order Review</a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <form id="customer_info" action="{{url('/payment')}}" method="post">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <table class="table table-bordered check-tbl">
                                <tr class="last">
                                    <th style="width: 70%;">PRODUCT NAME</th>
                                    <th>PRICE</th>
                                    <th>QTY</th>
                                     <th>GST</th>
                                    <th>SUBTOTAL</th>
                                </tr>
                                @php ($i = 1) 
                                @foreach ($order_details as $order_detail)
                                <tr>
                                    <td>{{$order_detail->product->name}}</td>
                                    <td>{{$order_detail->price}}</td>
                                    <td>{{$order_detail->quantity}}</td>
                                    <td>{{$order_detail->product->gst}}%</td>
                                    @php
                                    $product_with_gst = ($order_detail->quantity*$order_detail->price) + (($order_detail->quantity*$order_detail->price)/100)*$order_detail->product->gst; 
                                    @endphp
                                    <td><span>{{$product_with_gst}}</span> </td>
                                </tr>
                                @php ($i++) 
                                @endforeach
                            <!-- <tr>
                                <td>PARRYWARE Handshower Coral Hook and Hose (T9803A1)</td>
                                <td>₹728.81 </td>
                                <td>1</td>
                                <td><span>₹728.82</span> </td>
                            </tr>
                            <tr>
                                <td>Solid Concrete Blocks - 4"</td>
                                <td>₹32.81</td>
                                <td>200</td>
                                <td><span>₹6,562.50</span> </td>
                            </tr> -->
                            @php
                            $total_amount=0;
                            $totals = 0;
                            foreach($order_details as $order_detail){

                            $product_price = ($order_detail->quantity*$order_detail->price) + (($order_detail->quantity*$order_detail->price)/100)*$order_detail->product->gst; 
                            $totals += $product_price;
                        }
                       
                        @endphp
                        <tr>
                            <td style="" class="text-right" colspan="4">
                            Subtotal </td>
                            <td><b>{{$totals}}</b></td>
                        </tr>
                       <!--  <tr >
                            <td style="" class="text-right" colspan="4">
                            Shipping Price </td>
                            <td ><b id="shipping_price">   </b></td>
                        </tr> -->
                            <!-- <tr>

                                <td style="" class="text-right" colspan="3">
                                Shipping & Handling (Multiple Rate - Shipping </td>
                                <td><b>₹11,880.00</b> </td>
                            </tr> -->
                            @if(!empty($order->coupon_amount))
                            <tr>
                                <td style="" class="text-right" colspan="4">
                                Coupon </td>
                                <td><b>{{$order->coupon_amount}}</b> </td>
                            </tr>
                            @endif
                           <!--  <tr>
                                <td style="" class="text-right" colspan="3">
                                Tax </td>
                                <td><b>₹2,513.66</b> </td>
                            </tr> -->
                            <tr class="last">
                                <td style="" class="text-right" colspan="4">
                                Grand Total </td>
                                <td ><b id="total" >      </b> </td>
                            </tr>
                        </table>
                        <input type="hidden" name="total" id="remove-coupone" class="paytotal" value= "">
                        <input type="hidden" class="form-control" name="order_id" id="order_id" value="{{ $order_id}}" >
                        <div class="button-set">
                            <p>Forgot an Item?<a href="#"> Edit Your Cart</a></p>
                            <button type="submit" class="btn-acc">Place order</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class=" col-md-3">           
    <div class="block block-progress">
        <div class="block-title">
            <strong><span>Your Checkout Progress</span></strong>
        </div>
        <div class="block-content">
            <dl>
                <div>
                    <dt  class="">Billing Address</dt>
                    <div id="movie-data" style="display: none;" style="
                    border:  1px solid #eee;
                    padding-left:  20px;
                    padding-top:  10px;
                    "></div>
                </div>

                <div id="#">
                    <dt class="">Shipping Address </dt>
                    <div id="movie-data1" style="display: none;" style="border:  1px solid #eee;
                    padding-left:  20px;
                    padding-top:  10px;
                    ">

                </div>
            </div>
             <div id="#">
                    <dt class="">Shipping Price </dt>
                    
                    <div style="border:  1px solid #eee;
                    padding-left:  20px;
                    padding-top:  10px;
                    ">
                    <p> One Kilometer Shipping Price : Rs.19</p>
                    Shipping Price :<p  id="movie-data11"></p>
                </div>
            </div>


        </dl>
    </div>
</div>
</div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">
    $('.billingaddress').on('change', function()
    {
        var type = this.value; 
        if(type=='new')
        {
            $(".displaybillingaddress").show();
            $("#shipaddress").hide();
            $("#shipaddressdifferent").hide(); 
            $("#billingcontinue").hide();   
        }
        else
        {
           $("#shipaddress").show();
           $("#shipaddressdifferent").show(); 
           $("#billingcontinue").show(); 
           $(".displaybillingaddress").hide();
       }
   });
</script>
<script type="text/javascript">
    $('.shippingaddress').on('change', function()
    {
        var type = this.value; 
        if(type=='new')
        {
            $(".displayshippingaddress").show();
            $("#shipping").hide();
            $("#shippingcontinue").hide();
            
        }
        else
        {
            $("#shipping").show();
            $("#shippingcontinue").show();
            $(".displayshippingaddress").hide();
        }
    });
</script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#billing_address_form").on("submit", function(e){
         e.preventDefault();

         $.ajax({
            url:"{{ url('/billing-address') }}",
            type: "post",
            data : $('#billing_address_form').serialize(),

            success: function(response)
            {
                //console.log(response);
                if(response.status=='success')
                {
                 $("#movie-data").html(response.billing_address);
                 $("#movie-data").show();
                 $("#collapse1").hide();
                 $("#collapse2").show();
             }
         },
         error: function(error){
            console.log(error);
        }
    })

     })

    });
</script>
<script type="text/javascript">
 $('.go-btn').click(function() {
     var billing_address_id  = $('#my-dropdown').val();
     var order_id  = $('#order_id').val();
      //alert(billing_address_id);
      if(billing_address_id>0)
      {
         $.ajax({
            url:"{{ url('/billing-address-customer') }}",
            type: "post",
            data :{billing_address_id:billing_address_id,order_id:order_id },

            success: function(response)
            {
                //console.log(response);
                if(response.status=='success')
                {

                 $("#movie-data").html(response.billing_address);
                 $("#movie-data").show();
                 $("#collapse1").hide();
                 $("#collapse2").show();
             }
         },
         error: function(error){
            console.log(error);
        }
    }) 
     }
     else
     {
        alert("pls select address")
    }
});
</script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#shipping_address_form").on("submit", function(e){
         e.preventDefault();

         $.ajax({
            url:"{{ url('/shipping-address') }}",
            type: "post",
            data : $('#shipping_address_form').serialize(),

            success: function(response)
            {
                //console.log(response);
                if(response.status=='success')
                {
                  //alert(response.shipping_price);
                $("#movie-data1").html(response.shipping_address);
                $("#movie-data11").html(response.shipping_price);
                $("#shipping_price").text(response.shipping_price);
                $("#total").text(response.total_amount);
                
                 $(".paytotal").val(response.total_amount);
                 
                 $("#movie-data1").show();
                  $("#movie-data11").show();
                 $("#collapse1").hide();
                 $("#collapse2").hide();
                 $("#collapse3").show();
             }
         },
         error: function(error){
            console.log(error);
        }
    })

     })

    });
</script>
<script type="text/javascript">
 $('.go-btn1').click(function() {
     var shipping_address_id  = $('#shippingaddress').val();
     var order_id  = $('#order_id').val();
      //alert(billing_address_id);
      if(shipping_address_id>0)
      {
         $.ajax({
            url:"{{ url('/shipping-address-customer') }}",
            type: "post",
            data :{shipping_address_id:shipping_address_id,order_id:order_id },

            success: function(response)
            {
               // console.log(response);
                if(response.status=='success')
                {
                    //alert(response.shipping_price);
                 $("#movie-data1").html(response.shipping_address);
                 $("#movie-data11").html(response.shipping_price);
                $("#shipping_price").text(response.shipping_price);
                 $("#total").text(response.total_amount);
                 $(".paytotal").val(response.total_amount);
                 $("#movie-data1").show();
                 $("#movie-data11").show();
                 $("#collapse1").hide();
                 $("#collapse2").hide();
                 $("#collapse3").show();
            }
         },
         error: function(error){
            console.log(error);
        }
    }) 
     }
     else
     {
        alert("pls select address")
    }
});
</script>

@endsection
