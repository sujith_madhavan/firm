@if(empty($category->parent_id))
@php ($page = $category->name)
@else
@php ($page = $category->parentCategory->name)
@endif    
@extends('layouts.app') 
@section('content') 
<div class="building-header">
    <div class="building-banner" 
    style="background-image: url({{ $category_image}} ) ">
    <div class="banner-content">
                <!-- @if(empty($category->parent_id))
                    <h3>{{ $category->name }} <span>PRODUCTS</span></h3>
                @else
                    <h5>{{ $category->parentCategory->name }}</h5>
                    <h3>{{ $category->name }} <span>PRODUCTS</span></h3>
                    @endif -->
                    <h3>{{ $category->banner_title }} <span></span></h3>
                </div>
            </div>
            <div class="breadcrumb">
                <a href="{{ url('/') }}"><img src="{{ asset('frontend/images/home.png') }}"></a>&nbsp;
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                @if(empty($category->parent_id))
                <p>&nbsp;{{ $category->name }}</p>
                @else
                <p>&nbsp;{{ $category->parentCategory->name }}&nbsp;</p>&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>
                <p>&nbsp;{{ $category->name }}</p>
                @endif
            </div>
            <div class="container-fluid">
                <div class="">
                    <div class="sidebar_prod">
                        <div class="shop-left-area" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="single-left-shop mb-40">
                                <form id="category_filters" action="{{ url('category',[$category->slug]) }}/filters" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="category_id" value="{{ $category->id }}">
                                    <div class="dropdown_categories">
                                        @if(count($category->childCategories))
                                        <div class="shop-title-hide mb-20" role="tab" id="headingOne">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <h3>CATEGORIES</h3>
                                            </a>
                                        </div>
                                        <div id="collapseOne" class="collapse in arrow-content" role="tabpanel" aria-labelledby="headingOne">
                                            <ul class="brand">
                                                @foreach($category->childCategories as $child_category)
                                                @if($child_category->status==1)
                                                <li><a href="{{ url('category',[$child_category->slug]) }}">{{ strtoupper($child_category->name) }}</a></li>
                                                @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif    
                                        @foreach($category->group->groupAttributes as $group_attribute)
                                        @php
                                        $expand = "false";
                                        $checked_status = "";
                                        if(in_array($group_attribute->attribute->id, $selected_attribute_ids))
                                        $expand = "true"; 
                                        @endphp
                                         @if($group_attribute->attribute->status==1)
                                        <div class="shop-title mb-20 filter-heading" role="tab" id="headingTwo">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ strtoupper($group_attribute->attribute->id) }}" aria-expanded="false" aria-controls="{{ strtoupper($group_attribute->attribute->id) }}">
                                                <h3>{{ strtoupper($group_attribute->attribute->common_name) }}</h3>
                                            </a>
                                        </div>
                                         @endif
                                        <div id="{{ strtoupper($group_attribute->attribute->id) }}" class="panel-collapse collapse cus-dropdown-categories" role="tabpanel" aria-labelledby="headingTwo">
                                            @foreach($group_attribute->attribute->AttributeValues as $attribute_value)
                                            @if(in_array($attribute_value->id, $selected_attribute_value_ids))
                                            @php($checked_status = "checked")
                                            @endif

                                            <div class="filter-list category_filter_01">
                                                <input id="{{ $attribute_value->id }}" name="attribute_values[]" type="checkbox" {{ $checked_status }} value="{{ $attribute_value->id }}">
                                                <label for="{{ $attribute_value->id }}"><span class="custom-checkbox"></span>{{ strtoupper($attribute_value->value) }}</label>
                                            </div>
                                            @php($checked_status = "")
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>    
                                </div>    
                            </div>

                        </div>
                        <div class="products-img">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 b">
                                    <h3>{{$category->name}}</h3>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="short-by">
                                        <label>
                                            <select name="sort_by" id="sort_by" class="s-hidden scroll" onchange="get_products(this.value)">
                                                @if ($sort_by == 'asc')
                                                <option value="">Sort By</option>
                                                <option selected value="asc">Low To High</option>
                                                <option value="desc">High To Low</option>
                                                @elseif ($sort_by == 'desc')
                                                <option value="">Sort By</option>
                                                <option value="asc">Low To High</option>
                                                <option selected value="desc">High To Low</option>
                                                @else
                                                <option selected value="">Sort By</option>
                                                <option value="asc">Low To High</option>
                                                <option value="desc">High To Low</option> 
                                                @endif 
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            @if(count($category_products))
                            @foreach($category_products as $category_product)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-img">
                                <div class="product-inner">
                                    <div class="image-container">
                                        <img src="{{$category_product->product->thumb_image_path}}">

                                        <div class="image-hover">
                                            <a href="{{url('/category/product')}}/{{$category_product->product->slug}}"><img src="{{asset('frontend/images/hover.png')}}">
                                                <p>VIEW PRODUCT</p></a>
                                            </div>

                                            <div class="overlay"></div>
                                        </div>
                                        <div class="product-detail">
                                            <p>{{ $category_product->product->name }}</p>
                                        @if(!empty($category_product->product->price))
                                        @if($category_product->product->price==1)

                                        @else
                                         @if($category_product->product->offer_valid && !empty($category_product->product->slashed_price))
                                            <p class="rate">

                                                Rs 
                                                <strike> {{ $category_product->product->price }} / </strike>
                                                <span>{{$category_product->product->slashed_price}} / {{$category_product->product->price_for}}</span>

                                            </p>
                                            @else
                                            <p class="rate"><span>Rs {{$category_product->product->price}} / {{$category_product->product->price_for}}</span></p>
                                            @endif
                                            @endif
                                            @endif

                                        </div>
                                        <div class="add-card">
                                            <div class="row">
                                                <div class="col-xs-6 detail">
                                                    <a href="{{url('/category/product')}}/{{$category_product->product->slug}}">View detail</a>
                                                </div>
                                                @if(!empty($category_product->product->price))
                                                @if($category_product->product->price==1)

                                                @else
                                                <div class="col-xs-6 card">
                                                    <a href="{{url('/list-cart-add')}}/{{$category_product->product->id}}">Add</a>
                                                </div>
                                                @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                <div class="row">
                                    <h3>NO PRODUCTS</h3>
                                </div>
                                @endif      
                            </div>
                            <div class="clearfix"></div>

                            <div class="view">

                                <div class="row">
                                    <div class="col-md-6 col-sm-12 left-view" >
                                        <!-- <a class="view-more" href="#">View more</a> -->
                                    </div>
                                    <div class="col-md-6 col-sm-12 right-view">
                                        <div class="view-inner">
                                           @if(method_exists($category_products,'links'))
                                           @if($category_products->total() > 12)
                                           <p>{{ $category_products->total() }} products</p>
                                           @endif    
                                           <span class="pull-right">
                                            {{ $category_products->links() }}
                                        </span>   
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('scripts')

        <script type="text/javascript">
            $(document).ready(function () {
                $(':checkbox').change(function() {
                    var values = $('input[name="attribute_values[]"]:checked').length;
                    if (values >= 1) {
                        $( "#category_filters" ).submit();
                    } 
                }); 

            });

            function get_products(sort_by){
                $( "#category_filters" ).submit();
            }
        </script>

        @endsection