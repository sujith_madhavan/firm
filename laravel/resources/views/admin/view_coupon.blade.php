@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.image-circle{
			border-radius: 50%;
		}

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

	</style>
	
@endsection
@section('content')
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <!-- <h5 class="txt-dark">Edit User</h5> -->
        </div>  
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
                <li class="active"><span>View-Coupon</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->                    
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">View Coupon</h6>
                    </div>
                    <div class="pull-right">
                       <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Code : </label>
                                    <label>{{ $coupon->code }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Description : </label>
                                    <label>{{ $coupon->description }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Valid On : </label>
                                    <label>{{  date('d-m-Y', strtotime($coupon->valid_on)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Valid For : </label>
                                    <label>{{ date('d-m-Y', strtotime($coupon->valid_for)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Valid Start On : </label>
                                    <label>{{ date('d-m-Y', strtotime($coupon->valid_start_on)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Valid End On : </label>
                                    <label>{{  date('d-m-Y', strtotime($coupon->valid_end_on)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Discount Type : </label>
                                    <label>{{  $coupon->discount_type }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Amount : </label>
                                    <label>{{  $coupon->amount }}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Minimum Spent : </label>
                                    <label>{{ $coupon->minimum_spent }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Maximum Spent : </label>
                                    <label>{{ $coupon->maximum_spent }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Usage Limit : </label>
                                    <label>{{ $coupon->usage_limt }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Usage Limit Per User : </label>
                                    <label>{{ $coupon->usage_limt_per_user }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Modified By : </label>
                                    <label>{{ $coupon->modifiedBy->name }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Created At : </label>
                                    <label>{{ date('d-m-Y h:i:s A', strtotime($coupon->created_at)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Updated At : </label>
                                    <label>{{ date('d-m-Y h:i:s A', strtotime($coupon->updated_at)) }}</label>
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->	
   
@endsection
