@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}

		.forex-color{
			background: #e63e0c;
		}

		.buttons-excel {
		  	background-color: #ea6c41 !important;
		  	border: solid 1px #ea6c41 !important;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	  	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h5 class="txt-dark">News Letters</h5>
	  	</div>  
	  	<!-- Breadcrumb -->
	  	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
		  		<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
		  		<li class="active"><span>News Letters</span></li>
			</ol>
	  	</div>
	  	<!-- /Breadcrumb -->          
	</div>
	<div class="row">
	  	<div class="col-sm-12">
			<div class="panel panel-default card-view">
  		  		<div class="panel-heading">
  					<div class="pull-left">
  					  <!-- <h6 class="panel-title txt-dark">Services</h6> -->
  					</div>
  					@if ($allowed_operations['add'])
  						<div class="pull-right">
  						   	<a href="{{ url('admin/news-letters/add') }}" class="btn btn-danger btn-circle  btn-sm forex-color" title="Add New" ><i class="fa fa-plus"></i></a>
  						</div>
  					@endif
  		  		</div>
		  		<div class="panel-wrapper collapse in">
					<div class="panel-body">
			  			<div id="validation">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
			  			</div>  
			  			<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>
											<th>Title</th>
											<th>Content</th> 
											<th style="width: 5%">Attachment</th>               
											<th>Expires At</th>
											<th>Modified By</th>
											<th>Modified At</th>
											@if ($allowed_operations['remove'])
												<th style="width: 5%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($news_letters as $news_letter)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td>{{ $news_letter->title }}</td>
						  						<td>{!! $news_letter->content !!}</td>
						  						<td>
						  							@if(!empty($news_letter->attachment))
						  								<a href="{{ asset('laravel/public/news_letters').'/'.$news_letter->attachment }}" download><img src="{{ asset('frontend/images/pdf.png') }}"></a>
						  							@endif
						  						</td>
						  						<td>{{ date('d-m-Y', strtotime($news_letter->expires_at)) }}</td>
						  						<td>{{ $news_letter->modifiedBy->name }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($news_letter->updated_at)) }}</td>
							  					@if ($allowed_operations['remove'])      
							   						<td>
							   							@if ($allowed_operations['remove'])     
							 								<a href="{{ url('admin/news-letters',[$news_letter->id]) }}/delete" class="btn btn-danger btn-xs delete-click" ><span class="fa fa-trash" ></span></a>
							 							@endif	
							   						</td>
						   						@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
					</div>
		  		</div>
			</div>
	  	</div>
	</div>
   
@endsection
@section('scripts')	
	<script type="text/javascript">      
		$(".delete-click").click(function (event) {
			event.preventDefault();
			var url = $(this).attr("href");
			swal({
				title: 'Are you sure?',
				text: "You won't be able to recover once deleted!",
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then(function () {
				$.ajax({
					type: "get",
					url: url,
					success: function(response){
						if(response.status == "success")
							  window.location.href="{!! url('/admin/news-letters') !!}";
					},
				});
			})
			  // alert(url);
		});
	</script>

@endsection