@extends('admin.layouts.app') 
@section('styles')
<style type="text/css">
.table-header-colour{
	background-color: #262626; 
	color: #fff;
}
</style>
@endsection
@section('content')
<div class="row heading-bg">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-dark">Manual Billing</h5>
	</div>  
	<!-- Breadcrumb -->
	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
		<ol class="breadcrumb">
			<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
			<li class="active"><span>Manual Billing</span></li>
		</ol>
	</div>
	<!-- /Breadcrumb -->          
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<!-- <h6 class="panel-title txt-dark">Gallery Image</h6> -->
				</div>

				<div class="pull-right">
					<a href="{{ url('admin/manual-billing-add') }}" class="btn btn-danger btn-circle  btn-sm forex-color" title="Add New" ><i class="fa fa-plus"></i></a>
				</div>	
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div id="validation">
						@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
					</div>					  
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="datable_1" class="table table-bordered display">
								<thead>
									<tr>
										<th style="width: 8%">S.No</th>            
										<th>Kind Attention</th>
										<th>Ref No</th>
										<th>Subject </th>
										<th>Net Total</th>
										<th>Print Invoice</th>
										<th style="width: 12%">Action</th>

									</tr>
								</thead>
								<tbody>
									@php ($i = 1) 
									@foreach ($manual_billings as $manual_billing)                        
									<tr>
										<td>{{ $i }}</td>
										<td>{{ $manual_billing->kind_attention }}</td>
										<td>{{ $manual_billing->ref_no }}	
										</td>
										<td>{{ $manual_billing->subject }}</td>
										<td>{{ $manual_billing->net_total }}</td> 
										<td>   
											<a href="{{url('/admin/manual_billing/print',[$manual_billing->id])}}"> <button class="btn btn-danger btn-xs" >Print Invoice</button></a>
										</td>    
										<td>   
											<a href="{{ url('admin/manual-billing',[$manual_billing->id]) }}/edit" class="btn btn-danger btn-xs" ><span class="fa fa-edit" ></span></a>
											<a href="{{ url('admin/manual-billing',[$manual_billing->id]) }}/delete" class="btn btn-danger btn-xs delete-click"><span class="fa fa-trash"></span></a>	
										</td>
									</tr>
									@php ($i++) 
									@endforeach
								</tbady>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">    
	$(".delete-click").click(function (event) {
		event.preventDefault();
		var url = $(this).attr("href");
		swal({
			title: 'Are you sure?',
			text: "You won't be able to recover once deleted!",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then(function () {
			$.ajax({
				type: "get",
				url: url,
				success: function(response){
					if(response.status == "success")
						window.location.href="{!! url('/admin/manual-billing') !!}";
				},
			});
		})
// alert(url);
});
</script>

@endsection
