@extends('admin.layouts.app') 
@section('styles')  

<style type="text/css">
.image-circle{
    border-radius: 50%;
}

</style>

<link href="{{asset('backend/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
<style type="text/css">

/*****order-view****/
.dashboard-backgrnd .side-bar {
    border: 1px solid #e75e00;
    margin: 25px 16px;
    box-shadow: 0px 2px 12px rgba(66, 80, 52, 0.88);
}
.dashboard-backgrnd .side-bar button {
    background: transparent;
    border: none;
    width: 100%;
    padding: 0px;
}
.dashboard-backgrnd .side-bar a {
    text-decoration: none;
}
.dashboard-backgrnd .side-bar button.active a h3 {
    color: #e75e00;
    text-align: center;
    font-size: 18px;
    font-family: "f2";
    font-weight: 900;
}


.ord-viw-head h4{
    font-family: "f1";
}
.ord-viw-head{
    color:#e75e0d;
}
.order-bottom{
    border-bottom:2px solid #eee;
}
.order-num-head {
    border:1px solid #ddd;
    margin-top: 20px;
    font-family: "f1";
}
.order-num-head h3{
   font-size: 16px;
   background:rgba(63, 87, 111, 0.9);
   margin: 0;
   padding-top: 6px;
   padding-bottom: 6px;
   padding-left: 15px;
   color: #fff;
   font-family: "f1";
}
.order-num-head span a{
    float:right;
    color:#fff;
    padding-right: 15px;
    font-family: "f1";
}
.para-txt-order p{
    padding-left:15px;
    line-height: 25px;
    font-family: "f1";
    margin:0;
}
.order-padd-vw{
    padding-right:15px;
}
.para-txt-order{
    padding-top: 10px;
    padding-bottom: 10px;
}
.border-ha th,td{
    border:1px solid #ccc;
    text-align: center;
    font-family: "f1";
    color:#333;
}
.border-ha{
    border:none;
}
.btn-bc-order{
    display: inline-block;
    padding: 4px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    background: #e75e0d;
    color: #fff;
    font-family: "f1";
    font-size:12px;
}
.btn-bc-order2{
    display: inline-block;
    padding: 4px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    border: 1px solid transparent;
    border-radius: 4px;
    background: #dcdbdb;
    color: #222;
    font-family: "f1";
    font-size:12px;
}
.lft-btn{
    float: right;
    margin-right: 15px;
    font-family: "f1";
}
.slct-cust-as{

    width: auto !important;

}
.clr-org{
    color:#e75e0d;

}
/***end***/
</style>

<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12 dashboard-right tabcontent" id="account">
                    <div class="row order-bottom">
                        <div class="ord-viw-head col-md-6">
                            <h4> Order FIRM{{ $order->id }} | {{ date('d-m-Y h:i:s A', strtotime($order->created_at)) }} </h4>

                        </div>
                        <div class="btn-bc-order-ct col-md-6 text-right">
                             <a href="{{ url()->previous() }}"><button class="btn-bc-order2"><i class="fa fa-arrow-circle-left"></i> back</button></a>
                           <!--  <button class="btn-bc-order">Vendor Verfication Finish</button>
                            <button class="btn-bc-order">send Email</button> -->
                           <!--  <button class="btn-bc-order">Credit Memo</button> -->

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="order-num-head">
                                <h3> Order FIRM{{ $order->id }} (the confirmation email is not sent)</h3>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="para-txt-order">
                                            <p>Order Date</p>
                                            <p>Order Status</p>
                                            <p>Amount<br><br><br></p>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="para-txt-order">
                                            <p>{{ date('d-m-Y h:i:s A', strtotime($order->created_at)) }}</p>
                                            <p>{{ $order->transaction_status }}</p>
                                            <p>{{ $order->amount }}
                                            </p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="order-num-head">
                                <h3> Account Information</h3>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="para-txt-order">
                                            <p>Customer Name</p>
                                            <p>Email</p>
                                            <p>Mobile</p>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="para-txt-order">
                                            <p>{{ $order->customer->first_name }}</p>
                                            <p> <a href="#"> {{ $order->customer->email }}</a>
                                                <p>{{ $order->customer->mobile }}</p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">

                        </div>
                        <div class="col-md-6">
                            <div class="order-num-head">
                                <h3> Billing Address
                                    <span> <a href="#">Edit </a></span> </h3>

                                    <div class="para-txt-order">
                                       @if(!empty( $order->billingDetail->last_name)) <p>{{ $order->billingDetail->first_name }} {{ $order->billingDetail->last_name }} </p>
                                       @endif
                                       @if(!empty( $order->billingDetail->address)) <p>{{  $order->billingDetail->address }}</p>
                                       @endif
                                       @if(!empty( $order->billingDetail->address2))<p>{{  $order->billingDetail->address1 }}, {{  $order->billingDetail->address2 }},</p>@endif
                                       @if(!empty( $order->billingDetail->city))  <p>{{  $order->billingDetail->city }},{{  $order->billingDetail->pincode }}, @if(!empty( $order->billingDetail->country)){{  $order->billingDetail->country }}@endif</p>
                                       @endif
                                       @if(!empty( $order->billingDetail->mobile))
                                       <p>T: {{ $order->billingDetail->mobile }}</p>
                                       @endif
                                   </div>

                               </div>
                           </div>
                           <div class="col-md-6">
                            <div class="order-num-head">
                                <h3> Shipping Address
                                    <span> <a href="#">Edit </a></span> </h3>

                                    <div class="para-txt-order">
                                        @if(!empty( $order->shippingDetail->last_name)) <p>{{ $order->shippingDetail->first_name }} {{ $order->shippingDetail->last_name }} </p>
                                        @endif
                                        @if(!empty( $order->shippingDetail->address)) <p>{{  $order->shippingDetail->address }}</p>
                                        @endif
                                        @if(!empty( $order->shippingDetail->address2))<p>{{  $order->shippingDetail->address1 }}, {{  $order->shippingDetail->address2 }},</p>@endif
                                        @if(!empty( $order->shippingDetail->city))  <p>{{  $order->shippingDetail->city }},{{  $order->shippingDetail->pincode }}, @if(!empty( $order->shippingDetail->country)){{  $order->shippingDetail->country }}@endif</p>
                                        @endif
                                        @if(!empty( $order->shippingDetail->mobile))
                                        <p>T: {{ $order->shippingDetail->mobile }}</p>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="order-num-head">
                                    <h3> Payment Information </h3>
                                    <div class="para-txt-order">
                                        <p>Order was placed using INR</p>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="order-num-head">
                                    <h3> Shipping & Handling Information </h3>
                                    <div class="para-txt-order">
                                        <p> Vendor Product Flatrate - Product Flatrate Shipping <span><i class="fa fa-rupee"></i></span> 0.00 </p>

                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="order-num-head border-ha">
                                    <h3> Item Ordered </h3>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th> Product</th>
                                                <th> Item Status</th>
                                                <th> Price</th>
                                                <th> Qty</th>
                                                <th> Subtotal</th>
                                                <th> Tax Amount</th>
                                                <th> Tax Percentage</th>
                                                <th> Discount Amount </th>
                                                <th> Row Total</th>

                                            </tr>
                                            @php ($i = 1) 
                                            @foreach ($order->orderDetails as $order_detail)   
                                            <tr>
                                             @if(!empty($order_detail->product->name))
                                             <td>{{ ucwords($order_detail->product->name) }}</td>
                                             @else
                                             <td>Delete Product{{ ucwords($order_detail->product_name) }}</td>
                                             @endif
                                             <td>{{ ucwords($order_detail->order->order_status)}} </td>
                                             @php

                                             $sub_total = $order_detail->quantity*$order_detail->price;
                                             $total=$order_detail->gst_amount+$sub_total+$order_detail->order->coupon_amount;
                                             @endphp
                                             <td> <span> <i class="fa fa-rupee"></i></span> {{ number_format($order_detail->price,2) }}</td>
                                             <td> {{$order_detail->quantity}}</td>
                                             <td> <span> <i class="fa fa-rupee"></i></span> {{$sub_total}}</td>
                                             <td> <span> <i class="fa fa-rupee"></i></span> {{$order_detail->gst_amount}}</td>
                                             <td> <span> <i class="fa fa-rupee"></i></span>{{$order_detail->gst}}% </td>
                                             <td> <span> <i class="fa fa-rupee"></i></span>@if(!empty($order_detail->order->coupon_amount)){{$order_detail->order->coupon_amount}}@else
                                                0.00
                                            @endif</td>
                                            <td> <span> <i class="fa fa-rupee"></i></span> {{$total}}</td>
                                        </tr>
                                        @php ($i++) 
                                        @endforeach

                                        <tr>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="order-num-head">
                                <h3> Comments History </h3>
                                <div class="para-txt-order">
                                    @if($order->order_status === 'confirmed')
                                    <form id="add_category" method="post" action="{{ url('admin/vendor_assign',[$order->id]) }}/status" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <p> Add vendor comments</p>
                                        <label class="control-label mb-10 text-left">Select Vendor *</label><br> 
                                        <select class="form-control" required name="vendor_id" id="vendor_id">
                                            <option value="0">Select Vendor</option>
                                            @foreach ($vendors as $vendor) 
                                            <option value="{{$vendor->id}}">{{ ucwords($vendor->business_name) }}</option>
                                            @endforeach
                                        </select>

                                        <p> Status</p>
                                        <p><select  name="status"
                                            class="form-control slct-cust-as">
                                            <option value="vendor assigned">Vendor assigned</option>
                                            <option value="cancelled">Cancelled</option>


                                        </select>
                                    </p>
                                    <div class="form-group">
                                      <p>  <label for="comment">Comment:</label><br>
                                        <textarea cols="40" rows="3" name="comments" id="comment" required=""></textarea>
                                    </p> </div>
                                    <div class="checkbox">

                                        <button type="submit" class="btn-bc-order lft-btn"><span> <i class="fa fa-check-circle" aria-hidden="true"></i></span> Submit Comment</button>
                                    </div>

                                </form>

                                @elseif($order->order_status =='vendor assigned')

    <form id="add_category" method="post" action="{{ url('admin/vendor_assign',[$order->id]) }}/status" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                
                                       
                                        <p> Status</p>
                                        <p><select  name="status"
                                            class="form-control slct-cust-as">
                                            <option value="shipped">Shipped</option>
                                        </select>
                                    </p><br>
                                    <div class="form-group">
                                         <p>  <label for="comment">Shipping Date:</label><br>
                                       <input type="text" class="form-control datepicker-autoclose" name="start_date" placeholder="Start Date"> </p>
                                    </div>
                                    <div class="form-group">
                                      <p>  <label for="comment">Tracking Number:</label><br>
                                        <input type="text" class="form-control" name="tracking_no" >
                                    </p> </div>
                                    <div class="form-group">
                                      <p>  <label for="comment">Comment:</label><br>
                                        <textarea cols="40" rows="3" name="comments" id="comment" required=""></textarea>
                                    </p> </div>
                                    <div class="checkbox">

                                        <button type="submit" class="btn-bc-order lft-btn"><span> <i class="fa fa-check-circle" aria-hidden="true"></i></span> Submit Comment</button>
                                    </div>
</form>
                               
                                 @elseif($order->order_status =='shipped')

    <form id="add_category" method="post" action="{{ url('admin/vendor_assign',[$order->id]) }}/status" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                 <p> Status</p>
                                        <p><select  name="status"
                                            class="form-control slct-cust-as">
                                            <option value="delivered">Delivered</option>
                                        </select>
                                    </p><br>
                                     <div class="checkbox">

                                        <button type="submit" class="btn-bc-order lft-btn"><span> <i class="fa fa-check-circle" aria-hidden="true"></i></span> Submit Comment</button>
                                    </div>
                                    </form>
                                @endif
                        
                                <hr>
                                <p><span> <i class="fa fa-file-o"></i></span>  {{ date('d-m-Y h:i:s A', strtotime($order->created_at)) }} | Completed</p>
                                <p> {{ $order->transaction_status }}</p>
                                @php ($i = 1) 
                                @foreach ($order_trackings as $order_tracking)  
                                <hr>
                                <p> <span> <i class="fa fa-file-o"></i></span> {{ date('d-m-Y h:i:s A', strtotime($order_tracking->created_at)) }} | Processing </p>
                                <p> {{ $order_tracking->status }}</p> 
                                @if($order_tracking->status === 'shipped')
                                @if(!empty($order_tracking->order->estimated_shipping))
                                <p> Shipping Date : {{$order_tracking->order->estimated_shipping}}</p>
                                <p> tracking_id : {{$order->tracking_no }}</p>
                                @endif
                                 @endif
                                <p> {{ $order_tracking->comments }}</p> 
                                @php ($i++) 
                                @endforeach
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="order-num-head">

                            <h3> Order Totals </h3>
                            @php
                            $totals = 0;
                            $sub_totals=0;
                            $tax_amounts=0;
                            foreach($order->orderDetails as $order_detail){

                            $product_price = ($order_detail->quantity*$order_detail->price) + (($order_detail->quantity*$order_detail->price)/100)*$order_detail->gst;

                            $totals += $product_price;

                            $sub_total = $order_detail->quantity*$order_detail->price;
                            $sub_totals += $sub_total;

                            $tax_amount = (($order_detail->quantity*$order_detail->price)/100)*$order_detail->gst;

                            $tax_amounts += $tax_amount;
                        }
                        $total_amount = $totals-$order->coupon_amount;
                        @endphp
                        <div class="para-txt-order text-right">
                            <div class="row">
                                <p><span class="col-md-8"> Sub totals</span> <span class="col-md-3"> <i class="fa fa-rupee"></i> {{ $sub_totals }}</span></p>

                               <!--  <p><span class="col-md-8"> Shipping Handling</span> <span class="col-md-3"> <i class="fa fa-rupee"></i> 0.00</span></p> -->
                                <p><span class="col-md-8"> Tax</span> <span class="col-md-3"> <i class="fa fa-rupee"></i> {{ $tax_amounts }}</span></p>


                                <p><span class="col-md-8"> Grand Totals </span><span class="col-md-3 clr-org"> <i class="fa fa-rupee"></i>{{ $totals}}</span></p>

                                <p><span class="col-md-8"> Total Paid</span> <span class="col-md-3 clr-org"> <i class="fa fa-rupee"></i>{{ $totals}}</span></p>

                               <!--  <p><span class="col-md-8"> Total Refund</span> <span class="col-md-3 clr-org"> <i class="fa fa-rupee"></i> 590.00</span> </p> -->

                                <!-- <p><span class="col-md-8">Total Due </span><span class="col-md-3 clr-org"> <i class="fa fa-rupee"></i> 0.00</span></p> -->
                            </div>
                        </div>

                    </div>
                </div>



            </div>
        </div>
    </div>
</div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('backend/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script> 

<script>
    // Clock pickers
    $(document).ready(function() {



    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });
    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
});
</script>
@endsection