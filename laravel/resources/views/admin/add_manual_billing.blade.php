@extends('admin.layouts.app') 
@section('styles')
<style>

.offer-firmer1 textarea {
	border: 1px solid #ddd;
	text-align: left;
	background: none;
	color:#000;

}
.offer-firmer textarea {
	width: 181px;
	text-align: center;
	background: none;
	color:#000;
}
.offer-firmer1 textarea:focus {
	outline: none;
}
.offer-firmer textarea:focus {
	outline: none;
}
.offer-firmer1 b{
	color:#000;
}
.offer-firmer p{
	color:#000;
}

.offer-firmer td {
	border: 1px solid #ccc !important;
	color: #222;
	text-align: center;
	
}

.offer-firmer th {
	border: 1px solid #ccc;
	color: #222;
	font-weight: 700;
	text-align: center;
	padding: 10px;
}
.offer-firmer input {
	border: 1px solid #ddd;
	color: #222;
	height: 30px;
	background: none;
	margin: 10px 0;
	padding: 10px;
}
.offer-firmer input:focus {
	outline: none;
}
.btn-sub-offer{
	padding: 10px 20px;
	background: #e75e0d;
	color:#fff;
	text-align: right;
	border: none;
	margin: 20px 0;
	float: right;
	cursor: pointer;
	margin-right:60px;
}
.btn-sub-offer:hover{
	background: #222;

}
.offer-ic i{
	width: 50px;
	height:30px;
	line-height: 30px;
	background: #ea6c41;
	border-radius:50px;
	margin-top:8px;
	color:#fff;
	cursor: pointer;
}
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div id="validation">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
			<div class="offer-firmer1">
				
				<form method="post" action="{{ url('admin/manual-billing/insert') }}">
					{{ csrf_field() }}
					<p> To,</p>
			<!-- 		<textarea rows="4" cols="50" name="address" class="text-left" placeholder="Address" required>
						</textarea> -->
						<fieldset>
							
					<textarea class="textarea_editor form-control"  rows="6" cols="50" name="address" name="description" required></textarea>
			
				</fieldset>

					
					<div class="offer-firmer">
						<b>Ref No </b> <span class="ref-nam" style="padding-left: 50px;
">: </span><input type="text" name="ref_no" value="" required><br> <b> Kind Attention </b><span class="kind-att" style="padding-left: 3px;">:</span> <input type="text" name="kind_attention" value="" required><br> <b> Subject </b> <span class="kind-subj" style="padding-left: 44px;">:</span> <input type="text" name="subject" value="" required><br>
						<p> Dear sir,</p>
						<p> As per your enquiry we are Submitting our offer</p>

						<div class="table-border">
							<table class="table" style="border-collapse: inherit;">
								<tr>
									<th> S.No</th>
									<th> Description </th>
									<th> Qty </th>
									<th> Unit Price </th>
									
									<th> Amount </th>
								</tr>
								<tbody id="rowAppend">
									<tr>
										<td>1</td>
										<td>
											<textarea rows="4" name="lists[1][description]" cols="50" required=""></textarea> </td>
											<td> <input type="text" name="lists[1][qty]" value="" placeholder="200nos" required> </td>
											<td> <input type="text" name="lists[1][price]" value="" placeholder="73/-" required> </td>
											

											<td> <input type="text" name="lists[1][amount]" value="" placeholder="14,600/-" required> </td>

											<td class="offer-ic" style="border: none !important;">
												<i id="addMore" class="fa fa-plus"></i> <br>
												<!-- <i  class="fa fa-minus remove"></i> -->
											</td>
										</tr>
									</tbody>
									<tr>
										<td> </td>
										<td>GST <input type="text" name="gst_percentage" value="5" placeholder="5%"></td>
										<td> </td>
										
										<td></td>
										<td> <input type="text" name="gst_amount" value="" placeholder="730/-" required> </td>
									</tr>
									
									<tr>
										<td> </td>
										<td> Transport Loading &amp;<br> Unloading charges </td>
										<td> </td>
										
										<td> </td>
										<td> <input type="text" name="shipping_amount" value="" placeholder="400/-" required> </td>
									</tr>
									<tr>
										<td> </td>
										<td> Net Total </td>
										<td> </td>
										
										<td></td>
										<td> <input type="text" name="net_total" value="" placeholder="15,730/-" required> </td>
									</tr>
								</table>
							</div>
							<button type="submit" class="btn-sub-offer" value=""> Submit </button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@endsection
	@section('scripts')
	<!-- Form Wizard JavaScript -->
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>

	<script src="{{ asset('backend/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>

	<!-- Bootstrap Wysuhtml5 Init JavaScript -->
	<script src="{{ asset('backend/dist/js/bootstrap-wysuhtml5-data.js') }}"></script>

	<script>
		$(function(){
			var i =2
			$('#addMore').on('click', function() {

	        // var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
	        // data.find("input").val('09AM- 10AM');
	        $('<tr><td>'+i+'</td><td><textarea rows="4" name="lists['+i+'][description]" cols="50"></textarea> </td><td> <input type="text" name="lists['+i+'][qty]" value="" placeholder="200nos"> </td><td> <input type="text" name="lists['+i+'][price]" value="" placeholder="73/-"> </td><td> <input type="text" name="lists['+i+'][amount]" value="" placeholder="14,600/-"> </td><td class="offer-ic" style="border: none !important;"> <br><i class="fa fa-minus remove"></td></tr>').appendTo($('#rowAppend'));
	        i++;
	    });

			$(document).on('click', '.remove', function(e) {
				e.preventDefault();
				var trIndex = $(this).closest("tr").index();
	           	// alert(trIndex);
	           	if(trIndex<=0) {
	           		swal("Sorry!! Can't remove first row!");
	           	} 
	           	else {
	           		$(this).closest("tr").remove();
	           	}
	           });
		}); 

	</script>

	@endsection