<html>
<head>
</head>
<style>
.offer-firmer1 textarea {
    border: none;
    text-align: left;
}

.offer-firmer1 textarea:focus {
    outline: none;
}

.offer-firmer textarea {
    border: none;
    text-align: center;
}

.offer-firmer textarea:focus {
    outline: none;
}

.offer-firmer td {
    border: 1px solid #000;
    color: #222;
    text-align: center;
    padding: 10px;
}

.offer-firmer .table-bordered>tbody>tr>th {
    border: 1px solid #000;
    color: #222;
    font-weight: 700;
    text-align: center;
    padding: 10px;
}

.offer-firmer input {
    border: none;
    color: #222;
    height: 30px;
}

.offer-firmer input:focus {
    outline: none;
}

.btn-sub-offer {
    padding: 10px 20px;
    background: #e75e0d;
    color: #fff;
    text-align: right;
    border: none;
    margin: 20px 0;
    float: right;
    cursor: pointer;
}

.btn-sub-offer:hover {
    background: #222;
}
.offer2-img {
    width: 15%;
    text-align: center;
    margin: 0 auto;
}
.offer2-img img{
    width: 100%;

}
.offer-fir{
    text-align: center;
    margin: 4rem 0;
}

</style>

<body onload="printPage()">

    <div class="offer-firmer1">
        <div class="container">
          <div class="offer2-img">
           <img src="{{asset('frontend/images/logonew.jpg')}}" class="img-reponsive">
       </div>
       <p align=center style="margin-bottom: 0.14in"><font face="Times New Roman, serif"><font size=4><u><b>OFFER</b></u></font></font></p>
       <p align=right style="margin-bottom: 0.14in"><font face="Times New Roman, serif"><font size=4><b>Date
        : {{date("d-m-Y", strtotime($manual_billing->created_at))}}</b></font></font></p>


        <p> <b>To,</b></p>
        <p>{!! $manual_billing->address !!}</p>
        <div class="row offer-firmer">
            <p><b>Ref No</b><span class="ref-nam" style="padding-left: 56px;
">: </span> {{$manual_billing->ref_no}} </p>
            <p><b> Kind Attention </b>: <span class="kind-att" style="padding-left: ;"></span>{{$manual_billing->kind_attention}}</p>
            <p><b> Subject </b> <span class="kind-subj" style="padding-left: 52px;">:</span> {{$manual_billing->subject}}</p>
            <p> Dear sir,</p>
                <p>As per your enquiry we are Submitting our offer</p>
            <div class="table-responsive table-border">
                <table class="table table-bordered" style="width:  100%; border-collapse: collapse;">
                    <tr>
                        <th> S.No</th>
                        <th> Description</th>
                        <th> Qty </th>
                        <th> Unit Price</th>
                        <th> Amount</th>
                    </tr>
                    @php ($i = 1) 
                    @foreach ($manual_billing_products as $manual_billing_product) 
                    <tr>
                        <td> {{ $i }}</td>
                        <td>
                           {{ $manual_billing_product->description }}
                        </td>
                        <td> {{ $manual_billing_product->qty }}</td>
                        <td> {{ $manual_billing_product->price }}/- </td>
                        <td> {{ $manual_billing_product->amount }}/-</td>
                    </tr>
                    @php ($i++) 
                    @endforeach
                    <tr>
                        <td> </td>
                        <td> GST {{$manual_billing->gst_percentage}}%</td>
                        <td> </td>
                        <td> </td>
                        <td> {{$manual_billing->gst_amount}}/-</td>
                    </tr>
                    
                    <tr>
                        <td> </td>
                        <td> Transport Loading &amp;<br> Unloading charges</td>
                        <td> </td>
                        <td> </td>
                        <td> {{$manual_billing->shipping_amount}}/-</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td> Net Total</td>
                        <td> </td>
                        <td> </td>
                        
                        <td> {{$manual_billing->net_total}}/- </td>
                    </tr>
                </table>
            </div> 



            <p STYLE="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3><U><B>Terms
            and Conditions :</B></U></font></font></p>
            <ul>
               <li><p style="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3>Payment
               Terms : 100% against Proforma Invoice.</font></font></p>
               
               <li><p style="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3>Delivery:
               Seven to Fifteen days from the date of your order.</font></font></p>
               <li><p style="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3>Tax
                   Clause: Any change in Tax during supply of Material to be Borne by
               the Client.</font></font></p>
              
               <li><p style="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3>Validity
               : 15 Days</font></font></p>
               <li><p style="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3>Kindly
                   call us, if you have any clarification on this and we are waiting
               for valuable order for the same.</font></font></p>
           </ul>            
           <p STYLE="margin-bottom: 0in"><br>
           </p>
          
           <p STYLE="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3><B>For
           Firmer.</B></font></font></p>
           <p STYLE="margin-bottom: 0in"><br>
           </p>

           

           <div class="offer-fir">
            <p style="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3> Q-Block, No.93, 4th Main Road, Anna Nagar, Chennai - 600 040.</font></font></p>
            <p style="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3> +91 99400-87788 | firmeronlineshop@gmail.com</font></font></p>
            <p style="margin-bottom: 0in"><font face="Times New Roman, serif"><font size=3>WWW.firmer.in </font></font></p>
        </div>                                                        



    </div>        
</div>
</div>

</body>
 <script>
        function printPage() {
            window.print();
        }
    </script>
</html>