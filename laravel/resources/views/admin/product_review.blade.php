@extends('admin.layouts.app') 
@section('styles')
<style type="text/css">
.table-header-colour{
	background-color: #262626; 
	color: #fff;
}
</style>
@endsection
@section('content')
<div class="row heading-bg">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-dark">Product Review</h5>
	</div>  
	<!-- Breadcrumb -->
	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
		<ol class="breadcrumb">
			<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
			<li class="active"><span>Product Review</span></li>
		</ol>
	</div>
	<!-- /Breadcrumb -->          
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<!-- <h6 class="panel-title txt-dark">Gallery Image</h6> -->
				</div>

			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div id="validation">
						@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
					</div>					  
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="datable_1" class="table table-bordered display">
								<thead>
									<tr>
										<th style="width: 8%">S.No</th>    
										<th>Product Name</th>
										<th>Customer Name</th>
										<th>Name</th>
										<th>Review Summary</th>
										<th>Review</th>
										<th>Value</th>
										<th>Quality</th>
										<th>Price</th>
										<th style="width: 10%">Status</th>
										<th style="width: 8%">Action</th>

									</tr>
								</thead>
								<tbody>
									@php ($i = 1) 
									@foreach ($product_reviews as$product_review)                        
									<tr>
										<td>{{ $i }}</td>
										<td>{{ $product_review->product->name }}</td>
										<td>{{ $product_review->customer->first_name }}</td>
										<td>{{ $product_review->name }}</td>
										<td>{{ $product_review->review_summary }}</td>
										<td>{{ $product_review->review }}</td>
										<td>{{ $product_review->value }}</td>
										<td>{{ $product_review->quality }}</td>
										<td>{{ $product_review->price }}</td>
										<td>
											@if ($product_review->status)
											<a href="{{ url('admin/product-review',[$product_review->id]) }}/status/0" class="btn btn-success btn-xs">Active</a>
											@else    
											<a href="{{ url('admin/product-review',[$product_review->id]) }}/status/1" class="btn btn-danger btn-xs">Deactivate</a>
											@endif 
										</td>

										<td>
											<a href="{{ url('admin/product-review',[$product_review->id]) }}/delete" class="btn btn-danger btn-xs delete-click"><span class="fa fa-trash"></span></a> 
										</td>

									</tr>
									@php ($i++) 
									@endforeach
								</tbady>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">      
	$(".delete-click").click(function (event) {
		event.preventDefault();
		var url = $(this).attr("href");
		swal({
			title: 'Are you sure?',
			text: "You won't be able to recover once deleted!",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then(function () {
			$.ajax({
				type: "get",
				url: url,
				success: function(response){
					if(response.status == "success")
						window.location.href="{!! url('/admin/product-review') !!}";
				},
			});
		})
// alert(url);
});
</script>
@endsection
