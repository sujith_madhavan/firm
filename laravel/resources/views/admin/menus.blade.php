@extends('admin.layouts.app') 
@section('styles')
	<style type="text/css">
		.table-header-colour{
			background-color: #262626; 
			color: #fff;
		}
	</style>
@endsection
@section('content')
  	<div class="row heading-bg">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	  		<h5 class="txt-dark">Menus</h5>
		</div>  
		<!-- Breadcrumb -->
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
				<li class="active"><span>Menus</span></li>
			</ol>
		</div>
		<!-- /Breadcrumb -->          
  	</div>
  	<div class="row">
	  	<div class="col-sm-12">
		  	<div class="panel panel-default card-view">
			  	<div class="panel-heading">
				  	<div class="pull-left">
					  <!-- <h6 class="panel-title txt-dark">Gallery Image</h6> -->
				  	</div>
				  	@if ($allowed_operations['add'])
					  	<div class="pull-right">
						 	<a href="" class="btn btn-danger btn-circle  btn-sm" title="Add New" data-toggle="modal" data-target="#myModal" ><i class="fa fa-plus"></i></a>
					  	</div>
					  	<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
						  	<div class="modal-dialog">
							  	<div class="modal-content">
								  	<div class="modal-header">
									  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									  	<h5 class="modal-title" id="myModalLabel">Add Menu</h5>
								  	</div>
								  	<form id="add_menu" action="{{ url('admin/menus/add') }}" method="post" enctype="multipart/form-data">
									  	{{csrf_field()}}
									  	<div class="modal-body">
										  	<div class="form-group">
											  	<label for="" class="control-label mb-10">Name</label>
											  	<input name="name" type="text" class="form-control" required>
										  	</div> 
										  	<div class="form-group">
											  	<label for="" class="control-label mb-10">Position</label>
											  	<input name="position" type="number" class="form-control" required>
										  	</div>
										  	<div class="form-group">
											  	<label for="" class="control-label mb-10">Category</label>
											  	<select class="form-control" name="category" id="category" required>
											  		<option selected value="0">Select Category</option>
											  		@foreach ($categories as $category) 
											  			<option value="{{ $category->id }}">{{ ucwords($category->name) }}</option>
											  		@endforeach
											  	</select>
										  	</div>
									  	</div>
									  	<div class="modal-footer">
										  	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  	<button type="submit" class="btn btn-info">ADD</button>
									  	</div>
								  	</form>
							  	</div>
						  	</div>
					  	</div>
					@endif  	
			  	</div>
			  	<div class="panel-wrapper collapse in">
				  	<div class="panel-body">
						<div id="validation">
					  		@if ($errors->any())
						  		<div class="alert alert-danger">
							  		<ul>
								  		@foreach ($errors->all() as $error)
									  		<li>{{ $error }}</li>
								  		@endforeach
							  		</ul>
						  		</div>
					  		@endif
						</div>					  
						<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>                     
											<th>Name</th>
											<th>Category</th>
											<th>Position</th>
											<th>Modified By</th>
											<th>Modified At</th>
											@if ($allowed_operations['status'])
												<th style="width: 10%">Status</th>
											@endif
											@if ($allowed_operations['remove'] || $allowed_operations['edit'])	
												<th style="width: 12%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($menus as $menu)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td>{{ $menu->name }}</td>
						  						<td>{{ $menu->category->name }}</td>
						  						<td>{{ $menu->position }}</td>
						  						<td>{{ $menu->modifiedBy->name }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($menu->updated_at)) }}</td>
						  						@if ($allowed_operations['status'])
							  						<td>
														@if ($menu->status)
														  	<a href="{{ url('admin/menus',[$menu->id]) }}/status/0" class="btn btn-success btn-xs">Active</a>
														@else    
														  	<a href="{{ url('admin/menus',[$menu->id]) }}/status/1" class="btn btn-danger btn-xs">Deactivate</a>
														@endif 
							  						</td>
							  					@endif
							  					@if ($allowed_operations['remove'] || $allowed_operations['edit']) 	       
								   					<td>
								   						@if ($allowed_operations['edit'])     
										 					<a href="" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal{{ $menu->id }}"><span class="fa fa-edit" ></span></a>
										 					
											 				<div id="myModal{{ $menu->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
									   							<div class="modal-dialog">
										 							<div class="modal-content">
										   								<div class="modal-header">
											 								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											 								<h5 class="modal-title" id="myModalLabel">Edit Menu</h5>
										   								</div>
										   								<form action="{{ url('admin/menus/') }}/{{ $menu->id }}/edit" method="post" enctype="multipart/form-data">
											 								{{csrf_field()}}
											 								<div class="modal-body">
																			   <div class="form-group">
																				 <label for="" class="control-label mb-10">Name</label>
																				 <input name="name" type="text" class="form-control" required value="{{ $menu->name }}">
																			   </div>
		                     													<div class="form-group">
																				 <label for="" class="control-label mb-10">Position</label>
																				 <input name="position" type="number" class="form-control" required value="{{ $menu->position }}">
																			   </div>
																			   <div class="form-group">
																				 <label for="" class="control-label mb-10">Category</label>
																				 <select class="form-control" name="category_id" id="category_id" required>
																				 	@foreach ($categories as $category)
																				 		@if($category->id == $menu->category_id) 
																				 			<option selected style="font-weight:bold;" value="{{ $category->id }}">{{ ucwords($category->name) }}</option>
																				 		@else
																				 			<option value="{{ $category->id }}">{{ ucwords($category->name) }}</option>	
																				 		@endif	
																				 	@endforeach
																				 </select>
																			   </div>
											 								</div>
											 								<div class="modal-footer">
																			   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																			   <button type="submit" class="btn btn-info">Update</button>
											 								</div>
										   								</form>
										 							</div>
									   							</div>
									 						</div>
									 					@endif
									 					@if ($allowed_operations['remove'])	
								 							<a href="{{ url('admin/menus',[$menu->id]) }}/delete" class="btn btn-danger btn-xs delete-click"><span class="fa fa-trash"></span></a> 
								 						@endif	
								   					</td>
							   					@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
				  	</div>
			  	</div>
		  	</div>
	  	</div>
  	</div>
@endsection

@section('scripts')

</script>
  <script type="text/javascript">      
	  $(".delete-click").click(function (event) {
		  event.preventDefault();
		  var url = $(this).attr("href");
		  swal({
			  title: 'Are you sure?',
			  text: "You won't be able to recover once deleted!",
			  type: 'warning',
			  showCancelButton: false,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
		  }).then(function () {
			  $.ajax({
				  type: "get",
				  url: url,
				  success: function(response){
					  if(response.status == "success")
						  window.location.href="{!! url('/admin/menus') !!}";
				  },
			  });
		  })
		  // alert(url);
	  });
  </script>
  <script type="text/javascript">
		$(document).ready(function() {
            $("#add_menu").submit(function(e){           	
            	var category = $("#category").val();
                if (category <= 0) {
                	swal("Select Category.");
                	e.preventDefault();
                }              
            });
        });
	</script>
@endsection
