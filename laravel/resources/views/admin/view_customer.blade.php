@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.image-circle{
			border-radius: 50%;
		}

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

	</style>
	
@endsection
@section('content')
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <!-- <h5 class="txt-dark">Edit User</h5> -->
        </div>  
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
                <li class="active"><span>View-Customer</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->                    
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">View Customer</h6>
                    </div>
                    <div class="pull-right">
                       <a href="{{ url()->previous() }}" class="btn btn-danger" title="Add New" >Back</i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Name : </label>
                                    <label>{{ $customer->first_name }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Last Name : </label>
                                    <label>{{  $customer->last_name }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Email : </label>
                                    <label>{{ $customer->email }}</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Mobile : </label>
                                    <label>{{ $customer->mobile }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Created At : </label>
                                    <label>{{  date('d-m-Y h:i:s A', strtotime($customer->created_at)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Updated At : </label>
                                    <label>{{ date('d-m-Y h:i:s A', strtotime($customer->updated_at)) }}</label>
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">

                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Addresses</h6>
                    </div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body"> 
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_112" class="table table-bordered display">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Name</th>
                                            <th>Company Name</th>
                                            <th>Mobile</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Pincode</th>
                                            <th>Country</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php ($i = 1) 
                                        @foreach ($customer->addresses as $address)                        
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $address->first_name }} {{ $address->last_name }}</td>
                                                <td>{{ $address->company_name }}</td>
                                                <td>{{ $address->mobile }}</td>
                                                <td>{{ $address->address }},{{ $address->address1 }},{{ $address->address2 }}</td>
                                                <td>{{ $address->city }}</td>
                                                <td>{{ $address->state }}</td>
                                                <td>{{ $address->pincode }}</td>
                                                <td>{{ $address->country }}</td>

                                            </tr>
                                            @php ($i++) 
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">

                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Orders</h6>
                    </div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body"> 
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="datable_1" class="table table-bordered display">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Transaction Id</th>
                                            <th>Transaction Status</th>
                                            <th>Order Status</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php ($i = 1) 
                                        @foreach ($customer->orders as $order)                        
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $order->transaction_id }}</td>
                                                <td>{{ $order->transaction_status }}</td>
                                                <td>{{ $order->order_status }}</td>
                                                <td>{{ $order->amount }}</td> 
                                            </tr>
                                            @php ($i++) 
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
@endsection
