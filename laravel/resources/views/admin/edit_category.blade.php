@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li><a href="{{ url('admin/categories') }}">Categories</a></li>
	            <li class="active"><span>Edit-Category</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Update Category</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                    <div class="form-wrap">
                      		<form id="edit_category" method="post" action="{{ url('admin/categories',[$category->id]) }}/edit" enctype="multipart/form-data">
                    		  	{{ csrf_field() }}
                    			<div class="form-wrap col-md-6 col-lg-6">
                    				<div class="form-group">
										<label for="" class="control-label mb-10">Current Parent Category : </label>
										@if(empty($category->parent_id))
											<label for="" class="control-label mb-10">No Parent Category</label>
										@else
											<label for="" class="control-label mb-10">{{ ucwords($category->parentCategory->name) }}</label>
										@endif		
									</div>
									<div class="form-group">
										<label class="control-label mb-10">To Change Category * </label>
										<select class="selectpicker addgroupimage" data-style="form-control btn-default btn-outline" name="category" id="category" required>
											<option selected value="0">No Category</option>
											@foreach($active_categories as $active_category)
												<option value="{{ $active_category->id }}">{{ ucwords($active_category->name) }}</option>
												@if(!empty($active_category->childCategories))
													<optgroup label="{{ ucwords($active_category->name) }}">
														@foreach($active_category->childCategories as $child_category)
															<option value="{{ $child_category->id }}">{{ ucwords($child_category->name) }}</option>
														@endforeach
													</optgroup>	
												@endif	
											@endforeach
										</select>
									</div>
									<div class="form-group {{$errors->has('group_id')?'has-error':''}}">
		                                <label class="control-label mb-10 text-left">Select Group *</label><br> 
		                                <select class="form-control" required name="group_id" id="group_id" >
					                    	<option value="0">Select Group</option>
			                                @foreach ($groups as $group)
			                                	@if($group->id == $category->group_id) 
		                                			<option selected style="font-weight:bold;" value="{{$group->id}}">{{ ucwords($group->name) }}</option>
		                                		@else
		                                			<option value="{{$group->id}}">{{ ucwords($group->name) }}</option>
		                                		@endif		
			                            	@endforeach
		                            	</select>
		                            </div>
                    				<div class="form-group {{$errors->has('name')?'has-error':''}}">
                                        <label class="control-label mb-10 text-left">Name *</label>
                                        <input type="text" name="name" class="form-control" required value="{{ $category->name }}">
                                        @if($errors->has('name'))
                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                    @if(empty($category->parent_id))
                                    <div class="form-group displayimage" >
		                                <label class="control-label mb-10 text-left"></label>
		                                <img class="image-circle" src="{{ $category->image_path }}" width="100" height="100" />
		                            </div>

                                    <div class="form-group {{$errors->has('image')?'has-error':''}} displayimage">
                                        <label class="control-label mb-10 text-left">Image *</label>
                                        <input type="file" name="image" class="form-control">
                                        @if($errors->has('image'))
                                            <span class="help-block">{{ $errors->first('image') }}</span>
                                        @endif
                                    </div>
                                    @endif
                                    <div class="form-group {{$errors->has('title')?'has-error':''}}">
                                        <label class="control-label mb-10 text-left">Banner Title *</label>
                                        <input type="text" name="title" class="form-control" required value="{{ $category->banner_title }}">
                                        @if($errors->has('title'))
                                            <span class="help-block">{{ $errors->first('title') }}</span>
                                        @endif
                                    </div>
                    				<div style="text-align: right;">
                    					<button class="btn btn-danger"  role="button" type="submit">Submit</button>
                    				</div>  
                      			</div>
                      		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection
@section('scripts')	
	<script type="text/javascript">
	 	$(document).ready(function() {
            $("#edit_category").submit(function(e){         	

            	var group_id = $('#group_id').val();           	

                if (group_id <= 0) {
                	swal("Select Group.");
                	e.preventDefault();
                }
           
            });
        });
	</script>
	<script type="text/javascript">
$('.addgroupimage').on('change', function()
    {
        //alert(this.value);
        var type = this.value; 
        if(type=='0')
        {
            $(".displayimage").show();
        }
        else
        {
             $(".displayimage").hide();
        }
    });
</script>
@endsection