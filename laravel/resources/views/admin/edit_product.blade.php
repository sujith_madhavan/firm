@extends('admin.layouts.app') 
@section('styles')	
<link href="{{asset('backend/vendors/bower_components/jquery-wizard.js/css/wizard.css')}}" rel="stylesheet" type="text/css"/>

<!-- jquery-steps css -->
<link rel="stylesheet" href="{{asset('backend/vendors/bower_components/jquery.steps/demo/css/jquery.steps.css')}}">

<style type="text/css">
.has-error {
	color: #ef0a15;
}
.allsplits{
	background: #fff;
	/* padding: 0; */
	overflow-y: scroll;
	height: 300px;
}
#attribute{
	width: 90%;
	margin: 0 0 0 10px;
}
#attribute .checkbox{

	display: inline-block !important;
}
</style>

@endsection
@section('content')	
<div class="row heading-bg">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<!-- <h5 class="txt-dark">Edit User</h5> -->
	</div>  
	<!-- Breadcrumb -->
	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
		<ol class="breadcrumb">
			<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
			<li><a href="{{ url('admin/products') }}">Product List</a></li>
			<li class="active"><span>Edit-Product</span></li>
		</ol>
	</div>
	<!-- /Breadcrumb -->                    
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Update Product</h6>
				</div>
				<div class="pull-right">
					<a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div id="validation">
						@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
					</div>
					<div class="form-wrap">
						<form id="example-advanced-form" method="post" action="{{ url('admin/products',[$product->id]) }}/edit" enctype="multipart/form-data">
							{{ csrf_field() }}
							<input type="hidden" id="old_product_name" name="product_name" value="{{ $product->name }}">
							<input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}">
							<input type="hidden" id="old_product_code" name="product_code" value="{{ $product->code }}">
							<h3><span class="number"><i class="icon-user-following txt-black"></i></span><span class="head-font capitalize-font">Basic</span></h3>
							<fieldset>
								<div class="row">
									<div class="form-wrap col-md-6 col-lg-6">
										<div class="form-group {{$errors->has('name')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Name *</label>
											<input type="text" name="name" id="product_name" class="form-control" value="{{ $product->name }}" required>
											@if($errors->has('name'))
											<span class="help-block">{{ $errors->first('name') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('code')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Code *</label>
											<input type="text" id="product_code" name="code" class="form-control" value="{{ $product->code }}" required>
											@if($errors->has('code'))
											<span class="help-block">{{ $errors->first('code') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('hsc_code')?'has-error':''}}">
											<label class="control-label mb-10 text-left">HSN Code *</label>
											<input type="text" name="hsc_code" class="form-control" value="{{ $product->hsc_code }}">
											@if($errors->has('hsc_code'))
											<span class="help-block">{{ $errors->first('hsc_code') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('old_price')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Basic Price *</label>
											<input type="text" id="" class="form-control myContent"  name="old_price" value=" {{ $product->price }}" required>
											@if($errors->has('old_price'))
											<span class="help-block">{{ $errors->first('old_price') }}</span>
											@endif
										</div>

										<div class="form-group {{$errors->has('new_price')?'has-error':''}} " >
											<label class="control-label mb-10 text-left">Special MRP Price</label><br> 
											<input type="text" class="form-control basic_price mymrp" id=""  name="new_price" value="{{ $product->slashed_price }}"  >

											@if($errors->has('new_price'))
											<span class="help-block">{{ $errors->first('new_price') }}</span>
											@endif
										</div>
										@if(!empty($product->slashed_price))
										<div class="input-group dates {{$errors->has('start_date')?'has-error':''}} ">
											<input type="text" class="form-control datepicker-autoclose" name="start_date" placeholder="Start Date" value="{{ date('d-m-Y', strtotime($product->start_date)) }}"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
											<input type="text" class="form-control datepicker-autoclose" name="end_date"  placeholder="End Date" value="{{ date('d-m-Y', strtotime($product->end_date)) }}"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
											@if($errors->has('start_date'))
											<span class="help-block">{{ $errors->first('start_date') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('discount')?'has-error':''}} special_mrp" >
											<label class="control-label mb-10 text-left ">Discount</label><br> 
											<input type="text" class="form-control " id="discounts" name="discount" value="{{$product->discount }}" >
											@if($errors->has('discount'))
											<span class="help-block">{{ $errors->first('discount') }}</span>
											@endif
										</div>
										@else
										
											<div class="input-group dates {{$errors->has('start_date')?'has-error':''}} " style="display: none;">
												<input type="text" class="form-control datepicker-autoclose" name="start_date" placeholder="Start Date" value="{{ date('d-m-Y', strtotime($product->start_date)) }}"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
												<input type="text" class="form-control datepicker-autoclose" name="end_date"  placeholder="End Date" value="{{ date('d-m-Y', strtotime($product->end_date)) }}"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
												@if($errors->has('start_date'))
												<span class="help-block">{{ $errors->first('start_date') }}</span>
												@endif
											</div>
										
											<div class="form-group {{$errors->has('discount')?'has-error':''}} special_mrp" style="display: none;">
												<label class="control-label mb-10 text-left ">Discount</label><br> 
												<input type="text" class="form-control " id="discounts" name="discount" value="{{$product->discount }}" >
												@if($errors->has('discount'))
												<span class="help-block">{{ $errors->first('discount') }}</span>
												@endif
											</div>
										
										@endif

									</div>
									<div class="form-wrap col-md-6 col-lg-6">
										<div class="form-group {{$errors->has('price_for')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Price For *</label><br> 
											<select name="price_for"  class="form-control" required>
												<option value="">Select Price For</option>
												<option value="Kg" {{ $product->price_for == 'Kg' ? 'selected' : '' }}>Kg</option>
												<option value="Piece" {{ $product->price_for == 'Piece' ? 'selected' : '' }}>Piece</option>
												<option value="Box" {{ $product->price_for == 'Box' ? 'selected' : '' }}>Box</option>
												<option value="Tin" {{ $product->price_for == 'Tin' ? 'selected' : '' }}>Tin</option>
												<option value="Sq.Ft" {{ $product->price_for == 'Sq.Ft' ? 'selected' : '' }}>Sq.Ft</option>
												<option value="Bag" {{ $product->price_for == 'Bag' ? 'selected' : '' }}>Bag</option>
												<option value="CFT" {{ $product->price_for == 'CFT' ? 'selected' : '' }}>CFT</option>
												<option value="Litre" {{ $product->price_for == 'Litre' ? 'selected' : '' }}>Litre</option>
												<option value="Metre" {{ $product->price_for == 'Metre' ? 'selected' : '' }}>Metre</option>
												<option value="Sheet" {{ $product->price_for == 'Sheet' ? 'selected' : '' }}>Sheet</option>
												
											</select>
											@if($errors->has('price_for'))
											<span class="help-block">{{ $errors->first('price_for') }}</span>
											@endif
										</div>	
										<div class="form-group {{$errors->has('box_quantity')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Box Quantity</label><br> 
											<input type="number" class="form-control"  name="box_quantity" value="{{ $product->box_quantity }}" >
											@if($errors->has('box_quantity'))
											<span class="help-block">{{ $errors->first('box_quantity') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('stock')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Stock *</label><br> 
											<input type="number" class="form-control"  name="stock" value="{{ $product->stock }}" required>
											@if($errors->has('stock'))
											<span class="help-block">{{ $errors->first('stock') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('gst')?'has-error':''}}">
											<label class="control-label mb-10 text-left">GST *</label><br> 
											<input type="number" class="form-control"  name="gst" value="{{ $product->gst }}" required>
											@if($errors->has('gst'))
											<span class="help-block">{{ $errors->first('gst') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('minimum_quantity')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Minimum Quantity *</label><br> 
											<input type="number" class="form-control"  name="minimum_quantity" value="{{ $product->minimum_quantity }}" required>
											@if($errors->has('minimum_quantity'))
											<span class="help-block">{{ $errors->first('minimum_quantity') }}</span>
											@endif
										</div>
										<label class="control-label mb-10 text-left">Product Display In : </label><br>
										<div class="checkbox checkbox-danger checkbox-circle" style="display: inline;">
											<div class="col-md-3 col-lg-3">
												@if($product->featured)
												<input type="checkbox" checked name="featured" value="1">
												<label>Featured</label>
												@else
												<input type="checkbox" name="featured" value="1">
												<label>Featured</label>
												@endif	
											</div>
											<div class="col-md-3 col-lg-3">
												@if($product->new_product)
												<input type="checkbox" checked  name="new_product" value="1">
												<label>New</label>
												@else
												<input type="checkbox" name="new_product" value="1">
												<label>New</label>
												@endif
											</div>
											<div class="col-md-3 col-lg-3">
												@if($product->top_selling)	
												<input type="checkbox" name="top_selling" checked value="1">
												<label>Top Selling</label>
												@else
												<input type="checkbox" name="top_selling" value="1">
												<label>Top Selling</label>
												@endif	
											</div>	
										</div> 
									</div>	
								</div>
							</fieldset>

							<h3><span class="number"><i class="icon-bag txt-black"></i></span><span class="head-font capitalize-font">Attribute</span></h3>
							<fieldset>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-wrap">
											<div class="form-wrap col-md-6 col-lg-6">
												<div class="form-group">
													<label for="" class="control-label mb-10">Brand</label>
													<select class="form-control" name="brand_id" id="brand_id">
														<option value="00">Select Brand</option>
														@foreach ($brand_logos as $brand_logo)
														@if($brand_logo->id == $product->brand_logo_id) 
														<option selected style="font-weight:bold;" value="{{ $brand_logo->id }}">{{ ucwords($brand_logo->name) }}</option>
														@else
														<option value="{{ $brand_logo->id }}">{{ ucwords($brand_logo->name) }}</option>	
														@endif	
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label for="" class="control-label mb-10">Product Group</label>
													<select class="form-control" name="product_group" id="product_group">
														<option value="">Select Product Group</option>
														@foreach ($product_groups as $product_group)
														@if($product_group->id == $product->product_group_id) 
														<option selected style="font-weight:bold;" value="{{ $product_group->id }}">{{ ucwords($product_group->name) }}</option>
														@else
														<option value="{{ $product_group->id }}">{{ ucwords($product_group->name) }}</option>	
														@endif	
														@endforeach
													</select>
												</div>
												<div class="form-group {{$errors->has('categories')?'has-error':''}}">
													<label class="control-label mb-10 text-left">Select  Categories * </label>
													<br>
													<select class="form-control multiple-checkboxes" id="categories" name=categories[] multiple="multiple" onchange="get_attributes();">


														@foreach ($categories as $category) 
														@if(in_array($category->id, $selected_categories))
														<option value="{{ $category->id }}" selected>{{ucwords($category->name)}}</option>
														@else
														<option value="{{ $category->id }}">{{ucwords($category->name)}}</option>
														@endif



														@foreach($category->childCategories as $child_category)
														@if(in_array($child_category->id, $selected_categories))

														<option value="{{ $child_category->id }}" selected>&nbsp;&nbsp;&nbsp;- -{{ ucwords($child_category->name) }}</option>
														@else
														<option value="{{ $child_category->id }}">&nbsp;&nbsp;&nbsp;- -{{ ucwords($child_category->name) }}</option>
														@endif


														@foreach($child_category->childCategories as $child_category)
														@if(in_array($child_category->id, $selected_categories))

														<option value="{{ $child_category->id }}" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- - -{{ ucwords($child_category->name) }}</option>
														@else
														<option value="{{ $child_category->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- - -{{ ucwords($child_category->name) }}</option>
														@endif


														@endforeach
														@endforeach	

														@endforeach
													</select>
												</div>
												<div class="form-group {{$errors->has('related_categories')?'has-error':''}}">
													<label class="control-label mb-10 text-left">Select Related Categories * </label>
													<br>
													<select class="form-control multiple-checkboxes" id="related_categories" name=related_categories[] multiple="multiple">
														@foreach($categories as $category)
														@if(in_array($category->id, $related_categories))
														<option value="{{ $category->id }}" selected>{{ucwords($category->name)}}</option>
														@else
														<option value="{{ $category->id }}">{{ucwords($category->name)}}</option>
														@endif
														@if(!empty($category->childCategories))
														<optgroup label="{{ ucwords($category->name) }}">
															@foreach($category->childCategories as $child_category)
															@if(in_array($child_category->id, $related_categories))
															<option value="{{ $child_category->id }}" selected>{{ucwords($child_category->name)}}</option>
															@else
															<option value="{{ $child_category->id }}">{{ucwords($child_category->name)}}</option>
															@endif
															@if(!empty($child_category->childCategories))
															<optgroup label="{{ ucwords($category->name) }}">
																@foreach($child_category->childCategories as $child_category2)
																@if(in_array($child_category2->id, $related_categories))
																<option value="{{ $child_category2->id }}" selected>{{ucwords($child_category2->name)}}</option>
																@else
																<option value="{{ $child_category2->id }}">{{ucwords($child_category2->name)}}</option>
																@endif
																@endforeach
															</optgroup>	
															@endif
															@endforeach
														</optgroup>	
														@endif
														@endforeach
													</select>
												</div>
											</div>
											<div class="form-wrap col-md-6 col-lg-6 allsplits">     
												<div class="form-group">
													<label class="control-label mb-10 text-left">Attributes *</label><br> 
												</div>
												<div class="form-group" id="attribute">
													@php($attribute_ids = array())
													@foreach ($product->productAttributes as $product_attribute)
													@if(!in_array($product_attribute->attribute_id, $attribute_ids))
													@php($attribute_ids[] = $product_attribute->attribute_id)
													<div class='form-group'>
														<label class='control-label mb-10 text-left'>{{ $product_attribute->attribute->name }}</label><br>
														@foreach ($product_attribute->attribute->AttributeValues as $attribute_value)
														@php ($i = 1)
														@foreach ($product_attribute->productAttributeValues as $product_attribute_value)
														@if($attribute_value->id == $product_attribute_value->attribute_value_id)
														<div class="checkbox checkbox-danger checkbox-circle" style="display: inline;">
															<input type="checkbox" checked name="{{ $product_attribute->attribute->name }}[]" value="{{ $attribute_value->id }}">
															<label for="checkbox-12">{{ $attribute_value->value }}</label>
														</div>
														&nbsp;		  
														@php($i = 2)
														@break	
														@endif	
														@endforeach	
														@if($i == 1)
														<div class="checkbox checkbox-danger checkbox-circle" style="display: inline;">
															<input type="checkbox" name="{{ $product_attribute->attribute->name }}[]" value="{{ $attribute_value->id }}">
															<label for="checkbox-12">{{ $attribute_value->value }}</label>
														</div>
														&nbsp;		
														@endif
														@endforeach
													</div>
													@endif	
													@endforeach
												</div> 
											</div>
										</div>
									</div>
								</div>
							</fieldset>

							<h3><span class="number"><i class="icon-credit-card txt-black"></i></span><span class="head-font capitalize-font">Images</span></h3>
							<fieldset>
								<!--CREDIT CART PAYMENT-->
								<div class="row">
									<div class="col-sm-12">
										<div class="col-md-12">
											<table  class="table table-hover small-text" id="tb">
												<tr>
													<th style="font-weight: bold;">
													Choose Image / Video</th>
													<th style="width: 5%"></th>
													<th><a href="javascript:void(0);" style="font-size:18px; text-align: right;" id="addMore" title="Add More Row"><span class="glyphicon glyphicon-plus"></span></a></th>
												</tr>
												<tbody id="rowAppend">
													@php ($i = 1) 
													@foreach($product->productGalleries as $product_gallery)	
													<tr>
														<td>
															<img class="image-circle " src="{{ $product_gallery->thumb_image_path }}" width="50" height="50" />
															<input type="hidden" name="gallery{{ $i }}" value="{{ $product_gallery->id }}">
															<input type="file" id="image{{ $i }}" name="image{{ $i }}" class="form-control">
														</td>
														<td>
															( OR )
														</td>
														<td>
															<input type="text" id="video{{ $i }}" name="video{{ $i }}" class="form-control" value="{{ $product_gallery->video }}" placeholder="Youtube URL">
														</td>
														<td><a href="{{ url('admin/product-image',[$product_gallery->id]) }}/delete"  title="Remove item"  class="btn-remove btn-remove2 delete-click"><span class="fa fa-trash"></span></a></td>
													</tr>
													@php ($i++) 
													@endforeach
												</tbody>
											</table>
										</div>											
									</div>
								</div>
								<!--CREDIT CART PAYMENT END-->
							</fieldset>

							<h3><span class="number"><i class="icon-basket-loaded txt-black"></i></span><span class="head-font capitalize-font">Description</span></h3>
							<fieldset>
								<div class="form-wrap col-md-12 col-lg-12">
									<div class="form-group {{$errors->has('description')?'has-error':''}}">
										<label class="control-label mb-10 text-left">Description *</label>

										<textarea class="textarea_editor form-control" name="description" required>{!! $product->description !!}</textarea>
										@if($errors->has('description'))
										<span class="help-block">{{ $errors->first('description') }}</span>
										@endif
									</div>
									<div class="actions clearfix" style="text-align: right;">
										<button class="btn btn-danger"  role="button" type="submit">Submit</button>
									</div>	
								</div>
							</fieldset>
								<!-- <div class="actions clearfix" style="text-align: right;">
									<button class="btn btn-danger"  role="button" type="submit">Submit</button>
								</div> -->
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection
	@section('scripts') 

	<!-- Form Wizard JavaScript -->
	<script src="{{ asset('backend/bower_components/jquery.steps/build/jquery.steps.min.js') }}"></script>
	<script src="{{ asset('backend/dist/js/jquery.validate.min.js')}}"></script>
	
	<!-- Form Wizard Data JavaScript -->
	<script src="{{ asset('backend/dist/js/form-wizard-data.js') }}"></script>

	<!-- wysuhtml5 Plugin JavaScript -->
	<script src="{{ asset('backend/bower_components/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>
	
	<script src="{{ asset('backend/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>
	
	<!-- Bootstrap Wysuhtml5 Init JavaScript -->
	<script src="{{ asset('backend/dist/js/bootstrap-wysuhtml5-data.js') }}"></script>

	<script>
		$(function(){
			var i = {{ $i }};
			$('#addMore').on('click', function() {

				if(i <= 4){

					$('<tr><td><input type="hidden" value="0" name="gallery'+i+'"><input type="file" name="image'+i+'" class="form-control" ></td><td>( OR )</td><td><input type="text" name="video'+i+'" class="form-control"  placeholder="Youtube URL"></td>').appendTo($('#rowAppend'));

					i++;
				}
				else{
					swal("Sorry!! Only Three rows Can be added");
				}

			});

		}); 

	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#example-advanced-form").submit(function(e){  

				var product_name = $("#product_name").val();
				var product_code = $("#product_code").val();
				var product_id = $("#product_id").val();

				e.preventDefault();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: "{{ url('admin/check-update-product-details') }}",
					type: 'post',
					data: {name:product_name,
						code:product_code,
						product_id:product_id
					},
					dataType: 'json',
					success:function(response)
					{
						console.log(response);
						if(response.status == "success")
							$("#example-advanced-form")[0].submit();
						else
							swal(response.msg);

					},
					error: function(error)
					{
						console.log(error);
					}

				});
				

			});
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('.multiple-checkboxes').multiselect();  
		});
		function get_attributes()
		{
			var category_ids = $("#categories").val();
			if (categories != null) {	
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: "{{ url('admin/getattributes') }}",
					type: 'post',
					data: {category_ids:category_ids},
					dataType: 'json',
					success:function(response)
					{
						$("#attribute").html(response.content);
		    //         	var len = response.length;
		    //         	//alert(len);
		    //         	$("#attribute").empty();
		    //         	for( var i = 0; i<len; i++)
		    //             {
		    //                 var id = response[i]['id'];
		    //                 var name = response[i]['name'];
		    //                 $("#attribute").append("<div class='form-group'><label class='control-label mb-10 text-left'>"+name+"<input  type='text' class='form-control' name="+name+" ></div>");

						// }
		              //$("#attribute").val(response);
		          },
		          error: function(error)
		          {
		          	console.log(error);
		          }

		      });
			}    
		}

	</script>

	<script type="text/javascript">  
		$(document).ready(function() {    
			$(".delete-click").click(function (event) {
				event.preventDefault();
				var url = $(this).attr("href");
		  //alert(url)
		  swal({
		  	title: 'Are you sure?',
		  	text: "You won't be able to recover once deleted!",
		  	type: 'warning',
		  	showCancelButton: false,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Yes, delete it!'
		  }).then(function () {
		  	$.ajax({
		  		type: "get",
		  		url: url,
		  		success: function(response){
		  			if(response.status == "success")
		  				window.location.href=response.url;
		  		},
		  	});
		  })
		  // alert(url);
		});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.basic_price').on('input', function() {
        //alert(this.value);
        var discount;
        var basic_price = $('.myContent').val();
        var mrp_price = $('.mymrp').val();
        //alert(basic_price);
        //alert(mrp_price);
        if(mrp_price!='')
        {
        	discount = (mrp_price * 100) / basic_price;
			//alert(discount);
			total =100-discount;

			$('.special_mrp').show();
			$('#discounts').val(Math.round(total));

		}
		else
		{
			$('.special_mrp').hide();
		}
	});
		});

	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.basic_price').on('input', function() {
        //alert(this.value);
       	//var discount;
        //var basic_price = $('#myContent').val();
        var mrp_price = $('.mymrp').val();
        //alert(basic_price);
        //alert(mrp_price);
        if(mrp_price!='')
        {
        	$('.dates').show();
        }
        else
        {
        	$('.dates').hide();
        }
    });
		});

	</script>
	<script>
    // Clock pickers
    $(document).ready(function() {



    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('.datepicker-autoclose').datepicker({
    	autoclose: true,
    	todayHighlight: true
    });
    jQuery('#date-range').datepicker({
    	toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
    	todayHighlight: true
    });
    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
    	buttonClasses: ['btn', 'btn-sm'],
    	applyClass: 'btn-danger',
    	cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
    	timePicker: true,
    	format: 'MM/DD/YYYY h:mm A',
    	timePickerIncrement: 30,
    	timePicker12Hour: true,
    	timePickerSeconds: false,
    	buttonClasses: ['btn', 'btn-sm'],
    	applyClass: 'btn-danger',
    	cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
    	format: 'MM/DD/YYYY',
    	minDate: '06/01/2015',
    	maxDate: '06/30/2015',
    	buttonClasses: ['btn', 'btn-sm'],
    	applyClass: 'btn-danger',
    	cancelClass: 'btn-inverse',
    	dateLimit: {
    		days: 6
    	}
    });
});
</script>
@endsection