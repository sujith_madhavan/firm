@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.image-circle{
			border-radius: 50%;
		}

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

	</style>
	
@endsection
@section('content')
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <!-- <h5 class="txt-dark">Edit User</h5> -->
        </div>  
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
                <li class="active"><span>View-Vendor</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->                    
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">View Vendor</h6>
                    </div>
                    <div class="pull-right">
                       <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4 class="txt-dark">Vendor Information : </h4>
                                </div>
                            </div>
                            @if(!empty($vendor->image))
                                <div class="col-sm-12"> 
                                    <div class="form-group">
                                        <img class="image-circle" src="{{ $vendor->image_path }}" width="200" height="200" />
                                    </div>    
                                </div>
                            @endif     
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Business Name : </label>
                                    <label>{{ $vendor->business_name }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Email : </label>
                                    <label>{{ $vendor->email }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">GST : </label>
                                    <label>{{  $vendor->gst }}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Business Type : </label>
                                    <label>{{ $vendor->business_type }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Mobile : </label>
                                    <label>{{ $vendor->mobile }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Pan Card : </label>
                                    <label>{{  $vendor->pan }}</label>
                                </div>
                            </div>
                            <br>
                            <br> 
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4 class="txt-dark">Contact Details : </h4>
                                </div>
                            </div>  
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Contact Person : </label>
                                    <label>{{ $vendor->contact_person }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Address : </label>
                                    <label>{{ $vendor->address }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">District : </label>
                                    <label>{{  $vendor->district }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Phone : </label>
                                    <label>{{  $vendor->phone }}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Contact Designation : </label>
                                    <label>{{ $vendor->designation }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">State : </label>
                                    <label>{{ $vendor->state }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Pincode : </label>
                                    <label>{{  $vendor->pincode }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Service Distance(In Km) : </label>
                                    <label>{{  $vendor->distance }}</label>
                                </div>
                            </div> 
                            <br>
                            @if(count($vendor->bankDetails))
                               <div class="col-md-12">
                                   <div class="form-group">
                                       <h4 class="txt-dark">Bank Details : </h4>
                                   </div>
                               </div>
                               @foreach ($vendor->bankDetails as $bankDetail)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left">Account Number : </label>
                                            <label>{{ $bankDetail->account_number }}</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left">Bank : </label>
                                            <label>{{ $bankDetail->bank_name }}</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left">IFSC Code : </label>
                                            <label>{{  $bankDetail->ifsc }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left">Account name : </label>
                                            <label>{{ $bankDetail->account_name }}</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left">Branch : </label>
                                            <label>{{ $bankDetail->branch }}</label>
                                        </div>
                                    </div>   
                               @endforeach      
                            @endif  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->	
   
@endsection
