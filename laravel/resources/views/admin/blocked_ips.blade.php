@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}

		.forex-color{
			background: #e63e0c;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	  	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h5 class="txt-dark">Blocked Ips</h5>
	  	</div>  
	  	<!-- Breadcrumb -->
	  	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
		  		<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
		  		<li class="active"><span>Blocked Ips</span></li>
			</ol>
	  	</div>
	  	<!-- /Breadcrumb -->          
	</div>
	<div class="row">
	  	<div class="col-sm-12">
			<div class="panel panel-default card-view">
		  		<div class="panel-heading">
					<div class="pull-left">
					  <!-- <h6 class="panel-title txt-dark">Services</h6> -->
					</div>
		  		</div>
		  		<div class="panel-wrapper collapse in">
					<div class="panel-body">
			  			<div id="validation">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
			  			</div>  
			  			<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>
											<th>Ip Address</th>                     
											<th>Attempt</th>
											<th>Modified At</th>	
											<th style="width: 12%">Action</th>
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($blocked_ips as $blocked_ip)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td>{{ $blocked_ip->ip_address }}</td>
						  						<td>{{ $blocked_ip->attempts }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($blocked_ip->updated_at)) }}</td>    
						   						<td>        
						 							<a href="{{ url('admin/blocked-ips',[$blocked_ip->id]) }}/delete" class="btn btn-danger btn-xs" ><span class="fa fa-trash" ></span></a>
						   						</td>
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
					</div>
		  		</div>
			</div>
	  	</div>
	</div>
   
@endsection
@section('scripts')	

@endsection