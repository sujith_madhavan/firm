@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li><a href="{{ url('admin/coupons') }}">Coupons</a></li>
	            <li class="active"><span>Update-Coupon</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Update Coupon</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                    <div class="form-wrap">
                      		<form id="edit_coupon" method="post" action="{{ url('admin/coupons',[$coupon->id]) }}/edit" enctype="multipart/form-data">
                    		  	{{ csrf_field() }}
                    			<div class="form-wrap">
                    				<div class="col-sm-6">
	                    				<div class="form-group {{$errors->has('code')?'has-error':''}}">
	                                        <label class="control-label mb-10 text-left">Code *</label>
	                                        <input type="text" name="code" class="form-control" required value="{{ $coupon->code }}">
	                                        @if($errors->has('code'))
	                                            <span class="help-block">{{ $errors->first('code') }}</span>
	                                        @endif
	                                    </div>
	                                    <div class="form-group {{$errors->has('description')?'has-error':''}}">
	                                        <label class="control-label mb-10 text-left">Description *</label>
	                                        <input type="text" name="description" class="form-control" required value="{{ $coupon->description }}">
	                                        @if($errors->has('description'))
	                                            <span class="help-block">{{ $errors->first('description') }}</span>
	                                        @endif
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Valid On</label>
	                                        <input type="date" name="valid_on" class="form-control" value="{{ $coupon->valid_on }}">
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Valid For</label>
	                                        <input type="date" name="valid_for" class="form-control" value="{{ $coupon->valid_for }}">
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Valid Start On</label>
	                                        <input type="date" name="valid_start_on" class="form-control" value="{{ $coupon->valid_start_on }}">
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Valid End On</label>
	                                        <input type="date" name="valid_end_on" class="form-control" value="{{  $coupon->valid_end_on }}" >
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Discount Type</label>
	                                        <input type="text" name="discount_type" class="form-control" value="{{  $coupon->discount_type }}">
	                                    </div>                					
                    				</div>    
                    				<div class="col-sm-6">
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Amount</label>
	                                        <input type="number" name="amount" class="form-control" value="{{ $coupon->amount }}">
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Minimum Spent</label>
	                                        <input type="text" name="minimum_spent" class="form-control" value="{{ $coupon->minimum_spent }}">
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Maximum Spent</label>
	                                        <input type="text" name="maximum_spent" class="form-control" value="{{ $coupon->maximum_spent }}">
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Usage Limit</label>
	                                        <input type="text" name="usage_limt" class="form-control" value="{{ $coupon->usage_limt }}">
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="control-label mb-10 text-left">Usage Limit Per User </label>
	                                        <input type="text" name="usage_limt_per_user" class="form-control" value="{{ $coupon->usage_limt_per_user }}">
	                                    </div>                					
                    				</div>   
                      			</div>
                      			<div class="col-sm-12">
                      				<div style="text-align: right;">
                    					<button class="btn btn-danger"  role="button" type="submit">Submit</button>
                    				</div>
                      			</div>
                      		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection

@section('scripts')	
	
@endsection