@extends('admin.layouts.app') 
@section('styles')
	<style type="text/css">
		.table-header-colour{
			background-color: #262626; 
			color: #fff;
		}
	</style>
@endsection
@section('content')
  	<div class="row heading-bg">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	  		<h5 class="txt-dark">Coupons</h5>
		</div>  
		<!-- Breadcrumb -->
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
				<li class="active"><span>Coupons</span></li>
			</ol>
		</div>
		<!-- /Breadcrumb -->          
  	</div>
  	<div class="row">
	  	<div class="col-sm-12">
		  	<div class="panel panel-default card-view">
			  	<div class="panel-heading">
				  	<div class="pull-left">
					  <!-- <h6 class="panel-title txt-dark">Gallery Image</h6> -->
				  	</div>
				  	@if ($allowed_operations['add'])
					  	<div class="pull-right">
						   	<a href="{{ url('admin/coupons/add') }}" class="btn btn-danger btn-circle  btn-sm forex-color" title="Add New" ><i class="fa fa-plus"></i></a>
						</div>
					@endif  	
			  	</div>
			  	<div class="panel-wrapper collapse in">
				  	<div class="panel-body">
						<div id="validation">
					  		@if ($errors->any())
						  		<div class="alert alert-danger">
							  		<ul>
								  		@foreach ($errors->all() as $error)
									  		<li>{{ $error }}</li>
								  		@endforeach
							  		</ul>
						  		</div>
					  		@endif
						</div>					  
						<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>                     
											<th>Code</th>
											<th>Description</th>
											<th>Modified By</th>
											<th>Modified At</th>
											@if ($allowed_operations['status'])
												<th style="width: 10%">Status</th>
											@endif
											@if ($allowed_operations['edit'] || $allowed_operations['view'])	
												<th style="width: 12%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($coupons as $coupon)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td>{{ $coupon->code }}</td>
						  						<td>{{ $coupon->description }}</td>
						  						<td>{{ $coupon->modifiedBy->name }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($coupon->updated_at)) }}</td>
						  						@if ($allowed_operations['status'])
							  						<td>
														@if ($coupon->status)
														  	<a href="{{ url('admin/coupons',[$coupon->id]) }}/status/0" class="btn btn-success btn-xs">Active</a>
														@else    
														  	<a href="{{ url('admin/coupons',[$coupon->id]) }}/status/1" class="btn btn-danger btn-xs">Deactivate</a>
														@endif 
							  						</td>
							  					@endif
							  					@if ($allowed_operations['edit'] || $allowed_operations['view']) 	       
								   					<td>
								   						@if ($allowed_operations['view'])
						   									<a href="{{ url('admin/coupons',[$coupon->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
						   								@endif
							   							@if ($allowed_operations['edit'])                 
							 								<a href="{{ url('admin/coupons',[$coupon->id]) }}/edit" class="btn btn-danger btn-xs" ><span class="fa fa-edit" ></span></a>
							 							@endif	
								   					</td>
							   					@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
				  	</div>
			  	</div>
		  	</div>
	  	</div>
  	</div>
@endsection

@section('scripts')

@endsection
