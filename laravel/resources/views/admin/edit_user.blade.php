@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}
	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li><a href="{{ url('admin/users') }}">Admins</a></li>
	            <li class="active"><span>Edit-Admin</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Update Admin</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                    <div class="form-wrap">
                      		<form id="edit_user" method="post" action="{{ url('admin/users',[$user->id]) }}/edit" enctype="multipart/form-data">
                    		  	{{ csrf_field() }}
                    			<div class="form-wrap col-md-6 col-lg-6">
                    				<div class="form-group {{$errors->has('role')?'has-error':''}}">
										<label for="" class="control-label mb-10">Role *</label>
										<select class="form-control" name="role" id="role" required>
											@foreach ($roles as $role)
												@if($role->id == $user->role_id) 
													<option selected style="font-weight:bold;" value="{{ $role->id }}">{{ ucwords($role->name) }}</option>
												@else
													<option value="{{ $role->id }}">{{ ucwords($role->name) }}</option>	
												@endif	
											@endforeach
										</select>
										@if($errors->has('role'))
		                                    <span class="help-block">{{ $errors->first('role') }}</span>
		                                @endif
									</div>
                    				<div class="form-group {{$errors->has('name')?'has-error':''}}">
                                        <label class="control-label mb-10 text-left">Name *</label>
                                        <input type="text" name="name" class="form-control" required value="{{ $user->name }}">
                                        @if($errors->has('name'))
                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group {{$errors->has('email')?'has-error':''}}">
                                        <label class="control-label mb-10 text-left">Email *</label>
                                        <input type="email" name="email" class="form-control" required value="{{ $user->email }}">
                                        @if($errors->has('email'))
                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group">
		                                <label class="control-label mb-10 text-left"></label>
		                                <img class="image-circle" src="{{ $user->image_path }}" width="100" height="100" />
		                            </div>
                                    <div class="form-group {{$errors->has('image')?'has-error':''}}">
                                        <label class="control-label mb-10 text-left">Profile Image</label>
                                        <input type="file" name="image" class="form-control">
                                        @if($errors->has('image'))
                                            <span class="help-block">{{ $errors->first('image') }}</span>
                                        @endif
                                    </div>
                    				<div style="text-align: right;">
                    					<button class="btn btn-danger"  role="button" type="submit">Submit</button>
                    				</div>  
                      			</div>
                      		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection
@section('scripts')	
	
	<script type="text/javascript">
		$(document).ready(function() {
            $("#edit_user").submit(function(e){           	
            	var role = $("#role").val();
                if (role <= 0) {
                	swal("Select Role.");
                	e.preventDefault();
                }              
            });
        });
	</script>
@endsection