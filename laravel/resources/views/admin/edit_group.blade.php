@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li><a href="{{ url('admin/groups') }}">Groups</a></li>
	            <li class="active"><span>Edit-Group</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Update Group</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                    <div class="form-wrap">
	                    	<form id="add_brand" method="post" action="{{ url('admin/groups',[$group->id]) }}/edit" enctype="multipart/form-data">
							  	{{ csrf_field() }}
								<div class="form-wrap col-md-6 col-lg-6">
									<div class="form-group {{$errors->has('name')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Name *</label>
					                    <input type="text" name="name" class="form-control" required value="{{ $group->name }}">
					                    @if($errors->has('name'))
					                        <span class="help-block">{{ $errors->first('name') }}</span>
					                    @endif
					                </div>
					                <div class="form-group">
		                                <label class="control-label mb-10 text-left">Select Attributes</label><br> 
		                                @foreach ($attributes as $attribute)
											@php ($i = 1)
	                                		@foreach ($group->groupAttributes as $groupAttribute)
	                                			@if($groupAttribute->attribute_id == $attribute->id)	  
	                                				<div class="checkbox checkbox-danger">
														<table class="table">
												<tr><th style="width:20%;"><input id="checkbox6" type="checkbox" name="attribute_ids[]" value="{{ $attribute->id }}" checked>
														<label for="checkbox6">{{ $attribute->name }}</th> <td>:</td> <th><input type="text" name="position{{$attribute->id}}"
                         placeholder="Enter Position" value="{{$groupAttribute->position}}" ></th></tr></label>
                     </table>
													</div>
	                                				@php($i = 2)
	                                				@break	
	                                			@endif	
	                                		@endforeach	
	                                		@if($i == 1)
	                                			<div class="checkbox checkbox-danger">
													<table class="table">
												<tr><th style="width:20%;"><input id="checkbox6" type="checkbox" name="attribute_ids[]" value="{{ $attribute->id }}">
												<label for="checkbox6">{{ $attribute->name }}</th> <td>:</td> <th><input type="text" name="position{{$attribute->id}}"
                         placeholder="Enter Position" ></th></tr></label>
                         </table>
												</div>	
	                                		@endif 
		                            	@endforeach
		                            </div>
									<div style="text-align: right;">
										<button class="btn btn-danger"  role="button" type="submit">Submit</button>
									</div>  
					  			</div>
					  		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection
