@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}
	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li class="active"><span>Add-News Letter</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Create News Letter</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                    <div class="form-wrap">
	                    	<form id="add_news_letter" method="post" action="{{ url('admin/news-letters/add') }}" enctype="multipart/form-data">
							  	{{ csrf_field() }}
								<div class="form-wrap col-md-6 col-lg-6">
									<div class="form-group {{$errors->has('title')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Title *</label>
					                    <input type="text" name="title" class="form-control" required ">
					                    @if($errors->has('title'))
					                        <span class="help-block">{{ $errors->first('title') }}</span>
					                    @endif
					                </div>
					                <div class="form-group {{$errors->has('attachment')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Attachment</label>
					                    <input type="file" name="attachment" class="form-control">
					                    @if($errors->has('attachment'))
					                        <span class="help-block">{{ $errors->first('attachment') }}</span>
					                    @endif
					                </div>
					                <div class="form-group {{$errors->has('expires_at')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Expires At</label>
					                    <input type="date" name="expires_at" class="form-control" required>
					                    @if($errors->has('expires_at'))
					                        <span class="help-block">{{ $errors->first('expires_at') }}</span>
					                    @endif
					                </div>			 
					  			</div>
					  			<div class="form-wrap col-md-6 col-lg-6">
									 
					  			</div>
					  			<div class="form-wrap col-md-12 col-lg-12">
		  				  			<div class="form-group {{$errors->has('content')?'has-error':''}}">
	  				                    <label class="control-label mb-10 text-left">Content *</label>
	  									<textarea class="textarea_editor form-control" name="content"></textarea>
	  				                    @if($errors->has('content'))
	  				                        <span class="help-block">{{ $errors->first('content') }}</span>
	  				                    @endif
	  				                </div>
	  				                <div style="text-align: right;">
										<button class="btn btn-danger"  role="button" type="submit">Submit</button>
									</div> 
					  			</div>	
					  		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection
	

 @section('scripts') 

	<!-- wysuhtml5 Plugin JavaScript -->
	<script src="{{ asset('backend/bower_components/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>
	
	<script src="{{ asset('backend/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>
	
	<!-- Bootstrap Wysuhtml5 Init JavaScript -->
	<script src="{{ asset('backend/dist/js/bootstrap-wysuhtml5-data.js') }}"></script>

@endsection