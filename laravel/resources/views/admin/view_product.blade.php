@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.image-circle{
			border-radius: 50%;
		}

	</style>
	
@endsection
@section('content')
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <!-- <h5 class="txt-dark">Edit User</h5> -->
        </div>  
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
                <li class="active"><span>View-Product</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->                    
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">View Product</h6>
                    </div>
                    <div class="pull-right">
                       <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                          <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Name : </label>
                                    <label>{{ ucwords($product->name) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Code : </label>
                                    <label>{{ ucwords($product->code) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">HSC Code : </label>
                                    <label>{{ $product->hsc_code }}</label>
                                </div>
                                @if(!empty($product->brandLogo))
                                    <div class="form-group">
                                        <label class="control-label mb-10 text-left">Brand : </label>
                                        <label>{{ $product->brandLogo->name ?? '' }}</label>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Category: </label>
                                    <label>
                                        @foreach ($product->productCategories as $product_category)
                                            {{  ucwords($product_category->category->name) }},
                                        @endforeach 
                                    </label>   
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Old Price : </label>
                                    <label>{{  $product->slashed_price }} / {{  $product->price_for }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Price : </label>
                                    <label>{{  $product->price }} / {{  $product->price_for }}</label>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label mb-10 text-left">Discount : </label>
                                    <label>{{  $product->discount }}</label>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label mb-10 text-left">Start Date : </label>
                                    <label>{{  $product->start_date }}</label>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label mb-10 text-left">End Date : </label>
                                    <label>{{  $product->end_date }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Box Quantity: </label>
                                    <label>{{  $product->box_quantity }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Stock : </label>
                                    <label>{{  $product->stock }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">GST : </label>
                                    <label>{{  $product->gst }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Minimum Quantity : </label>
                                    <label>{{  $product->minimum_quantity }}</label>
                                </div>                                
                            </div>
                            <div class="col-md-6">                             
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Related Category : </label>
                                    <label>
                                        @foreach ($product->relatedCategories as $related_category)
                                            {{  ucwords($related_category->category->name) }},
                                        @endforeach 
                                    </label>
                                </div>
                                @php($attribute_ids = array())
                                @foreach ($product->productAttributes as $product_attribute)
                                    @if(!in_array($product_attribute->attribute_id, $attribute_ids))
                                        @php($attribute_ids[] = $product_attribute->attribute_id)
                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left">{{  ucwords($product_attribute->attribute->name) }} : </label>
                                            @foreach ($product_attribute->attribute->AttributeValues as $attribute_value)
                                                @foreach ($product_attribute->productAttributeValues as $product_attribute_value)
                                                    @if($attribute_value->id == $product_attribute_value->attribute_value_id)
                                                        <div class="checkbox checkbox-danger checkbox-circle" style="display: inline;">
                                                            <input type="checkbox" disabled checked>
                                                            <label for="checkbox-12">{{ $attribute_value->value }}</label>
                                                        </div>
                                                        &nbsp;        
                                                        @break  
                                                    @endif  
                                                @endforeach 
                                            @endforeach
                                        </div>
                                    @endif    
                                @endforeach
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Description :</label><br>
                                    <label for="checkbox6">{!! $product->description !!}</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Gallery :</label><br> 
                                </div>
                                @foreach ($product->productGalleries as $product_gallery)
                                    @if($product_gallery->type == 'IMAGE')
                                        <div class="form-group col-md-4">
                                            <img src="{{ $product_gallery->image_path }}" width="200" height="200" /></td>
                                        </div>
                                    @else
                                        <div class="form-group col-md-4">
                                            <iframe width="200" height="200"
                                                src="{{ $product_gallery->video }}">
                                            </iframe>
                                        </div>
                                    @endif    
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->	
   
@endsection
