@extends('admin.layouts.app') 
@section('styles')	
@section('styles')

	<link href="{{asset('backend/vendors/bower_components/jquery-wizard.js/css/wizard.css')}}" rel="stylesheet" type="text/css"/>
		
	<!-- jquery-steps css -->
	<link rel="stylesheet" href="{{asset('backend/vendors/bower_components/jquery.steps/demo/css/jquery.steps.css')}}">
		
<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}
	</style>
	
@endsection
	
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li class="active"><span>Add-Banner</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Create Banner</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                	<div id="validation">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
			  			</div>
	                    <div class="form-wrap">
	                    	<form id="add_news_letter" method="post" action="{{ url('admin/banners/add') }}" enctype="multipart/form-data">
							  	{{ csrf_field() }}
								<div class="form-wrap col-md-6 col-lg-6">
<!--
									<div class="form-group {{$errors->has('content')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Title1</label>
					                    <input type="text" name="content" maxlength="15" class="form-control" required>
					                    @if($errors->has('content'))
					                        <span class="help-block">{{ $errors->first('content') }}</span>
					                    @endif
					                </div>
					                <div class="form-group {{$errors->has('content_medium')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Title2</label>
					                    <input type="text" name="content_medium" maxlength="25" class="form-control" required>
					                    @if($errors->has('content_medium'))
					                        <span class="help-block">{{ $errors->first('content_medium') }}</span>
					                    @endif
					                </div>

-->
	<div class="form-group {{$errors->has('description')?'has-error':''}}">
			  				                    <label class="control-label mb-10 text-left">Title *</label>
			  				                  
			  									<textarea class="textarea_editor form-control" name="content_small" required></textarea>
			  				                    @if($errors->has('content_small'))
			  				                        <span class="help-block">{{ $errors->first('content_small') }}</span>
			  				                    @endif
			  				                </div>
					                <!-- <div class="form-group {{$errors->has('content_small')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Title3</label>
					                    <textarea  name="content_small" class="form-control" required></textarea>
					                    
					                    @if($errors->has('content_small'))
					                        <span class="help-block">{{ $errors->first('content_small') }}</span>
					                    @endif
					                </div> -->
					                <div class="form-group {{$errors->has('image')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Image</label>
					                    <input type="file" name="image" class="form-control" required>
					                    @if($errors->has('image'))
					                        <span class="help-block">{{ $errors->first('image') }}</span>
					                    @endif
					                </div>
					                <div class="form-group {{$errors->has('position')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Position</label>
					                    <input type="number" name="position" class="form-control" required>
					                    @if($errors->has('position'))
					                        <span class="help-block">{{ $errors->first('position') }}</span>
					                    @endif
					                </div>			 
					  			</div>
					  			<div class="form-wrap col-md-6 col-lg-6">
									 
					  			</div>
					  			<div class="form-wrap col-md-12 col-lg-12">
	  				                <div style="text-align: right;">
										<button class="btn btn-danger"  role="button" type="submit">Submit</button>
									</div> 
					  			</div>	
					  		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection
	

@section('scripts') 
<!-- Form Wizard JavaScript -->
	<script src="{{ asset('backend/bower_components/jquery.steps/build/jquery.steps.min.js') }}"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
	
	<!-- Form Wizard Data JavaScript -->
	<script src="{{ asset('backend/dist/js/form-wizard-data.js') }}"></script>

	<!-- wysuhtml5 Plugin JavaScript -->
	<script src="{{ asset('backend/bower_components/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>
	
	<script src="{{ asset('backend/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>
	
	<!-- Bootstrap Wysuhtml5 Init JavaScript -->
	<script src="{{ asset('backend/dist/js/bootstrap-wysuhtml5-data.js') }}"></script>

@endsection