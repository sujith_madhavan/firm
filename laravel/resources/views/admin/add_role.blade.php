@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li><a href="{{ url('admin/roles') }}">Roles</a></li>
	            <li class="active"><span>Add-Role</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Create Role</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                    <div class="form-wrap">
	                    	<form id="add_brand" method="post" action="{{ url('admin/roles/add') }}" enctype="multipart/form-data">
							  	{{ csrf_field() }}
								<div class="form-wrap col-md-12 col-lg-12">
									<div class="form-group {{$errors->has('name')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Name *</label>
					                    <input type="text" name="name" class="form-control" required ">
					                    @if($errors->has('name'))
					                        <span class="help-block">{{ $errors->first('name') }}</span>
					                    @endif
					                </div>
					                <div class="form-group">
		                                <label class="control-label mb-10 text-left">Select Permissions</label><br>
		                                @foreach ($modules as $module)
		                                	<label class="control-label mb-10 text-left">{{ ucwords($module->name) }}</label><br>
		                                	@foreach ($module->operations as $operation)
		                                		@if($operation->type != 'LIST')
		                                			<div class="checkbox checkbox-danger checkbox-circle" style="display: inline;">
														<input type="checkbox" name="{{ $module->code }}[]" value="{{ $operation->id }}">
														<label for="checkbox-12">{{ ucwords($operation->name) }}</label>
													</div>
													&nbsp;	  
		                                		@endif	
		                                	@endforeach	
		                                	<br>
		                                	<br>
		                            	@endforeach
		                            </div>
									<div style="text-align: right;">
										<button class="btn btn-danger"  role="button" type="submit">Submit</button>
									</div>  
					  			</div>
					  		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection
