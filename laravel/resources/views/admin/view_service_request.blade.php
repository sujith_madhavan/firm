@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.image-circle{
			border-radius: 50%;
		}

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

	</style>
	
@endsection
@section('content')
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <!-- <h5 class="txt-dark">Edit User</h5> -->
        </div>  
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
                <li class="active"><span>View-Service-Request</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->                    
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">View Service Request</h6>
                    </div>
                    <div class="pull-right">
                       <a href="{{ url()->previous() }}" class="btn btn-danger" title="Add New" >Back</i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Name : </label>
                                    <label>{{ $service_request->name }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Email : </label>
                                    <label>{{ $service_request->email }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Mobile : </label>
                                    <label>{{  $service_request->mobile }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Service : </label>
                                    <label>{{  $service_request->service }}</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Message : </label>
                                    <label>{{ $service_request->message }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Created At : </label>
                                    <label>{{ date('d-m-Y h:i:s A', strtotime($service_request->created_at)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Updated At : </label>
                                    <label>{{  date('d-m-Y h:i:s A', strtotime($service_request->updated_at)) }}</label>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->	
   
@endsection
