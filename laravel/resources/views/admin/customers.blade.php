@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}

		.forex-color{
			background: #e63e0c;
		}

		.buttons-excel {
		  	background-color: #ea6c41 !important;
		  	border: solid 1px #ea6c41 !important;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	  	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h5 class="txt-dark">Customers</h5>
	  	</div>  
	  	<!-- Breadcrumb -->
	  	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
		  		<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
		  		<li class="active"><span>Customers</span></li>
			</ol>
	  	</div>
	  	<!-- /Breadcrumb -->          
	</div>
	<div class="row">
	  	<div class="col-sm-12">
			<div class="panel panel-default card-view">
		  		<div class="panel-heading">
					<div class="pull-left">
					  <!-- <h6 class="panel-title txt-dark">Services</h6> -->
					</div>
		  		</div>
		  		<div class="panel-wrapper collapse in">
					<div class="panel-body">
			  			<div id="validation">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
			  			</div>  
			  			<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="customers" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>
											<th>First Name</th>                     
											<th>Last Name</th>
											<th>Email</th>
											<th>Mobile</th>
											<th>Modified At</th>
											@if ($allowed_operations['status'])
												<th style="width: 10%">Status</th>
											@endif
											@if ($allowed_operations['view'] || $allowed_operations['edit'])	
												<th style="width: 12%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($customers as $customer)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td>{{ $customer->first_name }}</td>
						  						<td>{{ $customer->last_name }}</td>
						  						<td>{{ $customer->email }}</td>
						  						<td>{{ $customer->mobile }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($customer->updated_at)) }}</td>
						  						@if ($allowed_operations['status'])
							  						<td>
														@if ($customer->status)
														  	<a href="{{ url('admin/customers',[$customer->id]) }}/status/0" class="btn btn-success btn-xs">Active</a>
														@else    
														  	<a href="{{ url('admin/customers',[$customer->id]) }}/status/1" class="btn btn-danger btn-xs">Deactivate</a></td>
														@endif 
							  						</td>
							  					@endif
							  					@if ($allowed_operations['view'] || $allowed_operations['edit'])      
							   						<td>
							   							@if ($allowed_operations['view'])
						   									<a href="{{ url('admin/customers',[$customer->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
						   								@endif
								   						@if ($allowed_operations['edit'])     
										 					<a href="" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal{{ $customer->id }}"><span class="fa fa-edit" ></span></a>
										 					
											 				<div id="myModal{{ $customer->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
									   							<div class="modal-dialog">
										 							<div class="modal-content">
										   								<div class="modal-header">
											 								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											 								<h5 class="modal-title" id="myModalLabel">Update Customer Password</h5>
										   								</div>
										   								<form action="{{ url('admin/customers/password-reset') }}" method="post" enctype="multipart/form-data">
											 								{{csrf_field()}}
											 								<input name="customer_id" type="hidden" class="form-control" value="{{ $customer->id }}">
											 								<div class="modal-body">
																			   <div class="form-group">
																				 <label for="" class="control-label mb-10">Password</label>
																				 <input name="password" type="text" class="form-control" required >
																			   </div>
											 								</div>
											 								<div class="modal-footer">
																			   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																			   <button type="submit" class="btn btn-info">Update</button>
											 								</div>
										   								</form>
										 							</div>
									   							</div>
									 						</div>
									 					@endif
	
							   						</td>
						   						@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
					</div>
		  		</div>
			</div>
	  	</div>
	</div>
   
@endsection
@section('scripts')	

	<!-- Data table JavaScript -->
	<script src="{{ asset('backend/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{asset('backend/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}} "></script>
	<script src="{{asset('backend/bower_components/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
	<script src="{{asset('backend/bower_components/jszip/dist/jszip.min.js')}}"></script>
	<script src="{{asset('backend/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
	<script src="{{asset('backend/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
	
	<script src="{{asset('backend/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
	<script src="{{asset('backend/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
	<script src="{{asset('backend/dist/js/export-table-data.js')}}"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('#customers').DataTable( {
		        dom: 'Bfrtip',
		        buttons: [
			            {
			                extend: 'excelHtml5',
			                title: 'Customers',
			                exportOptions: {
				                columns: [1,2,3,4,5]
				            }
			            }
			        ]
		    } );
		} );
	</script>
@endsection