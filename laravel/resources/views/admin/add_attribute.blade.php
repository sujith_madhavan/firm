@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}
	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li class="active"><span>Add-Attribute</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Create Attribute</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                	<div id="validation">
					  		@if ($errors->any())
						  		<div class="alert alert-danger">
							  		<ul>
								  		@foreach ($errors->all() as $error)
									  		<li>{{ $error }}</li>
								  		@endforeach
							  		</ul>
						  		</div>
					  		@endif
						</div>
	                    <div class="form-wrap">
	                    	<form id="add_attribute" method="post" action="{{ url('admin/attributes/add') }}" enctype="multipart/form-data">
							  	{{ csrf_field() }}
								<div class="form-wrap col-md-6 col-lg-6">
					                <div class="form-group {{$errors->has('name')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Name *</label>
					                    <input type="text" name="name" class="form-control" required value="{{ old('name') }}">
					                    @if($errors->has('name'))
					                        <span class="help-block">{{ $errors->first('name') }}</span>
					                    @endif
					                </div>
					                <div class="form-group {{$errors->has('common_name')?'has-error':''}}">
					                    <label class="control-label mb-10 text-left">Common Name (Display Frontend)*</label>
					                    <input type="text" name="common_name" class="form-control" placeholder="Common name" required value="{{ old('common_name') }}">
					                    @if($errors->has('common_name'))
					                        <span class="help-block">{{ $errors->first('common_name') }}</span>
					                    @endif
					                </div>
					                <table  class="table table-hover small-text" id="tb">
					                	<tr>
					                		<th style="font-weight: bold;">Value</th>
					                		<th><a href="javascript:void(0);" style="font-size:18px;" id="addMore" title="Add More Person"><span class="glyphicon glyphicon-plus"></span></a></th>
					                	</tr>
					                	<tbody id="rowAppend">
					                		<tr>
					                			<td>
					                				<input type="text" class="form-control" name="values[]" required />
					                			</td>
					                			<td><a href="" class="remove text-danger"><span class="fa fa-trash"></span></td>
					                		</tr>
					                	</tbody>
					                </table>	 
					  			</div>
					  			<div class="form-wrap col-md-12 col-lg-12">
	  				                <div style="text-align: right;">
										<button class="btn btn-danger"  role="button" type="submit">Submit</button>
									</div> 
					  			</div>	
					  		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection
	

 @section('scripts') 

	<script>
	  	$(function(){
	  		var i =1
	      	$('#addMore').on('click', function() {

	        // var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
	        // data.find("input").val('09AM- 10AM');
	        $('<tr><td><input type="text" name="values[]" class="form-control" required></td><td><a href="" class="remove text-danger"><span class="fa fa-trash"></span></td>').appendTo($('#rowAppend'));
		        i++;
	 		});

	       	$(document).on('click', '.remove', function(e) {
	       		e.preventDefault();
	           	var trIndex = $(this).closest("tr").index();
	           	// alert(trIndex);
	            if(trIndex<=0) {
	              	swal("Sorry!! Can't remove first row!");
	            } 
	            else {
	                $(this).closest("tr").remove();
	            }
	        });
	  	}); 

	</script>

@endsection