@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.image-circle{
			border-radius: 50%;
		}

	</style>
	
@endsection
@section('content')
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <!-- <h5 class="txt-dark">Edit User</h5> -->
        </div>  
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
                <li class="active"><span>View-Role</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->                    
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">View Role</h6>
                    </div>
                    <div class="pull-right">
                       <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Name : </label>
                                    <label>{{ $role->name }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Code : </label>
                                    <label>{{ $role->code }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Modified By : </label>
                                    <label>{{  $role->modifiedBy->name }}</label>
                                </div>
                            </div>
                            <div class="col-md-6">    
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Created At : </label>
                                    <label>{{ date('d-m-Y h:i:s A', strtotime($role->created_at)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Updated At : </label>
                                    <label>{{ date('d-m-Y h:i:s A', strtotime($role->updated_at)) }}</label>
                                </div>
                            </div>
                            <div class="col-md-12">    
                                <div class="form-group">
                                    <h6 class="panel-title txt-dark">Operations</h6>
                                    <br>
                                    @foreach ($role->roleModules as $role_module)
                                        <label class="control-label mb-10 text-left">{{ ucwords($role_module->module->name) }}: </label><br>
                                        @foreach ($role_module->roleModuleOperations as $role_module_operation)
                                            <div class="radio radio-danger" style="display: inline;">
                                                <input type="radio"  disabled checked="">
                                                <label for="radio24">{{ $role_module_operation->operation->name }}</label>
                                            </div>
                                            &nbsp;
                                        @endforeach
                                        <br> 
                                        <br>
                                    @endforeach
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->	
   
@endsection
