@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	        <!-- <h5 class="txt-dark">Edit User</h5> -->
	    </div>  
	    <!-- Breadcrumb -->
	    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	        <ol class="breadcrumb">
	            <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
	            <li class="active"><span>Edit-Shipping-Detail</span></li>
	        </ol>
	    </div>
	    <!-- /Breadcrumb -->                    
	</div>
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel panel-default card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Update Shipping Detail</h6>
	                </div>
	                <div class="pull-right">
	                   <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                	@if ($errors->any())
                            <div class="alert alert-danger" style="display: block;">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
	                    <div class="form-wrap">
                      		<form id="edit_user" method="post" action="{{ url('admin/orders',[$shipping_detail->order_id]) }}/shipping-detail" enctype="multipart/form-data">
                    		  	{{ csrf_field() }}
                    			<div class="form-wrap col-md-6 col-lg-6">
                    				<div class="form-group">
	                                    <label for="name"> First Name* </label>
	                                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{ $shipping_detail->first_name }}" required>
	                                </div>
	                                <div class="form-group">
	                                    <label for="name"> Last Name </label>
	                                    <input type="text" class="form-control" name="last_name" value="{{ $shipping_detail->last_name }}" id="last_name">
	                                </div>
	                                <div class="form-group">
	                                    <label for=""> Company </label>
	                                    <input type="text" class="form-control" name="company" value="{{ $shipping_detail->company }}" id="company">
	                                </div>

	                                <div class="form-group">
	                                    <label> Mobile number<span>*</span></label>
	                                    <input type="text" maxlength="10" pattern="[9|8|7]\d{9}$" class="form-control" name="mobile" id="mobile" value="{{ $shipping_detail->mobile }}" required>
	                                </div>
	                                <div class="form-group">
	                                    <label> Fax </label>
	                                    <input type="text" class="form-control" name="fax" id="fax" value="{{ $shipping_detail->fax }}">
	                                </div>
	                                <div class="reg-head3">
	                                    <h3> ADDRESS</h3>
	                                </div>
	                                <div class="form-group">
	                                    <label> Street Address<span>*</span></label>
	                                    <input type="text" class="form-control" name="address" value="{{ $shipping_detail->address }}" required>
	                                </div>
	                                <div class="form-group">
	                                    <input type="text" class="form-control" name="address1" value="{{ $shipping_detail->address1 }}">
	                                </div>
	                                <div class="form-group">
	                                    <input type="text" class="form-control" name="address2" value="{{ $shipping_detail->address2 }}">
	                                </div>
	                                <div class="form-group">
	                                    <label> City<span>*</span></label>
	                                    <input type="text" class="form-control" name="city" value="{{ $shipping_detail->city }}" required>
	                                </div>
	                                <div class="form-group">
	                                    <label> State<span>*</span> </label>
	                                    <input type="text" class="form-control" name="state" value="{{ $shipping_detail->state }}" required>
	                                </div>
	                                <div class="form-group">
	                                    <label> Postal code<span>*</span></label>
	                                    <input type="text" class="form-control" name="pincode" value="{{ $shipping_detail->pincode }}" required>
	                                </div>
	                                <div class="form-group">
	                                    <label> Country<span>*</span></label>
	                                    <input type="text" class="form-control" name="country" value="{{ $shipping_detail->country }}" required>
	                                </div>
                    				<div style="text-align: right;">
                    					<button class="btn btn-danger"  role="button" type="submit">Submit</button>
                    				</div>  
                      			</div>
                      		</form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
   
@endsection
@section('scripts')	
	
@endsection