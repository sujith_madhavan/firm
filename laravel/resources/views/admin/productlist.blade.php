@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}

		.forex-color{
			background: #e63e0c;
		}
		.buttons-excel {
		  	background-color: #ea6c41 !important;
		  	border: solid 1px #ea6c41 !important;
		}
	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	  	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h5 class="txt-dark">Product List</h5>
	  	</div>  
	  	<!-- Breadcrumb -->
	  	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
		  		<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
		  		<li class="active"><span>Product List</span></li>
			</ol>
	  	</div>
	  	<!-- /Breadcrumb -->          
	</div>
	<div class="row">

	  	<div class="col-sm-12">

			<div class="panel panel-default card-view">

		  		<div class="panel-heading">
					<div class="pull-left">
					
					</div>
					@if ($allowed_operations['add'])
						<div class="pull-right">
						   	<a href="{{ url('admin/products/add') }}" class="btn btn-danger btn-circle  btn-sm forex-color" title="Add Product" ><i class="fa fa-plus"></i></a>
						   	
						   	<a href="" class="btn btn-danger btn-circle  btn-sm" title="Import Product" data-toggle="modal" data-target="#download_excel_format" ><i class="fa fa-download"></i></a>
						</div>
					
						<a href="{{ url('admin/products/excel-download') }}" class="btn btn-danger btn-circle  btn-sm forex-color" title="Export Product" ><i class="fa fa-download"></i></a>

						<a href="" class="btn btn-danger btn-circle  btn-sm" title="Import Product" data-toggle="modal" data-target="#myModal" ><i class="fa fa-upload"></i></a>

						<div id="download_excel_format" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
						  	<div class="modal-dialog">
							  	<div class="modal-content">
								  	<div class="modal-header">
									  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									  	<h5 class="modal-title" id="myModalLabel">Sample Product Download</h5>
								  	</div>
								  	<form action="{{ url('admin/products/download-sample') }}" method="post" enctype="multipart/form-data">
									  	{{csrf_field()}}
									  	<div class="modal-body">
										  	<div class="form-group {{$errors->has('categories')?'has-error':''}}">
							                    <label class="control-label mb-10 text-left">Select  Category * </label>
			                                  	<br>
			                                  	<select class="form-control" id="category_id" name="category_id" required>
			                                  		<option value="">Select Category</option>
				                                    @foreach($categories as $category)
				                                      	<option value="{{ $category->id }}">{{ucwords($category->name)}}</option>
				                                    @endforeach
			                                  	</select>
							                </div>
							            </div>    
									  	<div class="modal-footer">
										  	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  	<button type="submit" class="btn btn-info">Download</button>
									  	</div>
								  	</form>
							  	</div>
						  	</div>
					  	</div>

					  	<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
						  	<div class="modal-dialog">
							  	<div class="modal-content">
								  	<div class="modal-header">
									  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									  	<h5 class="modal-title" id="myModalLabel">Import Products</h5>
								  	</div>
								  	<form action="{{ url('admin/products/upload-product') }}" method="post" enctype="multipart/form-data">
									  	{{csrf_field()}}
									  	<div class="modal-body">
										  	<div class="form-group">
											  	<label for="" class="control-label mb-10">* Uploading CSV should match Sample CSV and it should have only one sheet.</label>
											  	<label for="" class="control-label mb-10">* All column is manditory, Improper value results in inconsistant data error.</label>
											  	<label for="" class="control-label mb-10">* Product Name should not be repeated.</label>
											  	<label for="" class="control-label mb-10">* Category, Attributes, Attribute Values should be in comma(,) seperated.</label>
											  	<label for="" class="control-label mb-10">* Multiple Attribute Values should be in - seperated.</label>
											  	<label for="" class="control-label mb-10">* Old Price, New Price, Stock, Minimum Quantity should be numbers.</label>
											  	<label for="" class="control-label mb-10">* For every product minimum one image name should.</label>
											  	<!-- <label for="" class="control-label mb-10">* Featured, New, Top Selling should contain only 0(False) or 1(True).</label> -->
											  	<label for="" class="control-label mb-10">* Error Data will be downloaded as Excel.</label>
										  	</div> 
									  	</div>
									  	<div class="modal-body">
									  		<div class="form-group {{$errors->has('categories')?'has-error':''}}">
							                    <label class="control-label mb-10 text-left">Select  Category * </label>
			                                  	<br>
			                                  	<select class="form-control" id="category_id" name="category_id" required>
			                                  		<option value="">Select Category</option>
				                                    @foreach($categories as $category)
				                                      	<option value="{{ $category->id }}">{{ucwords($category->name)}}</option>
				                                    @endforeach
			                                  	</select>
							                </div>
										  	<div class="form-group">
											  	<label for="" class="control-label mb-10">Choose file *</label>
											  	<input name="product_file" type="file" class="form-control" required>
										  	</div> 
									  	</div>
									  	<div class="modal-footer">
										  	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  	<button type="submit" class="btn btn-info">ADD</button>
									  	</div>
								  	</form>
							  	</div>
						  	</div>
					  	</div>
					@endif  	
		  		</div>
		  		<div class="panel-wrapper collapse in">
					<div class="panel-body">
			  			<div id="validation">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
			  			</div>  
			  			<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th>S.No</th>
											<th>Image</th>
											<th style="width: 20%">Name</th>
											<th style="width: 10%">Code</th>
											<th style="width: 5%">Price</th>
											<th style="width: 10%">Modified By</th>
											<th style="width: 10%">Modified At</th>
											@if ($allowed_operations['status'])
												<th style="width: 5%">Status</th>
											@endif
											@if ($allowed_operations['view'] || $allowed_operations['edit'])	
												<th style="width: 30%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($products as $product)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td><img class="image-circle " src="{{ $product->thumb_image_path }}" width="50" height="50" /></td> 
						  						<td>{{ ucwords($product->name) }}</td>
						  						<td>{{ ucwords($product->code) }}</td>
						  						<td>{{ number_format($product->price,2) }}</td>
						  						<td>{{ $product->modifiedBy->name }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($product->updated_at)) }}</td>
						  						@if ($allowed_operations['status'])
							  						<td>
														@if ($product->status)
														  	<a href="{{ url('admin/products',[$product->id]) }}/status/0" class="btn btn-success btn-xs">Active</a>
														@else    
														  	<a href="{{ url('admin/products',[$product->id]) }}/status/1" class="btn btn-danger btn-xs">Deactivate</a></td>
														@endif 
							  						</td>
							  					@endif
							  					@if ($allowed_operations['view'] || $allowed_operations['edit'])      
							   						<td>
							   							@if ($allowed_operations['view'])
						   									<a href="{{ url('admin/products',[$product->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
						   								@endif
							   							@if ($allowed_operations['edit'])                 
							 								<a href="{{ url('admin/products',[$product->id]) }}/edit" class="btn btn-danger btn-xs" ><span class="fa fa-edit" ></span></a>
							 							@endif	
							 								<a href="{{ url('admin/products',[$product->id]) }}/duplicate" class="btn btn-danger btn-xs" ><span >Duplicate</span></a>
							 								
							 								<a href="{{ url('admin/products',[$product->id]) }}/delete" class="btn btn-danger btn-xs delete-click"><span class="fa fa-trash"></span></a> 
							   						</td>
						   						@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
					</div>
		  		</div>
			</div>
	  	</div>
	</div>
   
@endsection
@section('scripts')	
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">  
	$(document).ready(function() {  

		$(function(){    
			$(document).on('click', '.delete-click', function(e){
				e.preventDefault();
					var url = $(this).attr("href");
				  //alert(url)
				  swal({
				  	title: 'Are you sure?',
				  	text: "You won't be able to recover once deleted!",
				  	type: 'warning',
				  	showCancelButton: false,
				  	confirmButtonColor: '#3085d6',
				  	cancelButtonColor: '#d33',
				  	confirmButtonText: 'Yes, delete it!'
				  }).then(function () {
				  	$.ajax({
				  		type: "get",
				  		url: url,
				  		success: function(response){
				  			if(response.status == "success")
				  				window.location.href="{!! url('/admin/products') !!}";
				  		},
				  	});
				 })
			});    
		});

		// $(".delete-click").click(function (event) {
		// 	event.preventDefault();
		// 	var url = $(this).attr("href");
		//   alert(url)
		//   swal({
		//   	title: 'Are you sure?',
		//   	text: "You won't be able to recover once deleted!",
		//   	type: 'warning',
		//   	showCancelButton: false,
		//   	confirmButtonColor: '#3085d6',
		//   	cancelButtonColor: '#d33',
		//   	confirmButtonText: 'Yes, delete it!'
		//   }).then(function () {
		//   	$.ajax({
		//   		type: "get",
		//   		url: url,
		//   		success: function(response){
		//   			if(response.status == "success")
		//   				window.location.href="{!! url('/admin/products') !!}";
		//   		},
		//   	});
		//   })
		//   // alert(url);
		// });
	});
</script>
@endsection