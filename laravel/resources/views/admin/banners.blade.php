@extends('admin.layouts.app') 
@section('styles')
	<style type="text/css">
		.table-header-colour{
			background-color: #262626; 
			color: #fff;
		}
		.image-circle{
			border-radius: 50%;
		}

	</style>
@endsection
@section('content')
  	<div class="row heading-bg">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	  		<h5 class="txt-dark">Banners</h5>
		</div>  
		<!-- Breadcrumb -->
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
				<li class="active"><span>Banners</span></li>
			</ol>
		</div>
		<!-- /Breadcrumb -->          
  	</div>
  	<div class="row">
	  	<div class="col-sm-12">
		  	<div class="panel panel-default card-view">
			  	<div class="panel-heading">
				  	<div class="pull-left">
					  <!-- <h6 class="panel-title txt-dark">Gallery Image</h6> -->
				  	</div>
				  	@if ($allowed_operations['add'])
				  		<div class="pull-right">
				  		   	<a href="{{ url('admin/banners/add') }}" class="btn btn-danger btn-circle  btn-sm forex-color" title="Add New" ><i class="fa fa-plus"></i></a>
				  		</div>
				  	@endif 	
			  	</div>
			  	<div class="panel-wrapper collapse in">
				  	<div class="panel-body">
						<div id="validation">
					  		@if ($errors->any())
						  		<div class="alert alert-danger">
							  		<ul>
								  		@foreach ($errors->all() as $error)
									  		<li>{{ $error }}</li>
								  		@endforeach
							  		</ul>
						  		</div>
					  		@endif
						</div>					  
						<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>               
											<th>Image</th>
											<th>Position</th>
											
											<th>Title3</th>
											<th>Modified By</th>
											<th>Modified At</th>
											@if ($allowed_operations['status'])
												<th style="width: 10%">Status</th>
											@endif
											@if ($allowed_operations['remove'] || $allowed_operations['edit'])	
												<th style="width: 12%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($banners as $banner)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td><img class="image-circle" src="{{ $banner->image_path }}" width="50" height="50" /></td> 
						  						<td>{{ $banner->position }}</td>
						  						
						  						<td>{!!$banner->content_small !!}</td>
						  						<td>{{ $banner->modifiedBy->name }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($banner->updated_at)) }}</td>
						  						@if ($allowed_operations['status'])
							  						<td>
														@if ($banner->status)
														  	<a href="{{ url('admin/banners',[$banner->id]) }}/status/0" class="btn btn-success btn-xs">Active</a>
														@else    
														  	<a href="{{ url('admin/banners',[$banner->id]) }}/status/1" class="btn btn-danger btn-xs">Deactivate</a>
														@endif 
							  						</td>
							  					@endif
							  					@if ($allowed_operations['remove'] || $allowed_operations['edit']) 	       
								   					<td>
								   						@if ($allowed_operations['edit'])
							 								<a href="{{ url('admin/banners',[$banner->id]) }}/edit" class="btn btn-danger btn-xs" ><span class="fa fa-edit" ></span></a>
							 							@endif	
									 					@if ($allowed_operations['remove'])	
								 							<a href="{{ url('admin/banners',[$banner->id]) }}/delete" class="btn btn-danger btn-xs delete-click"><span class="fa fa-trash"></span></a> 
								 						@endif	
								   					</td>
							   					@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
				  	</div>
			  	</div>
		  	</div>
	  	</div>
  	</div>
@endsection

@section('scripts')
  <script type="text/javascript">      
	  $(".delete-click").click(function (event) {
		  event.preventDefault();
		  var url = $(this).attr("href");
		  swal({
			  title: 'Are you sure?',
			  text: "You won't be able to recover once deleted!",
			  type: 'warning',
			  showCancelButton: false,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
		  }).then(function () {
			  $.ajax({
				  type: "get",
				  url: url,
				  success: function(response){
					  if(response.status == "success")
						  window.location.href="{!! url('/admin/banners') !!}";
				  },
			  });
		  })
		  // alert(url);
	  });
  </script>
@endsection
