	<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span>Sidebar</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
				@if(Auth::user()->role->code == 'SUPER_ADMIN')
					<li>
	                    <a class="" href="{{url('/admin/home')}}" ><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
					</li>

					<li>
	                    <a class="" href="{{url('/admin/users')}}" ><div class="pull-left"><i class="fa fa-users mr-20"></i><span class="right-nav-text">Admins</span></div><div class="clearfix"></div></a>
					</li>

					<li>
	                    <a class="" href="{{url('/admin/roles')}}" ><div class="pull-left"><i class="fa fa-user-md mr-20"></i><span class="right-nav-text">Roles</span></div><div class="clearfix"></div></a>
					</li>
					<li>
	                    <a class="" href="{{url('/admin/blocked-ips')}}" ><div class="pull-left"><i class="fa fa-lock mr-20"></i><span class="right-nav-text">Blocked Ip</span></div><div class="clearfix"></div></a>
					</li>
					<li>
	                    <a class="" href="{{url('/admin/menus')}}" ><div class="pull-left"><i class="fa fa-bars mr-20"></i><span class="right-nav-text">Menus</span></div><div class="clearfix"></div></a>
					</li>
					<li>
	                    <a class="" href="{{url('/admin/categories')}}" ><div class="pull-left"><i class="fa fa-angle-double-down mr-20"></i><span class="right-nav-text">Categories</span></div><div class="clearfix"></div></a>
					</li>
					<li>
	                    <a class="" href="{{url('/admin/pincodes')}}" ><div class="pull-left"><i class="fa fa-product-hunt mr-20"></i><span class="right-nav-text">Pincodes</span></div><div class="clearfix"></div></a>
					</li>
					<li>
	                    <a class="" href="{{url('/admin/attributes')}}" ><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text">Attributes</span></div><div class="clearfix"></div></a>
					</li>
					<li>
	                    <a class="" href="{{url('/admin/groups')}}" ><div class="pull-left"><i class="fa fa-usb mr-20"></i><span class="right-nav-text">Groups</span></div><div class="clearfix"></div></a>
					</li>
					<li>
	                    <a class="" href="{{url('/admin/customers')}}" ><div class="pull-left"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Customers</span></div><div class="clearfix"></div></a>
					</li>
					<li>
	                    <a class="" href="{{url('/admin/service-requests')}}" ><div class="pull-left"><i class="fa fa-inbox mr-20"></i><span class="right-nav-text">Service Requests</span></div><div class="clearfix"></div></a>
	                </li>
	                <li>    
	                    <a class="" href="{{url('/admin/products')}}" ><div class="pull-left"><i class="fa fa-life-ring mr-20"></i><span class="right-nav-text">Products</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/coupons')}}" ><div class="pull-left"><i class="fa fa-gift mr-20"></i><span class="right-nav-text">Coupons</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/vendors')}}" ><div class="pull-left"><i class="fa fa-bank mr-20"></i><span class="right-nav-text">Vendors</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/news-letters')}}" ><div class="pull-left"><i class="fa fa-envelope mr-20"></i><span class="right-nav-text">News Letters</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/banners')}}" ><div class="pull-left"><i class="fa fa-image mr-20"></i><span class="right-nav-text">Banners</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/brand-logos')}}" ><div class="pull-left"><i class="fa fa-file-image-o mr-20"></i><span class="right-nav-text">Brand Logos</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/product-groups')}}" ><div class="pull-left"><i class="fa fa-object-group mr-20"></i><span class="right-nav-text">Product Groups</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/product-review')}}" ><div class="pull-left"><i class="fa fa-object-group mr-20"></i><span class="right-nav-text">Product Review</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/orders')}}" ><div class="pull-left"><i class="fa fa-cubes mr-20"></i><span class="right-nav-text">Orders</span></div><div class="clearfix"></div></a>
					</li>
					<li>    
	                    <a class="" href="{{url('/admin/manual-billing')}}" ><div class="pull-left"><i class="fa fa-cubes mr-20"></i><span class="right-nav-text">Manual Billing</span></div><div class="clearfix"></div></a>
					</li>
				@elseif(!empty(Auth::user()->role->roleModules))
					<li>
	                    <a class="" href="{{url('/admin/home')}}" ><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
					</li>
					@foreach (Auth::user()->role->roleModules as $roleModule) 
						<li>
							<a href="javascript:void(0);" data-toggle="collapse" data-target="#{{ $roleModule->module->code }}"><div class="pull-left"><i class="{{ $roleModule->module->icon }} mr-20"></i><span class="right-nav-text">{{ ucwords($roleModule->module->name) }}</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a> 
							<ul id="{{ $roleModule->module->code }}" class="collapse collapse-level-1 two-col-list">
							@if(!empty($roleModule->roleModuleOperations))	
								@foreach ($roleModule->roleModuleOperations as $roleModuleOperation)
									@if($roleModuleOperation->operation->independent) 
										<li>
											<a href="{{ route($roleModuleOperation->operation->route) }}">{{ $roleModuleOperation->operation->name }}</a>
										</li>
									@endif	
								@endforeach
							@endif
							</ul>	
						</li>
					@endforeach	
				@endif
				
				
				<!-- <li>
                    <a class="" href="{{url('/admin/users')}}" ><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Users</span></div><div class="clearfix"></div></a>
				</li> -->
				
			</ul>
		</div>
		<!-- /Left Sidebar Menu -->
		  <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid pt-25">