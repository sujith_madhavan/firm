</div>        <!-- Footer -->
  <footer class="footer container-fluid pl-30 pr-30">
      <div class="row">
          <div class="col-sm-12">
              <p>Developed By <a target="_blank" href="http://www.ninositsolution.com">Ninos IT Solution</a></p>
          </div>
      </div>
  </footer>
</div>
</div>
   
			

<script src="{{asset('backend/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/dist/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('backend/dist/js/dropdown-bootstrap-extended.js')}}"></script>
<script src="{{asset('backend/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backend/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>



<script src="{{asset('backend/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backend/vendors/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('backend/dist/js/dataTables-data.js')}}"></script>
<script src="{{asset('backend/dist/js/init.js')}}"></script>
<script src="{{asset('backend/dist/js/dataTables-data.js')}}"></script>

<!-- <script src="{{asset('backend/assets/global/vendor/summernote/summernote.min.js')}}"></script>
<script src="{{asset('backend/assets/global/js/Plugin/summernote.js')}}"></script>
<script src="{{asset('backend/assets/examples/js/forms/editor-summernote.js')}}"></script> -->
@yield('scripts')

<script>
    $(document).ready(function(){
         if($('.fixed-sidebar-left .side-nav>li>a').hasClass('active')){
            $('.fixed-sidebar-left .side-nav>li>a.active').next('ul.collapse').addClass('in');
        }
        $('.multi-select').multiSelect();

        
    });


   setTimeout(function() {
       $('#validation').fadeOut('fast');
   }, 3000);  
      
</script>



</body>

</html>
