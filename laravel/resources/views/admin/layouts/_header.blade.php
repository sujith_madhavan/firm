<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Firmer Foundation</title>
	

	<link rel="apple-touch-icon" href="{{asset('backend/assets/images/firm_logo.png')}}">
    <link rel="shortcut icon" href="{{asset('backend/assets/images/firm_logo.png')}}">
	
	<link href="{{asset('backend/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
	
	<link href="{{asset('backend/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
	
	<!-- bootstrap-select CSS -->
	<link href="{{asset('backend/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css"/>	

	<link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
	
    <!-- <link href="{{asset('backend/vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css"> -->
	<!-- switchery CSS -->
	<link href="{{asset('backend/vendors/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet" type="text/css"/>
	
	<link href="{{asset('backend/vendors/bower_components/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css"/>
	
	<!-- vector map CSS -->
	<link href="{{asset('backend/vendors/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" type="text/css"/>
	
	<!-- Custom CSS -->
	<link href="{{asset('backend/dist/css/style.css')}}" rel="stylesheet" type="text/css">
	
	<script src="{{asset('backend/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('backend/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('backend/sweetalert2/dist/sweetalert2.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">

	<script type="text/javascript" src="{{asset('backend/jquery-timepicker/jquery.timepicker.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('backend/jquery-timepicker/jquery.timepicker.css')}}" />
	<script type="text/javascript" src="{{asset('backend/jquery-timepicker/lib/bootstrap-datepicker.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('backend/jquery-timepicker/lib/bootstrap-datepicker.css')}}" />
	
	<!-- <link rel="stylesheet" href="{{asset('backend/assets/global/vendor/summernote/summernote.css')}}"> -->
	<link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css') }}" />
	@yield('styles')
<!-- 	<script type="text/javascript" src="{{asset('backend/jquery-timepicker/lib/site.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('backend/jquery-timepicker/lib/site.css')}}" /> -->
    <!-- <script src="{{asset('backend/vendors/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script> -->
    
    <script>
         $(window).load(function(){           
        @if (session('status'))
            swal({
                title: "{{session('message')}}",
                type:"{{session('status')}}",
                timer: 2000,
                showConfirmButton:false,
                animation:false,
            });
        @endif            
    });
    
    </script>
</head>
<body>
    <div class="wrapper theme-4-active pimary-color-red slide-nav-toggle ">