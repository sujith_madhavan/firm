@extends('admin.layouts.app') 
@section('styles')	

<style type="text/css">
.has-error {
	color: #ef0a15;
}

.image-circle{
	border-radius: 50%;
}

.forex-color{
	background: #e63e0c;
}
.buttons-excel {
	background-color: #ea6c41 !important;
	border: solid 1px #ea6c41 !important;
}
</style>

@endsection
@section('content')	
<div class="row heading-bg">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-dark">Orders</h5>
	</div>  
	<!-- Breadcrumb -->
	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
		<ol class="breadcrumb">
			<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
			<li class="active"><span>Orders</span></li>
		</ol>
	</div>
	<!-- /Breadcrumb -->          
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">

					<div  class="tab-struct custom-tab-1 mt-40">
						<ul role="tablist" class="nav nav-tabs" id="myTabs_7">
							<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_7" href="#home_7">Buyer Confirmation ({{count($orders)}})</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="vendor_tab_7" role="tab" href="#profile_7" aria-expanded="false">Vendor Confirmation ({{count($order_vendor_confirmeds)}})</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_7" role="tab" href="#shipping_7" aria-expanded="false">Shipping Order ({{count($order_shipped_details)}})</a></li>
							<li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_7" role="tab" href="#delivery_7" aria-expanded="false">Delivered ({{count($order_delivered_details)}})</a></li>

						</ul>
						<div class="tab-content" id="myTabContent_7">
							<div  id="home_7" class="tab-pane fade active in" role="tabpanel">
								<div id="validation">
									@if ($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
									@endif
								</div>  
								<div class="table-wrap">
									<div class="table-responsive">
										<table id="datable_1" class="table table-bordered display">
											<thead>
												<tr>
													<th>S.No</th>
													<th>Customer Name</th>
													<th>Amount</th>
													<th>Transaction Id</th>
													<th>Transaction Status</th>
													<th>Order Status</th>
													<th>Invoice</th>
													<th>Updated At</th>
													@if ($allowed_operations['view'] || $allowed_operations['edit'] || $allowed_operations['status'])	
													<th style="width: 15%">Action</th>
													@endif
												</tr>
											</thead>
											<tbody>
												@php ($i = 1) 
												@foreach ($orders as $order)                        
												<tr>
													<td>{{ $i }}</td> 
													<td>{{ ucwords($order->customer->first_name) }}</td>
													<td>{{ number_format($order->amount,2) }}</td>
													<td>{{ $order->transaction_id }}</td>
													<td>{{ $order->transaction_status }}</td>
													<td>{{ $order->order_status }}</td>
													 <td>    
                                                <a href="{{url('/admin/customer/print/orders',[$order->id])}}"> <button class="btn btn-danger btn-xs" >Print Invoice</button></a>
                                            </td>
													<td>{{ date('d-m-Y h:i:s A', strtotime($order->updated_at)) }}</td>
													@if ($allowed_operations['view'] || $allowed_operations['edit'] || $allowed_operations['status'])      
													<td>
														@if ($allowed_operations['view'])
														<a href="{{ url('admin/orders',[$order->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
														@endif
														@if ($allowed_operations['edit'])                 
														<a href="{{ url('admin/orders',[$order->id]) }}/shipping-detail" class="btn btn-danger btn-xs" ><span class="fa fa-edit" ></span></a>
														@endif
													</td>
													@endif	
												</tr>
												@php ($i++) 
												@endforeach
											</tbady>
										</table>
									</div>
								</div>
							</div>
							<div  id="profile_7" class="tab-pane fade" role="tabpanel">  
								<div class="table-wrap">
									<div class="table-responsive">
										<table id="datable_1" class="table table-bordered display">
											<thead>
												<tr>
													<th>S.No</th>
													<th>Customer Name</th>
													<th>Amount</th>
													<th>Transaction Id</th>
													<th>Transaction Status</th>
													<th>Order Status</th>
													<th>Invoice</th>
													<th>Updated At</th>
													@if ($allowed_operations['view'] || $allowed_operations['edit'] || $allowed_operations['status'])	
													<th style="width: 15%">Action</th>
													@endif
												</tr>
											</thead>
											<tbody>
												@php ($i = 1) 
												@foreach ($order_vendor_confirmeds as $order)                        
												<tr>
													<td>{{ $i }}</td> 
													<td>{{ ucwords($order->customer->first_name) }}</td>
													<td>{{ number_format($order->amount,2) }}</td>
													<td>{{ $order->transaction_id }}</td>
													<td>{{ $order->transaction_status }}</td>
													<td>{{ $order->order_status }}</td>
													 <td>    
                                                <a href="{{url('/admin/customer/print/orders',[$order->id])}}"><button class="btn btn-danger btn-xs" >Print Invoice</button></a>
                                            </td>
													<td>{{ date('d-m-Y h:i:s A', strtotime($order->updated_at)) }}</td>
													@if ($allowed_operations['view'] || $allowed_operations['edit'] || $allowed_operations['status'])      
													<td>
														@if ($allowed_operations['view'])
														<a href="{{ url('admin/orders',[$order->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
														@endif
														@if ($allowed_operations['edit'])                 
														<a href="{{ url('admin/orders',[$order->id]) }}/shipping-detail" class="btn btn-danger btn-xs" ><span class="fa fa-edit" ></span></a>
														@endif
													</td>
													@endif	
												</tr>
												@php ($i++) 
												@endforeach
											</tbady>
										</table>
									</div>
								</div>
							</div>
						
						<div id="shipping_7" class="tab-pane fade " role="tabpanel">
							<div class="table-wrap">
								<div class="table-responsive">
									<table id="datable_1" class="table table-bordered display">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Customer Name</th>
												<th>Amount</th>
												<th>Transaction Id</th>
												<th>Transaction Status</th>
												<th>Order Status</th>
												<th>Invoice</th>
												<th>Updated At</th>
												@if ($allowed_operations['view'] || $allowed_operations['edit'] || $allowed_operations['status'])	
												<th style="width: 15%">Action</th>
												@endif
											</tr>
										</thead>
										<tbody>
											@php ($i = 1) 
											@foreach ($order_shipped_details as $order)                        
											<tr>
												<td>{{ $i }}</td> 
												<td>{{ ucwords($order->customer->first_name) }}</td>
												<td>{{ number_format($order->amount,2) }}</td>
												<td>{{ $order->transaction_id }}</td>
												<td>{{ $order->transaction_status }}</td>
												<td>{{ $order->order_status }}</td>
												 <td>    
                                                <a href="{{url('/admin/customer/print/orders',[$order->id])}}"> <button class="btn btn-danger btn-xs" >Print Invoice</button>  </a>
                                            </td>
												<td>{{ date('d-m-Y h:i:s A', strtotime($order->updated_at)) }}</td>
												@if ($allowed_operations['view'] || $allowed_operations['edit'] || $allowed_operations['status'])      
												<td>
													@if ($allowed_operations['view'])
													<a href="{{ url('admin/orders',[$order->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
													@endif
													@if ($allowed_operations['edit'])                 
													<a href="{{ url('admin/orders',[$order->id]) }}/shipping-detail" class="btn btn-danger btn-xs" ><span class="fa fa-edit" ></span></a>
													@endif
												</td>
												@endif	
											</tr>
											@php ($i++) 
											@endforeach
										</tbady>
									</table>
								</div>
							</div>
						</div>
					<div id="delivery_7" class="tab-pane fade" role="tabpanel">
						<div class="table-wrap">
							<div class="table-responsive">
								<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th>S.No</th>
											<th>Customer Name</th>
											<th>Amount</th>
											<th>Transaction Id</th>
											<th>Transaction Status</th>
											<th>Order Status</th>
											<th>Invoice</th>
											<th>Updated At</th>
											@if ($allowed_operations['view'] || $allowed_operations['edit'] || $allowed_operations['status'])	
											<th style="width: 15%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
										@php ($i = 1) 
										@foreach ($order_delivered_details as $order)                        
										<tr>
											<td>{{ $i }}</td> 
											<td>{{ ucwords($order->customer->first_name) }}</td>
											<td>{{ number_format($order->amount,2) }}</td>
											<td>{{ $order->transaction_id }}</td>
											<td>{{ $order->transaction_status }}</td>
											<td>{{ $order->order_status }}</td>
											 <td>    
                                                <a href="{{url('/admin/customer/print/orders',[$order->id])}}"><button class="btn btn-danger btn-xs" >Print Invoice</button></a>
                                            </td>
											<td>{{ date('d-m-Y h:i:s A', strtotime($order->updated_at)) }}</td>
											@if ($allowed_operations['view'] || $allowed_operations['edit'] || $allowed_operations['status'])      
											<td>
												@if ($allowed_operations['view'])
												<a href="{{ url('admin/orders',[$order->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
												@endif
												@if ($allowed_operations['edit'])                 
												<a href="{{ url('admin/orders',[$order->id]) }}/shipping-detail" class="btn btn-danger btn-xs" ><span class="fa fa-edit" ></span></a>
												@endif
											</td>
											@endif	
										</tr>
										@php ($i++) 
										@endforeach
									</tbady>
								</table>
							</div>
						</div>
					</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>

</div>







@endsection
@section('scripts')	

@endsection