@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}

		.forex-color{
			background: #e63e0c;
		}

		.buttons-excel {
		  	background-color: #ea6c41 !important;
		  	border: solid 1px #ea6c41 !important;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	  	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h5 class="txt-dark">Vendors</h5>
	  	</div>  
	  	<!-- Breadcrumb -->
	  	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
		  		<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
		  		<li class="active"><span>Vendors</span></li>
			</ol>
	  	</div>
	  	<!-- /Breadcrumb -->          
	</div>
	<div class="row">
	  	<div class="col-sm-12">
			<div class="panel panel-default card-view">
		  		<div class="panel-heading">
					<div class="pull-left">
					  <!-- <h6 class="panel-title txt-dark">Services</h6> -->
					</div>
		  		</div>
		  		<div class="panel-wrapper collapse in">
					<div class="panel-body">
			  			<div id="validation">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
			  			</div>  
			  			<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>
											<th>Image</th>                
											<th>Business Name</th>
											<th>Mobile</th>
											<th>Pincode</th>
											<th>Modified By</th>
											<th>Modified At</th>
											@if ($allowed_operations['status'])
												<th style="width: 10%">Status</th>
											@endif
											@if ($allowed_operations['view'])
												<th style="width: 5%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($vendors as $vendor)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td><img class="image-circle" src="{{ $vendor->image_path }}" width="50" height="50" /></td>
						  						<td>{{ $vendor->business_name }}</td>
						  						<td>{{ $vendor->mobile }}</td>
						  						<td>{{ $vendor->pincode }}</td>
						  						<td>
						  							@if(!empty($vendor->modifiedBy))
						  								{{ $vendor->modifiedBy->name }}
						  							@endif
						  						</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($vendor->updated_at)) }}</td>
						  						@if ($allowed_operations['status'])
							  						<td>
														@if ($vendor->status)
														  	<a href="{{ url('admin/vendors',[$vendor->id]) }}/status/0" class="btn btn-success btn-xs">Verified</a>
														@else    
														  	<a href="{{ url('admin/vendors',[$vendor->id]) }}/status/1" class="btn btn-danger btn-xs">Not Verified</a></td>
														@endif 
							  						</td>
							  					@endif
							  					@if ($allowed_operations['view'])      
							   						<td>
							   							@if ($allowed_operations['view'])
						   									<a href="{{ url('admin/vendors',[$vendor->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
						   								@endif	
							   						</td>
						   						@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
					</div>
		  		</div>
			</div>
	  	</div>
	</div>
   
@endsection
@section('scripts')	


@endsection