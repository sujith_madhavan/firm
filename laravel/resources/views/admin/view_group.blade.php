@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.image-circle{
			border-radius: 50%;
		}

	</style>
	
@endsection
@section('content')
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <!-- <h5 class="txt-dark">Edit User</h5> -->
        </div>  
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/home') }}">Dashboard</a></li>
                <li class="active"><span>View-Group</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->                    
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">View Group</h6>
                    </div>
                    <div class="pull-right">
                       <a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Name : </label>
                                    <label>{{ $group->name }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Modified By : </label>
                                    <label>{{  $group->modifiedBy->name }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Created At : </label>
                                    <label>{{ date('d-m-Y h:i:s A', strtotime($group->created_at)) }}</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Updated At : </label>
                                    <label>{{ date('d-m-Y h:i:s A', strtotime($group->updated_at)) }}</label>
                                </div>
                            </div>
                            <div class="col-sm-6">    
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Attributes</label><br>
                                    @foreach ($group->groupAttributes as $groupAttribute)
                                        <div class="checkbox checkbox-danger">
                                            <input id="checkbox6" type="checkbox" disabled checked>
                                            <label for="checkbox6">{{ $groupAttribute->attribute->name }}</label>
                                        </div> 
                                    @endforeach
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->	
   
@endsection
