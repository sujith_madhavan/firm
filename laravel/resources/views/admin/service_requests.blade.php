@extends('admin.layouts.app') 
@section('styles')	

	<style type="text/css">
		.has-error {
		    color: #ef0a15;
		}

		.image-circle{
			border-radius: 50%;
		}

		.forex-color{
			background: #e63e0c;
		}

	</style>
	
@endsection
@section('content')	
	<div class="row heading-bg">
	  	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h5 class="txt-dark">Service Requests</h5>
	  	</div>  
	  	<!-- Breadcrumb -->
	  	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
		  		<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
		  		<li class="active"><span>Service Requests</span></li>
			</ol>
	  	</div>
	  	<!-- /Breadcrumb -->          
	</div>
	<div class="row">
	  	<div class="col-sm-12">
			<div class="panel panel-default card-view">
		  		<div class="panel-heading">
					<div class="pull-left">
					  <!-- <h6 class="panel-title txt-dark">Services</h6> -->
					</div>
		  		</div>
		  		<div class="panel-wrapper collapse in">
					<div class="panel-body">
			  			<div id="validation">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
			  			</div>  
			  			<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>                    
											<th>Name</th>
											<th>Email</th>
											<th>Mobile</th>
											<th>Service</th>
											<th>Created At</th>
											@if ($allowed_operations['view'] || $allowed_operations['remove'])	
												<th style="width: 12%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($service_requests as $service_request)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td>{{ $service_request->name }}</td>
						  						<td>{{ $service_request->email }}</td>
						  						<td>{{ $service_request->mobile }}</td>
						  						<td>{{ $service_request->service }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($service_request->updated_at)) }}</td>
							  					@if ($allowed_operations['view'] || $allowed_operations['remove'])      
							   						<td>
							   							@if ($allowed_operations['view'])
						   									<a href="{{ url('admin/service-requests',[$service_request->id]) }}/view" class="btn btn-danger btn-xs" ><span class="fa fa-file-text" ></span></a>
						   								@endif
							   							@if ($allowed_operations['remove'])     
							 								<a href="{{ url('admin/service-requests',[$service_request->id]) }}/remove" class="btn btn-danger btn-xs delete-click" ><span class="fa fa-trash" ></span></a>
							 							@endif		
							   						</td>
						   						@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
					</div>
		  		</div>
			</div>
	  	</div>
	</div>
   
@endsection
@section('scripts')	
	<script type="text/javascript">      
		$(".delete-click").click(function (event) {
			event.preventDefault();
			var url = $(this).attr("href");
			swal({
				title: 'Are you sure?',
				text: "You won't be able to recover once deleted!",
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then(function () {
				$.ajax({
					type: "get",
					url: url,
					success: function(response){
						if(response.status == "success")
							  window.location.href="{!! url('/admin/service-requests') !!}";
					},
				});
			})
			  // alert(url);
		});
	</script>
@endsection