@extends('admin.layouts.app') 
@section('styles')
	<style type="text/css">
		.table-header-colour{
			background-color: #262626; 
			color: #fff;
		}
	</style>
@endsection
@section('content')
  	<div class="row heading-bg">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	  		<h5 class="txt-dark">Product Groups</h5>
		</div>  
		<!-- Breadcrumb -->
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
				<li class="active"><span>Product Groups</span></li>
			</ol>
		</div>
		<!-- /Breadcrumb -->          
  	</div>
  	<div class="row">
	  	<div class="col-sm-12">
		  	<div class="panel panel-default card-view">
			  	<div class="panel-heading">
				  	<div class="pull-left">
					  <!-- <h6 class="panel-title txt-dark">Gallery Image</h6> -->
				  	</div>
				  	@if ($allowed_operations['add'])
					  	<div class="pull-right">
						 	<a href="" class="btn btn-danger btn-circle  btn-sm" title="Add New" data-toggle="modal" data-target="#myModal" ><i class="fa fa-plus"></i></a>
					  	</div>
					  	<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
						  	<div class="modal-dialog">
							  	<div class="modal-content">
								  	<div class="modal-header">
									  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									  	<h5 class="modal-title" id="myModalLabel">Add Pincode</h5>
								  	</div>
								  	<form action="{{ url('admin/product-groups/add') }}" method="post" enctype="multipart/form-data">
									  	{{csrf_field()}}
									  	<div class="modal-body">
										  	<div class="form-group">
											  	<label for="" class="control-label mb-10">Name</label>
											  	<input name="name" type="text" class="form-control" required>
										  	</div> 
									  	</div>
									  	<div class="modal-footer">
										  	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  	<button type="submit" class="btn btn-info">ADD</button>
									  	</div>
								  	</form>
							  	</div>
						  	</div>
					  	</div>
					@endif  	
			  	</div>
			  	<div class="panel-wrapper collapse in">
				  	<div class="panel-body">
						<div id="validation">
					  		@if ($errors->any())
						  		<div class="alert alert-danger">
							  		<ul>
								  		@foreach ($errors->all() as $error)
									  		<li>{{ $error }}</li>
								  		@endforeach
							  		</ul>
						  		</div>
					  		@endif
						</div>					  
						<div class="table-wrap">
							<div class="table-responsive">
				  				<table id="datable_1" class="table table-bordered display">
									<thead>
										<tr>
											<th style="width: 8%">S.No</th>    
											<th>Name</th>
											<th>Products</th>
											<th>Modified By</th>
											<th>Modified At</th>
											@if ($allowed_operations['status'])
												<th style="width: 10%">Status</th>
											@endif
											@if ($allowed_operations['edit'])
												<th style="width: 12%">Action</th>
											@endif
										</tr>
									</thead>
									<tbody>
					  					@php ($i = 1) 
					  					@foreach ($product_groups as $product_group)                        
											<tr>
						  						<td>{{ $i }}</td>
						  						<td>{{ $product_group->name }}</td>
						  						<td>
						  							@foreach($product_group->groupProducts as $group_product)
						  								{{ $group_product->product->name }},
						  							@endforeach
						  						</td>
						  						<td>{{ $product_group->modifiedBy->name }}</td>
						  						<td>{{ date('d-m-Y h:i:s A', strtotime($product_group->updated_at)) }}</td>
						  						@if ($allowed_operations['status'])
							  						<td>
														@if ($product_group->status)
														  	<a href="{{ url('admin/product-groups',[$product_group->id]) }}/status/0" class="btn btn-success btn-xs">Active</a>
														@else    
														  	<a href="{{ url('admin/product-groups',[$product_group->id]) }}/status/1" class="btn btn-danger btn-xs">Deactivate</a>
														@endif 
							  						</td>
							  					@endif
							  					@if ($allowed_operations['edit']) 	       
								   					<td>
								   						@if ($allowed_operations['edit'])     
										 					<a href="" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal{{ $product_group->id }}"><span class="fa fa-edit" ></span></a>
										 					
											 				<div id="myModal{{ $product_group->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
									   							<div class="modal-dialog">
										 							<div class="modal-content">
										   								<div class="modal-header">
											 								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											 								<h5 class="modal-title" id="myModalLabel">Edit Product Group</h5>
										   								</div>
										   								<form action="{{ url('admin/product-groups/') }}/{{ $product_group->id }}/edit" method="post" enctype="multipart/form-data">
											 								{{csrf_field()}}
											 								<div class="modal-body">
																			   <div class="form-group">
																				 <label for="" class="control-label mb-10">Name</label>
																				 <input name="name" type="text" class="form-control" required value="{{ $product_group->name }}">
																			   </div>
											 								</div>
											 								<div class="modal-footer">
																			   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																			   <button type="submit" class="btn btn-info">Update</button>
											 								</div>
										   								</form>
										 							</div>
									   							</div>
									 						</div>
									 					@endif	
								   					</td>
							   					@endif	
											</tr>
											@php ($i++) 
					  					@endforeach
									</tbady>
				  				</table>
							</div>
			  			</div>
				  	</div>
			  	</div>
		  	</div>
	  	</div>
  	</div>
@endsection

@section('scripts')

@endsection
