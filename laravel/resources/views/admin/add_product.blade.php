@extends('admin.layouts.app') 
@section('styles')

<link href="{{asset('backend/vendors/bower_components/jquery-wizard.js/css/wizard.css')}}" rel="stylesheet" type="text/css"/>

<!-- jquery-steps css -->
<link rel="stylesheet" href="{{asset('backend/vendors/bower_components/jquery.steps/demo/css/jquery.steps.css')}}">
<link href="{{asset('backend/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />


<style type="text/css">
.has-error {
	color: #ef0a15;
	}.allsplits{
		background: #fff;
		/* padding: 0; */
		overflow-y: scroll;
		height: 300px;
	}
	#attribute{
		width: 90%;
		margin: 0 0 0 10px;
	}
	#attribute .checkbox{

		display: inline-block !important;
	}
.has-error {
	color: #ef0a15;
}
.code-container{
	position:relative;
}
#get-code{
    background: #fff;
    width: 100%;
    left: 0;
    max-height: 200px;
    position: absolute;
    overflow-y: auto;
    padding: 0;
    display: none;
    height:auto;
    overflow-x: hidden;

}
#get-code li{
	padding:5px 10px;
	cursor:pointer;
}
#get-code li:hover{
	background:#eee;
}
</style>

<style type="text/css">

</style>

@endsection
@section('content')	
<div class="row heading-bg">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<!-- <h5 class="txt-dark">Edit User</h5> -->
	</div>  
	<!-- Breadcrumb -->
	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
		<ol class="breadcrumb">
			<li><a href="{{ url('admin/home') }}">Dashboard</a></li>
			<li><a href="{{ url('admin/products') }}">Product List</a></li>
			<li class="active"><span>Add-Product</span></li>
		</ol>
	</div>

	
	<!-- /Breadcrumb -->                    
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Create Product</h6>
				</div>
				<div class="pull-right">
					<a href="{{ url()->previous() }}" class="btn btn-danger" title="Back" >Back</i></a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div id="validation">
						@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
					</div>
					<div class="form-wrap">
						<form id="example-advanced-form" method="post" action="{{ url('admin/products/add') }}" enctype="multipart/form-data">
							{{ csrf_field() }}
							<h3><span class="number"><i class="icon-user-following txt-black"></i></span><span class="head-font capitalize-font">Basic</span></h3>
							<fieldset>
								<div class="row">
									<div class="form-wrap col-md-6 col-lg-6">
										<div class="form-group code-container {{$errors->has('name')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Name *</label>
											<input type="text" class="form-control"  id="product_name" name="name" class="form-control"  placeholder="product" required>

											@if($errors->has('name'))
											<span class="help-block">{{ $errors->first('name') }}</span>
											@endif
											<ul id="get-code">
											</ul>
										</div>

										<div class="form-group {{$errors->has('code')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Code *</label>

											<input type="text" id="product_code" name="code" class="form-control" value="{{ old('code') }}" required ">

											@if($errors->has('code'))
											<span class="help-block">{{ $errors->first('code') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('hsc_code')?'has-error':''}}">
											<label class="control-label mb-10 text-left">HSN Code</label>
											<input type="text" name="hsc_code" class="form-control" value="{{ old('hsc_code') }}">
											@if($errors->has('hsc_code'))
											<span class="help-block">{{ $errors->first('hsc_code') }}</span>
											@endif
										</div>

										<div class="form-group {{$errors->has('old_price')?'has-error':''}} ">
											<label class="control-label mb-10 text-left">Basic Price*</label>
											<input type="text" id="myContent" class="form-control basic_price"  name="old_price" value="{{ old('old_price') }}" required>

											@if($errors->has('old_price'))
											<span class="help-block">{{ $errors->first('old_price') }}</span>
											@endif
										</div>

										<div class="form-group {{$errors->has('new_price')?'has-error':''}} " >
											<label class="control-label mb-10 text-left">Special MRP Price</label><br> 
											<input type="text" class="form-control basic_price" id="mymrp"  name="new_price" value="{{ old('new_price') }}" >

											@if($errors->has('new_price'))
											<span class="help-block">{{ $errors->first('new_price') }}</span>
											@endif
										</div>
										
										<div class="input-group dates {{$errors->has('start_date')?'has-error':''}} "" style="display:none">
											<input type="text" class="form-control datepicker-autoclose" name="start_date" placeholder="Start Date"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
											<input type="text" class="form-control datepicker-autoclose" name="end_date"  placeholder="End Date"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
											@if($errors->has('start_date'))
											<span class="help-block">{{ $errors->first('start_date') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('discount')?'has-error':''}} special_mrp" style="display:none">
											<label class="control-label mb-10 text-left ">Discount</label><br> 
											<input type="text" class="form-control " id="discounts" name="discount" value="{{ old('discount') }}" >
											@if($errors->has('discount'))
											<span class="help-block">{{ $errors->first('discount') }}</span>
											@endif
										</div>
									</div>
									<div class="form-wrap col-md-6 col-lg-6">
										<div class="form-group {{$errors->has('price_for')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Price For*</label><br> 

											<select name="price_for"  class="form-control" required>
												<option value="">Select Price For</option>
												<option value="Kg">Kg</option>
												<option value="Piece">Piece</option>
												<option value="Box">Box</option>
												<option value="Tin">Tin</option>
												<option value="Sq.Ft">Sq.Ft</option>
												<option value="Bag">Bag</option>
												<option value="CFT">CFT</option>
												<option value="Litre">Litre</option>
												<option value="Metre">Metre</option>
												<option value="Sheet">Sheet</option>
											</select>
											@if($errors->has('price_for'))
											<span class="help-block">{{ $errors->first('price_for') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('box_quantity')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Box Quantity</label><br> 
											<input type="number" class="form-control"  name="box_quantity" value="{{ old('box_quantity') }}" >
											@if($errors->has('box_quantity'))
											<span class="help-block">{{ $errors->first('box_quantity') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('stock')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Stock *</label><br> 
											<input type="number" class="form-control"  name="stock" value="{{ old('stock') }}" required>
											@if($errors->has('stock'))
											<span class="help-block">{{ $errors->first('stock') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('gst')?'has-error':''}}">
											<label class="control-label mb-10 text-left">GST *</label><br> 
											<input type="number" class="form-control"  name="gst" value="{{ old('gst') }}" required>
											@if($errors->has('gst'))
											<span class="help-block">{{ $errors->first('gst') }}</span>
											@endif
										</div>
										<div class="form-group {{$errors->has('minimum_quantity')?'has-error':''}}">
											<label class="control-label mb-10 text-left">Minimum Quantity *</label><br> 
											<input type="number" class="form-control"  name="minimum_quantity" value="{{ old('minimum_quantity') }}" required>
											@if($errors->has('minimum_quantity'))
											<span class="help-block">{{ $errors->first('minimum_quantity') }}</span>
											@endif
										</div>
										<label class="control-label mb-10 text-left">Product Display In : </label><br>
										<div class="col-md-12 col-lg-12">
											<div class="col-md-3 col-lg-3">
												<div class="checkbox checkbox-danger checkbox-circle " style="display: inline;">
													<input type="checkbox" name="featured" value="1">
													<label>Featured</label>
												</div>
											</div>	
											<div class="col-md-2 col-lg-2">
												<div class="checkbox checkbox-danger checkbox-circle " style="display: inline;">
													<input type="checkbox" name="new_product" value="1">
													<label>New</label>
												</div>
											</div>	
											<div class="col-md-4 col-lg-4">	
												<div class="checkbox checkbox-danger checkbox-circle " style="display: inline;">	
													<input type="checkbox" name="top_selling" value="1">
													<label>Top Selling</label>
												</div>	
											</div> 
										</div> 
									</div>	
								</div>
							</fieldset>

							<h3><span class="number"><i class="icon-bag txt-black"></i></span><span class="head-font capitalize-font">Attribute</span></h3>
							<fieldset>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-wrap">
											<div class="form-wrap col-md-6 col-lg-6">
												<div class="form-group">
													<label class="control-label mb-10 text-left">Brand</label>
													<br>
													<select class="form-control" id="brand_id" name="brand_id">
														<option value="">Select Brand</option>
														@foreach($brand_logos as $brand_logo)
														<option value="{{ $brand_logo->id }}">{{ ucwords($brand_logo->name) }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group {{$errors->has('product_group')?'has-error':''}}">
													<label class="control-label mb-10 text-left">Select Product Group</label>
													<br>
													<select class="form-control" id="product_group" name="product_group">
														<option value="">Select Product Group</option>
														@foreach($product_groups as $product_group)
														<option value="{{ $product_group->id }}">{{ ucwords($product_group->name) }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group {{$errors->has('categories')?'has-error':''}}">

													<label class="control-label mb-10 text-left">Select  Categories* </label>

													<br>
													<select class="form-control multiple-checkboxes" id="categories" name=categories[] multiple="multiple" onchange="get_attributes();">
														@foreach($categories as $category)
														<option value="{{ $category->id }}">{{ucwords($category->name)}}</option>
														@if(!empty($category->childCategories))
														<optgroup label="{{ ucwords($category->name) }}">
															@foreach($category->childCategories as $child_category)
															<option value="{{ $child_category->id }}">{{ ucwords($child_category->name) }}</option>
															@if(!empty($child_category->childCategories))
															<optgroup label="{{ ucwords($category->name) }}">
																@foreach($child_category->childCategories as $child_category2)
																<option value="{{ $child_category2->id }}">{{ ucwords($child_category2->name) }}</option>
																@endforeach
															</optgroup>	
															@endif
															@endforeach
														</optgroup>	
														@endif
														@endforeach
													</select>
												</div>
												<div class="form-group {{$errors->has('related_categories')?'has-error':''}}">
													<label class="control-label mb-10 text-left">Select Related Categories * </label>
													<br>
													<select class="form-control multiple-checkboxes" id="related_categories" name=related_categories[] multiple="multiple">
														@foreach($categories as $category)
														<option value="{{ $category->id }}">{{ucwords($category->name)}}</option>
														@if(!empty($category->childCategories))
														<optgroup label="{{ ucwords($category->name) }}">
															@foreach($category->childCategories as $child_category)
															<option value="{{ $child_category->id }}">{{ ucwords($child_category->name) }}</option>
															@if(!empty($child_category->childCategories))
															<optgroup label="{{ ucwords($category->name) }}">
																@foreach($child_category->childCategories as $child_category2)
																<option value="{{ $child_category2->id }}">{{ ucwords($child_category2->name) }}</option>
																@endforeach
															</optgroup>	
															@endif
															@endforeach
														</optgroup>	
														@endif
														@endforeach
													</select>
												</div>

											</div>
											<div class="form-wrap col-md-6 col-lg-6 allsplits ">     
												<div class="form-group">
													<label class="control-label mb-10 text-left">Attributes *</label><br> 
												</div>
												<div class="form-group" id="attribute">
												</div> 
											</div>
										</div>
									</div>

								</div>
							</fieldset>

							<h3><span class="number"><i class="icon-credit-card txt-black"></i></span><span class="head-font capitalize-font">Images</span></h3>
							<fieldset>
								<!--CREDIT CART PAYMENT-->
								<div class="row">
									<div class="col-sm-12">
										<div class="col-md-12">
											<table  class="table table-hover small-text" id="tb">
												<tr>
													<th style="font-weight: bold;">
														Choose Image / Video
														<br>
														( Image minimum dimension 425X400 )
													</th>
													<th style="width: 5%"></th>
													<th><a href="javascript:void(0);" style="font-size:18px; text-align: right;" id="addMore" title="Add More Row"><span class="glyphicon glyphicon-plus"></span></a></th>
												</tr>
												<tbody id="rowAppend">
													<tr>
														<td>
															<input type="file" id="image1" name="image1" class="form-control">
														</td>
														<td>
															( OR )
														</td>
														<td>
															<input type="text" id="video1" name="video1" class="form-control" placeholder="Youtube URL">
														</td>
													</tr>
												</tbody>
											</table>
										</div>	

									</div>
								</div>
								<!--CREDIT CART PAYMENT END-->
							</fieldset>

							<h3><span class="number"><i class="icon-basket-loaded txt-black"></i></span><span class="head-font capitalize-font">Description</span></h3>
							<fieldset>
								<div class="form-wrap col-md-12 col-lg-12">
									<div class="form-group {{$errors->has('description')?'has-error':''}}">
										<label class="control-label mb-10 text-left">Description *</label>

										<textarea class="textarea_editor form-control" name="description" required></textarea>
										@if($errors->has('description'))
										<span class="help-block">{{ $errors->first('description') }}</span>
										@endif
									</div>
									<div style="text-align: right;">
										<button class="btn btn-danger"  role="button" type="submit">Submit</button>
									</div> 
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

@endsection


@section('scripts') 


<script src="{{ asset('backend/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<!-- Form Wizard JavaScript -->
<script src="{{ asset('backend/bower_components/jquery.steps/build/jquery.steps.min.js') }}"></script>
<script src="{{ asset('backend/dist/js/jquery.validate.min.js')}}"></script>

<!-- Form Wizard Data JavaScript -->
<script src="{{ asset('backend/dist/js/form-wizard-data.js') }}"></script>

<!-- wysuhtml5 Plugin JavaScript -->
<script src="{{ asset('backend/bower_components/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>

<script src="{{ asset('backend/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>

<!-- Bootstrap Wysuhtml5 Init JavaScript -->
<script src="{{ asset('backend/dist/js/bootstrap-wysuhtml5-data.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function(){
		$("#product_name").keyup(function(){
			var code = $(this).val();

			$.ajax({
				url:'{{url("admin/get_product")}}',
				method:'POST',
				data:{_token:'{{csrf_token()}}',code:code},
				dataType:'html',
				success:function(result){
					if(result!=''){
						$('#get-code').html(result).show();
					}
					else{
						$('#get-code').html('No results found').show();
					}
					
				},
				error:function(error){
				}
			})

		});
		$(document).on('click','#get-code li',function(){
			$('#product_name').val($(this).text());
			$('#get-code').html('').hide();

		})
		$("#product_name").blur(function(){
				$('#get-code').html('').hide();
		});

		
	});

</script>

<script>
	$(function(){
		var i =1
		$('#addMore').on('click', function() {

			if(i < 4){
				i++;
				$('<tr><td><input type="file" name="image'+i+'" class="form-control" ></td><td>( OR )</td><td><input type="text" name="video'+i+'" class="form-control"  placeholder="Youtube URL"></td>').appendTo($('#rowAppend'));


			}
			else{
				swal("Sorry!! Only four rows Can be added");
			}

		});

	}); 

</script>
<script type="text/javascript">
	$(document).ready(function() {

		$("#example-advanced-form").submit(function(e){
			e.preventDefault();         	
			var categories = $("#categories").val();
			var related_categories = $("#related_categories").val();
			var video1 = $("#video1").val(); 
			var group_id = $('#group_id').val();           	
			if (categories == null) {
				swal("Select Categories.");
				e.preventDefault();
			}
			if (related_categories == null) {
				swal("Select Related Categories.");
				e.preventDefault();
			}
			if (group_id <= 0) {
				swal("Select Group.");
				e.preventDefault();
			}
			if(document.getElementById("image1").files.length == 0 && video1 == ""){
				swal("Atleast one image or video required");
				e.preventDefault();

			}

			var product_name = $("#product_name").val();
			var product_code = $("#product_code").val();

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: "{{ url('admin/check-product-details') }}",
				type: 'post',
				data: {product_name:product_name,
					product_code:product_code
				},
				dataType: 'json',
				success:function(response)
				{
					console.log(response);
					if(response.status == "success")
						$("#example-advanced-form")[0].submit();
					else
						swal(response.msg);

				},
				error: function(error)
				{
					console.log(error);
				}

			});


		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.multiple-checkboxes').multiselect();  
	});


	function get_attributes()
	{
		var category_ids = $("#categories").val();
		if (categories != null) {	
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: "{{ url('admin/getattributes') }}",
				type: 'post',
				data: {category_ids:category_ids},
				dataType: 'json',
				success:function(response)
				{
					$("#attribute").html(response.content);

					// var len = response.length;
					//alert(len);
					//         	$("#attribute").empty();
					//         	for( var i = 0; i<len; i++)
					//             {
					//                 var id = response[i]['id'];
					//                 var name = response[i]['name'];
					//                 $("#attribute").append("<div class='form-group'><label class='control-label mb-10 text-left'>"+name+"<input  type='text' class='form-control' name="+name+" ></div>");

					// }
					//$("#attribute").val(response);
				},
				error: function(error)
				{
					console.log(error);
				}

			});

		}    
	}

</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.basic_price').on('input', function() {
        //alert(this.value);
        var discount;
        var basic_price = $('#myContent').val();
        var mrp_price = $('#mymrp').val();
        //alert(basic_price);
        //alert(mrp_price);
        if(mrp_price!='')
        {
        	discount = (mrp_price * 100) / basic_price;
			//alert(discount);
			total =100-discount;

			$('.special_mrp').show();
			$('#discounts').val(Math.round(total));

		}
		else
		{
			$('.special_mrp').hide();
		}
	});
	});

</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.basic_price').on('input', function() {
        //alert(this.value);
       	//var discount;
        //var basic_price = $('#myContent').val();
        var mrp_price = $('#mymrp').val();
        //alert(basic_price);
        //alert(mrp_price);
        if(mrp_price!='' || mrp_price!= 0)
        {
        	$('.dates').show();
        }
        else
        {
        	$('.dates').hide();
        }
    });
	});

</script>
<script>
    // Clock pickers
    $(document).ready(function() {
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('.datepicker-autoclose').datepicker({
    	autoclose: true,
    	todayHighlight: true
    });
    jQuery('#date-range').datepicker({
    	toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
    	todayHighlight: true
    });
    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
    	buttonClasses: ['btn', 'btn-sm'],
    	applyClass: 'btn-danger',
    	cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
    	timePicker: true,
    	format: 'MM/DD/YYYY h:mm A',
    	timePickerIncrement: 30,
    	timePicker12Hour: true,
    	timePickerSeconds: false,
    	buttonClasses: ['btn', 'btn-sm'],
    	applyClass: 'btn-danger',
    	cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
    	format: 'MM/DD/YYYY',
    	minDate: '06/01/2015',
    	maxDate: '06/30/2015',
    	buttonClasses: ['btn', 'btn-sm'],
    	applyClass: 'btn-danger',
    	cancelClass: 'btn-inverse',
    	dateLimit: {
    		days: 6
    	}
    });
});
</script>

@endsection