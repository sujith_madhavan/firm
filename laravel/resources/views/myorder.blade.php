@php ($page = "order")
@php ($customer_sidebar = "order")
@extends('layouts.app')
@section('styles')
    <link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection 
@section('content') 
<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
               @include('layouts._customer_sidebar')
                <div class="col-md-9 dashboard-right tabcontent" id="address">
                    <div class="reg-head">
                        <h2><img src="{{asset('frontend/images/cart-dashboard.png')}}"> My Order</h2>
                    </div>

                    <div class="log-border">
                        <div class="product-review">
                            <div class="reg-head">
                                <h2>ORDER</h2>
                            </div>
                          <div class="table-responsive">
                                  <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                           <th>S.No</th>
                                            <th>Order Id</th>
                                            <th>Order Date</th>
                                            <th>Status</th>
                                            <th>Amount</th>
                                            <th>View</th>
                                            <th>Print</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          @php ($i = 1) 
                                        @foreach ($orders as $order)
                                        <tr>
                                           <td>{{$i}}</td>
                                            <td>FIRM00{{$order->id}}</td>
                                            <td>{{date('d-m-Y', strtotime($order->created_at ))}}</td>
                                            <td>{{$order->transaction_status}}</td>
                                            <td>{{$order->amount}}</td>
                                            <td>
                                                <a href="{{url('/customer/order-detail',[$order->id])}}">View Detail</a>
                                            </td>
                                            <td>    
                                                <a href="{{url('/customer/print/orders',[$order->id])}}">Print Invoice</a>
                                            </td>
                                        </tr>
                                         @php ($i++) 
                                        @endforeach
                                       <!--  <tr>
                                           <td>2</td>
                                            <td>FIRM002</td>
                                            <td>5/3/2018</td>
                                            <td>Paid</td>
                                            <td>15000</td>
                                            <td><a href="order-detail.php">View Detail</a></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


@endsection
@section('scripts')

@endsection

