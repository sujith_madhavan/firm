<html>
<head>
  <style>
  @media print {
    .bg-clr{
      background: #333;
      -webkit-print-color-adjust: exact; 
      }}
    </style>
  </head>
  <body onload="printPage()">
    <div class="container-fluid">
      <table style="width:  100%; border-collapse: collapse;">
        <tr style="color:#000;border-bottom-left-radius: 20px;
        border-top-left-radius: 20px;">
        <td style="border:1px solid #000;border-bottom-left-radius: 20px;
        border-top-left-radius: 20px;text-align:center;">
        <img src="{{asset('frontend/images/lognew.png')}}" class="" alt="logo">
      </td>
      <td style="text-align:center;border:1px solid #000;border-right:none;">
        <h1 style="padding-top:10px;"> <b>FIRMER</b> </h1>
        <p> Q-Block, No.93, 4th Main Road, Anna Nagar, Chennai-40.</p>
        <p>+91 44 2619 2619 +91 99400-87788 </p>               
        <p style="padding-bottom:10px;"> info@firmer.in | www.firmer.in</p>                        
      </td>
      <td style="border:1px solid #000;padding-left:20px;border-left:none;">
        <p style="padding-top:10px;">Invoice No : FIRM-{{ $order->id }} / {{$from}}-{{$to}}</p>
        <p>Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ date("d M Y",strtotime($order->created_at)) }}</p>
        <h4 style="font-size: 24px;"> GSTIN <br> 33AABPK2799K2ZF</h4>
      </td>
    </tr>
  </table>
</div>

<div class="text-center">
  <h2 Style="text-align:center;"><strong> TAX INVOICE</strong></h2>
</div>
<div class="container-fluid">
  <table style="width:100%;">
    <tr style="color:#000;border-bottom-left-radius: 20px;
    border-top-left-radius: 20px;">
    <td style="border-right: 1px solid #000;padding-left:20px;border:1px solid #000;border-right:none;border-bottom-left-radius: 20px;
    border-top-left-radius: 20px;">
    <h3 style="padding-top: 20px;"> BILLED TO</h3>
    <p> <strong> Name </strong>&nbsp;&nbsp;&nbsp;&nbsp;: {{ $order->billingDetail->first_name ?? ''}} {{ $order->billingDetail->last_name ?? ''}}</p>
    <P> <strong> Address </strong> : {{ $order->billingDetail->address ?? ''}},{{ $order->billingDetail->city ?? ''}},{{ $order->billingDetail->state ?? ''}}, {{ $order->billingDetail->pincode ?? ''}}</P>
    @if($order->customer->user_type == 'COMPANY')
    <h4> GSTIN : {{ $order->customer->gst ?? ''}}</h4>

    @endif  
    <p> <strong> PLACE OF SUPPLY</strong> :{{ $order->billingDetail->state ?? ''}} </p>
    <p> <strong> GSTIN</strong> </p>
  </td>
  <td style="padding-left:20px;border:1px solid #000;border-bottom-right-radius: 20px;
  border-top-right-radius: 20px;">
  <h3 style="padding-top: 20px;"> DELIVERY AT</h3>
  <p> <strong> Name</strong> &nbsp;&nbsp;&nbsp;&nbsp;: {{ $order->shippingDetail->first_name ?? '' }} {{ $order->shippingDetail->last_name ?? ''}}</p>
  <P> <strong> Address</strong> : {{ $order->shippingDetail->address ?? ''}},{{ $order->shippingDetail->city ?? ''}},{{ $order->shippingDetail->state ?? ''}}, {{ $order->shippingDetail->pincode ?? ''}}</P>
  @if($order->customer->user_type == 'COMPANY')
  <h4> GSTIN : {{ $order->customer->gst ?? ''}}</h4>

  @endif 
  <p> <strong> PLACE OF SUPPLY</strong> : (State and Code)</p>
  <p> <strong> GSTIN</strong> </p>
</td>
</tr>
</table>
</div>
<p> <strong>  </strong></p>
<div class="container-fluid bor-tabl">

  <table class="table" style="border:  1px solid #000; width:100%;border-collapse: collapse;">
   <thead>
    <tr class="bg-clr" style="color: #fff;background: #333;">
      <th style="text-align:  center;border: 1px solid #000;border-right: 1px solid #fff;"> SL.<br> No</th>
      <th style="text-align:  center;border: 1px solid #000;border-right: 1px solid #fff;">  Description of Goods</th>
      <th style="text-align:  center;border: 1px solid #000;border-right: 1px solid #fff;"> HSN/SAC</th>
      <th style="text-align:  center;border: 1px solid #000;border-right: 1px solid #fff;"> GST <br> Rate</th>
      <th style="text-align:  center;border: 1px solid #000;border-right: 1px solid #fff;"> Quantity</th>
      <th style="text-align:  center;border: 1px solid #000;border-right: 1px solid #fff;"> Price</th>
      <th style="text-align:  center;border: 1px solid #000;border-right: 1px solid #fff;"> Per</th>
      <th style="text-align:  center;border: 1px solid #000;border-right: 1px solid #fff;"> Discount</th>
      <th style="text-align:  center;border: 1px solid #000;"> Amount</th>
    </tr>
  </thead>
  <tbody>
    @php($tax = 0)
    @php($grand_total = 0)
    @php($i = 1)
    @php($qty = 0)
    @foreach($order->orderDetails as $order_detail)
    <tr style="border-right: 1px solid #000;
    text-align: center;color:#000;">
    <td style=" vertical-align: top;border-right: 1px solid #000;">
      <p style="padding-top: 10px;">{{ $i }}</p>
    </td>
    <td style=" vertical-align: top;border-right: 1px solid #000;">
      <p style="text-align:  left;padding-left: 20px;padding-top:10px;">{{ $order_detail->product->name ?? ''}}</p>
    </td>
    <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">{{ $order_detail->product->hsc_code ?? ''}}
    </td>
    <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">{{ $order_detail->product->gst ?? ''}}% 
    </td>
    <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">{{ $order_detail->quantity }} Nos
    </td>
    <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">{{ $order_detail->price }}
    </td>
    <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">{{ $order_detail->product->price_for ?? '' }}
    </td>
    <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">Nil
    </td>
    @php
    $total = $order_detail->price * $order_detail->quantity;

    $tax += ($total * $order_detail->gst)/100;
    $grand_total += $total;
    $qty += $order_detail->quantity;
    $i++;
    @endphp
    <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;"> {{ $total }}
    </td>
  </tr>

  @endforeach 
  <tr style="border-right: 1px solid #000;border-bottom: 1px solid #000;
  text-align: center;color:#000;">
  <td style=" vertical-align: top;border-right: 1px solid #000;">
    <p style="padding-top: 10px;"></p>
  </td>
  <td style=" vertical-align: top;border-right: 1px solid #000;">
    <p style="text-align:  right;    padding-right: 20px; margin-top:50px;"> IGST </p>
    <p style="text-align:  right;    padding-right: 20px;"> CGST </p>
    <p style="text-align:   right;    padding-right: 20px;"> SGST </p>
    <p style="text-align:   right;    padding-right: 20px;"> Paise Rounded Off </p>
  </td>
  <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">
  </td>
  <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">

  </td>
  <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">
  </td>
  <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">
  </td>
  <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">
  </td>
  <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;">
  </td>
  <td style=" vertical-align: top;border-right: 1px solid #000;padding-top:10px;"> 
    <p style="padding-top:10px;border-top:1px solid #000;"> {{ round($grand_total,2) }}</p>
    <p style="margin-top:45px;">{{ round(($tax/2),2) }}</p>
    <p>{{ round(($tax/2),2) }}</p>
    @php
    $grand_final_total = $grand_total + $tax;
    $final_total = round($grand_final_total);
    $difference = $final_total - $grand_final_total; 
    @endphp
    <p>{{ abs(round($difference,2)) }}</p>
  </td>
</tr>
<tr style="color:#000;">
  <td style=" vertical-align: top;border: 1px solid #000;"></td>
  <td style="border:1px solid #000;text-align:  right; padding-right: 20px;"> Total</td>
  <td style=" vertical-align: top;border: 1px solid #000;"></td>
  <td style=" vertical-align: top;border: 1px solid #000;"></td>
  <td style="border:1px solid #000;text-align:center;"> {{ $qty }} Nos</td>
  <td style=" border: 1px solid #000;"></td>
  <td style=" border: 1px solid #000;"></td>
  <td style="border:1px solid #000;"></td>

  <td style="border:1px solid #000;text-align:center;"><i class="fa fa-inr" aria-hidden="true"></i> {{ $final_total }}</td>
</tr>
<tr style="color:#000;">
  <td colspan="8" style="border: none;padding-left: 20px;"> Amount Chargeable (in words) <br> <b>INR
    @php
    $words = numberTowords($final_total);
    @endphp
    {{ $words }}
  only</b> </td>
  <td style="border-left: none;text-align: right;padding-right: 20px;vertical-align:top;"> E. & O.E</td>
</tr>
<tr style="color:#000;">
  <td colspan="2" style="border: 1px solid #000;text-align: center;"> HSN/SAC</td>
  <td colspan="2" style="border: 1px solid #000;text-align:center"> Taxable <br>value</td>                                        

  <td style="border: 1px solid #000;text-align:center"> Rate</td>
  <td style="border: 1px solid #000;text-align:center"> CGST</td>
  <td style="border: 1px solid #000;text-align:center"> SGST</td>
  <td style="border: 1px solid #000;text-align:center"> IGST</td>
  <td style="border: 1px solid #000;text-align:center"> TOTAL Tax Amount </td>
</tr>
@php($c_s_tax = 0)
@php($full_tax = 0)
@foreach($order->orderDetails as $order_detail)

<tr style="color:#000;">             


  <td colspan="2" style="padding-left:20px;border: 1px solid #000;text-align:left"> {{ $order_detail->product->hsc_code ?? ''}} </td>
  @php
  $total = $order_detail->price * $order_detail->quantity;

  $tax += ($total * $order_detail->gst)/100;

  $qty += $order_detail->quantity;
  $half_tax_value = ($order_detail->gst)/2;
  $full_tax_value = $order_detail->gst;
  @endphp
  <td colspan="2" style="border: 1px solid #000;text-align:center"> {{ $total }}</td>
  <td style="border: 1px solid #000;text-align:center"> {{ round($full_tax_value,2) }}% </td>
  @php
  $tax_amount = ($total * $order_detail->gst)/100;
  $half_tax = $tax_amount/2;
  $c_s_tax += $half_tax;
  $full_tax += $tax_amount;
  @endphp
  <td style="border: 1px solid #000;text-align:center"> {{ $half_tax }}</td>
  <td style="border: 1px solid #000;text-align:center"> {{ $half_tax }} </td>
  <td style="border: 1px solid #000;text-align:center">  </td>
  <td style="border: 1px solid #000;text-align:center"> {{ $tax_amount }} </td>
</tr>
@endforeach 

<tr style="color:#000;">             

  <td colspan="2" style="font-weight:bold;padding-right:20px;border: 1px solid #000;text-align:right"> Total </td>
  <td colspan="2" style="font-weight:bold;border: 1px solid #000;text-align:center">{{ $total }}</td>
  <td style="font-weight:bold;border: 1px solid #000;text-align:center">  </td>
  <td style="font-weight:bold;border: 1px solid #000;text-align:center"> {{ $half_tax }}</td>
  <td style="font-weight:bold;border: 1px solid #000;text-align:center"> {{ $half_tax }} </td>
  <td style="font-weight:bold;border: 1px solid #000;text-align:center">  </td>
  <td style="font-weight:bold;border: 1px solid #000;text-align:center"> {{ $tax_amount }} </td>
</tr>
</table>

</div> 
<div class="container-fluid">
  <table style="width:  100%;border-collapse: collapse;">
    <tr style="color:#000;">
      <td colspan="8" style="border:  1px solid #000;border-top:none;">
        <p style="padding: 10px 0 40px 20px;"> Tax Amount (in words):<b>
          @php
          $amount_word =round($tax_amount);
          @endphp

          @if($amount_word == 0)
          INR ZERO
          @else   
          @php
          $amount_word =round($tax_amount);
          $words1 = numberTowords($amount_word);
          @endphp   
          {{ $words1 }} 
          @endif

        only</b></p>
      </td>
    </tr>
    <tr style="color:#000;">
      <td colspan="4" style="padding: 0 0 0 20px;border:  1px solid #000;border-right:none;">
        <p> Declaration<br> We declare that this invoice shows the actual price of the goods<br> described and that all particulars are true and correct.</p>
      </td>
      <td colspan="4" style="padding: 10px 0 5px 20;border:  1px solid #000;border-left:none;">
        Date and Time <span style="padding-left:70px;">:</span> {{ date("d M Y",strtotime($order->created_at)) }} at {{ date("h:i A",strtotime($order->created_at)) }}
        <p>company's Bank Details</p>
        <p>Bank Name <span style="padding-left:88px;">:</span> HDFC Bank</p>
        <p>A/c. No. <span style="padding-left:108px;">:</span> 50200031592210</p>
        <p>Branch and Ifs Code <span style="padding-left:31px;">:</span> Anna Nagar & HDFC0000017</p>
      </td>
    </tr>
    <tr style="color:#000;">
      <td colspan="8" style="padding:20px;border:1px solid #000;">
        <p>Customer's Seal and Signature<span style="float: right;padding-right: 50px;">  <strong style="font-weight: 700;    font-size: 20px;">Shipment, Loading and Unloading Charges Excluded</strong>  <br> <span style="text-align: center;margin: 0 auto;display: block;padding: 5px;"> Standard Delivery :7 to 15 Business days. </span></p>

      </td>
    </tr>
  </table>
  <p style="text-align:center;padding-top:20px;color:#000;"> This is computer generated invoice</p>
</div>

@php
function numberTowords($num)
{ 
  $ones = array(
  0 =>"ZERO", 
  1 => "ONE", 
  2 => "TWO", 
  3 => "THREE", 
  4 => "FOUR", 
  5 => "FIVE", 
  6 => "SIX", 
  7 => "SEVEN", 
  8 => "EIGHT", 
  9 => "NINE", 
  10 => "TEN", 
  11 => "ELEVEN", 
  12 => "TWELVE", 
  13 => "THIRTEEN", 
  14 => "FOURTEEN", 
  15 => "FIFTEEN", 
  16 => "SIXTEEN", 
  17 => "SEVENTEEN", 
  18 => "EIGHTEEN", 
  19 => "NINETEEN",
  "014" => "FOURTEEN" 
  ); 
  $tens = array( 
  0 => "ZERO",
  1 => "TEN",
  2 => "TWENTY", 
  3 => "THIRTY", 
  4 => "FORTY", 
  5 => "FIFTY", 
  6 => "SIXTY", 
  7 => "SEVENTY", 
  8 => "EIGHTY", 
  9 => "NINETY" 
  ); 
  $hundreds = array( 
  "HUNDRED", 
  "THOUSAND", 
  "MILLION", 
  "BILLION", 
  "TRILLION", 
  "QUARDRILLION" 
  ); //limit t quadrillion 
  $num = number_format($num,2,".",","); 
  $num_arr = explode(".",$num); 
  $wholenum = $num_arr[0]; 
  $decnum = $num_arr[1]; 
  $whole_arr = array_reverse(explode(",",$wholenum)); 
  krsort($whole_arr,1); 
  $rettxt = ""; 
  foreach($whole_arr as $key => $i){
  while(substr($i,0,1)=="0")
  $i=substr($i,1,5);
  if($i < 20){ 
  //echo "getting:".$i;
  $rettxt .= $ones[$i]; 
}elseif($i < 100){ 
if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
}else{ 
if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
} 
if($key > 0){ 
$rettxt .= " ".$hundreds[$key]." "; 
} 
} 
if($decnum > 0){ 
$rettxt .= " and "; 
if($decnum < 20){ 
$rettxt .= $ones[$decnum]; 
}elseif($decnum < 100){ 
$rettxt .= $tens[substr($decnum,0,1)]; 
$rettxt .= " ".$ones[substr($decnum,1,1)]; 
} 
} 
return $rettxt; 
} 

@endphp 
</body>
<script>
  function printPage() {
    window.print();
  }
</script>
</html>