
@php ($page = "about-us")
@extends('layouts.app') 
@section('content') 
<div class="building-header">
    <div class="building-banner about-banner banrr-bd-im">
        <div class="banner-content2">
            <h1 class="col-md-7 col-xs-12">"The Goal as a company is to have customer service that is not  just the best but legendary"<!--possible, <span> For Quality</span>--> </h1>
            <h2 class="col-md-5 col-xs-12"> About <span>us</span></h2>
        </div>
    </div>
    <div class="breadcrumb">
        <a href="index.php"><img src="{{asset('frontend/images/home.png')}}"></a>&nbsp;
        <i class="fa fa-angle-right" aria-hidden="true"></i>
        <p>&nbsp;About Us&nbsp;</p>
    </div>
    <div class="about-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <div class="about-head">
                    
                        <h3>C.KASTURI RAJ – A MAN OF HOPE AND SUCCESS</h3>
                           <h2>The Goal as a company is to have customer service that is not just the best but legendary</h2>
                        <p>C.Kasturi Raj started the company of<span> FIRM GROUP - FIRM FOUNDATIONS in the year 1992.</span> With 5 employees, Firm Foundations grew in the field of Real Estate as a Private Limited Company, FIRM FOUNDATIONS AND HOUSING PVT LTD, to be a family of 50 Employees and With a Happy Customer base of 1500 and still growing.  CKR has held key portfolios in Planning, Construction, Marketing, Purchasing and Liasoning. His strong project management and leadership skills and personnel supervision have helped the firm build and sustain profitable relationships with clients and vendors.</p>
                        <p>Another Feather in the Crown of Firm Group – FIRM HOSPITALS was added in the year 2012 and has been running successfully.  Firm Hospitals was conceptualized with the purpose of providing excellent, affordable, International quality health care for women. </p>

<p>After the successful completion of 25 years in the housing sector and 6 years in the hospital sector, the passion inside him urged him to endeavor into another effective platform of e-commerce, which has paved the way to the startup - FIRMER, an online portal for all  the construction needs of the market.</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="abt-img2">
                        <div class="abt-img">
                            <img src="{{asset('frontend/images/aboutimg.png')}}" alt="about" class="img-resposive">
                        </div>
                        <div class="abt-view">
                            <a href="http://www.firmfoundations.in/ckrblog/">
                            <img src="{{asset('frontend/images/blog.png')}}" alt="about" class="img-resposive">
                            Read my BLOG</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="about-title3">
            <h3><span><img src="{{asset('frontend/images/icon1.png')}}" alt="about"></span> AWARDs & ACCOLADES</h3>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-12">                       
                        <div class="thumb-img">  
                           <a href="{{asset('frontend/images/abt-aw-1.jpg')}}" data-lightbox="const-gallery">                     
                            <div class="hover-bg  cus-img4">
                                <img src="{{asset('frontend/images/abt-aw-1.jpg')}}" alt="award" class="img-resposive">
                                
                                <div class="caption-text">
                                    <h3>OUR CHAIRMAN RECEIVING THE GOLDEN BRICK AWARD.</h3>
                                    <p> Here is to another milestone, another feather added on to our cap! Mr.C.Kasturi Raj - Chairman and Managing Director of Firm Foundations & Housing Pvt Ltd received the award for "Most Promising Image of The Year" at the Golden Brick Awards - 2016, India International Real Estate Awards 2016 in Dubai.</p>
                                </div>
                                <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                            </div>
                            
                            </a>

                        </div>
                        
                        <div class="thumb-img">
                           <a href="{{asset('frontend/images/abt-aw-2.jpg')}}" data-lightbox="const-gallery"> 
                            <div class="hover-bg cus-img4">
                                <img src="{{asset('frontend/images/abt-aw-2.jpg')}}" alt="award" class="img-resposive">
                                <div class="caption-text">
                                    <h3>PERSONALITY OF THE MONTH</h3>
                                </div>
                                <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                            </div>
                            </a>
                        </div>
                         <div class="thumb-img">
                            <a href="{{asset('frontend/images/abt-aw-9.jpg')}}" data-lightbox="const-gallery">
                                <div class="hover-bg  cus-img4">
                                    <img src="{{asset('frontend/images/abt-aw-9.jpg')}}" alt="award" class="img-resposive">

                                    <div class="caption-text">
                                        <h3>Realty Plus Excellence Awards 2014.</h3>
                                        <p> work in the field of Real Estate Development by the governor of Tamil Nadu,
                                            Dr. K. Rosaiah, organized by India Today</p>
                                    </div>
                                    <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                    <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                                </div>

                            </a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="thumb-img">
                           <a href="{{asset('frontend/images/abt-aw-3.jpg')}}" data-lightbox="const-gallery">
                            <div class="hover-bg cus-img4">
                                <img src="{{asset('frontend/images/abt-aw-3.jpg')}}" alt="award" class="img-resposive">
                                <div class="caption-text">
                                    <h3>OUR CHAIRMAN MR.C.KASTURI RAJ RECEIVING THE AWARD.</h3>
                                    <p>Firm Foundations & Housing Ltd, was awarded Realty Leaders Summit & Awards 2015 For Achieving "Best Developer-Luxury Residential Of The Year (Tamilnadu)".</p>
                                </div>
                                 <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                            </div>
                            </a>
                        </div>
                        <div class="thumb-img">
                           <a href="{{asset('frontend/images/abt-aw-4.jpg')}}" data-lightbox="const-gallery">
                            <div class="hover-bg cus-img4">
                                <img src="{{asset('frontend/images/abt-aw-4.jpg')}}" alt="award" class="img-resposive">
                                <div class="caption-text">
                                    <h3>ANOTHER FEATHER IN OUR ALREADY LOADED CAP</h3>
                                    <p>We are proud to announce that our project ‘FIRM'S AVATAAR’ has been awarded the ‘Best Design Apartment project of the year 2014 , by Silicon India on 31st of Oct 2014.</p>
                                </div>
                                 <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                            </div>
                            </a>
                        </div>
                         <div class="thumb-img">
                            <a href="{{asset('frontend/images/abt-aw-3.jpg')}}" data-lightbox="const-gallery">
                                <div class="hover-bg cus-img4">
                                    <img src="{{asset('frontend/images/abt-aw-10.jpg')}}" alt="award" class="img-resposive">
                                    <div class="caption-text">
                                        <h3>Brand Excellence Award.</h3>
                                        <p>Brand Excellence Award in Real Estate Sector for the year 2014 by ABP News</p>
                                    </div>
                                    <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                    <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                         <div class="thumb-img">
                           <a href="{{asset('frontend/images/abt-aw-5.jpg')}}" data-lightbox="const-gallery">
                            <div class="hover-bg cus-img4">
                                <img src="{{asset('frontend/images/abt-aw-5.jpg')}}" alt="award" class="img-resposive">
                                <div class="caption-text">
                                    <h3>OUR CHAIRMAN MR.C.KASTURI RAJ RECEIVING THE AWARD.</h3>
                                    <p> Firm Foundations & Housing Ltd, was awarded the “Gobal Real Estate Brand Awards on 20th Feb 2015”. Staying true to our vision for a better future, we make consistent, impeccable deliveries. We are grateful for your years of support and acknowledgment.</p>
                                </div>
                                <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                            </div>
                        </a>
                        </div>
                         <div class="thumb-img">
                           <a href="{{asset('frontend/images/abt-aw-6.jpg')}}" data-lightbox="const-gallery">
                            <div class="hover-bg cus-img4">
                                <img src="{{asset('frontend/images/abt-aw-6.jpg')}}" alt="award" class="img-resposive">
                                <div class="caption-text">
                                    <h3>CONGRATULATIONS</h3>
                                    <P>Here is to another milestone,another feather added on to our cap! Mr.c. kasturiRaj-chairman</P>
                                </div>
                              <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                            </div>
                             </a>
                        </div>
                         <div class="thumb-img">
                            <a href="{{asset('frontend/images/abt-aw-11.jpg')}}" data-lightbox="const-gallery">
                                <div class="hover-bg cus-img4">
                                    <img src="{{asset('frontend/images/abt-aw-11.jpg')}}" alt="award" class="img-resposive">
                                    <div class="caption-text">
                                        <h3>Indian Leadership Award for Construction & Design</h3>
                                        <p> Indian Leadership Award for Construction & Design for the year 2014” by All India Achievers Foundation.</p>
                                    </div>
                                    <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                    <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="thumb-img">
                           <a href="{{asset('frontend/images/abt-aw-7.jpg')}}" data-lightbox="const-gallery">
                            <div class="hover-bg cus-img4">
                                <img src="{{asset('frontend/images/abt-aw-7.jpg')}}" alt="award" class="img-resposive">
                                <div class="caption-text">
                                    <h3>OUR CHAIRMAN MR.C.KASTURI RAJ RECEIVING THE AWARD.</h3>
                                    <p> Firm Foundations & Housing Ltd was awarded the " BRAND EXCELLENCE AWARD IN REAL ESTATE SECTOR" by ABP news on 14th of November 2014, at Taj Lands End, Mumbai.</p>
                                </div>
                                 <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                            </div>
                            </a>
                        </div>
                        <div class="thumb-img">
                           <a href="{{asset('frontend/images/abt-aw-8.jpg')}}" data-lightbox="const-gallery">
                            <div class="hover-bg cus-img4">
                                <img src="{{asset('frontend/images/abt-aw-8.jpg')}}" alt="award" class="img-resposive">
                                <div class="caption-text">
                                    <h3>ON 23RD MAY 2014.</h3>
                                    <p>Here is to another milestone,another feather added on to our cap! Mr.c. kasturiRaj-chairman and managing Director of firmer foundation & Housing Pvt Ltd recevied the awaed of "Most promising images of Year" ath the Golden.</p>
                                </div>
                               <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                            </div>
                            </a>
                        </div>
                         <div class="thumb-img">
                            <a href="{{asset('frontend/images/abt-aw-12.jpg')}}" data-lightbox="const-gallery">
                                <div class="hover-bg cus-img4">
                                    <img src="{{asset('frontend/images/abt-aw-12.jpg')}}" alt="award" class="img-resposive">
                                    <div class="caption-text">
                                        <h3>“LIBRARY ENTHUSIAST AWARD” BY EDUCATION DEPARTMENT</h3>
                                        <p> </p>
                                    </div>
                                    <span><img src="{{asset('frontend/images/gallery.png')}}" class="img-responsive"></span>
                                    <i><a href="#"><img src="{{asset('frontend/images/link.png')}}" class="img-responsive"></a></i>
                                </div>
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="about-title">
    <h3><span><img src="{{asset('frontend/images/speak.png')}}" alt="about"></span> MEDIA AND PRESS MEET</h3>
</div>
<div class="about-bg2">
    <div class="container-fluid">
        <div class="row">
            <div class="tl-abt">
<div class="cust-container">
    <div class="ban-splash">
        <div class="owl-carousel owl-theme abt-carousel">
           <div class="item"><img src="{{asset('frontend/images/bannernew1.jpg')}}" alt="aboutus"></div>
                            <div class="item"><img src="{{asset('frontend/images/bannernew2.jpg')}}" alt="aboutus"></div>
                               <div class="item"><img src="{{asset('frontend/images/bannernew3.jpg')}}" alt="aboutus"></div>
                                 <div class="item"><img src="{{asset('frontend/images/bannernew4.jpg')}}" alt="aboutus"></div>
                                   <div class="item"><img src="{{asset('frontend/images/bannernew5.jpg')}}" alt="aboutus"></div>
        </div>
       <!--  <div class="slider_nav">
            <button class="nxt-flash"><img src="{{asset('frontend/images/leftarrow.png')}}" alt=""></button>
            <button class="prv-flash"><img src="{{asset('frontend/images/right.png')}}" alt=""></button>
        </div> -->
    </div>
</div>

            </div>

        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="about-vision">
                <div class="about-visi-bg">
                    <h3> OUR VISION &nbsp;&nbsp;&nbsp;<span><img src="{{asset('frontend/images/eye.png')}}" alt="eye"></span></h3>
                    <h4> Tomorrow depends upon today</h4>
                    <p><b>Our VISION</b> is to provide all Customers with high Quality building materials, to accommodate their requirements and allow them to shop Anytime, Anywhere and from Any device. From pricing to payments, shipping to returns we have designed our technology, to bring brands and customers closer together, making shopping easier, hence- LOOKING NO FURTHER.</p>
                    <p><b>OBJECTIVE</b></p>
                    <p class="objects">
                    <span>To be precise</span>
                    <span>Visionary Leadership</span>
                    <span>Employee Productivity</span>
                     <span>Cost Control</span>
                      <span> Long range of the future.</span>
                    
                    
                    
                    </p>
                </div>
                <div class="img-abt2">
                    <img src="{{asset('frontend/images/afetr-tap.png')}}">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">           
                <div class="about-vi-bg">
                    <div class="about-vision abt-vs-2">
                    <h3> QUALITY POLICY &nbsp;&nbsp;&nbsp;<span><img src="{{asset('frontend/images/like.png')}}" alt="like"></span></h3>

                    <p>1.To give fast and reliable services to the customers.</p>

  <p>2.To find the solutions that best suits the needs of the customer.</p>

  <p>3.To achieve continual improvement in customer satisfaction by providing quality products and services.</p>
                </div>
            </div>
           
        </div>

    </div>

</div>


@endsection
