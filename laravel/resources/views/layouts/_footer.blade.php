<footer>
    <div class="footer-inner">
        <div class="social-icon">
            <p>Follow us</p>
            
            <ul class="social-network social-circle">
                
                <li><a href="https://www.facebook.com/firmer.f" target="_blank"  class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/Firmer17" target="_blank" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://plus.google.com/111044167917929404496" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                
                <li><a href="https://www.instagram.com/firmer_online_shop/?hl=en" target="_blank" class="icoinstagram" title="instagram"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UC6qUafZ1N-8fbvx-2A2zYGQ?view_as=subscriber" target="_blank" class="icoyoutube" title="youtube"><i class="fa fa-youtube"></i></a></li>
                <li><a href="https://in.pinterest.com/firmeronline/" target="_blank" class="icopinterest" title="pinterest"><i class="fa fa-pinterest"></i></a></li>
            </ul>
<!--
            <a href="#"><img src="{{asset('frontend/images/fac.png')}}"></a>
            <a href="#"><img src="{{asset('frontend/images/twi.png')}}"></a>
             <a href="#"><img src="{{asset('frontend/images/instagram.png')}}"></a>
            <a href="#"><img src="{{asset('frontend/images/google.png')}}"></a>
           
            <a href="#"><img src="{{asset('frontend/images/pintrest.png')}}"></a>
           
             <a href="#"><img src="{{asset('frontend/images/youtube.png')}}"></a>
         -->
     </div>
     <div class="footer">
        <div class="container-fluid">
            <div class="row rw-tp">
                <div class="col-sm-6 col-xs-12">
                    <div class="cnt-txt">
                        <div class="block-title"><strong><span>"A Satisfied Customer is the best business strategy of all "</span></strong></div>
                        <div class="block-content">
                            <!--                                <p>  Everything Should be made as simple as possible, but not simpler.</p>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="professional-right">
                        <p><strong><span>Know us Better</span></strong></p>
                        <div class="input-box">
                            <div class="row">
                                <input type="text" name="email" id="" title="Sign up for our newsletter" class="input-text required-entry validate-email" placeholder="Email Address">
                                <button type="submit" title="Subscribe" class="button btn-bn"><span><span>Subscribe</span></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="bottom-line">
            <div class="row">
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <h4>CONTACT INFORMATION</h4>
                    <div class="address">
                        <p>Q- Block No 93,<br> 4th Main Road,
                            Anna Nagar,<br> Chennai-40,</p>
                        </div>
                        <div class="Phone">
                            <p> +91 9940087788</p>
                        </div>
                        <div class="mail-us">
                            <p><a href="mailto:info@firmer.in">info@firmer.in</a><br><a href="mailto:enquiry@firmer.in">enquiry@firmer.in</a> </p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="footer-information footer-menu">
                            <h4>INFORMATION</h4>
                            <!--  <p><a href="{{url('/')}}">Home</a></p> -->
                            <p><a href="{{url('/about-us')}}">About us</a></p>
                            <!-- <p><a href="#">Services</a></p> -->
                            <!-- <p><a href="#">Products</a></p> -->
                            <p><a href="{{url('/login')}}">My Account</a></p>
                            <p><a href="{{url('/return-policy')}}">Return policy</a></p>
                            <p><a href="{{url('/terms-and-condition')}}">Terms and Conditions</a></p>
                            <p><a href="{{url('/privacy-policy')}}">Privacy Policy</a></p>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-6 col-xs-12">
                      <!--  <div class="footer-information">
                            <h4> WE SERVE</h4>
                            <div class="col-md-8">
                                
                                <img class="img-responsive" src="{{asset('frontend/images/map1.jpg')}}">
                            </div>
                            <!-- <img src="{{asset('frontend/images/service-1.jpg')}}"> -->
                            
                            <!--</div>-->
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="know-us">
                                <h4>PRODUCT ENQUIRY</h4>
                                <form method="post" action="{{ url('/contact-enquiry') }}">

                                    {{ csrf_field() }}   
                                    <div class="row">
                                        <div class="col-md-11">
                                            <div class="form-group">
                                              <div class="form-group {{$errors->has('name')?'has-error':''}}">
                                                <input type="name" class="form-control" id="name" placeholder="Name" name="name" required="">
                                                @if($errors->has('name'))
                                                <span class="help-block">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-11">
                                        <div class="form-group">
                                          <div class="form-group {{$errors->has('phone')?'has-error':''}}">
                                            <input type="phone" class="form-control" id="contact Number" placeholder="Contact Number" name="name" required="">
                                            @if($errors->has('phone'))
                                            <span class="help-block">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group">
                                     <div class="form-group {{$errors->has('email')?'has-error':''}}">
                                        <input type="email" class="form-control" id="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" placeholder="Email" name="email" required="">
                                        @if($errors->has('email'))
                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                 <div class="form-group {{$errors->has('message')?'has-error':''}}">
                                    <textarea rows="4" class=" form-control" id="message" placeholder="Message" name="message"></textarea>
                                    @if($errors->has('message'))
                                    <span class="help-block">{{ $errors->first('message') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11">
                            <div class="form-group button-group">
                                <button type="submit" name="submit" class="btn btn-default">SUBMIT</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="row">
     <div class="col-md-12 col-xs-12">
       <div class="paymnt">
        <span> We Accept</span> 
        
        
        
        <a href="#"><img src="{{asset('frontend/images/rupay.png')}}"></a>
        
        <a href="#"><img src="{{asset('frontend/images/visa-mastercard.png')}}"></a>             
        <a href="#"><img src="{{asset('frontend/images/paypal.png')}}"></a> 
        
    </div> 
    <div class="paymentcod">
                  <!-- <a href="#"><img src="{{asset('frontend/images/net.png')}}"></a> 
                    <a href="#"><img src="{{asset('frontend/images/cod.png')}}"></a>  -->
                    
                </div>  
            </div>
            
        </div>
        
    </div>

</div>

<div class="footer-down">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 footer-left">
                <p>@Firmer All Rights Reserved
                </p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 footer-right">
                <p>Designed By <a target="_blank" href="http://www.ninositsolution.com/" title="Ecommerce Website Designing and Development Company in Chennai" class="ecp_li"><span> Ninositsolution</span></a></p>
            </div>
        </div>
    </div>
</div>
</div>
</footer>




<script src="{{asset('frontend/js/jquery.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/bootstrap.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('frontend/js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/lightbox.js')}}"></script>

<script type="text/javascript" src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/bootstrap-typeahead.js')}}"></script>

<script>
    //new WOW().init();

</script>

@yield('scripts')

<script>
    $(document).ready(function() {
        var testimonials = $('.left-carousel .owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: false,
            dotsContainer: '#carousel-custom-dots',
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                530: {
                    items: 1
                },
                768: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        })
        
        //        $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
        //        $(".owl-next").html('<i class="fa fa-chevron-right"></i>');
        $('.owl-dot').click(function() {
            testimonials.trigger('to.owl.carousel', [$(this).index(), 300]);
        });

        var client = $('.right-carousel .owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: false,
            responsive: {
                0: {
                    items: 1
                },
                530: {
                    items: 3
                },
                768: {
                    items: 3
                },
                992: {
                    items: 4
                }
            }
        })
        $(".client-prev").click(function(e) {
            e.preventDefault();
            client.trigger('prev.owl.carousel');
        });
        $(".client-next").click(function(e) {
            e.preventDefault();
            client.trigger('next.owl.carousel');
        });
    });

    

</script>


<script type="text/javascript">
    
  $(document).ready(function() {
    var featured = $('.top-carousel .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        
        responsive: {
            0: {
                items: 1
            },
            530: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    })
});
  
  
  $(function() {
            // setTimeout(function() {
            //     $('#overlay').modal('show');

            // }, 3000);
            $('#overlay').on('show.bs.modal',function(){
             $('.carousel-outer').css('top','-125px');  
         });
            $('#overlay').on('hide.bs.modal',function(){
             $('.carousel-outer').css('top','auto');  
         });
            
            
            
            
        });
    </script>


    <script>
       var owl = $('.abt-carousel');
       owl.owlCarousel({
        loop: true,
        nav: true,
        margin: 10,
        dots:false,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            530: {
                items: 1
            },
            768: {
                items: 1
            },
            992: {
                items: 1
            }
        }
    });
       var owl = $('.abt-carousel');
       owl.owlCarousel();
       $('.nxt-flash').click(function() {
        owl.trigger('next.owl.carousel');
    })
       $('.prv-flash').click(function() {
        owl.trigger('prev.owl.carousel');
    });

</script>

<script type="text/javascript">
    $('#search_product').typeahead({
        scrollBar: true,
        alignWidth: true,

        ajax: '{{ route('product.names') }}'
    });
</script>

<script type="text/javascript">
 $("#confirmed1").submit(function()
 {
   var search_product = $('.search_product').val();
   var category = $("#category22").val();

   if(search_product || category)
   {
                    //alert(category);
                    return true;
                }
                else
                {
                   swal({ 
                      title: "Please Enter Search!",
                      text:  "You are now following",
                  });
                   return false;   
               }
           });  
       </script>
   </body>
   </html>
