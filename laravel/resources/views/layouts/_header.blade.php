<!DOCTYPE html>
<html>

<head>
    <title>Firmer</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/lightbox.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/query.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/animate.css')}}">
    <link rel="icon" href="{{asset('frontend/images/faviconfimer.jpg')}}">
    <link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{ asset('backend/sweetalert2/dist/sweetalert2.min.js') }}"></script>
   <!--  <link rel="stylesheet" href="{{asset('frontend/dist/sweetalert.css')}}">
    <script src="{{asset('frontend/dist/sweetalert.min.js')}}"></script> -->
    @yield('styles')


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqYNrJVs23qat6nJBU_a-1ElVNHHo6nyM&libraries=places&region=in"></script>

    <script type="text/javascript">
     function initialize() {
      var input = document.getElementById('searchTextField');
      var autocomplete = new google.maps.places.Autocomplete(input);
      google.maps.event.addListener(autocomplete, 'place_changed', function(){
         var place = autocomplete.getPlace();

         //alert(place.formatted_address);
$('#search-form').submit();
      })
  }
  google.maps.event.addDomListener(window, 'load', initialize);


   
</script>
</head>

<body>
    @if (session('status'))
    <script type="text/javascript">
       swal({
        title: "{{session('message')}}",
        type:"{{session('status')}}",
        timer: 1500,
        showConfirmButton:false,
        animation:false,
    });
</script>
@endif
<header style="position:relative">

    <div class="header-top animated bounceInDown">


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-xs-12 pading"> 
                    <div class="col-md-4 toll-free">
                        <p><img src="{{asset('frontend/images/contact.png')}}"><span>CALL US :<a href="{{('/')}}">+91-9940087788</a></span></p>
                    </div>
                    <div class="col-md-8 col-xs-12 pading">
                        <form id="confirmed1" class="searchdes" method="post" action="{{ url('/search-products') }}">
                            {{ csrf_field() }} 
                            <div class="col-md-4 col-xs-6 padd">
                                <div class="dropdown">
                                    <select class="selectpicker slct-top" id="category22" name="category" type="button" value="category">
                                        <span class="caret"></span>
                                        <option value="">Search Categories</option>
                                        @foreach(App\Category::whereNull('parent_id')->orderBy('name','asc')->get() as $category)
                                        <option value="{{ $category->slug }}">{{ ucwords($category->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-8 col-xs-6 padd"> 
                             <div style="position:relative">
                               <input type="text" class="form-control search_product" id="search_product" name="search_name" placeholder="Search your products" autocomplete="off"  style="position:relative">
                               <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>

                        </div>                      

                    </div>
                </form>    
            </div>
        </div>
                     <!--  <div class="col-md-6 col-sm-12">
                        <p><img src="{{asset('frontend/images/contact.png')}}"><span>TOLL FREE :<a href="+044-2526323250">044-2619 2619</a></span></p>
                    </div> -->

                    <div class="col-md-4 col-xs-12 pading"> 
                        <ul class="pading">
                             @if(empty($value = session('address')))
                            <div class="dropdown" style="display: inline-block">
                                <a href="" class=" dropdown-toggle" type="button" data-toggle="dropdown"><img style="vertical-align: bottom;" src="{{asset('frontend/images/loc.png')}}"> 
                                    @if(!empty($value = session('address')))
                                    {{$value = session('address')}}
                                    @else
                                    Find location
                                    @endif
                                    
                                    </a>
                                    <ul class="dropdown-menu" style="width:380px; right:0; left:auto; margin:0; padding:10px;">
                                        <p style="font-size: 12px;">Enter your exact Street Name/Apartment/Location to deliver.</p>
                                       <form method="post" action="{{ url('/searchpostal-code') }}" id="search-form">
                                        {{ csrf_field() }}
                                       <input id="searchTextField" class="form-control" type="text" size="50" name="address" id="query" placeholder="Enter a location" autocomplete="on">
                                      <!--  <input type="hidden" name=""> -->
                                       </form>
                                  </ul>
                              </div>
                              @endif
                            @if(Auth::guard('customers')->check())
                            <li class=""><a href="{{url('/logout')}}">Logout</a></li>
                            <li class="customer {{ Nav::isRoute('customer.dashboard', $activeClass = 'active') }}"><a class="" href="{{ url('/customer/dashboard') }}">Hi, {{ ucwords(Auth::guard('customers')->user()->first_name) }}</a></li>
                            <li class="customer {{ Nav::isRoute('customer.dashboard', $activeClass = 'active') }}"><a class="" href="{{ url('/customer/dashboard') }}">MY ACCOUNT </a></li>
                            @else 
                            <li class="location customer1 {{ Nav::isRoute('login', $activeClass = 'active') }}"><a href="{{url('/login')}}">
                                <!--<img src="{{asset('frontend/images/lock.png')}}">-->Customer</a></li>
                                <li class="customer"><a href="{{url('/vendor/register')}}">Vendor</a></li>
                                @endif    


                            </ul>
                        </div>
                        <div class="col-xs-12">
                            @if(!empty($value = session('address')))
                            <div class="dropdown pull-right" style="display: inline-block">
                                <a href="" class=" dropdown-toggle" type="button" data-toggle="dropdown"><img style="vertical-align: bottom;" src="{{asset('frontend/images/loc.png')}}"> 
                                    @if(!empty($value = session('address')))
                                    {{$value = session('address')}}
                                    @else
                                    Find location
                                    @endif
                                    
                                    </a>
                                    <ul class="dropdown-menu" style="width:380px; right:0; left:auto; margin:0; padding:10px;">
                                        <p style="font-size: 12px;">Enter your exact Street Name/Apartment/Location to deliver.</p>
                                       <form method="post" action="{{ url('/searchpostal-code') }}" id="search-form">
                                        {{ csrf_field() }}
                                       <input id="searchTextField" class="form-control" type="text" size="50" name="address" id="query" placeholder="Enter a location" autocomplete="on">
                                      <!--  <input type="hidden" name=""> -->
                                       </form>
                                  </ul>
                              </div>
                              @endif
                        </div>
                    </div>
                </div>
            </div>

        </header>
        <div class="container-fluid cus-container">
            <nav class="navbar navbar-default" data-offset-top="120">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                    <a class="navbar-brand logo-min" href="{{url('/')}}"><img src="{{asset('frontend/images/logo.png')}}" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">

                    <ul class="nav navbar-nav">
                        @foreach(App\Menu::where('status',1)->orderBy('position','asc')->get() as $menu)
                        <li class="main-menu {{ $page == $menu->category->name ? 'active' : '' }}"><a href="{{url('/category')}}/{{$menu->category->slug}}">{{$menu->name}}  <i class="fa fa-chevron-down" aria-hidden="true"></i></a><span class="toggle-menu">+</span>
                            @if(count($menu->category->childCategories))
                            <ul class="sub-menu">
                                @php ($i = 1) 
                                @foreach ($menu->category->childCategories as $childCategory)
                                @if($childCategory->status == 1) 
                                <li><a href="{{url('/category')}}/{{$childCategory->slug}}">{{ ucwords(strtolower($childCategory->name)) }}</a><span class="toggle-menu">+</span>
                                    @if(count($childCategory->childCategories))
                                    <ul class="third-level">
                                        @foreach ($childCategory->childCategories as $child_category)
                                        @if($child_category->status == 1) 
                                        <li><a href="{{url('/category')}}/{{$child_category->slug}}">{{ ucwords(strtolower($child_category->name)) }}</a></li>
                                        @endif    
                                        @endforeach
                                    </ul>
                                    @endif    
                                </li>
                                @endif    
                                @php ($i++) 
                                @endforeach
                            </ul>
                            @endif    
                        </li>
                        @endforeach    
                    </ul>

                </div>
                @if(Auth::guard('customers')->check())
                @if(!empty(Auth::guard('customers')->user()->carts->count()))  
                <a class="buy-now" href="{{url('/cart')}}">
                    <img src="{{asset('frontend/images/cart.png')}}">
                    <span>{{Auth::guard('customers')->user()->carts->count()}} </span>     
                </a>
                @endif  
                @endif  
            </nav>
        </div>
