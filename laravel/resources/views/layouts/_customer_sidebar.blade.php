<div class="col-md-3">
    <div class="side-bar tab">   
        
            <a class="{{ $customer_sidebar == 'dashboard' ? 'active' : '' }}" href="{{url('/customer/dashboard')}}"><h3>MY ACCOUNT</h3></a>
        
        <div class="{{ $customer_sidebar == 'account' ? 'active' : '' }}">
            <div  class="account-info">
               <a href="{{url('/customer/account-info')}}"> <h6><img src="{{asset('frontend/images/inf.png')}}">Account Information</h6></a>
            </div>
        </div>
        <div class="{{ $customer_sidebar == 'news' ? 'active' : '' }}">
            <div class="news-letter">
               <a href="{{url('/customer/news-letters')}}"> <h6><img src="{{asset('frontend/images/mess.png')}}">Newsletter</h6></a>
            </div>
        </div>
        <div class="{{ $customer_sidebar == 'address' ? 'active' : '' }}">
            <div class="news-letter">
                <!-- <a href="{{url('/customer/address/add')}}"><h6><img src="{{asset('frontend/images/flod.png')}}">Address Book</h6></a> -->
                 <a href="{{url('/customer/address-list')}}"><h6><img src="{{asset('frontend/images/flod.png')}}">Address Book</h6></a>
            </div>
        </div>
        <div class="{{ $customer_sidebar == 'order' ? 'active' : '' }}">
            <div class="news-letter">
               <a href="{{url('/customer/myorder')}}"> <h6><img src="{{asset('frontend/images/cart-dashboard.png')}}">My Order</h6></a>
            </div>
        </div>
        <div class="{{ $customer_sidebar == 'review' ? 'active' : '' }}">
            <div class="news-letter">
                <a href="{{url('/customer/product-review')}}"><h6><img src="{{asset('frontend/images/review1.png')}}">My Product Review</h6></a>
            </div>
        </div>
    </div>
</div>