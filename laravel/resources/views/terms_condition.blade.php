
@php ($page = "terms-condition")
@extends('layouts.app') 
@section('content') 



<div class="login-banner"></div>
<div class="building-header">
   <!--  <div class="building-banner">
        <div class="banner-content">

            <h5>privacy-policy</h5>
			<h3>IRON AND STEEL <span>PRODUCTS</span></h3>

        </div>
    </div> -->
    <div class="breadcrumb">
        <a href="index.php"><img src="{{asset('frontend/images/home.png')}}"></a>&nbsp;
        <i class="fa fa-angle-right" aria-hidden="true"></i>
        <p>&nbsp;Terms-condition&nbsp;</p>

    </div>




    <div class="container">

        <div class="std">
            <p style="text-align: center;color: #e75e0d;margin-bottom:40px;"><span style="font-size: x-large;"><strong><em><span style="text-decoration: underline;">Terms and Conditions of Use</span></em>
                </strong>
                </span>
            </p>
            <hr align="center" noshade="noshade" size="0" width="100%">
            <p>The domain name <b><a href="http://firmer.in">WWW.FIRMER.IN</a></b> (hereinafter the "Website/mobile app") is owned by <b>FIRM FOUNDATIONS</b> Private Limited, is a company incorporated under the companies Act, 1992, having its corporate office at No 93,4th Main Road ,Q Block ,Anna Nagar Chennai – 600040,India (hereinafter referred to as "<b>FIRMER</b>").</p>
            <p>These terms and conditions of use shall apply to all persons who access the website and to all persons to whom services are provided by FIRMER, the owner of the website. <b>FIRMER</b> essentially owns and operates an e commerce website in the name <b>FIRMER</b>, which acts as an online platform for selling different products to different users to access a range of products and to purchase the products.</p>
            <p>Access to the website and the services is subject to your compliance with and acceptance of these terms and conditions of use, the privacy policy, the disclaimer, the returns and cancellation policy and any other terms and conditions which may be applicable to availing the Services as detailed in the relevant page pertaining to such services, collectively referred to as "the Terms".</p>
            <p>Once accepted or deemed accepted, the terms shall form the legal contract (hereinafter "the User Agreement") between a user and <b>FIRMER</b> vis-à-vis the use and access of the Website, provision of services and all other matters arising out of or in connection therewith.</p>
            <p><strong><em><span style="text-decoration: underline;">Acceptance of the terms, modification and consequences of violation of the terms</span></em></strong></p>
            <p>Please ensure that you read and understand the terms carefully. By accessing the website and/or using the services, you agree to be bound by the terms and signify your, free, absolute and unqualified acceptance thereof without fraud, coercion or undue influence. If you do not agree with any of the terms, please do not access the website or use the services. The user further represents that the user has the necessary capacity to contract under Indian laws or the laws applicable to the user and is capable of performing the obligations required of the user under the terms. The user shall be obliged to inform <b>FIRMER</b> of any subsequent disability to contract, upon which <b>FIRMER</b> shall be entitled to terminate the user agreement without notice or to modify or alter the terms in their application to such user.</p>
            <p><b>FIRMER</b> may amend/modify the terms at any time, and such modifications shall be notified via email to our registered users and will be effective immediately upon posting of the modified terms on the website. You are required to review the modified terms periodically (as often as possible) to be aware of such modifications. Your continued access or use of the website or the services shall be deemed proof of your acceptance of the terms as modified.</p>
            <p>In the event that you access or use the website or the services in contravention or violation of any of the terms, you shall be deemed to be an unauthorized user and <b>FIRMER</b> shall be entitled to terminate the user agreement immediately, block your registered account and remove any information or posting which does not comply with the terms or any law in force in India. <b>FIRMER</b> shall also be entitled to block your access to the Website and / or seek any additional relief as available under law.</p>
            <p>"Website e website<a href="http://firmer.in"> “www.firmer.in”</a>shall mean the e-commerce website <b><a href="http://firmer.in">“www.firmer.in”</a></b> and further web pages/websites created by or on behalf of  <b>FIRMER</b>, related to the above-mentioned site;</p>
            <p>The singular includes the plural and vice versa, and words importing a gender include other genders.</p>
            <p><strong><em><span style="text-decoration: underline;">Role of Firmer </span></em></strong></p>
            <p>The role of FIRMER is that of a seller and the website is in the nature of an e- commerce site whereby users may browse through the various products which may be purchased through the website. In this regard, it is clarified that the role of <b>FIRMER</b> is limited to trading of products & services with the buyers. All commercial/contractual terms are between buyers and FIRMER, which includes without limitation price, shipping costs, payment methods, payment terms, date, period and mode of delivery only. All warranties related to products and services including post sale services related to the products will be the responsibility of the concerned manufacturer through FIRMER. </p>
            <p><strong><em><span style="text-decoration: underline;">Browsing, Access and Placing Orders on the Website</span></em></strong></p>
            <p>You may access the website for obtaining information on the products offered through the website, availing information on the products, (without violation of the user agreement) and related matters. Access and use of the website is prohibited for unauthorized users.</p>
            <p>When you access the website or in order to avail of the products and services listed on the website, you may do so by creating a registered account on the website. You will be permitted to access and browse the website without creating a registered account (guest user) provided that you are not an unauthorized user. You may use this registered account for future transactions or interactions with the website as well.</p>
            <p>We recommend that you create a registered account when you use the website for the following reasons, amongst others:</p>
            <ul type="disc">
                <li>- We provide information about our promotions, offers and discounts to our Registered Users regularly;</li>
                <li>- Certain details, such as delivery addresses and contact details, will be stored for future transactions as well; and,</li>
                <li>- You can also browse the history of your Registered Account including past transactions;</li>
            </ul>
            <p>The user may be required to submit certain information mandatorily in order to open a registered account or avail of the products and services. In the event that the user does not submit such information, <b>FIRMER</b> shall have the right to restrict the nature of information made available to such user and may also refuse to provide all or any services to the user. All such mandatory information as well as optional information shall be true and correct in every aspect. The user shall be solely liable for any consequences (civil and criminal) on account of the user submitting incorrect, false, deceptive, misleading or wrong information for any reason whatsoever <b>FIRMER</b> shall not, under any circumstances whatsoever, be liable for any such incorrect, false, deceptive, misleading or wrong information submitted to <b>FIRMER</b> or for ensuring the authenticity of any information. For more information on <b>FIRMER’s</b> and user’s liabilities for information, please see the disclaimer available here.</p>
            <p><strong><em><span style="text-decoration: underline;">Registered Account Access</span></em></strong></p>
            <ul type="disc">
                <li>- The Registered User shall keep confidential and not disclose to any Person, information pertaining to the Registered User's Account and all activities carried out by the Registered User through his / her Registered Account.</li>
                <li>- The Registered User shall access the Website and avail the Services only through his own Registered Account validly created and shall not permit any other Person to use or access the Website or Services through the User's Registered Account.</li>
                <li>- The Registered User shall immediately notify <b>FIRMER</b> of any unauthorized use of the Registered User's Registered Account or any other breach of security known to the User vis-à-vis the Registered Account.</li>
                <li>- The Registered User shall be responsible for the set-up or configuration of his/ her equipment for access to the Website and the Services.</li>
<!--
                <li>- The Registered User shall have option of uploading his existing supplier database like supplier name, contact person, phone number, email id, address, and any other details which shall become automatically available on the website for other Registered Users too for making transaction and all data including any public domain data entered by the Registered User on the website shall belong to Firmer foundation PVT LTD.</li>
                <li>- The Registered User shall be fully liable for all transactions and activity carried out through the Registered Account, unless the Registered User has informed Firmer foundation PVT LTD about any hacking, breach of security or unauthorized access of the Registered Account, whether past, possible or threatened.</li>
-->
            </ul>
            <p><strong><em><span style="text-decoration: underline;">Placing of order and payment</span></em></strong></p>
            <p>Once the User accesses the Website as a Buyer, he / she may go through the various Products and services listed therein on the Website and choose the one as per their needs.</p>
            <p>Prices of the Products displayed on the Website are deemed to be a part of the Terms and conditions and are subject to change as maybe determined and displayed by a FIRMER.</p>
            <p>After the Buyer's order is finalized and confirmed, the buyer has to make payment of convenience fee along with the total product price, thereafter Buyer may choose to make payment for his / her order in several ways:</p>
            <ul type="disc">
                <li>&nbsp;Payment by debit cards or credit cards</li>
                <li>- Internet banking</li>
                <li>- Cash on / before Delivery, if available for the relevant Product</li>
            </ul>
            <p>Some of these payment methods may not be available at all times, and the Buyer may be required to choose another form of payment. Further, each payment method the Buyer chooses, may be subject to certain restrictions or conditions as detailed in these Terms.</p>
            <p>Where the Buyer uses a debit card/ credit card, the Buyer must provide us with the correct information pertaining to the cardholder’s name, card type, the card number, the card expiry date and the security code on the back of the card (if any), in order to make payment on the Website. The Buyer shall be responsible for the correctness of the information provided. While availing the various payment methods listed on the Website, <b>FIRMER</b>  shall in no way be responsible or liable for any loss or damages incurred directly or indirectly by the Buyer, in case of the following:</p>
            <ul type="disc">
                <li>- Lack of due authorization for any transactions, or</li>
                <li>- In case of the Buyer exceeding any preset limit as determined mutually between the Buyer and his / her bank, if applicable, or;</li>
                <li>Transaction declined by the Buyer's bank for any other reason</li>
                <li>- Any issues of payment arising out of the transaction</li>
            </ul>
            <p>Once the Buyer chooses a payment method, the Buyer will be redirected to a secure payment gateway where he / she will be required to complete the transaction. Please note that such gateway is a third party site and will be governed by its own terms and conditions. We shall not be responsible for any activity, security breaches, claims or losses of any nature whatsoever that may occur on such site. For further information on our liability for third party sites, please see the Disclaimer available here.</p>
            <p>The Buyer hereby agrees and acknowledges that <b>FIRMER</b> does not qualify to be a banking or financial service but is merely a facilitator providing an electronic, automated online electronic payment facility, receiving payment through cash on delivery option, collection and remittance facility for the transactions on the Website using the existing authorized banking infrastructure and credit card payment gateway networks.</p>
            <p>If <b>FIRMER</b> has reason to believe that the Buyer has a prior history of questionable charges including breach of agreements with <b>FIRMER</b> previously or violation of any laws, policies, or any charges imposed by a banking facility, <b>FIRMER</b> , for the protection of its own interests, reserves the right to refuse to process transactions of such Buyers</p>
            <p>Incase Buyer defaults on payment or in the event of a cheque bounce, either <b>FIRMER</b>, have the full right to initiate appropriate legal action to recover the full value of the supplier invoice, interest, legal and other applicable charges.</p>
            <p>FIRMER may do such checks as it deems fit, to confirm credibility and commitment of Buyers, when they opt for the cash on delivery for the Products, for security or other reasons at the discretion of FIRMER. As a result of such a check, if FIRMER is not satisfied with the credibility of the Buyer or genuineness of the transaction, it will have the right to reject the receipt of / Buyer's commitment to pay cash on delivery for the Products purchased.</p>
            <p>FIRMER may delay dispatch of the Products to the Buyer, if <b>FIRMER</b> deems a transaction of unusually high volumes to be suspicious, to ensure safety of the transaction. In addition, FIRMER may remit the monies paid by the Buyer (instead of refunding the same to Buyer) or dispatch Products to law enforcement officials, at the request of law enforcement officials or in the event the Buyer is engaged in any form of illegal activity.</p>
            
            <p><strong><em><span style="text-decoration: underline;">Delivery Deadlines</span></em></strong></p>
            <p>FIRMER undertakes to make every reasonable effort to ensure that the products are delivered according to the delivery schedules mentioned on the website, at the time of confirming the order. However these are not fully guaranteed.</p>
            <p>
                Weekends, public holidays, together with delays caused by events beyond the control of FIRMER are not included when FIRMER publishes the estimated number of days for door to door delivery time on the website. The route and method by which the product is transported shall be at the sole discretion of FIRMER.
            </p>
            <p><strong><em><span style="text-decoration: underline;">Undeliverable or rejected products</span></em></strong></p>
            <p>In a situation where the FIRMER / authorized personnel are unable to deliver the product to the specified address, they will try and leave a notice at the address of the receiver / buyer stating that the delivery has been attempted and where the product can be collected from. If the delivery has not been successful after one more attempt or the receiver / buyer has not collected / refuses to collect the product from the address mentioned in the notice, the FIRMER shall contact the buyer for further instructions with regard to the delivery of the product. The buyer hereby agrees to bear the costs incurred by the FIRMER in forwarding, disposing of or returning the product and the charges (if any) for making a third and final attempt at delivering the products to the receiver / buyer.</p>
            <p>It is being further clarified that in case FIRMER does not receive any instructions from the Buyer within 30 (thirty) days from the time the FIRMER / its authorized personnel had contacted the receiver / Buyer after the second attempt to deliver the Products, then the Buyer agrees and gives FIRMER the permission to destroy, dispose or sell the Products as they please, without any further liability to the Buyer.</p>
            <p><strong><em><span style="text-decoration: underline;">Warranty</span></em></strong></p>
            <p>All products for display on the website may carry warranty, if any, provided by the respective manufacturers/ dealers. Since <b>FIRMER</b> acts only as a trader, <b>FIRMER</b> does not make any representation or warranty as to specifics of the products (including but not limited to quality, value, salability, etc.) or services listed on its website for sale. <b>FIRMER</b> shall not have any liability whatsoever for any aspect of the arrangements between the Seller and the User as regards the standards of the Products or services provided by the Sellers and <b>FIRMER</b> accepts no liability for any errors or omissions, whether on behalf of itself or third parties.</p>
            <p>Replacement or warranty on each product supplied shall be as determined and specified by the seller against the specific products displayed on the website. The buyer is hereby advised to go through the conditions of replacement and warranty, if applicable, before making a purchase of the product.</p>
            <p><strong><em><span style="text-decoration: underline;">Claims</span></em></strong></p>
            <p>The Buyer shall be entitled to claim refunds in case the delivery of the Products is delayed more than 7 days beyond the agreed time of delivery, as indicated by the Seller on the Website. To claim refund in cases of delayed delivery, the buyer can contact our customer care at&nbsp;<span style="text-decoration: underline;"><a href="mailto:info@firmer.in">info@firmer.in</a></span>&nbsp;and the refund shall be processed upon due verification of details by us, and in accordance with the refund policy specified in these Terms.</p>
            <p><strong><em><span style="text-decoration: underline;">Limitation of Firmer’s liability</span></em></strong></p>
            <p>While <b>FIRMER</b> has taken steps to ensure that all the information on the website is correct, FIRMER does not guarantee and shall not be responsible for the quality, accuracy or completeness of any data, information, Services, except as specifically mentioned in the Terms.</p>
            <p><b>FIRMER</b> shall not be responsible for any losses, damages, costs, claims, charges, suits or proceedings of any nature, whatsoever (whether direct, indirect, punitive, exemplary, consequential, under tort, or for loss of profit, reputation or goodwill, costs of procuring substitute services, costs on account of loss or unauthorized use or access of information on the Website, third-party actions) arising out of or in connection with the provision of the Services or access of the Website by a User for any reason whatsoever. The generality of the foregoing shall not prejudice the specific disclaimers stated elsewhere in the terms.</p>
            <p>Notwithstanding the above, if <b>FIRMER</b> is made party to any suit or legal proceedings, the liability of <b>FIRMER</b>, for any reason, whatsoever pertaining to the services provided to the user, including without limitation for loss, damage, wrong delivery, non-delivery of the products or any part thereof or for breach of these terms, willful negligence on part of FIRMER, is limited to the value of the transaction conducted by a registered user / guest user on the website.</p>
            <p><strong><em><span style="text-decoration: underline;">Use of information</span></em></strong></p>
            <p>"Confidential Information “shall mean and include, but is not restricted to, all information that is technical, and commercial concerning business, books of record and account, financial data, systems, software, services, wages related information, documents, prototypes, samples, media, documentation, discs and code, trade secrets, know-how, proprietary information (including listings and member directories), business and marketing plans, financial and operational information, and all other non-public information, material or data relating to the current and/ or future business and operations of <b>FIRMER</b> or its group companies, partners and affiliates, and analysis, compilations, studies, summaries, extracts or other documentation whether in written or oral form, provided by <b>FIRMER</b> or a third party person on behalf of <b>FIRMER</b> to the user pursuant to access or use of the website or the products displayed therein or services, or any other information which may come to the knowledge of the user and whether or not marked as confidential information.</p>
            <p>The user hereto agrees that the user shall not disclose the confidential information to any third party under any circumstances whatsoever without the prior written consent of FIRMER and that he / she will use such confidential / proprietary information solely for the purposes of availing the Services.</p>
            <p>Further, the User shall reveal Confidential Information to its employees or agents or other associates strictly on a "need to know" basis and shall impose upon them the confidentiality obligations stated herein.</p>
            <p>The obligation of non-disclosure described herein, will not be deemed to restrict the User from using and / or disclosing any of the Confidential Information which:</p>
            <ul class="disc">
                <li>Is or becomes publicly known or within the public domain without the breach of the Terms;</li>
                <li>- If the User is requested or required by law or by any Court or governmental agency or authority to disclose any of the Confidential Information, then the User will provide FIRMER with prompt notice of such request or requirement prior to such disclosure.</li>
            </ul>
            <p>No user shall use confidential information obtained from the services for further distribution, publication, public display, or preparation of derivative works or facilitate any of these activities in any way or reproduce, copy, access or download such confidential information for any purpose other than the user’s internal or personal purposes and undertakes not to make modifications to such information or make additional representations and warranties relating to such documents.</p>
            <p>Any violation of the above terms by the user shall be treated as a material breach of the terms entitling <b>FIRMER</b> to terminate the User Agreement, seek injunctive relief, damages and any other relief provided under law against such User.</p>
            <p>Upon the termination of the User Agreement, the User shall return to <b>FIRMER</b> all original documents, records, data and other material in the possession, custody or control of the User forming a part or incorporating any Confidential Information therein.</p>
            <p>The obligations set forth in this clause shall inure irrespective of the termination of the User Agreement for any reasons whatsoever.</p>
            <p><strong><em><span style="text-decoration: underline;">Intellectual property of FIRMER and third parties</span></em></strong></p>
            <p>While FIRMER owns all the Intellectual Property rights in the listings and databases, layout and design of the Website, all logos, brands, trademarks, designs, copyrights, service marks and related Intellectual Property rights in the Products being sold to the Users, shall be the sole and exclusive property of FIRMER who are displaying their Products on the Website. The Sellers in displaying their Products on our Site have represented that they either own or are authorized licensed owners in such intellectual property rights.</p>
            <p>Access to the website does not confer upon the user any license or right of any nature to use such logos, brands, trademarks, designs, copyrights and service marks and the user is prohibited from using the same in any manner. In the event that the user violates the above terms, <b>FIRMER</b> shall be entitled to proceed against the User for the protection of its rights, including (but not limited to) injunctive relief and damages.</p>
            <p>Any use by the user of such intellectual property (whether during the term of the user agreement or thereafter) for any reason whatsoever or in any media whatsoever, or any act committed by any user with the intention to violate or facilitating the violation of such intellectual property rights, shall constitute a violation of the intellectual property rights of <b>FIRMER</b> and <b>FIRMER</b> shall be entitled to take all action and/or initiate all legal proceedings of any nature in order to protect its rights and prevent further violation thereof, including (but not limited to) damages and injunctive relief.</p>
            <p><strong><em><span style="text-decoration: underline;">Prohibitions</span></em></strong></p>
            <p>In addition to the obligations on the User contained elsewhere in the Terms and without prejudice to the generality of any other prohibition contained therein, every User shall be prohibited from the following:</p>
            <ul class="disc">
                <li>- Allowing a person other than the User to access the User's Registered Account or to access the Website through another User's Registered Account</li>
                <li>- Using the Services or accessing the Website for any unlawful or illegal purpose</li>
                <li>- Allowing another Person to avail the Services under the User's Registered Account</li>
                <li>- Making commercial use of the Products and Services or other information on the Website</li>
                <li>- Contravening any laws of India or any other laws to which the User is subject to while accessing the Website or availing of the Services</li>
                <li>- Threatening, harming, bullying, harassing, annoying or otherwise displaying harmful or negative behavior on the Website in general or towards any particular User or any third party Person</li>
                <li>- Violating or attempting to violate the security systems and procedures of FIRMER and / or the Website; gaining unauthorized access to any non-public information of any other Users or Persons, including (but not limited to) accessing data and information not intended for such Users or logging onto a server or account which the User is not authorized to access</li>
                <li>- Attempting to probe, scan or test the vulnerability of a system, computer resource, server or network or attempting to breach security or authentication measures on the Website</li>
                <li>- Attempting to gain access or gaining access to parts of the Website to which the User has no right to access by hacking, stealing passwords or otherwise</li>
                <li>- Introducing, posting, submitting or transmitting any information, software, command or any other material which contains a virus, worm, time bombs, cancel bots, easter eggs or other harmful component into the Internet through the Website, the Website itself or <b>FIRMER</b> 's systems, networks or servers</li>
                <li>- Introducing, facilitating, transmitting, generating or posting spam messages, posts or e-mails of any nature whatsoever, including bulk e-mails, chain mails or commercial bulk mails or spam</li>
                <li>- Introducing, facilitating, transmitting, generating or posting spam messages, posts or e-mails of any nature whatsoever, including bulk e-mails, chain mails or commercial bulk mails or spam</li>
                <li>- Introducing any material or doing any act which would cause the networks, resources, systems or servers of <b>FIRMER</b> and / or the Website to slow down or multiplying traffic on the Website or in any other way interfering or hindering the proper functioning of such networks, resources, systems or servers</li>
                <li>- Publishing, displaying or submitting information which provides information on illegal activities, communities, groups, Persons or associations which are banned or which incite or participate in illegal activities, or other activities such as the sale of illegal arms, ammunition and weapons</li>
                <li>- Soliciting directly or indirectly any passwords or other personal information of any Users or other Persons for commercial or illegal purposes, or hosting, publishing or submitting information about other Persons without such Person’s consent</li>
                <li>- Using any "deep-link", "page-scrape", "robot", "spider" or other automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any portion of the Website or any Content, or in any way reproducing or circumventing the navigational structure or presentation of the Website or any content therein, to obtain or attempt to obtain any materials, documents or information through any means not purposely made available through the Website</li>
                <li>- Hosting, displaying, uploading, modifying, publishing, transmitting, updating or sharing any information on the Website that:</li>
                <ul class="circle">
                    <li>a. belongs to another person and to which the User does not have any right to;</li>
                    <li>b. is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, pedophilic, libelous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever;</li>
                    <li>c. is misleading in any way;</li>
                    <li>d. is patently offensive to the online community, such as sexually explicit content, or content that promotes violence, physical harm, obscenity of any kind against any individual or group or aims to sexually harass any individual or group;</li>
                    <li>e. harm minors in any way;</li>
                    <li>f. infringes any patent, trademark, copyright or other proprietary rights;</li>
                    <li>g. violates any law in force;</li>
                    <li>h. deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</li>
                    <li>i. impersonates another person;</li>
                    <li>j. contains software viruses or any other computer codes, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;</li>
                    <li>k. threatens the unity, integrity, defense, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation.</li>
                </ul>
            </ul>
            
            <p>In the event that you become aware of a violation of the above Terms by another User, we would request you to please inform our Grievance Officer (whose details are given below in this document) in writing or through email signed with an electronic signature of the same in as much detail as possible. Before we take any action, we may request you for further information as required by us or by law (as amended from time to time), based on which will we consider the next course of action to be adopted against such violations.</p>
            <ul class="disc">
                <li>- Sending or receiving messages which are offensive on moral, religious or political grounds or which are indecent in any manner including (but not limited to) being within the meaning of "indecent representation of women" under The Indecent Representation of Women (Prohibition) Act, 1986</li>
                <li>- Promoting or encouraging participation in contests, membership of competitor websites, gambling, lottery, sweepstakes or pyramid schemes</li>
                <li>- Altering or removing any of the Terms or other terms of use contained elsewhere on the Website or misrepresenting the same to any other person</li>
                <li>- Copying or reproducing any part of the Website for any purpose whatsoever (save as expressly allowed under these Terms) whether in electronic or hard form, whether manually or through automated methods including (but not limited to) as spiders, deep links, page scrapes or crawlers</li>
                <li>- Violating, by any act or omission in the course of access to the Website or availing the Services, the rights of any nature whatsoever of a third party Person or a User, including publicity and/or ownership rights and intellectual property rights (including but not limited to inducement to violate rights or links to pirated music or files of any nature) or inducing Users or Persons to deal in, buy or sell stolen or counterfeit goods or goods or services otherwise prohibited for trade under any law for the time being in force</li>
                <li>- Restricting another User's access to the Website, Services or other enjoyment of the User's rights vis-à-vis the Website and / or Services</li>
                <li>- Doing any act of any nature whatsoever which would cause <b>FIRMER</b> to lose any services or supplies from <b>FIRMER’s</b> vendors, suppliers or Sellers of any nature whatsoever, or disturb or affect <b>FIRMER’s</b> relationship with its associates, suppliers, service providers, Sellers or vendors or otherwise create any liability of any nature whatsoever on <b>FIRMER</b></li>
            </ul>
            <p>In the event that the User breaches, violates or does not comply with any of the above mentioned covenants, then, without prejudice to any other rights of <b>FIRMER</b> under the Terms or under law, <b>FIRMER</b> shall have the following rights:</p>
            <ul class="disc">
                <li>- To remove any such information, data, blog posts, messages or other material, which violate any of the above covenants;</li>
                <li>- To suspend or delete the User's Registered Account;</li>
                <li>- To take action, if required under any Applicable Law;</li>
                <li>- To suspend or prohibit the User's access to the Website or the User's Registered Account;</li>
                <li>- To terminate the User Agreement, access to the Website and / or provision of the Services without becoming liable to pay any damages;</li>
                <li>- To proceed against the User for all or any losses or damages caused to <b>FIRMER</b> of any nature whatsoever (including loss of business, reputation, goodwill and profit) and to initiate or assist in all legal proceedings of any nature (civil or criminal) against the User;</li>
                <li>- <b>FIRMER</b> shall also be indemnified for all proceedings, losses and damages that may be instituted against or incurred by <b>FIRMER</b> on account of such a breach.</li>
            </ul>
            <p><strong><em><span style="text-decoration: underline;">Breach</span></em></strong></p>
            <p>In the event that any User commits or attempts to commit or abets the commitment of, in the sole discretion and opinion of <b>FIRMER</b>, a breach of any nature whatsoever of the Terms or the User Agreement, whether in letter or spirit, then without prejudice to the rights of <b>FIRMER</b> contained elsewhere in the Terms or under law, <b>FIRMER</b> shall have all the following rights:</p>
            <ul class="disc">
                <li>- To terminate, without reasons or notice, the User Agreement with a User;</li>
                <li>- To block, suspend, delete or restrict access to a Registered Account by a User;</li>
                <li>- To block or restrict access to the Website;</li>
                <li>- To refuse or cease the provision of Services to such User;</li>
                <li>- To remove any information of any nature whatsoever on the Website posted, uploaded, published, displayed or submitted by a User;</li>
                <li>- To initiate all or any legal proceedings against a User for breach, civil and / or criminal, including (but not limited to) proceedings for indemnity, losses, damages and / or injunctions as appropriate;</li>
                <li>- To cooperate (without notice to the User) with all or any Governmental or legal authorities involved in any investigations, including the release of any information submitted by a User to <b>FIRMER</b>/ on the Website;</li>
            </ul>
            <p>In the event that a User is blocked, suspended or otherwise prevented or prohibited from accessing the Website, Services and / or the User's Registered Account, then the User shall not attempt to access the Website or procure Services through another User's Registered Account or by creating another Registered Account or through any other false identity.</p>
            <p><strong><em><span style="text-decoration: underline;">Indemnification</span></em></strong></p>
                    <p>The User agrees that the User shall indemnify and keep indemnified <b>FIRMER</b>, its group companies, subsidiaries, associates, employees, licensees and partners from all or any losses, damages, suits, claims, charges, proceedings, actions, demands and costs of any nature whatsoever incurred by such Person on account of any act or omission of the User while accessing the Website or availing of the Services or a breach of the Terms committed by the User.</p>
            <p><strong><em><span style="text-decoration: underline;">Force Majeure</span></em></strong></p>
                    <p>If FIRMER’S performance of any of its obligations hereunder is prevented, restricted or interfered with by reason of fire or other casualty or accident, strike or labor disputes, maintenance or failure of any network, computer resource, system or server, failure of the server’s uptime, war or other violence, any law or regulation of any Government, or any act or condition whatsoever beyond its reasonable control (each such occurrence being hereinafter referred to as "Force Majeure") then <b>FIRMER</b> shall be excused from such performance to the extent of such prevention, restriction and interference provided, however, that <b>FIRMER</b> shall give notice (except where such notice is impractical or not possible) to the User, of such Force Majeure, including a description, reasonably specifying the cause of non-performance hereunder, whenever such causes are removed.</p>
                    <p>In the event that <b>FIRMER's</b> performance of any of its obligations hereunder is delayed as a result of a Force Majeure, the obligation of <b>FIRMER</b> to perform such obligation shall stand extended for the term the Force Majeure existed. However, in case <b>FIRMER</b> is unable to perform any material obligation under this Agreement for a continuous period of 180 (one hundred and eighty) days because of any Force Majeure, then either of the Parties will have the right to terminate this Agreement effective 30 (thirty) days after the expiration of such 180 (one hundred and eighty) days period from the date the act Force Majeure has occurred.</p>
            <p>&nbsp;</p>
            <p><strong><em><span style="text-decoration: underline;">Termination</span></em></strong></p>
                    <p><b>FIRMER</b> may, without notice in its sole discretion, and at any time and without giving notice or reasons, terminate the User Agreement with any User and / or restrict a User’s use or access to the Website and / or Service (or any part thereof) for the following reasons:</p>
            <ul class="disc">
                <li>- That <b>FIRMER</b> , in its discretion, is of the view that the User has violated the Terms in letter or in spirit;</li>
                <li>- That in the opinion of <b>FIRMER</b> or any Governmental authority or body, it is not in public interest to continue to provide Services to the User or allow access to the Website;</li>
                <li>- That the User is declared a bankrupt or into any compromise or arrangement with its creditors.</li>
            </ul>
                    <p>The above termination shall be without prejudice to the other rights of <b>FIRMER</b> whatsoever.</p>
                    <p>In addition to the above, either <b>FIRMER</b> or a User may terminate the User Agreement by giving the other a prior written notice of 7 days to this effect without assigning reasons. At the discretion of <b>FIRMER</b>, such notice period may be waived or reduced by <b>FIRMER</b> vis-à-vis a User.</p>
            <p><strong><em><span style="text-decoration: underline;">Maintenance</span></em></strong></p>
                    <p><b>FIRMER</b> shall have the right, at its sole discretion, to carry out maintenance, repair, upgrading, testing, updating content or other works on the Website or to its servers at any time, and for such reason may deactivate or suspend the User’s Registered Account, access to the Website or the Services. <b>FIRMER</b> shall not, for any reason whatsoever, be liable for losses and damages of any nature whatsoever on account of the above.</p>
            <p><strong><em><span style="text-decoration: underline;">Modification of services</span></em></strong></p>
                    <p><b>FIRMER</b> shall be entitled to add, delete, modify, cease, discontinue, amend or change all or any of the Services at its discretion at any time. Such addition, deletion, modification, cessation, discontinuance, amendment or change shall take effect immediately. On no account shall <b>FIRMER</b> be liable in any manner whatsoever for the discontinuation, modification, stoppage, amendment or change to any Services.</p>
            <p><strong><em><span style="text-decoration: underline;">Advertisers</span></em></strong></p>
                    <p>FIRMER does not endorse or recommend any advertiser or any of their products or services. All correspondences, commercial dealings and transactions of any nature whatsoever by a User with such an advertiser, and all the terms of such dealings transactions (including but not limited to sale prices, fees, warranties, representations and other conditions) shall be at the User’s own risk and as agreed between such advertiser and the User alone. <b>FIRMER</b> shall not, under any circumstances whatsoever, be held liable for any aspect or dispute arising out of or in connection with any such dealings or transactions.</p>
            <p><strong><em><span style="text-decoration: underline;">Additional communication via messages</span></em></strong></p>
                    <p>In addition to sending you SMSs in consequence of a transaction by you on the Website, we may also send you SMSs with promotional content on our offers, sales, discounts and related information if you agree to this at the time of accessing the Website. In such cases, you expressly disclaim and waive all claims and complaints against FIRMER in the event that you receive such SMSs, irrespective of whether or not you have registered on the National Customer Preference Register, and will indemnify FIRMER for all or any losses, damages, suits or claims made on <b>FIRMER</b> on account of such SMSs being sent to you.</p>
            <p><strong><em><span style="text-decoration: underline;">&nbsp;Relationship between parties</span></em></strong></p>
                    <p>The relationship between <b>FIRMER</b> and the User shall be on a principal-to-principal basis and nothing in the Terms shall be construed so as to imply a relationship of agency, employment, partnership, joint venture, franchise or technical collaboration between both <b>FIRMER</b> and the User.</p>
            <p><strong><em><span style="text-decoration: underline;">Governing law and jurisdiction</span></em></strong></p>
            <p>These Terms and the User Agreement shall be governed by and construed in accordance with the laws of India. All disputes arising out of or in any way connected with these Terms and / or the User Agreement shall be deemed to have arisen at Chennai, India and the appropriate courts of law in Chennai, India shall have exclusive jurisdiction to resolve the same.</p>
            <p><strong><em><span style="text-decoration: underline;">&nbsp;Waiver</span></em></strong></p>
            <p>Failure to exercise any right under these Terms, in any one or more instances, shall not constitute a waiver of such rights or any other rights in any other instances.</p>
            <p><strong><em><span style="text-decoration: underline;">Severability</span></em></strong></p>
            <p>If one or more of the provisions hereof shall be void, invalid, illegal or unenforceable in any respect under any Applicable Law or decision, the validity, legality, and enforceability of the remaining provisions herein contained shall not be affected or impaired in any way.</p>
            <p><strong><em><span style="text-decoration: underline;">Assignment</span></em></strong></p>
                    <p><b>FIRMER</b> may assign the User Agreement to any Person without notice to the User. The User shall not be entitled to assign the User Agreement, either fully or in part, with any of its rights and obligations to any other Person without the prior written consent of <b>FIRMER</b>.</p>
            <p><strong><em><span style="text-decoration: underline;">Entire agreement</span></em></strong></p>
                    <p>No oral agreement exists between the User and <b>FIRMER</b>. The User Agreement shall constitute the entire and exclusive agreement between User and <b>FIRMER</b> with respect to the subject matter hereof, and shall supersede and subsume any prior agreements, documents and or communications regarding such subject matter.</p>
            <p><strong><em><span style="text-decoration: underline;">Notice</span></em></strong></p>
            <p>All notices shall be in English and in writing and sent to the following addresses:</p>
            <ul class="disc">
                <li>- If sent or addressed to the User, then to the address provided by the User in the User's Registered Account;</li>
                <li>- If sent or addressed to <b>FIRMER</b>, then to the following address: Name and Designation: <b>FIRMER</b>, No 93,4th Main Road,Q Block ,Anna Nagar Chennai – 600040,India. E-mail Address: <a href="mailto:info@firmer.in">info@firmer.in.</a></li>
            </ul>
            <p>Notices by FIRMER to a User shall be displayed on these Terms, elsewhere on the Website or sent to the User at the above mentioned address.</p>
            <p><strong><em><span style="text-decoration: underline;">Grievances and violations</span></em></strong></p>
            <p>In the event that a User has any other grievance in relation to the Website, or if a User becomes aware of an abuse or violation of the Terms, then the User may contact FIRMER Grievance Officer at the following address: Name and Designation: FIRMER, No 93,4th Main Road ,Q Block ,Anna Nagar Chennai – 600040,India.,  E-mail Address:<a href="mailto:info@firmer.in">info@firmer.in</a></p>
            
        </div>
        

    </div>
</div>

@endsection