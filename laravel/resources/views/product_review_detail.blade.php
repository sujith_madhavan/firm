@php ($page = "productreview")
@php ($customer_sidebar = "productreview")
@extends('layouts.app')
@section('styles')
    <link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection 
@section('content')

<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
                @include('layouts._customer_sidebar')
                <div class="col-md-9 dashboard-right tabcontent" id="review">
                    <div class="product-detail">
                        <div class="reg-head">
                            <h2><img src="{{asset('frontend/images/review1.png')}}">My Product Review</h2>
                        </div>
                        <div class="log-border">
                            <div class="product-review">
                                <div class="reg-head">
                                    <h2>REVIEW DETAIL</h2>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <div class="photo2">
                                            <img src="{{$product_review_detail->product->image_path}}" class="img-resposive">
                                        </div>
                                        <p class="cus-rating"> Average Customer Rating :</p>

                                    </div>
                                    <div class="col-md-9 col-sm-9">

                                        <div class="product-st">
                                            <p> {{ $product_review_detail->product->name ?? ''}} </p>
                                        </div>
                                        <div class="rating">
                                            <h5> Your Rating :</h5>
                                            <p><span class="heading"> Quality </span>
                                                 @for ($i = 0; $i < ($product_review_detail->quality); $i++)
                                                <span class="fa fa-star checked"></span>
                                                @endfor
                                                <!-- <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span> --></p>
                                            <p><span class="heading" style="margin-right:10px;"> value </span>
                                                @for ($i = 0; $i < ($product_review_detail->value); $i++)
                                                <span class="fa fa-star checked"></span>
                                                 @endfor
                                               </p>

                                            <p><span class="heading" style="margin-right:13px;"> price </span>
                                                @for ($i = 0; $i < ($product_review_detail->price); $i++)
                                                <span class="fa fa-star checked"></span>
                                                  @endfor</p>
                                            <p> Your review (submitted on march 28,2018 {{date("$product_review_detail->created_at", time())}} ) </p>
                                            <p class="site-line"> {{ $product_review_detail->review }}
                                        </div>
                                    </div>
                                </div>
                                <a href="{{url('/customer/product-review')}}" class="back"> &laquo; Back to My Reviews </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
