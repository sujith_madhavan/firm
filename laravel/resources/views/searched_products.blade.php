@if(empty($category->parent_id))
@php ($page = $search_name)
@else
@php ($page = $search_name)
@endif    
@extends('layouts.app') 
@section('content') 
<div class="building-header">
    <div class="building-banner" 
    style="background-image: url({{ asset('frontend/images/banner1.jpg') }} ) ">
    <div class="banner-content">
        <h3><span>SEARCH PRODUCTS</span></h3>
    </div>
</div>
<div class="breadcrumb">
    <a href="{{ url('/') }}"><img src="{{ asset('frontend/images/home.png') }}"></a>&nbsp;
    <i class="fa fa-angle-right" aria-hidden="true"></i>
    <p>&nbsp;{{ $search_name }}</p>
</div>
<div class="container-fluid">
    <div class="">
        <div class="sidebar_prod">
            @if(count($categories))
            <div class="shop-left-area" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="single-left-shop mb-40">
                    <form id="category_filters" action="" method="post">
                        {{ csrf_field() }}
                        <div class="dropdown_categories">
                            <div class="shop-title-hide mb-20" role="tab" id="headingOne">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h3>CATEGORIES</h3>
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse in arrow-content" role="tabpanel" aria-labelledby="headingOne">
                                <ul class="brand">
                                    @foreach($categories as $category)
                                    <li><a href="{{ url('category',[$category->slug]) }}">{{ strtoupper($category->name) }}</a></li>
                                    @endforeach
                                </ul>
                            </div>   
                        </div>              
                    </div>    
                </div>
                @endif 

            </div>
            <div class="products-img">
                <div class="row">
                    <div class="col-md-6 col-sm-6 b">
                        <h3>Search results for ' {{ ucwords($search_name) }} '</h3>
                    </div>
                </div>
            </form>
            <div class="row">
                @if(count($products))
                @foreach($products as $product)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-img">
                    <div class="product-inner">
                        <div class="image-container">
                            <img src="{{$product->thumb_image_path}}">

                            <div class="image-hover">
                                <a href="{{url('/category/product')}}/{{$product->slug}}"><img src="{{asset('frontend/images/hover.png')}}">
                                    <p>VIEW PRODUCT</p></a>
                                </div>

                                <div class="overlay"></div>
                            </div>
                            <div class="product-detail">
                                <p>{{ $product->name }}</p>
                                 @if(!empty($product->price))
                                    @if($product->price==1)

                                    @else
                                @if($product->offer_valid && !empty($product->slashed_price))
                                <p class="rate">
                                    Rs
                                    <strike> {{ $product->price }} / </strike>
                                    <span> {{$product->slashed_price}} / {{$product->price_for}}</span></p>
                                    @else
                                    <p class="rate"><span>Rs {{$product->price}} / {{$product->price_for}}</span></p>
                                    @endif
                                    @endif
                                    @endif
                                </div>
                                <div class="add-card">
                                    <div class="row">
                                        <div class="col-xs-6 detail">
                                            <a href="{{url('/category/product')}}/{{$product->slug}}">View detail</a>
                                        </div>
                                        @if(!empty($product->price))
                                        @if($product->price==1)

                                        @else
                                        <div class="col-xs-6 card">
                                            <a href="{{url('/list-cart-add')}}/{{$product->id}}">Add</a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="row">
                            <h3>NO PRODUCTS</h3>
                        </div>
                        @endif      
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="view">

                        <div class="row">
                            <div class="col-md-6 col-sm-12 left-view" >
                                <!-- <a class="view-more" href="#">View more</a> -->
                            </div>
                            <div class="col-md-6 col-sm-12 right-view">
                                <div class="view-inner">
                                   @if(method_exists($products,'links'))
                                   @if($products->total() > 12)
                                   <p>{{ $products->total() }} products</p>
                                   @endif    
                                   <span class="pull-right">
                                    {{ $products->links() }}
                                </span>   
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection