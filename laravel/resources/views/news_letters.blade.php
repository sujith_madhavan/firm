@php ($page = "news")
@php ($customer_sidebar = "news")
@extends('layouts.app')
@section('styles')
    <link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection 
@section('content') 
<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
                @include('layouts._customer_sidebar')
                <div class="col-md-9 dashboard-right tabcontent" id="address">
                    <div class="reg-head">
                        <h2><img src="{{asset('frontend/images/mess.png')}}"> Newsletter</h2>
                    </div>
                    <div class="log-border">
                        <div class="product-review">
                            <div class="reg-head">
                                <h2>Newsletter Details</h2>
                            </div>
                             <div class="table-responsive download-pdf">
                                  <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                           <th>Date</th>
                                            <th>Tittle</th>
                                            <th>Download</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($news_letters as $news_letter)
                                            <tr>
                                                <td>{{ date('d-m-Y', strtotime($news_letter->updated_at)) }}</td>
                                                <td>{{ $news_letter->title }}</td>
                                                <td><a href="{{ asset('laravel/public/news_letters').'/'.$news_letter->attachment }}" download><img src="{{ asset('frontend/images/pdf.png') }}"></a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')

@endsection
