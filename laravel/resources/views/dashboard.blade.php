@php ($page = "dashboard")
@php ($customer_sidebar = "dashboard")
@extends('layouts.app') 
@section('content') 
<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
                @include('layouts._customer_sidebar')
                <div class="col-md-9 dashboard-right tabcontent" id="account">
                    <div class="dashboard-body">
                        <h3>MY DASHBOARD</h3>
                        <div class="my-dash-board">
                         Hello, {{ ucwords($customer->first_name) }} {{ $customer->last_name }}!
                         From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.
                     </div>
                 </div>
                 <div class="dashboard-info">
                    <h6><img src="{{asset('frontend/images/inf.png')}}">Account Information</h6>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <div class="dashboard-infocontent">
                                <h6>Contact Information <a href="{{url('/customer/account-info')}}"><img src="{{asset('frontend/images/edit.png')}}"></a></h6>
                                <p>{{ ucwords($customer->first_name) }} {{ $customer->last_name }}<br>
                                 {{ $customer->email }} <br>
                                 {{ $customer->mobile }}<br>
                                 {{ $customer->user_type }}<br>
                                 {{ $customer->gst }}
                             </p>
                         </div>
                     </div>
                     <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="dashboard-infocontent">
                            <h6>Newsletter<img src="{{asset('frontend/images/edit.png')}}"></h6>
                            <p class="news-letter">Your have {{$count}} newsletter </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="billing-address">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 address-left">
                        <div class="default-address">
                            <h6>Contact Information</h6>
                            @if(!empty($billing_address)) 
                            <p class="left-address"><span>Default Billing address: </span><br> 
                              
                                {{ ucwords($billing_address->first_name) }} {{ $billing_address->last_name }}<br>
                                {{ ucwords($billing_address->company) }} {{ $billing_address->mobile }}<br>
                                {{ ucwords($billing_address->address) }}<br>

                                @if(!empty($billing_address->address1))
                                {{ ucwords($billing_address->address1) }}<br>
                                @endif
                                @if(!empty($billing_address->address2))
                                {{ $billing_address->address2 }}<br>
                                @endif
                                @if(!empty($billing_address->fax))
                                {{ $billing_address->fax }}<br>
                                @endif
                                {{ ucwords($billing_address->city) }}, {{ ucwords($billing_address->state) }}<br>
                                {{ $billing_address->pincode }}, {{ ucwords($billing_address->country) }}. 

                            </p>
                            <h5><a href="{{url('/customer/address-edit',[$billing_address->id])}}"><i class="fa fa-angle-right"></i>Edit address</a></h5>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 address-right">
                        <div class="default-address">
                            <h6 class="manage-address">Manage Address<img src="{{asset('frontend/images/edit.png')}}"></h6>
                            @if(!empty($shipping_address)) 
                            <p class="right-address"><span>Default Shipping address:</span><br>  
                                {{ ucwords($shipping_address->first_name) }} {{ $shipping_address->last_name }}<br>
                                {{ ucwords($shipping_address->company) }} {{ $shipping_address->mobile }}<br>
                                {{ ucwords($shipping_address->address) }},<br>

                                @if(!empty($shipping_address->address1))
                                {{ $shipping_address->address1 }}<br>
                                @endif
                                @if(!empty($shipping_address->address2))
                                {{ $shipping_address->address2 }}<br>
                                @endif
                                @if(!empty($shipping_address->fax))
                                {{ $shipping_address->fax }},<br>
                                @endif
                                {{ ucwords($shipping_address->city) }}, {{ ucwords($shipping_address->state) }}<br>
                                {{ $shipping_address->pincode }}, {{ ucwords($shipping_address->country) }}. </p>
                                <h5 class="angle-right"><a href="{{url('/customer/address-edit',[$shipping_address->id])}}"> <i class="fa fa-angle-right"></i>Edit address</a></h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@endsection
