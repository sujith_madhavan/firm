
@php ($page = "privacy-policy")
@extends('layouts.app') 
@section('content') 

<!--
<div class="building-banner">
		<div class="banner-content">
			<h5>BUILDING MATERIALS</h5>
			<h3>IRON AND STEEL <span>PRODUCTS</span></h3>
		</div>
	</div>
	<div class="breadcrumb">
		<a href="index.php"><img src="images/home.png"></a>&nbsp;
		<i class="fa fa-angle-right" aria-hidden="true"></i>
		<p>&nbsp;privacy-policy&nbsp;</p>
	</div>
-->
<div class="login-banner"></div>
	<div class="building-header">
	<!-- <div class="building-banner">
		<div class="banner-content">

			<h5>privacy-policy</h5>
			<h3>IRON AND STEEL <span>PRODUCTS</span></h3>

		</div>
	</div> -->
	<div class="breadcrumb">
		<a href="index.php"><img src="{{asset('frontend/images/home.png')}}"></a>&nbsp;
		<i class="fa fa-angle-right" aria-hidden="true"></i>
		<p>&nbsp;privacy-policy&nbsp;</p>
		
	</div>
	<div class="container">

<!--<div class="container">-->
    
        <div class="std">
            <p style="text-align: center;color: #e75e0d;margin-bottom:40px;"><span style="font-size: x-large;">Privacy Policy</span></p>
            <div align="center">
                <hr align="center" noshade="noshade" size="0" width="100%">
            </div>
<!--            <p>privacy-policy</p>-->
            <p>THIS PRIVACY POLICY IS AN ELECTRONIC RECORD IN THE FORM OF AN ELECTRONIC CONTRACT FORMED UNDER THE INFORMATION TECHNOLOGY ACT, 2000 AND THE RULES MADE THEREUNDER AND THE AMENDED PROVISIONS PERTAINING TO ELECTRONIC DOCUMENTS / RECORDS IN VARIOUS STATUTES AS AMENDED BY THE INFORMATION TECHNOLOGY ACT, 2000. THIS PRIVACY POLICY DOES NOT REQUIRE ANY PHYSICAL, ELECTRONIC OR DIGITAL SIGNATURE.</p>
            <p>THIS PRIVACY POLICY IS A LEGALLY BINDING DOCUMENT BETWEEN YOU AND FIRMER (BOTH TERMS DEFINED BELOW). THE TERMS OF THIS PRIVACY POLICY WILL BE EFFECTIVE UPON YOUR ACCEPTANCE OF THE SAME (DIRECTLY OR INDIRECTLY IN ELECTRONIC FORM, BY CLICKING ON THE ACCEPTANCE/ AGREE BUTTON OR BY USE OF THE WEBSITE OR BY OTHER MEANS) AND WILL GOVERN THE RELATIONSHIP BETWEEN YOU AND <b> FIRMER</b> FOR YOUR USE OF THE WEBSITE (DEFINED BELOW).</p>
            <p>THIS DOCUMENT IS PUBLISHED AND SHALL BE CONSTRUED IN ACCORDANCE WITH THE PROVISIONS OF THE INFORMATION TECHNOLOGY (REASONABLE SECURITY PRACTICES AND PROCEDURES AND SENSITIVE PERSONAL DATA OF INFORMATION) RULES, 2011 UNDER INFORMATION TECHNOLOGY ACT, 2000; THAT REQUIRE PUBLISHING OF THE PRIVACY POLICY FOR COLLECTION, USE, STORAGE AND TRANSFER OF SENSITIVE PERSONAL DATA OR INFORMATION.</p>
            <p>PLEASE READ THIS PRIVACY POLICY CAREFULLY. BY USING THE WEBSITE, YOU INDICATE THAT YOU UNDERSTAND, AGREE AND CONSENT TO THIS PRIVACY POLICY. IF YOU DO NOT AGREE WITH THE TERMS OF THIS PRIVACY POLICY, PLEASE DO NOT USE THIS WEBSITE. YOU HEREBY PROVIDE YOUR UNCONDITIONAL CONSENT OR AGREEMENTS TO <b>FIRMER</b> AS PROVIDED UNDER SECTION 43A AND SECTION 72A OF INFORMATION TECHNOLOGY ACT, 2000.</p>
            <p>By providing us your Information or by making use of the facilities provided by the Website, You hereby consent to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal Information by <b>FIRMER</b> as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of Your Information shall not cause any loss or wrongful gain to you or any other person.</p>
            <p><b>FIRMER</b> and its subsidiaries and affiliates and Associate Companies (individually and/ or collectively, "<b>FIRMER</b>") is/are concerned about the privacy of the data and information of users (including sellers and buyers/customers whether registered or non- registered) accessing, offering, selling or purchasing products or services on <b>FIRMER</b>  websites, mobile sites or mobile applications ("Website") on the Website and otherwise doing business with <b>FIRMER</b>. Associate Companies here shall have the same meaning as ascribed in Companies Act, 2013.</p>
            <p>The terms We /Us / Our individually and collectively refer to each entity being part of the definition of <b>FIRMER</b> and the terms "You" / "Your" / "Yourself refer to the users. 
                This Privacy Policy is a contract between You and the respective <b>FIRMER</b> entity whose Website You use or access or You otherwise deal with. This Privacy Policy shall be read together with the respective Terms of Use or other terms and condition of the respective <b>FIRMER</b> entity and its respective Website or nature of business of the Website.</p>
            <p><b>FIRMER</b> has provided this Privacy Policy to familiarize you with:</p>
            <ul class="disc">
                <li>The type of data or information that You share with or provide to <b>FIRMER</b> and that <b>FIRMER</b>  collects from You;</li>
                <li>The purpose for collection of such data or information from You;<b>FIRMER</b>  information security practices and policies; and</li>
                <li><b>FIRMER</b> ’s policy on sharing or transferring Your data or information with third parties.</li>
                <li>This Privacy Policy may be amended / updated from time to time. We suggest that you regularly check this Privacy Policy to apprise yourself of any updates. Your continued use of Website or provision of data or information thereafter will imply Your unconditional acceptance of such updates to this Privacy Policy.</li>
            </ul>
            <p>1. Information collected and storage of such Information:</p>
            <p>We collect information from you when you register on our site, place an order, subscribe to our newsletter or fill out a form.</p>
            <p>When ordering or registering on our site, as appropriate, you may be asked to enter Information (which shall also include data) provided by You to <b>FIRMER</b> or collected from You by <b>FIRMER </b>, and may consist of Personal Information and Non-Personal Information.</p>
            <p><strong>1.1 Personal Information</strong></p>
            <p>Personal Information is Information collected that can be used to uniquely identify or contact You. Personal Information for the purposes of this Privacy Policy shall include, but not be limited to:</p>
            <ul class="disc">
                <li>Your name,</li>
                <li>Your address,</li>
                <li>Your telephone number,</li>
                <li>Your e-mail address or other contact information,</li>
                <li>Your date of birth,</li>
                <li>Your gender,</li>
                <li>Information regarding your transactions on the Website, (including sales or purchase history),</li>
                <li>Your financial information such as bank account information or credit card or debit card or other payment instrument details,</li>
                <li>Your financial information such as bank account information or credit card or debit card or other payment instrument details,</li>
                <li>Internet Protocol address,</li>
                <li>Any other items of sensitive personal data or information as such term is defined under the Information Technology (Reasonable Security Practices and Procedures and Sensitive Personal Data of Information) Rules, 2011 enacted under the Information Technology Act, 2000;</li>
                <li>Identification code of your communication device which You use to access the Website or otherwise deal with any <b>FIRMER</b> entity,</li>
                <li>Any other Information that You provide during Your registration process, if any, on the Website.</li>
            </ul>
            <p>Such Personal Information may be collected in various ways including during the course of You:</p>
            <ul class="disc">
                <li>registering as a user on the Website,</li>
                <li>registering as a seller on the Website,</li>
                <li>availing certain services offered on the Website. Such instances include, but are not limited to making an offer for sale, online purchase, participating in any online survey or contest, communicating with <b>FIRMER</b> "s customer service by phone, email or otherwise or posting user reviews on the items available on the Website, or</li>
                <li>otherwise doing business on the Website or otherwise dealing with any <b>FIRMER</b> entity.</li>
            </ul>
                <p>We may receive Personal information about you from third parties, such as social media services, commercially available sources and business partners. If you access Website through a social media service or connect a service on Website to a social media service, the information we collect may include your user name associated with that social media service, any information or content the social media service has the right to share with us, such as your profile picture, email address or friends list, and any information you have made public in connection with that social media service. When you access the Website or otherwise deal with any <b>FIRMER</b> entity through social media services or when you connect any Website to social media services, you are authorizing AUB BUILDUPONLINE PVT LTD to collect, store, and use and retain such information and content in accordance with this Privacy Policy.</p>
            <p><strong>1.2 Non-Personal Information</strong></p>
                <p><b>FIRMER</b> may also collect information other than Personal Information from You through the Website when You visit and / or use the Website. Such information may be stored in server logs. This Non-Personal Information would not assist <b>FIRMER</b> to identify You personally.</p>
            <p>This Non-Personal Information may include:</p>
            <ul class="disc">
                <li>Your geographic location,</li>
                <li>details of Your telecom service provider or internet service provider,</li>
                <li>the type of browser (Internet Explorer, Firefox, Opera, Google Chrome etc.),</li>
                <li>the operating system of Your system, device and the Website You last visited before visiting the Website,</li>
                <li>The duration of Your stay on the Website is also stored in the session along with the date and time of Your access,</li>
            </ul>
                <p>Non-Personal Information is collected through various ways such through the use of cookies. <b>FIRMER</b> may store temporary or permanent cookies on Your computer. You can erase or choose to block these cookies from Your computer. You can configure Your computer’s browser to alert You when we attempt to send You a cookie with an option to accept or refuse the cookie. If You have turned cookies off, You may be prevented from using certain features of the Website.</p>
                <p>Ads: <b>FIRMER</b>  may use third-party service providers to serve ads on <b>FIRMER</b> behalf across the internet and sometimes on the Website. They may collect Non- Personal Information about Your visits to the Website, and Your interaction with our products and services on the Website.</p>
            <p>Please do note that Personal Information and Non Personal Information may be treated differently as per this Privacy Policy.</p>
                <p>You hereby represent to <b>FIRMER</b> that</p>
            <ul class="disc">
                <li>the Information you provide to <b>FIRMER</b>  from time to time is and shall be authentic, correct, current and updated and You have all the rights, permissions and consents as may be required to provide such Information to <b>FIRMER</b> .</li>
                <li>Your providing the Information to <b>FIRMER</b> consequent storage, collection, usage, transfer, access or processing of the same shall not be in violation of any third party agreement, laws, charter documents, judgments, orders and decrees.</li>
            </ul>
                <p><b>FIRMER</b> and each of <b>FIRMER</b> entities officers, directors, contractors or agents shall not be responsible for the authenticity of the Information that You or any other user provide to <b>FIRMER</b> . You shall indemnify and hold harmless <b>FIRMER</b> and each of <b>FIRMER</b> entities officers, directors, contracts or agents and any third party relying on the Information provided by you in the event You are in breach of this Privacy Policy including this provision and the immediately preceding provision above.</p>
            <p>Your Information will primarily be stored in electronic form however certain data can also be stored in physical form. We may store, collect, process and use your data in countries other than Republic of India but under compliance with applicable laws. We may enter into agreements with third parties (in or outside of India) to store or process your information or data. These third parties may have their own security standards to safeguard your information or data and we will on commercial reasonable basis require from such third parties to adopt reasonable security standards to safeguard your information / data.</p>
            <p>You may, however, visit our site anonymously.</p>
            <p>2.Purpose for collecting, using, storing and processing Your Information</p>
                <p><b>FIRMER</b> collects, uses, stores and processes Your Information for any purpose as may be permissible under applicable laws (including where the applicable law provides for such collection, usage, storage or processes in accordance with the consent of the user) connected with a function or activity of each of <b>FIRMER</b> entities and shall include the following:</p>
            <ul class="disc">
                <li>2.1 To personalize your experience (your information helps us to respond better to your individual needs)</li>
                <li>2.2 To improve our website (we continually strive to improve our website offerings, based on the information and feedback we receive from you)</li>
                <li>2.3 To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)</li>
                <li>2.4 To process transactions</li>
                <li>2.5 To provide value added services such as Single Sign On. Single Sign On shall mean a session/user authentication process that permits a user to enter his/her name or mobile number or e-mail address or any combination thereof and password in order to access multiple websites and applications;</li>
                <li>2.6 To facilitate various program and initiatives launched by <b>FIRMER</b> or third party service providers and business associates</li>
                <li>2.7 Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested.</li>
                <li>2.8 To administer a contest, promotion, survey or other site feature</li>
                <li>2.9 To protect the integrity of the Website;</li>
                <li>2.10 To respond to legal, judicial, quasi-judicial process and provide information to law enforcement agencies or in connection with an investigation on matters related to public safety, as permitted by law;</li>
                <li>2.11 To conduct analytical studies on various aspects including user behaviour, user preferences etc.;</li>
                <li>2.12 To permit third parties who may need to contact users who have bought products from the Website to facilitate installation, service and any other product related support;</li>
                <li>2.13 To implement information security practices;</li>
                <li>2.14 To determine any security breaches, computer contaminant or computer virus;</li>
                <li>2.15 To investigate, prevent, or take action regarding illegal activities and suspected fraud,</li>
                <li>2.16 To undertake forensics of the concerned computer resource as a part of investigation or internal audit;</li>
                <li>2.17 To trace computer resources or any person who may have contravened, or is suspected of having or being likely to contravene, any provision of law including the Information Technology Act, 2000 that is likely to have an adverse impact on the services provided on any Website or by <b>FIRMER</b> ;</li>
                <li>2.18 To enable a potential buyer or investor to evaluate the business of <b>FIRMER</b> (as further described in paragraph Business/ Assets Sale or Transfers of the Privacy Policy).</li>
                <li>2.19 To send periodic emails</li>
                <li>2.20 The email address you provide for order processing, may be used to send you information and updates pertaining to your order, in addition to receiving occasional company news, updates, related product or service information, etc.</li>
            </ul>
            <p><em>Note: If at any time you would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</em></p>
            <p>Individually and collectively referred to as (Purposes)]</p>
                <p>You hereby agree and acknowledge that the Information so collected is for lawful purpose connected with a function or activity of each of the <b>FIRMER</b> entities or any person on their respective behalf, and the collection of Information is necessary for the Purposes.</p>
            <p>3. Sharing and disclosure of Your Information</p>
            <ul class="disc">
                <li>3.1 You hereby unconditionally agree and permit that <b>FIRMER</b>  may transfer, share, disclose or part with all or any of Your Information, within and outside of the Republic of India to various <b>FIRMER</b>  entities and to third party service providers / partners / banks and financial institutions for one or more of the Purposes or as may be required by applicable law. In such case we will contractually oblige the receiving parties of the Information to ensure the same level of data protection that is adhered to by <b>FIRMER</b> under applicable law.</li>
                <li>3.2 You acknowledge and agree that, to the extent permissible under applicable laws, it is adequate that when <b>FIRMER</b>  transfers Your Information to any other entity within or outside Your country of residence, <b>FIRMER</b> will place contractual obligations on the transferee which will oblige the transferee to adhere to the provisions of this Privacy Policy.</li>
                <li>3.3 <b>FIRMER</b>  may share statistical data and other details (other than Your Personal Information) without your express or implied consent to facilitate various programm or initiatives launched by <b>FIRMER</b> , its affiliates, agents, third party service providers, partners or banks & financial institutions, from time to time. We may transfer/disclose/share Information (other than Your Personal Information) to those parties who support our business, such as providing technical infrastructure services, analyzing how our services are used, measuring the effectiveness of advertisements, providing customer / buyer services, facilitating payments, or conducting academic research and surveys. These affiliates and third party service providers shall adhere to confidentiality obligations consistent with this Privacy Policy. Notwithstanding the above, We use other third parties such as a credit/debit card processing company, payment gateway, pre-paid cards etc. to enable You to make payments for buying products or availing services on <b>FIRMER</b> . When You sign up for these services, You may have the ability to save Your card details for future reference and faster future payments. In such case, We may share Your relevant Personal Information as necessary for the third parties to provide such services, including your name, residence and email address. The processing of payments or authorization is solely in accordance with these third parties policies, terms and conditions and we are not in any manner responsible or liable to You or any third party for any delay or failure at their end in processing the payments.</li>
                <li>3.4 <b>FIRMER</b> may also share Personal Information if <b>FIRMER</b> believes it is necessary in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of various terms and conditions or our policies. </li>
                <li> 3.5 We reserve the right to disclose your information when required to do so by law or regulation, or under any legal obligation or order under law or in response to a request from a law enforcement or governmental agency or judicial, quasi-judicial or any other statutory or constitutional authority or to establish or exercise our legal rights or defend against legal claims.</li>
                <li>3.6 You further agree that such disclosure, sharing and transfer of Your Personal Information and Non-Personal Information shall not cause any wrongful loss to You or to any third party, or any wrongful gain to us or to any third party.</li>
            </ul>
            <p>4. Links to third party websites</p>
            <ul class="disc">
                <li>4.1 Links to third-party advertisements, third-party websites or any third party electronic communication service may be provided on the Website which are operated by third parties and are not controlled by, or affiliated to, or associated with, <b>FIRMER</b>, unless expressly specified on the Website.</li>
                <li>4.2 <b>FIRMER</b> is not responsible for any form of transmission, whatsoever, received by You from any third party website. Accordingly, <b>FIRMER</b>  does not make any representations concerning the privacy practices or policies of such third parties or terms of use of such third party websites, nor does <b>FIRMER</b> control or guarantee the accuracy, integrity, or quality of the information, data, text, software, music, sound, photographs, graphics, videos, messages or other materials available on such third party websites. The inclusion or exclusion does not imply any endorsement by <b>FIRMER</b> of the third party websites, the website’s provider, or the information on the website. The information provided by You to such third party websites shall be governed in accordance with the privacy policies of such third party websites and it is recommended that You review the privacy policy of such third party websites prior to using such websites.</li>
            </ul>
            <p>5. Security &amp; Retention</p>
            <ul class="disc">
                <li>5.1 The security of your Personal Information is important to us. <b>FIRMER</b> strives to ensure the security of Your Personal Information and to protect Your Personal Information against unauthorized access or unauthorized alteration, disclosure or destruction. For this purpose, <b>FIRMER</b>  adopts internal reviews of the data collection, storage and processing practices and security measures, including appropriate encryption and physical security measures to guard against unauthorized access to systems where <b>FIRMER</b> stores Your Personal Information. Each of the <b>FIRMER</b> entity shall adopt reasonable security practices and procedures as mandated under applicable laws for the protection of Your Information. Provided that Your right to claim damages shall be limited to the right to claim only statutory damages under Information Technology Act, 2000 and You hereby waive and release all <b>FIRMER</b>  entities from any claim of damages under contract or under tort.</li>
                <li>5.2 If you choose a payment gateway to complete any transaction on Website then Your credit card data may be stored in compliance with industry standards/ recommended data security standard for security of financial information such as the Payment Card Industry Data Security Standard (PCI-DSS).</li>
                <li>5.3 <b>FIRMER</b>  may share your Information with third parties under a confidentiality agreement which inter alia provides for that such third parties not disclosing the Information further unless such disclosure is for the Purpose. However, <b>FIRMER</b>  is not responsible for any breach of security or for any actions of any third parties that receive Your Personal Information. <b>FIRMER</b>  is not liable for any loss or injury caused to You as a result of You providing Your Personal Information to third party (including any third party websites, even if links to such third party websites are provided on the Website).</li>
                <li>5.4 Notwithstanding anything contained in this Policy or elsewhere, <b>FIRMER</b>  shall not be held responsible for any loss, damage or misuse of Your Personal Information, if such loss, damage or misuse is attributable to a Force Majeure Event (as defined below).</li>
                <li>5.5 A "Force Majeure Event" shall mean any event that is beyond the reasonable control of <b>FIRMER</b> and shall include, without limitation, sabotage, fire, flood, explosion, acts of God, civil commotion, strikes or industrial action of any kind, riots, insurrection, war, acts of government, computer hacking, unauthorised access to computer, computer system or computer network,, computer crashes, breach of security and encryption (provided beyond reasonable control of <b>FIRMER</b> ), power or electricity failure or unavailability of adequate power or electricity.</li>
                <li>5.6 While We will endeavour to take all reasonable and appropriate steps to keep secure any Personal Information which We hold about You and prevent unauthorized access, You acknowledge that the internet or computer networks are not fully secure and that We cannot provide any absolute assurance regarding the security of Your Personal Information.</li>
                <li>5.7 You agree that all Personal Information shall be retained till such time required for the Purpose or required under applicable law, whichever is later. Non-Personal Information will be retained indefinitely.</li>
            </ul>
            <p>6. User discretion and opt out</p>
            <ul class="disc">
                <li>6.1 You agree and acknowledge that You are providing your Information out of your free will. You have an option not to provide or permit <b>FIRMER</b>  to collect Your Personal Information or later on withdraw Your consent with respect to such Personal Information so provided herein by sending an email to the grievance officer or such other electronic address of the respective <b>FIRMER</b> entity as may be notified to You. In such case, You should neither visit any Website nor use any services provided by <b>FIRMER</b>  entities nor shall contact any of <b>FIRMER</b>  entities. Further, <b>FIRMER</b>  may not deliver products to You, upon Your order, or <b>FIRMER</b>  may deny you access from using certain services offered on the Website.</li>
                <li>6.2 You can add or update Your Personal Information on regular basis. Kindly note that <b>FIRMER</b>  would retain Your previous Personal Information in its records.</li>
            </ul>
            <p>7. Grievance Officer</p>
            <ul class="disc">
                <li>7.1 If you find any discrepancies or have any grievances in relation to the collection, storage, use, disclosure and transfer of Your Personal Information under this Privacy Policy or any terms of FIRMER’S Terms of Use, Term of Sale and other terms and conditions or polices of any <b>FIRMER</b> entity, please contact the following:</li>
                <li>7.27.2 For <b>FIRMER</b>. </li>
                <li><b>Mr.C.KASTURI RAJ, the designated grievance officer under Information Technology Act, 2000 </b>
                    E-mail:<b><a href="mailto:md@firmer.in">md@firmer.in </a></b>
</li>
           <li>
               The details of the grievance officer may be changed by us from time to time by updating this Privacy Policy.
           </li>
            </ul>
            <p>8. Business / Assets Sale or Transfers</p>
             <ul class="disc">
                <li>8.1 <b>FIRMER</b> may sell, transfer or otherwise share some or all of its assets, including Your Information in connection with a merger, acquisition, reorganization or sale of assets or business or in the event of bankruptcy. Should such a sale or transfer occur, such <b>FIRMER</b> entity will reasonably ensure that the Information you have provided and which we have collected is stored and used by the transferee in a manner that is consistent with this Privacy Policy. Any third party to which any of <b>FIRMER</b> entity transfers or sells as aforesaid will have the right to continue to use the Information that you provide to us or collected by us immediately prior to such transfer or sale.</li>
                <li>8.2 You are also under an obligation to use this Website for reasonable and lawful purposes only, and shall not indulge in any activity that is not envisaged through the Website.</li>
            </ul>
            <p>9. Further Acknowledgements</p>
            <ul class="disc">
                <li>9.1 It is clear and easily accessible and provide statements of <b>FIRMER</b> policies and practices with respect to the Information;</li>
                <li>9.2 The logos and service marks displayed on the Website ("Marks") are the property of <b>FIRMER</b> or their vendors or respective third parties. You are not permitted to use the Marks without the prior consent of <b>FIRMER</b>, the vendor or the third party that may own the Marks.</li>
                <li>9.3 provides for the various types of personal or sensitive personal data of information to be collected;</li>
                <li>9.4 provides for the purposes of collection and usage of the Information;</li>
                <li>9.5 provides for disclosure of Information; and</li>
                <li>9.6 provides for reasonable security practices and procedures.</li>
            </ul>
            <p>&nbsp;</p>
            
        </div>
    </div>
</div>


@endsection
