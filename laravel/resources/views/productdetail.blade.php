@if(!empty($product->product_category->category->parent_id))
@php ($page = $product->product_category->category->parentCategory->name)
@else
@php ($page = $product->product_category->category->name)
@endif

@extends('layouts.app') 
@section('content') 

<div class="producthetail-header">
  <div class="building-banner" style="background-image: url({{ $category_image }})">
    <div class="banner-content">
     @if(!empty($product->product_category->category->parent_id))
     <h5>{{ $product->product_category->category->parentCategory->name }}</h5>
     @endif
     <h3>{{ $product->product_category->category->name }} <span></span></h3>
   </div>
 </div>
 <div class="breadcrumb">
  <a href="{{url('/')}}"><img src="{{asset('frontend/images/home.png')}}"></a>&nbsp;
  @if(!empty($product->product_category->category->parent_id))
  <i class="fa fa-angle-right" aria-hidden="true"></i>
  <p>&nbsp;{{ $product->product_category->category->parentCategory->name }}&nbsp;</p>
  @endif
  &nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>
  <p>&nbsp;{{ $product->product_category->category->name }}</p>    
</div>
</div>

<div class="compare-area compare-single-productt">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-5 col-sm-12 col-xs-12">
        <div class="single-thumbnail-wrapper">
          <div class="single-product-tab">
            <ul class="single-tab-control" role="tablist">
             @php ($i = 1) 
             @foreach ($product->productGalleries as $product_galleri) 
             <li @if($i==1) class="active" @endif>
              <a href="#tab{{$i++}}" aria-controls="tab-1" role="tab" data-toggle="tab">

                @if($product_galleri->type =="IMAGE")
                <img src="{{$product_galleri->image_path}}" alt="Domino" class="img-responsive" />
                @else
                <img src="{{asset('frontend/images/images1.png')}}" alt="Domino" class="img-responsive" />                               
                @endif
              </a>
            </li>
            @php ($i++) 
            @endforeach
          </ul>
        </div>
        <div class="single-cat-main">
          <div class="tab-content">
            @php ($i = 1) 
            @foreach ($product->productGalleries as $product_galleri) 
            <div role="tabpanel" class="tab-pane @if($i==1) active @endif " id="tab{{$i++}}">

              <div class="tab-single-image">
               @if($product_galleri->type =="IMAGE")
               <img src="{{$product_galleri->image_path}}" alt="" class="img-responsive" />
               @else
               <iframe height="405"  src="{{ $product_galleri->video }}" frameborder="0" allowfullscreen>
               </iframe>
               @endif
             </div>
           </div>
           @php ($i++) 
           @endforeach
         </div>
       </div>
     </div>
   </div>
   <div class="col-md-7 description-head">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12 product-detail">
        <div class="product-inner">
          <div class="product-head">
            <h2> {{$product->name}}</h2>


            @if($product->price==0)
            <button type="button" class="theme-btn" data-toggle="modal" data-target="#myModal"> Ask an Expert </button>

            @elseif($product->price==1)
            <button type="button" class="theme-btn" data-toggle="modal" data-target="#myModal">Price Based on Quantity </button>

            @elseif($product->offer_valid && !empty($product->slashed_price))  
            Starting from Rs <strike> {{ $product->price }} / </strike>&nbsp<span style="
            font-size: 16px;font-weight: 700;">Rs {{$product->slashed_price}} / {{$product->price_for}}  </span>

            @else
            Starting from <span style="font-size: 16px; font-weight: 700;
            ">Rs {{$product->price}} / {{$product->price_for}}  </span>
            @endif


          </div>
          @if(!empty($product->price))
          @if($product->price==1)


          @else
          <form id="addtocart" method="post" class="formid" action="{{url('/cart-add')}}" >
           {{ csrf_field() }}   
           <div class="grade-point bg">
            <table>
              @foreach ($sorted_attributes as $sorted_attribute) 
              @if($loop->iteration<=3 )
              <tr>
                <td> {{ $sorted_attribute['arrtibute_name'] }} </td>
                <td> :</td>
                <td>         
                  {{ $sorted_attribute['arrtibute_values'] }}
                </td>
              </tr>
              @endif
              @endforeach

            </table>
            <div class="form-group {{$errors->has('quantity')?'has-error':''}}">
              @if(!empty($product->box_quantity))
              <div class="input-group plus-minus"> 
                <span class="input-group-btn">

                  <button type="button" data-row="0" data-min="{{$product->minimum_quantity}}" class="quantity-left btn btn-theme btn-number qty_dec"  data-type="minus" data-field="">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>

                <input type="text" id="quantity0" name="quantity"  maxlength="12" class="form-control input-number qty quantity011" value="{{$product->minimum_quantity}}" min="" max="100" >
                <input type="hidden" name="box_quantity" value="{{$product->box_quantity}}" id="box_quantity">

                <span class="input-group-btn">
                  <button type="button" data-row="0" data-max="{{$product->stock}}" class="quantity-right btn btn-theme btn-number qty_inc" data-type="plus" data-field="">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
                <span class="kilo-gr">{{$product->price_for}}</span>
              </div>
              @else
              <div class="input-group plus-minus"> 
                <span class="input-group-btn">

                  <button type="button" data-row="0" data-min="{{$product->minimum_quantity}}" class="quantity-left btn btn-theme btn-number "  data-type="minus" data-field="">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>

                <input type="text" id="quantity0" name="quantity" class="form-control input-number" value="{{$product->minimum_quantity}}" min="" max="100">

                <span class="input-group-btn">
                  <button type="button" data-row="0" data-max="{{$product->stock}}" class="quantity-right btn btn-theme btn-number " data-type="plus" data-field="">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
                <span class="kilo-gr">{{$product->price_for}}</span>
              </div>

              @endif
              @if($errors->has('quantity'))
              <span class="help-block">{{ $errors->first('quantity') }}</span>
              @endif
            </div>
          </div>
          @if(!empty($product->box_quantity))
          <div class="add-to-carts">
            <div class="clearfix" style="margin-top: 10px"></div>
            <label for="sqft" style="display: block;float: left;padding: 10px 0px;font-size: 16px;">No of Boxes:</label>
            <div class="qty-holder" style="width: 45px ;    display: inline;
            padding-left: 10px;">
            <input type="text" readonly="" name="qty" id="qty" maxlength="12" value="1" title="Boxex" class="input-text " style="/*! padding: 7px; */height: 40px;width: 45px;text-align: center;font-size: 16px;color: black;">
          </div>


          <span id="ajax_loader" style="display:none"><i class="ajax-loader small animate-spin"></i></span>
        </div>
        @endif
        <input type="hidden" class="form-control" id="product_id" name="product_id" value="{{ $product->id }}">
        <div class="add-cart">
          @if(Auth::guard('customers')->check())
          <button type="button"  name="submit" class="btn btn-cart add-to-cart" data-type="plus" data-field=""> Add to cart</button>  
          @else
          <button type="button"  name="submit" class="btn btn-cart addtocart" data-type="plus" data-field=""> Add to cart</button> 
          @endif
          <button type="submit" class="btn btn-cart" data-type="plus" data-field=""> Buy Now</button>  
        </div> 
      </form>
      @endif
      @endif
      <div class="product-head1">

        <p> <span>Customer reviews ({{count($product->productReviews)}})</span> 1-{{count($product->productReviews)}} of {{count($product->productReviews)}} reviews </p>

      </div>
      <a href="#service-three" data-toggle="tab">  <p class="clr-pa pull-right" >Write a reviews <span>&raquo;</span></p></a>
      <p class="avg-rating"> Average rating ({{$product->product_average}})</p>




      <div class="rating product-star">
        @php
        $average = floor($product->product_average);
        $decimal = floatval($product->product_average)-intval($average);

        if(!empty($decimal))
        {
         $empty_star=5-$average-1;
       }
       else
       {                        
        $empty_star=5-$average;
      }
      $decimal = (string)$decimal;
      @endphp

      @for ($i = 0; $i < $average; $i++)
      <span class="fa fa-star checked"></span>
      @endfor


      @if($decimal == '0.1' || $decimal == '0.2' || $decimal == '0.3' )

      <span class="fa fa-star-half "></span>
      @endif
      @if($decimal == '0.4' || $decimal == '0.5' || $decimal == '0.6') 
      <span class="fa fa-star-half "></span>
      @endif
      @if($decimal == 0.7 || $decimal == 0.8 || $decimal == 0.9)
      <span class="fa fa-star-half "></span>
      @endif


      @for ($i = 0; $i<$empty_star; $i++)
      <span class="fa fa-star-o" data-rating="2"></span>
      @endfor

    </div>
  </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
  <div class="product-list">
    <div class="product-bottom">
      <div class="product-head2">
        <div class="product-dim">
          @if(0<$product->stock)
          <h3> Stock Available </h3>
          @else
          <h3 style="color:red">Out Of Stock</h3>
          @endif
          @if(!empty($product->product_group_id))  
          <div class="hover-img">

            <p> More Product + <span><a class="arrowup" href="#"><img src="{{asset('frontend/images/arrowup.png')}}"></a></span></p>
            <div class="stock-product">
              <div class="in-stock">
                <h6>{{$product->name}}</h6>
                <div class="row">
                  <div class="col-xs-6 left-price">
                    <p>Price displayed as per <span>Ton</span></p>
                  </div>
                  <div class="col-xs-6 right-stock">
                    <p>Avaliability : In stock</p>
                  </div>
                </div>
                <div class="cus-table">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Product name</th>
                        <th>price</th>
                        <th>qty</th>
                        <th>request for price</th>
                      </tr>
                    </thead>
                    <tbody>

                     @php ($i = 1) 
                     @foreach($product->productGroup->groupProducts as $group_product) 
                     <tr>
                      <th>{{$group_product->product->name}}</th>
                      <th>{{$group_product->product->price}}</th>
                      <th>{{$group_product->product->stock}}</th>
                      <th class="request-quote">Request quote <i class="fa fa-angle-double-right"></i></th>
                    </tr>
                    @php ($i++) 
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        @endif
      </div>
      <div class="stock-img">
        <img src="{{asset('frontend/images/stock.png')}}    ">
      </div>

    </div>
  </div>

  <div class="clearfix"></div>
  <div class="more-review text-left">
    <p> We support online 24/7</p>
    <p> <span> Call </span> :+91-9940087788</p>
    <p> <span> Mail </span>: info@firmer.in</p>
  </div>
  <div class="row">
    <div class="col-xs-6 service-enquiry">
      <div class="service-img">
        <img src="{{asset('frontend/images/support.png')}}">
      </div>
    </div>
    <div class="col-xs-6 service-enquiry">
      <div class="theme-btn1">
        <button type="button" class="theme-btn" data-toggle="modal" data-target="#myModal">Service Enquiry </button>

        <div class="modal right fade" id="myModal" role="dialog">
          <div class="modal-dialog newopen modal-dialog-slideout">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2>Request for Service</h2>
              </div>
              <div class="modal-body">
                <form method="post" action="{{ url('/service-enquiry') }}">
                  {{ csrf_field() }}   
                  <div class="contact pop-up-enq">
                    <div class="eqry row">
                      <label for="">Name :</label>
                      <div class="form-group {{$errors->has('name')?'has-error':''}}">
                        <input type="text" name="name" id="name" minlength="3" maxlength="32" required="required">
                        @if($errors->has('name'))
                        <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif
                      </div>

                      <input type="hidden" name="product_name" value="{{$product->name}}">
                      <label for="">Mail - ID :</label>
                      <div class="form-group {{$errors->has('email')?'has-error':''}}">
                        <input type="email" name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" required="required">
                        @if($errors->has('email'))
                        <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                      </div>
                      <label for="">Mobile No :</label>
                      <div class="form-group {{$errors->has('phone')?'has-error':''}}">
                        <input type="text" name="phone" maxlength="10" pattern="[9|8|7]\d{9}$" title="10 Numeric characters only" required="required">
                        @if($errors->has('phone'))
                        <span class="help-block">{{ $errors->first('phone') }}</span>
                        @endif
                      </div>
                      <label for="">Services :</label>
                      <div class="form-group {{$errors->has('services')?'has-error':''}}">
                        <select name="service" id="cusSelectbox" class="s-hidden scroll form-control">
                          <option value="Select">--Select Services--</option>
                          <option value="Electrical">Electrical</option>
                          <option value="Plumbing">Plumbing</option>
                          <option value="Interior work">Interior work</option>
                          <option value="Exterior painting">Exterior painting</option>
                          <option value="Interior painting">Interior painting</option>
                          <option value="Civil works">Civil works</option>
                          <option value="Landscaping">Landscaping</option>
                          <option value="Tank/ sump cleaning">Tank/ sump cleaning</option>
                          <option value="Product Enquiry">Product Enquiry</option>
                        </select>
                        @if($errors->has('services'))
                        <span class="help-block">{{ $errors->first('services') }}</span>
                        @endif
                      </div>

                      <label for="">Message :</label>
                      <div class="form-group {{$errors->has('message')?'has-error':''}}">
                        <!--                                                                    <input type="text" class="mess-age" name="message" id="e_message" required="required">-->
                        <textarea class="mess-age" name="message" id="e_message"></textarea>
                        @if($errors->has('message'))
                        <span class="help-block">{{ $errors->first('message') }}</span>
                        @endif
                      </div>
                      <button class="sub-mit-cont" type="submit" name="submit" id=""><i class="fa fa-paper-plane" aria-hidden="true">        
                      </i> SEND </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-xs-6">
      <div class="review-center">
        <p> ({{$product->two_star}} reviews)</p>
        <p> ({{$product->three_star}} reviews)</p>
        <p> ({{$product->four_star}} reviews)</p>
        <p> ({{$product->five_star}} reviews)</p>
      </div>
    </div>

    <div class="col-md-6 col-xs-6">
      <div class="rate-star1">
        <p> <i class="fa fa-star "></i>
          <i class="fa fa-star "></i> (2 star)</p>
          <p> <i class="fa fa-star "></i>
            <i class="fa fa-star "></i> <i class="fa fa-star "></i> (3 star)</p>
            <p> <i class="fa fa-star "></i>
              <i class="fa fa-star "></i> <i class="fa fa-star "></i>
              <i class="fa fa-star "></i> (4 star)</p>
              <p> <i class="fa fa-star "></i>
                <i class="fa fa-star "></i> <i class="fa fa-star "></i>
                <i class="fa fa-star "></i> <i class="fa fa-star "></i> (5 star)</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="product-listbottom">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 product-info cus-tab">
        <div class="tab" role="tabpanel">
          <ul id="myTab" class="nav nav-tabs nav_tabs" role="tablist">

            <li  role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
            <!-- <li  role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">Specification</a></li> -->
            <li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Review</a></li>

          </ul>
          <div class="tabs">
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="Section1">
                <section class="container product-info1">
                  <div class="para-as">
                    <p>
                      {!! $product->description !!}
                    </p>
                  </div>
                </section>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="Section2">
                <section class="container product-info1">
                  <div class="para-as">
                    <ul>
                     <table>
                       @foreach ($product->productAttributes as $product_attribute) 

                       <tr>
                        <td class="data"> {{$product_attribute->attribute->name}} </td>
                        <td> :</td>
                        <td> @foreach ($product_attribute->productAttributeValues as $product_attribute_value) 
                          {{ $product_attribute_value->attributeValue->value }}
                          @if(!$loop->last)
                          ,
                          @endif()
                        @endforeach </td>
                      </tr>
                      @endforeach
                    </table>
                  </ul>
                </div>
              </section>
            </div>
            @if(Auth::guard('customers')->check())
            <div role="tabpanel" class="tab-pane fade" id="Section3">
             <section class="container product-info1">
              <div class="review-three">
                <div class="container">
                  <div class="row">
                    @foreach ($product_review_lists as $product_review_list) 
                    <div class="col-sm-6" style="margin-bottom: 10px;">
                      <h5>Name</h5>
                      <p><b>{{$product_review_list->name}}</b></p>
                      <h5>Summary of your Review</h5>
                      <p>{{$product_review_list->review_summary}}</p>
                    </div>
                    <div class="col-sm-6 new-rew">
                      <h5 class="rat-firm">Quality</h5>
                      <div class="rating product-star">  
                        @for($i=1;$i<=$product_review_list->quality;$i++)
                        <span class="fa fa-star checked"></span>
                        @endfor

                      </div>
                    </div>
                    <div class="col-sm-6 new-rew">
                      <h5 class="rat-firm">Value</h5>
                      <div class="rating product-star">      
                       @for($i=1;$i<=$product_review_list->value;$i++)
                       <span class="fa fa-star checked"></span>
                       @endfor      
                     </div>
                   </div>
                   <div class="col-sm-6 new-rew">
                    <h5 class="rat-firm">Price</h5>
                    <div class="rating product-star">      
                      @for($i=1;$i<=$product_review_list->price;$i++)
                      <span class="fa fa-star checked"></span>
                      @endfor    
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <h5>Review</h5>
                    <p>{{$product_review_list->review}}</p>
                  </div>
                  <div style="margin-top: 10px;" calss="review-aa">
                    <hr style="width: 100%;">
                  </div>
                  @endforeach
                  
                  <div class="col-md-12">
                    <div class="review-aa">
                      <!-- <p> Be the first to review this product</p> -->
                      <!--  <hr> -->
                      <h3> WRITE YOUR OWN REVIEW </h3>
                      <p>How do you rate this product? *</p>
                    </div>
                    <form id="" method="post" action="{{ url('/product-review') }}">
                      {{ csrf_field() }}  
                      <div class="table-responsive">
                        <table class="table">
                          <tr>
                            <th> </th>
                            <th> 1 STAR</th>
                            <th> 2 STARS</th>
                            <th> 3 STARS</th>
                            <th> 4 STARS</th>
                            <th> 5 STARS</th>
                          </tr>

                          <tr>
                            <td> Quality </td>
                            <td>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="quality" value="1" @if(!empty($product_review)) 
                                  {{ $product_review->quality == '1' ? 'checked' : ''}}  @endif >

                                  <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                </label>
                              </div>
                            </td>
                            <td>
                              <div class="radio">
                                <label>
                                 <input type="radio" name="quality" value="2" @if(!empty($product_review))  {{ $product_review->quality == '2' ? 'checked' : ''}}  @endif >
                                 <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                               </label>
                             </div>
                           </td>
                           <td>
                            <div class="radio">
                              <label>
                                <input type="radio" name="quality" value="3" @if(!empty($product_review))  {{ $product_review->quality == '3' ? 'checked' : ''}}  @endif >
                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="radio">
                              <label>
                               <input type="radio" name="quality" value="4" @if(!empty($product_review))  {{ $product_review->quality == '4' ? 'checked' : ''}}  @endif>
                               <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                             </label>
                           </div>
                         </td>
                         <td>
                          <div class="radio">
                            <label>
                              <input type="radio" name="quality" value="5" @if(!empty($product_review))  {{ $product_review->quality == '5' ? 'checked' : ''}}  @endif>
                              <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td> value </td>
                        <td>
                          <div class="radio">
                            <label>
                             <input type="radio" name="value" title="Bad" value="1" @if(!empty($product_review))  {{ $product_review->value == '1' ? 'checked' : ''}}  @endif>
                             <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                           </label>
                         </div>
                       </td>
                       <td>
                        <div class="radio">
                          <label>
                            <input type="radio" name="value" title="Good" value="2" @if(!empty($product_review))  {{ $product_review->value == '2' ? 'checked' : ''}}  @endif>
                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="radio">
                          <label>
                            <input type="radio" name="value" title="very Good" value="3" @if(!empty($product_review)) {{ $product_review->value == '3' ? 'checked' : ''}}  @endif>
                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="radio">
                          <label>
                            <input type="radio" name="value" title="Great" value="4" @if(!empty($product_review))  {{ $product_review->value == '4' ? 'checked' : ''}}  @endif>
                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="radio">
                          <label>
                            <input type="radio" name="value" title="Awesome" value="5" @if(!empty($product_review))  {{ $product_review->value == '5' ? 'checked' : ''}}  @endif >
                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                          </label>
                        </div>
                      </td>

                    </tr>

                    <tr>
                      <td> price </td>
                      <td>
                        <div class="radio">
                          <label>
                           <input type="radio" name="price" title="Bad" value="1" @if(!empty($product_review)) {{ $product_review->price == '1' ? 'checked' : ''}}  @endif>
                           <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                         </label>
                       </div>
                     </td>
                     <td>
                      <div class="radio">
                        <label>
                          <input type="radio" name="price" title="Good" value="2" @if(!empty($product_review)) {{ $product_review->price == '2' ? 'checked' : ''}}  @endif>
                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                        </label>
                      </div>
                    </td>
                    <td>
                      <div class="radio">
                        <label>
                          <input type="radio" name="price" title="very Good" value="3" @if(!empty($product_review)) {{ $product_review->price == '3' ? 'checked' : ''}}  @endif>
                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                        </label>
                      </div>
                    </td>
                    <td>
                      <div class="radio">
                        <label>
                          <input type="radio" name="price" title="Great" value="4" @if(!empty($product_review))  {{ $product_review->price == '4' ? 'checked' : ''}}  @endif>
                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                        </label>
                      </div>
                    </td>
                    <td>
                      <div class="radio">
                        <label>
                          <input type="radio" name="price" title="Awesome" value="5" @if(!empty($product_review)) {{ $product_review->price == '5' ? 'checked' : ''}}  @endif>
                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                        </label>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>

              <div class="form-group">
                <div class="form-group {{$errors->has('name')?'has-error':''}}">
                  <label for="name"> Nick Name <span>*</span></label>
                  <input type="text" name="name" class="form-control" @if(!empty($product_review))  value="{{ $product_review->name }}"  @endif placeholder="">
                  @if($errors->has('name'))
                  <span class="help-block">{{ $errors->first('name') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <div class="form-group {{$errors->has('review_summary')?'has-error':''}}">
                  <label> Summary of your Review <span>*</span></label>
                  <input type="text" class="form-control" name="review_summary" @if(!empty($product_review))  value="{{ $product_review->review_summary }}"  @endif>
                  @if($errors->has('review_summary'))
                  <span class="help-block">{{ $errors->first('review_summary') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group">
               <div class="form-group {{$errors->has('review')?'has-error':''}}">
                <label> Review <span>*</span></label>
                <textarea name="review" rows="6" class="form-control"> @if(!empty($product_review))  {{ $product_review->review }}  @endif</textarea>
                @if($errors->has('review'))
                <span class="help-block">{{ $errors->first('review') }}</span>
                @endif
              </div>
              <input type="hidden" class="form-control" name="product_id" value="{{ $product->id }}">
              <input type="hidden" class="form-control" name="customer_id" value="{{ Auth::guard('customers')->user()->id }}">

            </div>

            <button type="submit" name="submit" class="btn btn-login2 pull-right"> Submit Review </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
@else 
<div role="tabpanel" class="tab-pane fade" id="Section3">
 <section class="container product-info1">
  <div class="review-three">
    <div class="container">
      <div class="row">
        @foreach ($product_review_lists as $product_review_list) 
        <div class="col-sm-6" style="margin-bottom: 10px;">
          <h5>Name</h5>
          <p><b>{{$product_review_list->name}}</b></p>
          <h5>Summary of your Review</h5>
          <p>{{$product_review_list->review_summary}}</p>
        </div>
        <div class="col-sm-6 new-rew">
          <h5 class="rat-firm">Quality</h5>
          <div class="rating product-star">  
            @for($i=1;$i<=$product_review_list->quality;$i++)
            <span class="fa fa-star checked"></span>
            @endfor

          </div>
        </div>
        <div class="col-sm-6 new-rew">
          <h5 class="rat-firm">Value</h5>
          <div class="rating product-star">      
           @for($i=1;$i<=$product_review_list->value;$i++)
           <span class="fa fa-star checked"></span>
           @endfor      
         </div>
       </div>
       <div class="col-sm-6 new-rew">
        <h5 class="rat-firm">Price</h5>
        <div class="rating product-star">      
          @for($i=1;$i<=$product_review_list->price;$i++)
          <span class="fa fa-star checked"></span>
          @endfor    
        </div>
      </div>
      <div class="col-xs-12">
        <h5>Review</h5>
        <p>{{$product_review_list->review}}</p>
      </div>
      <div style="margin-top: 10px;" calss="review-aa">
        <hr style="width: 100%;">
      </div>
      @endforeach

      <div class="col-md-12">

        <div class="review-aa">
           <!--  <p> Be the first to review this product</p> 
            <hr> -->
            <div class="review-bottom">
              <h3> WRITE YOUR OWN REVIEW </h3>
              <p>How do you rate this product? *</p>
              <a class="review-right" href="{{url('/login')}}"> Login</a>
            </div>
          </div>



        </div>
      </div>
    </div>
  </div>
</section>
</div>
@endif 
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="building-header">

  @foreach($product_categories as $category_product)

  <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 product-img">
    <div class="product-inner">
      <div class="image-container">
        <img src="{{$category_product->product->thumb_image_path}}">

        <div class="image-hover">
          <a href="{{url('/category/product')}}/{{$category_product->product->slug}}"><img src="{{asset('frontend/images/hover.png')}}">
            <p>VIEW PRODUCT</p></a>
          </div>

          <div class="overlay"></div>
        </div>
        <div class="product-detail">
          <p>{{ $category_product->product->name }}</p>
         @if(!empty($category_product->product->price))
          @if($category_product->product->price==1)

          @else
           @if($category_product->product->offer_valid && !empty($category_product->product->slashed_price))
          <p class="rate"> 
            <strike>Rs {{ $category_product->product->price }} / {{ $category_product->product->price_for }}</strike>
            <span>Rs {{$category_product->product->slashed_price}} / {{$category_product->product->price_for}}</span></p>
            @else
            <p class="rate"><span>Rs {{$category_product->product->price}} / {{$category_product->product->price_for}}</span></p>
            @endif
            @endif
            @endif
          </div>
          <div class="add-card">
            <div class="row">
              <div class="col-xs-6 detail">
                <a href="{{url('/category/product')}}/{{$category_product->product->slug}}">View detail</a>
              </div>
              <div class="col-xs-6 card">
                <a href="{{url('/list-cart-add')}}/{{$category_product->product->id}}">Add</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach

    </div>      
  </div>


  @endsection
  @section('scripts')
  <script type="text/javascript">

    $('.add-to-cart').click(function() {
     var quantity = $('#quantity0').val();
     var product_id  = $('#product_id').val();
      //alert(quantity);
      if(quantity==0)
      {
        swal({ 
          title: "Please Select Quantity!",
          text:  "You are now following",
        });
      }
      else
      {
       $.ajax({
        url:"{{ url('/add-to-cart') }}",
        type: "post",
        data : $('#addtocart').serialize(),

        success: function(response)
        {
            //console.log(response);
            if(response.status=='success'){
              swal({
                title:response.msg,
                type:response.status,
                timer: 1500,
                showConfirmButton:false,
                animation:false,
              });
            }
          },
          error: function(error){
            console.log(error);
          }
        })
     }
   })
 </script>
 <script>
  $(document).ready(function(){
    changeBoxValue();
    
    
    $('.qty_inc').click(function(){
      changeBoxValue();
            // var sqft=parseInt( $('#quantity0').val());
            // var qty=parseInt( $('#qty').val());
            // //alert(sqft);
            // var sqftperbox=parseInt($('.qty').val());
            // //alert(sqftperbox);
            // qty=Math.ceil(sqft/sqftperbox);
            // //alert(qty);
            // $('#qty').val(qty);
          });
    $('.qty_dec').click(function(){
      changeBoxValue();
            // var sqft=parseInt( $('#quantity0').val());
            // var qty=parseInt( $('#qty').val());
            // var sqftperbox=parseInt($('.qty').val());
            // qty=Math.ceil(sqft/sqftperbox);
            // //alert(qty);
            // $('#qty').val(qty);
          });

    function changeBoxValue(){
      var sqft=parseInt( $('#quantity0').val());
      var box_quantity=parseInt( $('#box_quantity').val());
      var no_of_box = Math.ceil(sqft/box_quantity);

            //alert(no_of_box);
            $('#qty').val(no_of_box);
          }
        // $('.quantity0').change(function(){

        //     var sqft=parseInt( $('#quantity0').val());
        //     alert(sqft);
        //     var qty=parseInt( $('#qty').val());
        //     var sqftperbox=parseInt($('.qty').val());
        //     qty=Math.ceil(sqft/sqftperbox);
        //     $('#qty').val(qty);
        // });
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.addtocart').on( "click", function(){
          swal({ 
            title: "Please Login!",
            text:  "You are now following",
          });
        });
      });
    </script>

    <script type='text/javascript'>
     $(document).ready(function() {
       $(".formid").submit(function(e){

         var coupontotal  = $('#quantity0').val();
     //alert(coupontotal);
     if(coupontotal==0)
     {

       swal({ 
        title: "Please Select Quantity!",
        text:  "You are now following",
      });
       e.preventDefault(e);
       return  false;
     }
     else
     {
      return  true;
    }


  });
     });
   </script>
   @endsection