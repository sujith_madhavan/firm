@php ($page = "productreview")
@php ($customer_sidebar = "productreview")
@extends('layouts.app')
@section('styles')
<link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection 
@section('content') 

<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
             @include('layouts._customer_sidebar')
             <div class="col-md-9 dashboard-right tabcontent" id="address">
                <div class="reg-head">
                    <h2><img src="{{asset('frontend/images/cart-dashboard.png')}}"> My Order</h2>
                </div>
                <div class="log-border">
                    <div class="product-review">
                        <div class="reg-head">
                            <h2>ORDER DETAIL</h2>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Product Code</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Gst</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @php ($i = 1) 
                                   @foreach ($order_details as $order_detail)

                                   <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$order_detail->product->code ?? ''}}</td>
                                    <td>{{$order_detail->product->name ?? ''}}</td>
                                    <td>{{$order_detail->price }}</td>
                                    <td>{{$order_detail->quantity}}</td>
                                    <td>{{$order_detail->product->gst ?? ''}}%</td>
                                    @php
                                   $product_price = ($order_detail->quantity*$order_detail->price) + (($order_detail->quantity*$order_detail->price)/100)*$order_detail->gst;  
                                    @endphp
                                    <td>{{$product_price}}</td>
                                </tr>
                                @php ($i++) 
                                @endforeach
                                       <!--  <tr>
                                            <td>2</td>
                                            <td>FIRM2</td>
                                            <td>Tata Steel</td>
                                            <td>500</td>
                                            <td>10Kg</td>
                                            <td>5000</td>
                                        </tr> -->
                                        @php
                                        $totals = 0;
                                        foreach($order_details as $order_detail){
                                       $product_price = ($order_detail->quantity*$order_detail->price) + (($order_detail->quantity*$order_detail->price)/100)*$order_detail->gst; 
                                        $totals += $product_price;
                                    }
                                     $total_amount = $totals-$order->coupon_amount;
                                    @endphp
                                     @if(!empty($order->coupon_amount))
                                     <tr>
                                        <td colspan="6">
                                            <p class="left-amount"> Coupon:</p>
                                        </td>

                                        <td class="amount-total">{{$order->coupon_amount}}</td>
                                    </tr>
                                     @endif
                                    <tr>
                                        <td colspan="6">
                                            <p class="left-amount"> Total Amount:</p>
                                        </td>

                                        <td class="amount-total">{{$total_amount}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
@section('scripts')

@endsection


