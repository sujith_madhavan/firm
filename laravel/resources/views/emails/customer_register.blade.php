<html>
	<head>
		<style>
		    p{
		        font-family: "f1";
		    }
		    .bill-para{
		        border:1px solid #e75e0d;
		        padding-top:10px;
		        padding-bottom:10px;
		    }
		    .bill-para1 ul{
		        list-style: disc;
		        font-family: "f1";
		    }
		    .bill-para1 h4{
		        text-align: center;
		      font-weight: normal;
		    font-size: 28px;
		    }
		    .bill-para{
		        font-family:"f1";
		    }
		    .bill-para1 p{
		        padding-top:20px;
		    }
		    .bill-head span{
		        font-weight:700;
		    }
		    .bill-head h1{
		        font-weight: normal;
		            font-size: 40px;
		  
		    }
		    .bill-head h2{
		        font-weight: normal;
		            font-size: 20px;
		    }
		</style>
	</head>
	<body>
		<div class="container">
    <div class="row">
       <div class="col-md-6">
           <div class="bill-img">
               <img src="{{asset('frontend/images/lognew.png')}}" class="img-responisve" alt="">           
               
           </div>
           <div class="bill-head">
           <h1> Welcome to <span>firmer.in</span></h1>
           <h2>Dear {{ $name }},</h2></div>
           <p> To log when Visiting our site just click Login or My Account at the top of every page, and then enter your email address and password</p>
           <div class="bill-para">
           <ul>
               <li> Use the following Values when prompted to log in:</li>
               <li>Email:{{ $email }}</li>
                      <li>password:{{ $password }}</li>

                      </ul>
                      </div>
                      <div class="bill-para1">
                      <p>When you log in to your account, you will be able to do the following:</p>
                      <ul>
                          <li>Proceed through checkout faster whrn making a purchase</li>
                         <li>Check the status of orders</li>
                         <li>View past orders</li>
                         <li>Makes changes to your password</li>
                         <li>Store alternative address(for shipping to multiple family members and friends!)
                         </li>
                         
                          </ul>
                      <p>If you have any questions, please feel free to contact  us at info@firmer.in or by <br>phone at +91 9940087788</p>
                      
                      <h4>Thank you, firmer.in!</h4>
		           </div>
		       </div>
		        
		    </div>
		</div>	
	</body>
</html>