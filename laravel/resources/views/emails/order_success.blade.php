<html>
	<head>
		<style>

		    .bg-cl {
		        background: rgba(248, 188, 249, 0.27);
		    }

		    @media print {
		        .bg-cl {
		            background: rgba(248, 188, 249, 0.27);
		            -webkit-print-color-adjust: exact;
		        }
		        .bg-border {
		            background: #eee;
		            -webkit-print-color-adjust: exact;
		        }
		        .bg-cl {
		            background: rgba(248, 188, 249, 0.27);
		        }

		</style>

	</head>
	<body>
		<div class="container">
		    <img src="{{asset('frontend/images/lognew.png')}}" class="img-responsive" alt="" style="padding-left:30px;">
		    <div class="bg-cl">
		        <table style="width:100%;border-collapse: collapse;">

		            <tr class="vendorListHeading" style="color:#000;">
		                <td style="width:60%; text-align:center;border-right: 1px dashed;padding: 30 30px 30 30px;">
		                    <h1 style="font-size:30px;font-weight:normal;">THANK YOU FOR YOUR ORDER FROM <b> FIRMER.IN </b></h1>
		                    <p>Once your package ships we will send an email with a link to track your order summary is below.Thank you again for your business.</p>
		                </td>
		                <td style="padding-left: 20px;">
		                    <h4><b>Order Questions?</b></h4>
		                    <p><b>Call Us: </b>+91 9940087788</p>
		                    <p><b>Email: </b>info@firmer.in</p>
		                </td>
		            </tr>

		        </table>
		    </div>
		</div>
		<div class="bill-order" style="text-align:center; padding:20px 0 20px 0;">
		    <p style="text-align:center;font-size:24px;">Your order #{{ $order->id }}</p>
		    <p style="text-align:center;">Placed on {{ date("M d,Y h:i A",strtotime($order->created_at)) }} IST</p>
		</div>
		<div class="container-fluid">
		    <div class="table-responsive">
		        <table style="width:100%;border:2px dashed #eee;border-top:none;border-collapse: collapse;">
		            <!--        <table class="table" style="border:  1px solid #eee;">-->
		            <thead>
		                <tr class="bg-border" style="color: #333;background:#eee;">
		                    <th style="border-bottom:none;text-align:left;border-right:none;padding-left:10px;">Item</th>
		                    <th style="border-bottom:none;text-align:left;border=right:none;">Sku</th>
		                    <th style="border-bottom:none;text-align:left;border-right:none;">Qty</th>

		                    <th style="border-bottom:none;">Subtotal</th>
		                    <th style="border-bottom:none;">Total</th>
		                </tr>
		            </thead>
		            <tbody>
		            	@php($tax = 0)
		            	@php($grand_total = 0)
		            	@foreach($order->orderDetails as $order_detail)
			                <tr style="border-bottom: 2px dashed #eee;color: #333;">
			                    <td style="padding-left: 10px;">{{ $order_detail->product->name }}</td>
			                    <td>{{ $order_detail->product->hsc_code }}</td>
			                    <td><i class="fa fa-inr" aria-hidden="true"></i> {{ $order_detail->quantity }}</td>
			                    <td style="text-align:center;">{{ $order_detail->price }}</td>
			                    @php
			                    	$total = $order_detail->price * $order_detail->quantity;

			                    	$tax += ($total * $order_detail->product->gst)/100;
			                    	$grand_total += $total;
			                    @endphp
			                    <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true"></i> {{ round($total,2) }}</td>

			                </tr>
			            @endforeach    

		                <tr style="color: #333;">
		                    <td colspan="2"></td>
		                    <td>Subtotal</td>
		                    <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true"></i>{{ round($grand_total,2) }}</td>
		                    <td></td>
		                </tr>
		                <tr style="color: #333;">
		                    <td colspan="2"></td>
		                    <td>Shipping & Handling</td>
		                    <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true"></i> 0.00</td>
		                    <td></td>
		                </tr>
		                <tr style="color: #333;">
		                    <td colspan="2"></td>
		                    <td>Tax</td>
		                    <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true"></i>{{ round($tax,2) }}</td>
		                    <td></td>
		                </tr>
		                <tr style="color: #333;">
		                    <td colspan="2"></td>
		                    <td>Grand Total</td>
		                    @php
		                    	$final_total = $grand_total + $tax;
		                    @endphp
		                    <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true"></i>{{ round($final_total,2) }}</td>
		                    <td></td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
		</div>
		<div class="container">

		    <table style="width:100%; margin-top:30px;border-collapse: collapse;">
		        <tr style="color:#000;">
		            <td style="padding-left:20;">
		                <h4><b>BILL TO:</b></h4>
		                <p>{{ $order->billingDetail->first_name }} {{ $order->billingDetail->last_name }}</p>
		                <p>{{ $order->billingDetail->company }}</p>
		                <p>{{ $order->billingDetail->address }}</p>
		                <p>{{ $order->billingDetail->address1 }}</p>
		                <p>{{ $order->billingDetail->address2 }}</p>
		                <p>{{ $order->billingDetail->city }},{{ $order->billingDetail->state }}, {{ $order->billingDetail->pincode }}</p>
		                <p>T: +91 {{ $order->billingDetail->mobile }}</p>
		                <h4><b>SHIPPING METHOD:</b></h4>
		                <p>Vendor Product Flatrate - Product Flatrate Shipping</p>
		            </td>
		            <td>
		                <h4><b>SHIP TO:</b></h4>
		                <p>{{ $order->shippingDetail->first_name }} {{ $order->shippingDetail->last_name }}</p>
		                <p>{{ $order->shippingDetail->company }}</p>
		                <p>{{ $order->shippingDetail->address }}</p>
		                <p>{{ $order->shippingDetail->address1 }}</p>
		                <p>{{ $order->shippingDetail->address2 }}</p>
		                <p>{{ $order->shippingDetail->city }},{{ $order->shippingDetail->state }}, {{ $order->shippingDetail->pincode }}</p>
		                <p>T: +91 {{ $order->shippingDetail->mobile }}</p>

		                <h4><b>PAYMENT METHOD:</b></h4>
		                <p>CC Avenue MCPG Payment</p>

		            </td>
		        </tr>

		    </table>
		    <h3 style="text-align:center;font-size:26px;font-weight:normal;"> Thank you, firmer.in!</h3>
		</div>
	</body>
</html>