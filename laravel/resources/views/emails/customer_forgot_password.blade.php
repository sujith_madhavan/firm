<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <h3>Hi {{ $name }}</h3><br>
    <p>Please Click the link to reset your password.</p><br>
    <a href="{{ url('customer/reset-password/') }}/{{ $token }}" target="_blank">Click Here</a>
</body>
</html>