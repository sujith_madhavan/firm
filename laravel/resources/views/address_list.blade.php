@php ($page = "address")
@php ($customer_sidebar = "address")
@extends('layouts.app')
@section('styles')
<link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection 
@section('content') 
<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>

    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
                @include('layouts._customer_sidebar')
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 dashboard-right tabcontent" id="address">
                            <div class="reg-head">
                                <h2><img src="{{asset('frontend/images/flod.png')}}"> Billing Address</h2>
                            </div>

                            @if(!empty($billing_addres)) 
                            <div class="log-border">
                               {{ ucwords($billing_addres->first_name) }} {{ $billing_addres->last_name }}<br>
                               {{ ucwords($billing_addres->company) }},{{ $billing_addres->mobile }}<br>
                               {{ ucwords($billing_addres->address) }},<br>

                               @if(!empty($billing_addres->address1))
                               {{ $billing_addres->address1 }}<br>
                               @endif
                               @if(!empty($billing_addres->address2))
                               {{ $billing_addres->address2 }}<br>
                               @endif
                                @if(!empty($billing_addres->fax))
                               {{ $billing_addres->fax }}<br>
                               @endif
                               {{ ucwords($billing_addres->city) }}, {{ ucwords($billing_addres->state) }}<br>
                               {{ $billing_addres->pincode }}, {{ ucwords($billing_addres->country) }}<br><br>
                               <p><a href="{{url('/customer/address-edit',[$billing_addres->id])}}">Edit Address</a> </p>
                           </div>
                           @endif

                       </div>
                       <div class="col-md-6 dashboard-right tabcontent" id="address">
                        <div class="reg-head">
                            <h2><img src="{{asset('frontend/images/flod.png')}}"> Shipping Address</h2>
                        </div>
                        @if(!empty($shipping_addres)) 
                        <div class="log-border">

                         {{ ucwords($shipping_addres->first_name) }} {{ $shipping_addres->last_name }}<br>
                         {{ ucwords($shipping_addres->company) }},{{ $shipping_addres->mobile }}<br>
                         {{ ucwords($shipping_addres->address) }}<br>

                         @if(!empty($shipping_addres->address1))
                         {{ $shipping_addres->address1 }}<br>
                         @endif
                         @if(!empty($shipping_addres->address2))
                         {{ $shipping_addres->address2 }}<br>
                         @endif
                        @if(!empty($shipping_addres->fax))
                         {{ $shipping_addres->fax }},<br>
                         @endif
                         {{ ucwords($shipping_addres->city) }}, {{ ucwords($shipping_addres->state) }}<br>
                         {{ $shipping_addres->pincode }}, {{ ucwords($shipping_addres->country) }}<br><br>
                         <p><a href="{{url('/customer/address-edit',[$shipping_addres->id])}}">Edit Address</a> </p>
                     </div>
                     @endif
                 </div>
             </div>

             <h2 class="add-addess"><img src="{{asset('frontend/images/flod.png')}}"> Add Address</h2>
             <a class="add-btn" href="{{url('/customer/address/add')}}">Add Address</a>
             <div class="row">
               @php ($i = 1) 
               @foreach ($customer_addresses as $customer_address)
               <div class="col-md-6 dashboard-right tabcontent" id="address">

                   <div class="log-border">
                    {{ ucwords($customer_address->first_name) }} {{ $customer_address->last_name }}<br>
                    {{ ucwords($customer_address->company) }},{{ $customer_address->mobile }}<br>
                    {{ ucwords($customer_address->address) }}<br>
                    @if(!empty($customer_address->address1)) 
                         {{ $customer_address->address1 }}<br>
                    @endif
                    @if(!empty($customer_address->address2))
                         {{ $customer_address->address2 }}<br>
                    @endif
                    @if(!empty($customer_address->fax))
                    {{ $customer_address->fax }},<br>
                    @endif
                    {{ ucwords($customer_address->city) }}, {{ ucwords($customer_address->state) }}<br>
                    {{ $customer_address->pincode }}, {{ ucwords($customer_address->country) }}<br>
                    <br>
                    <p><a href="{{url('/customer/address-edit',[$customer_address->id])}}">Edit Address</a> <span class="separator">|</span>
                     <a href="{{ url('/customer/address-delete',[$customer_address->id])}}" class=" delete-click">Delete Address</a></p>
                </div>


            </div>
            @php ($i++) 
            @endforeach

        </div>

    </div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>


@endsection
@section('scripts')
<!-- <link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
<script src="{{ asset('backend/sweetalert2/dist/sweetalert2.min.js') }}"></script> -->
<script type="text/javascript">      
  $(".delete-click").click(function (event) {
      event.preventDefault();
      var url = $(this).attr("href");
      console.log(url);
      swal({
          title: 'Are you sure?',
          text: "You won't be able to recover once deleted!",
          type: 'warning',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
      }).then(function () {
          $.ajax({
              type: "get",
              url: url,
              success: function(response){
                console.log(response);
                if(response.status == "success")
                    window.location.href="{{ url('/customer/address-list') }}";
              },
          });
      })
          // alert(url);
      });
  </script>
  @endsection
