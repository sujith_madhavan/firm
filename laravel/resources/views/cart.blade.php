@php ($page = "home")
@extends('layouts.app') 
@section('content') 

<div class="login-banner"></div>
<div class="shop-card">
  <div class="container">
    <h2>Shopping Cart</h2>
    <div class="row">
      <div class="col-sm-12 col-md-8 col-lg-9">
        <div class="cart-table">
         <form id="customer_info" action="{{url('/cart-update')}}" method="post">
          {{ csrf_field() }}
          <table class="table">
            <thead>
              <tr>
                <th><i class="fa fa-trash delete-shp" aria-hidden="true"></i></th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Unit Price</th>
                <th>Gst</th>
                <th>Qty</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
              @php ($i = 1) 
              @foreach ($carts as $cart) 
              <tr>
                <th class="remove"><a href="{{ url('/cart-delete',[$cart->id])}}"  title="Remove item"  class="btn-remove btn-remove2 delete-click"><img src="{{asset('frontend/images/icon.png')}}"></a></th>

                <th class="product-img"><a href="" class="cart-img"><img src="{{$cart->product->thumb_image_path}}"></a></th>
                <th class="product-title">
                  <h2><a href="">{{$cart->product->name}}</a></h2>
                </th>
                <th class="cart-price"><span><i class="fa fa-inr" aria-hidden="true"></i>
                  @if($cart->product->offer_valid)
                  @php($price = $cart->product->slashed_price)
                  {{$cart->product->slashed_price}}
                  @else
                  @php($price = $cart->product->price)
                  {{$cart->product->price}} 
                  @endif   
                </span></th>
                <th class="">
                  {{$cart->product->gst}}%
                </th>
                <th class="quantity">
                  <a class="input-group" href="#">

                    <span class="input-group-btn">
                      <button type="button" data-row="{{$loop->index}}" data-min="{{$cart->product->minimum_quantity}}" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="">
                        <span class="glyphicon glyphicon-minus"></span>
                      </button>
                    </span>
                    <input type="hidden" value="{{ $cart->id }}" name="carts[]">
                    <input type="text" id="quantity{{$loop->index}}" name="quantities[]" class="form-control input-number" value="{{ $cart->quantity }}" min="" max="100">

                    <span class="input-group-btn minus-btn">
                      <button data-row="{{$loop->index}}" data-max="{{$cart->product->stock}}" type="button" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">
                        <span class="glyphicon glyphicon-plus"></span>
                      </button>
                    </span>
                  </a>
                </th>
                 @php
                  $product_with_gst = ($cart->quantity*$price) + (($cart->quantity*$price)/100)*$cart->product->gst; 
                @endphp
               
                <th class="sub-total"><span><i class="fa fa-inr" aria-hidden="true"></i>{{$product_with_gst}}</span></th>
              </tr>
              @php ($i++) 
              @endforeach
            </tbody>
          </table>
          <div class="table-footer">
            <div class="row">
              <div class="left-button col-lg-6 col-md-12">
               <a href="{{ url('/')}}"><button type="button" title="" class="button btn-continue"><span>Continue Shopping</span></button></a>
             </div>
             <div class="right-button col-lg-6 col-md-12">

              <div class="col-md-12 col-lg-6 clear-cart">
               <a href="{{ url('/clear-cart')}}" id="cart-clear"> <button type="submit" name="" value="" title="" class="button btn-empty" ><span>Clear Shopping Cart</span></button></a>
             </div>

             <div class="col-md-12 col-lg-6 clear-cart">
              <button type="submit" name="" value="" title="" class="button btn-update"><span>Update Shopping Cart</span></button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="col-sm-12 col-md-4 col-lg-3">
  <div class="cart-collaterals">
    <form id="discount-coupon-form" action="" method="">
      <div class="discount">
        <div role="tab" id="cartthree" class="filter-heading">
          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree">
            <h2 class="opened">Discount Codes</h2>
          </a>
        </div>
        <div class="discount-form brand collapse arrow-content coupontrophiden" id="collapsethree" role="tabpanel" aria-labelledby="headingthree">
          <input type="hidden" name="remove" id="remove-coupone" value="0">
          <div class="input-box">
            <label for="coupon_code">Enter your coupon code if you have one.</label>
            <input class="input-text" id="coupon-code" name="coupon_code" value="">
            <span id="sign_email" class="text-danger"></span>
          </div>

          <div class="buttons-set">
            <button type="button" title="" id="coupon-code-submit" class="button" value=""><span>Apply Coupon</span></button>
          </div>
        </div>
      </div>
    </form>
  </div>
<!--   <div class="shipping">
    <div role="tab" id="headingFour" class="filter-heading">
      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
        <h2 class="opened">Estimate Shipping</h2>
      </a>
    </div>
    <div id="collapseFour" class="brand collapse arrow-content shipping-form" role="tabpanel" aria-labelledby="headingFour">
      <form action="" method="" id="">
        <p>Enter your destination to get a shipping estimate.</p>
        <ul>
          <li>
            <label for="country" class=""><em>*</em>Country</label>
            <div class="input-box">
              <select name="country_id" id="country" class="validate-select box-size" title="Country">
                <option value=""></option>
                <option value="IN" selected="selected">India</option>        
              </select>
            </div>
          </li>
          <li>
            <label for="region_id">State/Province</label>
            <div class="input-box">
              <select id="region_id" name="region_id" title="State/Province" style="display:none;" class="box-size">
                <option value="">Please select region, state or province</option>
              </select>
              <input type="text" id="region" name="region" value="" title="State/Province" class="input-text box-size" style="">
            </div>
          </li>
          <li>
            <label for="postcode">Zip/Postal Code</label>
            <div class="input-box">
              <input class="input-text box-size" id="estimateshipping" type="text"  name="postal_code" value="">
              <span id="estimateshipping_error" class="text-danger"></span>
            </div>
          </li>
        </ul>
        <div class="buttons-set">
          <button type="button" title="" id="estimate-shipping" class="button" value=""><span>Get a Quote</span></button>
        </div>
      </form>
    </div>
  </div> -->
 <!--  @php
  $totals = 0;
  $total_with_gst = 0;
  foreach ($carts as $cart) 
  {

    //print_r($cart);
    $product_price = ($cart->quantity*$cart->product->price);
     //without_out = 200
    $product_with_gst = ($cart->quantity*$cart->product->price) + (($cart->quantity*$cart->product->price)/100)*$cart->product->gst; 
    //with_gst=224 gst=12% 200 + (200/100)*12
    $totals += $product_price;
    $total_with_gst +=$product_with_gst;
    //echo $totals."-".$total_with_gst."-".$cart->product->gst."<br>";

  }
  $gst_amount = $total_with_gst - $totals;
  @endphp -->
  @php
  $totals = 0;
  foreach($carts as $cart){
  if($cart->product->offer_valid)
  $price = $cart->product->slashed_price;
  else
  $price = $cart->product->price;

  $product_price = ($cart->quantity*$price) + (($cart->quantity*$price)/100)*$cart->product->gst; 
  $totals += $product_price;
}
@endphp

<div class="totals">
  <h2>Cart Totals</h2>
  <form class="form" id="customer_info" action="{{url('/customer/shipping-address')}}" method="post">

    {{ csrf_field() }}
    <div class="total-shopping">
      <table id="shopping-cart-totals-table">
        <colgroup>
          <col>
          <col width="1">
        </colgroup>
        <tfoot id="totalamountcoupon" style="display: none;">
          <tr>
            <th style="" class="a-right" colspan="1">
              Grand Total
            </th>
            <th style="" class="a-right">
              <span class="price coupontotal"><i class="fa fa-inr" aria-hidden="true"></i></span>
            </th>
          </tr>
        </tfoot>
        <tfoot>
          <tr>
            <th style="" class="a-right" colspan="1">
              Grand Total
            </th>
            <th style="" class="a-right">
              <span id="grand_total" class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{ $totals }}</span>
            </th>
          </tr>
        </tfoot>
        <tbody class="total">
          <tr>
            <th style="" class="a-right" colspan="1">
            Subtotal </th>
            <th style="" class="a-right">
              <span class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{$totals}}</span> </th>
            </tr>
            <tr id="shipping" style="display: none;">
              <th style="" class="a-right" colspan="1">
              Shipping Price </th>
              <th style="" class="a-right">
                <span id="shipping_price"><i class="fa fa-inr" aria-hidden="true"></i></span> </th>
              </tr>
              <input type="hidden" class="coupontotal totalamount1"  name="total" id="totalamount" value="{{ $totals }}">
            </tbody>
            <tbody id="coupon_code" style="display: none;">
              <tr>
                <th style="" class="a-right" colspan="1">
                Coupon  </th>
                <th style="" class="a-right">
                  <span  class="price coupon_amount "><i class="fa fa-inr" aria-hidden="true"></i></span></th>
                </tr>

                <input type="hidden" name="coupon_id" id="coupon_id"  >
                <input type="hidden" class="coupon_amount" name="coupon_amount" >
              </tbody>

            </table>
            <div class="upload-customer">* unloading will be on customer's scope.</div>
            <ul class="checkout-types buttons-set">
              <li>
                <button type="submit" title="Proceed to Purchase" class="button">
                  <span>Proceed to Purchase</span>
                </button>

              </li>
            </ul>
          </div>
        </form>
      </div>
    </div>

  </div>
</div>
</div>
@endsection
@section('scripts')
<script type='text/javascript'>
 $(document).ready(function() {
   $(".form").submit(function(e){

     var coupontotal  = $('.totalamount1').val();
    //alert(coupontotal);
     if(coupontotal==0)
     {
       swal({ 
        title: "Please Select Quantity!",
        text:  "You are now following",
      });
       e.preventDefault(e);
       return  false;
     }
     else
     {
      return  true;
    }


  });
 });
</script>

<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });


  $(".delete-click").click(function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
      //console.log(url);
      swal({
        title: 'Are you sure?',
        text: "You won't be able to recover once deleted!",
        type: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function () {
        $.ajax({
          type: "get",
          url: url,
          success: function(response){
                //console.log(response);
                if(response.status == "success")
                  window.location.href="{{ url('/cart') }}";
              },
            });
      })
    });

  $("#cart-clear").click(function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
      //console.log(url);
      swal({
        title: 'Are you sure?',
        text: "You won't be able to recover once deleted!",
        type: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function () {
        $.ajax({
          type: "get",
          url: url,
          success: function(response){
            //console.log(response);
            if(response.status == "success")
              window.location.href="{{ url('/cart') }}";
          },
        });
      })
    });




  $('#coupon-code-submit').on('click', function(){

   if($("#coupon-code" ).val()=='')
   {
    $("#coupon-code").css("border-color", "#FF0000");
    $('').attr('disabled',true);
    $("#sign_email").text("*You have to enter your Coupon Code!");
  }
  else
  {
    var code = $('#coupon-code').val();
    var amount = $('#totalamount').val();
     //alert(amount);
     $.ajax(
     {
      type: 'post',
      url: "{{ url('/coupon-code') }}",
      data: {code:code,amount:amount},
      success: function(response)
      {
        //console.log(response.status);

        if(response.status=='error'){
          swal({
            title:response.msg,
            type:response.status,
            timer: 1500,
            showConfirmButton:false,
            animation:false,
          });
        }
        else
        {
          console.log(response.amount);
           // $('#coupon_code').append('<li><span contenteditable="true">' + $('#coupon_amount').val(response.amount) + '</span><span class="deleteThis">X</span></li>');
            // $('#coupon_code').val(response.amount);
            // $('#coupon_label').val(response.amount); 
            $('.coupon_amount').append(response.amount).val(response.amount);
            $('#coupon_id').append(response.id).val(response.id);
            $('.coupontotal').append(response.totals).val(response.totals);

            $(".total").hide();
            $(".coupontrophiden").hide();
            $("#coupon_code").show();
            $("#totalamountcoupon").show();
            swal({
              title:response.msg,
              type:response.status,
              timer: 1500,
              showConfirmButton:false,
              animation:false,
            });
          }

        },
        error: function(error) 
        {
          console.log(error);

          alert('Error occured');
        }
      });
   }  
 });
</script>
<script type="text/javascript">
 $('#estimate-shipping').on('click', function(){

   if($("#estimateshipping" ).val()=='')
   {
    $("#estimateshipping").css("border-color", "#FF0000");
    $('').attr('disabled',true);
    $("#estimateshipping_error").text("*You have to enter your Estimate Shipping!");
  }
  else
  {
    var postal_code = $('#estimateshipping').val();
//alert(postal_code);
$.ajax(
{
  type: 'post',
  url: "{{url('/estimate-shipping')}}",
  data: {postal_code:postal_code},
  success: function(response)
  {
    console.log(response);

    if(response.status=='success'){
      var grand_total = parseInt($("#grand_total" ).text());
          //console.log(grand_total);
          //alert(response.shipping_price);
          var shipping_price = parseInt(response.shipping_price);
         //alert(shipping_price);
         var final_total = shipping_price + grand_total;
         $('#grand_total').text(final_total);
         $('#shipping_price').text(response.shipping_price);
         $("#shipping").show();

       }
       else
       {

       }

     },
     error: function(error) 
     {
      console.log(error);

      alert('Error occured');
    }
  });
}  
});
</script>
@endsection