
@php ($page = "review")
@php ($customer_sidebar = "review")
@extends('layouts.app')
@section('styles')
<link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection 
@section('content') 
<div class="dashboard-header">
    <div class="dashboard-banner">
    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
                @include('layouts._customer_sidebar')
                <div class="col-md-9 dashboard-right tabcontent" id="review">
                    <div class="reg-head">
                        <h2><img src="{{asset('frontend/images/review1.png')}}"> My Product Review</h2>
                    </div>

                    <div class="log-border">
                        <div class="product-review">
                           <div class="reg-head">
                            <h2>PRODUCT REVIEWS</h2>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    @foreach ($product_reviews as $product_review)  
                                    <tr>
                                        <th> {{date('d-m-Y', strtotime($product_review->created_at ))}} </th>
                                        <th> {{$product_review->product->name}} </th>
                                        <th> <!-- {{count($product_review->product->product_average)}} -->
                                            @php
                                            $average = ($product_review->quality + $product_review->value + $product_review->price )/3;
                                            
                                            @endphp
                                            @for ($i = 0; $i < round($average) ; $i++)
                                            
                                            <span class="fa fa-star checked"></span>@endfor
                                        </th>
                                        <th> {{$product_review->review_summary}} </th>
                                        <th> <a href="{{url('/customer/product-review-detail',[$product_review->id])}}"> view Details </a></th>
                                    </tr>
                                    @endforeach                                   
                                </tbody>
                            </table>
                        </div>
                        <div class="view">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 left-view" >
                                    <!-- <a class="view-more" href="#">View more</a> -->
                                </div>
                                <div class="col-md-6 col-sm-12 right-view">
                                    <div class="view-inner">
                                       @if(method_exists($product_reviews,'links'))   
                                       <span class="pull-right">
                                        {{ $product_reviews->links() }}
                                    </span>   
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

@endsection
