@php ($page = "index") @extends('layouts.app') @section('content')
<div class="modal-page">
    <div class="modal animated bounceInUp" id="myPincode">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br>
                    <h4 class="modal-title"> Location </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group lab-con  col-md-offset-1 col-md-10">
                        <label> Enter Pincode (or) City </label>
                        <input type="text" class="form-control" name="city" value="">
                        <button type="submit" class="btn btn-pop"> Find </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="carousel-outer">
    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner carousel-zoom">
            @foreach($banners as $banner)
            <div class="{{ $loop->index==0?'active':'' }} item">
                <img src="{{ $banner->image_path }}">
                <div class="carousel-caption animated bounceInDown" >
                    <h3>{{ $banner->content }}</h3>
                    <h2></h2>
                    <h4><span>{!! $banner->content_small !!}</span></h4>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="customer-service">
        <div class="cus-caption">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 animated bounceInUp">

                   <div class="col-md-2 col-sm-6 cus-works">
                    <div class="works">
                        <div class="painting">
                            <img src="{{asset('frontend/images/painting.png')}}">
                            <h5>Interior Design</h5>
                        </div>
                        <p>
                            Co - Create your interior design with experts.<!--and get an instant quote with space craft--></p>
                            <div class="paint-indx">
                                <a href="#" class="enquirybtn" data-toggle="modal" data-target="#myModal1"> Enquiry</a>
                                <!--                                <a href="#" class="enquirybtn"> View </a>-->
                            </div>
                        </div>

                    </div>
                    <div class="col-md-2 col-sm-6 cus-works">
                        <div class="works">
                            <div class="painting">
                                <img src="{{asset('frontend/images/paint-brus.png')}}">
                                <h5 class="painting">Painting Interior/Exterior </h5>
                            </div>
                            <p>
                            Firmer's trained & certified professionals offer top notch Painting services. </p>
                            <!--model-->
                            <div class="paint-indx">
                                <a href="#" class="enquirybtn" data-toggle="modal" data-target="#myModal8"> Enquiry</a>
                                <!--                                <a href="#" class="enquirybtn"> View </a>-->
                            </div>
                        </div>
                    </div>

                    <!--                    </div>-->
                    <!-- <div class="col-md-4 col-sm-4 col-xs-12 cus-search animated bounceInUp">
                            <div class="inner-search">
                                <form action="#" method="">
                                    <div class="search-lay">
                                        <input type="text" name="title" required="" placeholder="Search Your Product...">
                                        <button type="submit"><img src="{{asset('frontend/images/search.png')}}"></button>
                                    </div>
                                </form>
                            </div>
                        </div>-->

                        <!--                    <div class="col-md-3 col-sm-12 col-xs-12 animated bounceInUp">-->



                           <div class="col-md-2 col-sm-6 cus-works">
                            <div class="works">
                                <div class="painting">
                                    <img src="{{asset('frontend/images/cloudy.png')}}">
                                    <h5> Weather Coating</h5>
                                </div>
                                <p>We would love to restore and protect your property to keep it looking its best for years to come.</p>
                                <div class="paint-indx">
                                    <a href="#" class="enquirybtn" data-toggle="modal" data-target="#myModal4"> Enquiry</a>
                                    <!--                                <a href="#" class="enquirybtn"> View </a>-->
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-2 col-sm-6 cus-works">
                            <div class="works">
                                <div class="painting">
                                    <img src="{{asset('frontend/images/plum.png')}}">
                                    <h5>Plumbing works</h5>
                                </div>
                                <p>you can avail our Plumbing services anywhere  with just a few clicks of a button </p>
                                <div class="paint-indx">
                                    <a href="#" class="enquirybtn" data-toggle="modal" data-target="#myModal3"> Enquiry</a>
                                    <!--                                <a href="#" class="enquirybtn"> View </a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6 cus-works">
                            <div class="works">
                                <div class="painting">
                                    <img src="{{asset('frontend/images/electrical.png')}}">
                                    <h5>Electrical Works</h5>
                                </div><p>
                                All Electrical Services Under One Umbrella</p>
                                <div class="paint-indx">
                                    <a href="#" class="enquirybtn" data-toggle="modal" data-target="#myModal2"> Enquiry</a>
                                    <!--                                <a href="#" class="enquirybtn"> View </a>-->
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($latest_products->count() > 0)
    <div class="index-body">
        <div class="container-fluid">
            <div class="latest-product">
                <h3>Our Latest Products</h3>
            </div>
            <div class="row">
                <div class="top-carousel">
                    <div class="owl-carousel owl-container owl-theme">
                        @foreach($latest_products as $latest_product)
                        <a href="{{url('/category/product')}}/{{$latest_product->slug}}">
                            <div class="item">
                                <div class="product-img">
                                    <div class="image-container">
                                        <img src="{{$latest_product->image_path}}">
                                        <div class="image-hover">
                                            <img src="{{asset('frontend/images/hovericon_ser.png')}}">
                                            <p>Starting from <span> RS:{{$latest_product->price}} per {{$latest_product->price_for}}</span> </p>
                                        </div>
                                        <div class="overlay"></div>
                                    </div>
                                    <div class="product-detail">
                                        <p>{{ $latest_product->name }}</p>
                                        <span>view our product<img src="{{asset('frontend/images/nav.png')}}"></span>
                                    </div>
                                </div>
                            </div>
                        </a>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif @if($featured_products->count() > 0)
    <div class="index-body">
        <div class="container-fluid">
            <div class="latest-product">
                <h3>Our Featured Products</h3>
            </div>
            <div class="row">

                @foreach($featured_products as $featured_product)
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="{{url('/category/product')}}/{{$featured_product->slug}}">
                        <div class="product-img">
                            <div class="image-container">
                                <img src="{{$featured_product->image_path}}">
                                <div class="image-hover">
                                    <img src="{{asset('frontend/images/hovericon_ser.png')}}">
                                    <p>Starting from <span> RS:{{$featured_product->price}} per {{$featured_product->price_for}}</span> </p>
                                </div>
                                <div class="overlay"></div>
                            </div>
                            <div class="product-detail">
                                <p>{{ $featured_product->name }}</p>
                                <span>view our product<img src="{{asset('frontend/images/nav.png')}}"></span>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach

            </div>
        </div>
    </div>

    @endif @if($top_selling_products->count() > 0)
    <div class="index-body">
        <div class="container-fluid">
            <div class="latest-product">
                <h3>Our Top Selling Products</h3>
            </div>
            <div class="row">

                @foreach($top_selling_products as $top_selling_product)
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="{{url('/category/product')}}/{{$top_selling_product->slug}}">
                        <div class="product-img">
                            <div class="image-container">
                                <img src="{{$top_selling_product->image_path}}">
                                <div class="image-hover">
                                    <img src="{{asset('frontend/images/hovericon_ser.png')}}">
                                    <p>Starting from <span> RS:{{$top_selling_product->price}} per {{$top_selling_product->price_for}}</span> </p>
                                </div>
                                <div class="overlay"></div>
                            </div>
                            <div class="product-detail">
                                <p>{{ $top_selling_product->name }}</p>
                                <span>view our product<img src="{{asset('frontend/images/nav.png')}}"></span>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                
            </div>
        </div>
    </div>

    @endif

    <div class="professional">
        <!--    <img src="images/bestbanner.jpg">-->
        <div class="container-fluid">
            <div class="professional-img">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <div class="professional-left">
                                <h3>Providing professional construction services since 1992.</h3>
                                <img src="{{asset('frontend/images/ser_good.png')}}">
                            </div>
                        </div>

                        <div class="col-md-4 textus">


                            <p><span>" </span>Consistently Amazing  customer  service,doesn’t happen by Accident, it happen on Purpose 
                                <span>" </span></p> 





                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
    <div>
        <div class="container-fluid">
            <div class="banner-footer">
                <h4>CLIENTS</h4>
                <!-- <p>We offer quality tiling and painting solutions for interior and exterior.</p>-->
            </div>

            <div class="testimonial">
                <div class="row">

                    <div>
                        <div class="col-md-12 left-carousel ">
                            <div class="owl-carousel owl-container owl-theme">
                                <div class="item test-1-full">
                                    <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
                                        <p class="descrp-end">They respect our sentiments with their well mannered relationship, high standard of quality and and “on time” commitments in full. The MD and his staff members are very courteous.</p><i class="fa fa-quote-right" aria-hidden="true"></i>
                                    </div>
                                    <div class="cus-img2">
                                        <img src="{{asset('frontend/images/bhaskar.png')}}">

                                    </div>
                                    <div class="testimonalnames">
                                       <h6>Dr.C.Bhasker, M.B.B.S., & Mrs.Sucharita Bhasker</h6>
                                       <p>Family Physician</p>

                                   </div>

                               </div>
                               <div class="item test-1-full">
                                <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
                                    <p class="descrp-end">Their customer focus is very good & they not only met but exceeded our expectations in some of their commitments.- My relationship with them is based on their' honesty ' & Mutual trust '<!-- strengthened by excellent understanding and communication.
                                       "We wish them all the very best in all their new endeavors. "--></p><i class="fa fa-quote-right" aria-hidden="true"></i>
                                   </div>
                                   <div class="cus-img2">
                                    <img src="{{asset('frontend/images/sekar.png')}}">

                                </div>
                                <div class="testimonalnames">
                                    <h6>Mr.V.Sekar, B.E, MBA.,</h6>
                                    <p>Retd from a Senior position in MNC</p>
                                    
                                </div>
                            </div>

                            <div class="item test-1-full">
                                <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
                                    <p class="descrp-end">Mr.C.Kasturi Raj has a very dedicated team, sincere and loyal from the set of team doing desk work at his office to the people doing manual job at the site.  <!--Honesty, straightforward and open minded dealings he has with the clients has earned him a unique place for him in the minds of the customers.-->
                                    </p><i class="fa fa-quote-right" aria-hidden="true"></i>
                                </div>
                                <div class="cus-img2">
                                    <img src="{{asset('frontend/images/venkatraman.png')}}">

                                </div>
                                <div class="testimonalnames">
                                    <h6>Mr.T.K.Venkataraman, M.A.B.L.,</h6>
                                    <p>Advocate</p>
                                    
                                </div>
                            </div>
                            <div class="item test-1-full">
                                <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
                                    <p class="descrp-end">We are very happy to be associated with the company. We wish them to grow from strength to strength and celebrate many more jubilees.</p><i class="fa fa-quote-right" aria-hidden="true"></i>
                                </div>
                                <div class="cus-img2">
                                    <img src="{{asset('frontend/images/chinnaswami.png')}}">
                                    
                                </div>
                                <div class="testimonalnames">
                                    <h6>Dr.C.Chinnaswami, M.S., M.Ch., F.R.C.S., D.Sc., (Hon)Consultant Urologist</h6>
                                    <p>Director Madras Institute of Urology- Vijaya Hospital</p>
                                    
                                </div>
                            </div>


                            
                            <div class="item test-1-full">
                                <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
                                    <p class="descrp-end">My experience with you and your team was professional and to the point which I found very easy to work with!!

                                    Thanking you and wishing you the best in all your future endeavors.</p><i class="fa fa-quote-right" aria-hidden="true"></i>
                                </div>
                                <div class="cus-img2">
                                    <img src="{{asset('frontend/images/kurin.png')}}">
                                    
                                </div>
                                <div class="testimonalnames">
                                    <h6>Dr.K.Kurian, BDS. MDS</h6>
                                    <p>Prof & Head of Dept- Oral Medicine & Radiology</p>
                                    
                                </div>
                            </div>
                            <div class="item test-1-full">
                                <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
                                    <p class="descrp-end">It is a successful vibrant Organisation totally committed to the complete satisfaction of their clientele.
<!--Their success is primarily attributable to the efforts of Mr.C.Kasturi Raj, the Chairman & Managing Director – full of zeal and zest – backed by a team of dedicated and loyal – both white and blue collared staff – all of whom nurture the one and only goal of Customer Satisfaction.
    I take this opportunity to wish the -->I wish the organisation the very best to earn more laurels in the year to come .  </p><i class="fa fa-quote-right" aria-hidden="true"></i>
</div>
<div class="cus-img2">
    <img src="{{asset('frontend/images/shanmugam.png')}}">

</div>
<div class="testimonalnames">
    <h6> Mr.R.Shanmugam </h6>
    <p>Managing Director ,Candid Power Systems (P) Ltd. ,Chennai- 30</p>

</div>
</div>
<div class="item test-1-full">
    <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
        <p class="descrp-end">I am fully happy with the company. They have an excellent leader 
        Mr.C.Kasturi Raj. They do proper planning and their execution is prompt.  </p><i class="fa fa-quote-right" aria-hidden="true"></i>
    </div>
    <div class="cus-img2">
        <img src="{{asset('frontend/images/viswanathan.png')}}">

    </div>
    <div class="testimonalnames">
        <h6>Mr.P.L. Viswanathan, I.P.S.,- Land Owner</h6>
        <p>Former Addl. D.G. of Police</p>

    </div>
</div>
<div class="item test-1-full">
    <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
        <p class="descrp-end">Mr.Kasturi Raj has been very fair in his dealings and kept up all that was agreed upon. The staffs have been very courteous and helpful complying to my needs.
            <!-- I feel very fortunate in getting acquainted with Mr.Kasturi Raj, who is now our good friend and well wisher. I wish him and his staff all the best and hope many will get fortunate like me in years to come.--> </p><i class="fa fa-quote-right" aria-hidden="true"></i>
        </div>
        <div class="cus-img2">
            <img src="{{asset('frontend/images/krishan.png')}}">

        </div>
        <div class="testimonalnames">
         <h6>Dr.U.S.Krishnamurthy, G.M.V.C. M.V.Sc. Phd.</h6>
         <p >Prof & Head, Dept of Animal Genetic

         Madras Veterinary college</p>

     </div>
 </div>
 <div class="item test-1-full">
    <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
        <p class="descrp-end" >I would like to appreciate your sincerity in on time delivery and with good quality. I am also happy that the whole transaction with you was clear and transparent.<!-- and without any misrepresentation. I wish you good luck for all your future ventures.--></p><i class="fa fa-quote-right" aria-hidden="true"></i>
    </div>
    <div class="cus-img2">
        <img src="{{asset('frontend/images/sundharam.png')}}">

    </div>
    <div class="testimonalnames">
        <h6> Lt.Gen.(Dr) V.J.Sundaram</h6>
        <p>Advisor The Institution of Engineers (India)</p>

    </div>
</div>

<div class="item test-1-full">
    <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
        <p class="descrp-end"> They recognize the needs of millennium. They have earned a reputation for quality and innovation.
            They have a very dedicated staffs who work together along with their MD , formulating & strengthening the desired goals. <!-- MD -Mr. Kasturi Raj formulating, planning and strategizing the desired goals. I wish them all the success for their future.--></p><i class="fa fa-quote-right" aria-hidden="true"></i>
        </div>
        <div class="cus-img2">
            <img src="{{asset('frontend/images/sridhar.png')}}">

        </div>
        <div class="testimonalnames">
            <h6> B.Sridhar  </h6>
            <p>Managing Director-Chennai Arithritis and Rheumatism center.Annanager East.Chennai
            </p>

        </div>
    </div>
    <div class="item test-1-full">
        <div class="cus-feedback2"><i class="fa fa-quote-left" aria-hidden="true"></i>
            <p class="descrp-end">Kasturi Raj has showcased exemplary vision in adapting and raising the bar in quality and customer satisfaction. Their Success is a direct result of the hardwork  & Commitment .<!-- to excellence and attention to detail that Mr. Kasturi Raj has instilled in this company. I am confident that the company will have many more successful years in the future and grow from strength to strength.--></p><i class="fa fa-quote-right" aria-hidden="true"></i>
        </div>
        <div class="cus-img2">
            <img src="{{asset('frontend/images/selvanambi.png')}}">

        </div>
        <div class="testimonalnames">
            <h6>Dr.Chelvanambi  </h6>
            <p>Dentist</p>

        </div>
    </div>

</div>
<ul id='carousel-custom-dots' class='owl-dots'>
    <li class='owl-dot'></li>
    <li class='owl-dot'></li>
    <li class='owl-dot'></li>
    <li class='owl-dot'></li>
</ul>
</div>
</div>
<div class="col-md-12 right-carousel">
    <div class="owl-carousel owl-container owl-theme">
        @foreach($brand_logos as $brand_logo)
        <div class="item">
            <div>
                <div class="item-inner">
                    <a href="{{ url('/brand-logo',[$brand_logo->slug]) }}">
                    <img src="{{ $brand_logo->image_path }}">
                    </a>
                </div>
            </div>
        </div>
        @endforeach
                        <!--                             <div class="item">
                                <div>
                                    <div class="item-inner">
                                        <img src="{{asset('frontend/images/prt_logo1.jpg')}}">
                                    </div>
                                    <div class="item-inner">
                                        <img src="{{asset('frontend/images/prt_logo4.jpg')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <div class="item-inner">
                                        <img src="{{asset('frontend/images/prt_logo2.jpg')}}">
                                    </div>
                                    <div class="item-inner">
                                        <img src="{{asset('frontend/images/prt_logo5.jpg')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <div class="item-inner">
                                        <img src="{{asset('frontend/images/prt_logo3.jpg')}}">
                                    </div>
                                    <div class="item-inner">
                                        <img src="{{asset('frontend/images/prt_logo6.jpg')}}">
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="arrow">
                            <p><a class="client-prev" href="#"><img src="{{asset('frontend/images/arrow2.png')}}"></a>
                                <a class="client-next" href="#"><img src="{{asset('frontend/images/arrow1.png')}}"></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>







    

<!-- Modal2 -->
<div class="modal fade " id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog openbox" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Enquiry</h4>
    </div>
    <div class="modal-body">

        <form method="post" action="{{ url('/product-enquiry') }}">
            {{ csrf_field() }}   

            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder=" Customer Name" required="">
            </div>
            <div class="form-group">
                <input type="Number" class="form-control" name="phone" placeholder=" Contact Number" required="" maxlength="10">
            </div>
            <div class="form-group">

                <input type="email" class="form-control" name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" required="required" placeholder="Email Id">
            </div>
            
            <div class="form-group">
             <select class="form-control" name="type" required="">
                    <option value="">Select</option>
                    <option value="Interior Wall Painting">Interior Wall Painting </option>
                    <option value="Repainting">Repainting </option>
                    <option value="New Painting">New Painting</option>
                    <option value="Exterior Wall Painting">Exterior Wall Painting</option>
                    <option value="Textured Wall Painting">Textured Wall Painting </option>
            </select>
        </div>

        <div class="form-group">

            <textarea class="form-control" name="message" placeholder="Message" required=""></textarea>
        </div>
        <div class="form-group">

            <button type="submit" class=" submit btn btn-cart1" placeholder="sumbit"> submit</button>

        </div>
    </form>

</div>

</div>
</div>
</div>
<!--end-->


<!-- Modal2 -->
<div class="modal fade " id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog openbox" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Enquiry</h4>
    </div>
    <div class="modal-body">

        <form method="post" action="{{ url('/product-enquiry') }}">
            {{ csrf_field() }}   

            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder=" Customer Name" required="">
            </div>
            <div class="form-group">
                <input type="Number" class="form-control" name="phone" placeholder=" Contact Number" required="" maxlength="10">
            </div>
            <div class="form-group">

                <input type="email" class="form-control" name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" required="required" placeholder="Email Id">
            </div>
            
            <div class="form-group">
             <select class="form-control" name="type" required="">
                <option value="">Select</option>
                <option value="Modular Kitchen">Modular Kitchen</option>
                <option value="Wardrobe and storage units">Wardrobe and storage units</option>
                <option value="Crockery Units">Crockery Units</option>
                <option value="Entertainment Units">Entertainment Units </option>
                <option value="Study Tables">Study Tables </option>
                <option value="Painting & Wall Papers">Painting & Wall Papers </option>
                <option value="False Ceiling">False Ceiling </option>
            </select>
        </div>

        <div class="form-group">

            <textarea class="form-control" name="message" placeholder="Message" required=""></textarea>
        </div>
        <div class="form-group">

            <button type="submit" class=" submit btn btn-cart1" placeholder="sumbit"> submit</button>

        </div>
    </form>

</div>

</div>
</div>
</div>
<!--end-->
<!-- Modal3 -->
<div class="modal fade " id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog openbox" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Enquiry</h4>
    </div>
    <div class="modal-body">

        <form method="post" action="{{ url('/product-enquiry') }}">
            {{ csrf_field() }} 

            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder=" Customer Name" required="">
            </div>
            <div class="form-group">
                <input type="Number" class="form-control" name="phone" required="" placeholder=" Contact Number" maxlength="10">
            </div>
            <div class="form-group">

                <input type="email" name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" required="required" class="form-control" placeholder="Email Id">
            </div>
            
            <div class="form-group">
             <select class="form-control" name="type" required="">
                <option value="">Select</option>
                <option value="Switches And Fuses">Switches And Fuses</option>
                <option value="Fan - Repair /Replace">Fan - Repair /Replace</option>
                <option value="Light - Repair /Replace">Light - Repair /Replace</option>
                <option value="Electrical Services">Electrical Services </option>
                <option value="Inverter">Inverter </option>
                <option value="Wiring">Wiring </option>
                <option value="Smart Home Solutions">Smart Home Solutions </option>
                <option value="Other Electrical services">Other Electrical services </option>
            </select>
        </div>

        <div class="form-group">

            <textarea class="form-control" name="message" placeholder="Message" required=""></textarea>
        </div>
        <div class="form-group">

            <button type="submit" class=" submit btn btn-cart1" placeholder="sumbit"> submit</button>

        </div>
    </form>

</div>

</div>
</div>
</div>
<!--end-->




<!-- Modal4 -->
<div class="modal fade " id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog openbox" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Enquiry</h4>
    </div>
    <div class="modal-body">

        <form method="post" action="{{ url('/product-enquiry') }}">
            {{ csrf_field() }} 

            <div class="form-group">
                <input type="text" class="form-control" name="name" required="" placeholder=" Customer Name">
            </div>
            <div class="form-group">
                <input type="number" name="phone"  maxlength="10" required="" class="form-control" placeholder=" Contact Number">
            </div>
            <div class="form-group">
                <input type="email" name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" required="required" class="form-control" placeholder="Email Id">
            </div>
            
            <div class="form-group">
             <select class="form-control" name="type" required="">
                <option value="">Select</option>
                <option value="Tap,Wash Basin,Sink">Tap,Wash Basin,Sink</option>
                <option value="Block & Leakages">Block & Leakages</option>
                <option value="Toilet And Sanitary Work">Toilet And Sanitary Work</option>
                <option value="Bathroom Fittings">Bathroom Fittings </option>
                <option value="Plumbing Services">Plumbing Services</option>
                <option value="Water Motor">Water Motor </option>
                <option value="Water Proofing">Water Proofing </option>

            </select>
        </div>

        <div class="form-group">

            <textarea class="form-control" required="" name="message" placeholder="Message"></textarea>
        </div>
        <div class="form-group">

            <button type="submit" class=" submit btn btn-cart1" placeholder="sumbit"> submit</button>

        </div>
    </form>

</div>

</div>
</div>
</div>
<!--end-->


<!-- Modal4 -->
<div class="modal fade " id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog openbox" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Enquiry</h4>
    </div>
    <div class="modal-body">

       <form method="post" action="{{ url('/product-enquiry') }}">
            {{ csrf_field() }} 

            <div class="form-group">
                <input type="text" class="form-control" name="name" required="" placeholder=" Customer Name">
            </div>
            <div class="form-group">
                <input type="Number" name="phone" maxlength="10" required="" class="form-control" placeholder=" Contact Number">
            </div>
            <div class="form-group">

                <input type="email" name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" required="required" class="form-control" placeholder="Email Id">
            </div>
            
            <div class="form-group">
             <select class="form-control" name="type" required="">
                 <option value="">Select</option>
                 <option value="Insultec (Paint)">Insultec (Paint)</option>
                 <option value="Insulla - (Tile)">Insulla - (Tile)</option>

             </select>
         </div>

         <div class="form-group">

            <textarea class="form-control" required="" name="message" placeholder="Message"></textarea>
        </div>
        <div class="form-group">

            <button type="submit" class=" submit btn btn-cart1" placeholder="sumbit"> submit</button>

        </div>
    </form>

</div>

</div>
</div>
</div>
<!--end-->


@endsection
