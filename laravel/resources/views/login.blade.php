@php ($page = "login")
@extends('layouts.app') 
@section('content') 
<div class="login-banner log-img-new"></div>

<div class="login-reg">
	<div class="container">
		<div class="row">
			<div class="log-head">
				<h2> LOGIN OR REGISTRATION </h2>
			</div>
			<div class="log-border">
				<div class="row">
				    <div class="col-md-6 col-sm-12 col-xs-12">
				    
				        <div class="log-para">
				            <h3> REGISTER HERE </h3>
				            <p>
				                By creating an account with our store, you will be able to move through the purchase process faster, store multiple shipping addresses, view and track your orders in your account and more.
				            </p>
				    
				        </div>
				        <a href="{{ url('/register') }}"><button type="submit" class="btn btn-login1 pull-right">REGISTER</button></a>
				    </div>
				    
				    
				    <div class="col-md-6 col-sm-12 col-xs-12">
				    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

				    <form action="{{ url('/login') }}" method="post">
				    	{{csrf_field()}}
				        <div class="log-head">
				            <h3> LOGIN HERE </h3>
				        </div>
				    
				        <div class="form-group">
				            <label for=""> Email* </label>
				            <input type="email" class="form-control" placeholder="" value="{{ old('email')}}" id="email" name="email" required>
				            <span id="sign_email" class="text-danger"></span>
				        </div>
				        <div class="form-group">
				            <label for=""> password* </label>
				            <input type="password" class="form-control" placeholder="" id="login_password" name="password" required=""><span id="err_logpassword" class="text-danger"></span>
				        </div>
				        <div class="forgot-as">
				        <a href="{{url('/customer/forgot-password')}}"> Forgot your password? </a>
				        
				        <button type="submit" class="btn btn-login pull-right" id="login"> Login </button>
				    </div>
				    </form>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')	
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script type="text/javascript">
	 $('document').ready(function(){      
	        //alert('crt'); 
	        
	        $("#email").focusout(function(){
	            if($(this).val()==''){
	                $(this).css("border-color", "#FF0000");
	                    $('#login').attr('disabled',true);
	                     $("#sign_email").text("* You have to enter the email!");
	            }
	            else
	            {
	                $(this).css("border-color", "#2eb82e");
	                $('#login').attr('disabled',false);
	                $("#sign_email").text("");

	            }
	        });

	        $("#login_password").focusout(function(){
	            if($(this).val()==''){
	                $(this).css("border-color", "#FF0000");
	                    $('#login').attr('disabled',true);
	                     $("#err_logpassword").text("* You have to enter the password!");
	            }
	            else
	            {
	                $(this).css("border-color", "#2eb82e");
	                $('#login').attr('disabled',false);
	                $("#err_logpassword").text("");

	            }
	        });
	    });
	</script>
@endsection

