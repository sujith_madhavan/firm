<?php $page="forgot";?>  
@extends('layouts.app') 
@section('content')
<div class="login-banner"></div>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="login-title text-center">
				<h2> Enter Otp </h2>
				<p>doloremque laudantium, totam rem. </p>
			</div>
		</div>

		<div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">

			<div class="login-border">
				<form id="otp" action="{{url('/otp/login')}}" method="post">
                	{{ csrf_field() }}
                	@if ($errors->any())
                        <div class="alert alert-danger" style="display: block;">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @isset($error_message)
					    <div class="alert alert-danger" style="display: block;">
                            <ul>
                                <li>{{ $error_message }}</li>
                            </ul>
                        </div>
					@endisset
                	<input type="hidden" name="customer_id" id="customer_id" value="{{ $customer_id }}" >
					<div class="form-group">
						<label for="otp"> Otp*</label>
						<input type="number" name="otp" id="otp" class="form-control" required>
					</div>
					
					<button type="submit" class="btn btn-login">Submit</button>
				</form>	
			</div>
		</div>
	</div>
</div>


@endsection
