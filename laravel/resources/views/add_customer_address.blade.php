@php ($page = "address")
@php ($customer_sidebar = "address")
@extends('layouts.app')
@section('styles')
    <link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection 
@section('content') 
<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
                @include('layouts._customer_sidebar')
                <div class="col-md-9 dashboard-right tabcontent" id="address">
                    <div class="reg-head">
                        <h2><img src="{{asset('frontend/images/flod.png')}}"> Add New Address</h2>
                    </div>

                    <div class="log-border">
                        <form id="customer_info" action="{{url('/customer/address/add')}}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                @if ($errors->any())
                                    <div class="alert alert-danger" style="display: block;">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="reg-head3">
                                    <h3> CONTACT INFORMATION</h3>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="name"> First Name* </label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('first_name') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="name"> Last Name </label>
                                    <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" id="last_name">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for=""> Company </label>
                                    <input type="text" class="form-control" name="company" value="{{ old('company') }}" id="company">
                                </div>

                                <div class="form-group col-md-6">
                                    <label> Mobile number<span>*</span></label>
                                    <input type="text" maxlength="10" pattern="[9|8|7]\d{9}$" class="form-control" name="mobile" id="mobile" value="{{ old('mobile') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label> Fax </label>
                                    <input type="text" class="form-control" name="fax" id="fax" value="{{ old('fax') }}">
                                </div>
                                <div class="reg-head3">
                                    <h3> ADDRESS</h3>
                                </div>
                                <div class="form-group col-md-12">
                                    <label> Street Address<span>*</span></label>
                                    <input type="text" class="form-control" name="address" value="{{ old('address') }}" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" name="address1" value="{{ old('address1') }}">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" name="address2" value="{{ old('address2') }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label> City<span>*</span></label>
                                    <input type="text" class="form-control" name="city" value="{{ old('city') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label> State<span>*</span> </label>
                                    <input type="text" class="form-control" name="state" value="{{ old('state') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label> Postal code<span>*</span></label>
                                    <input type="text" class="form-control" name="pincode" value="{{ old('pincode') }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label> Country<span>*</span></label>
                                    <input type="text" class="form-control" name="country" value="{{ old('country') }}" required>
                                </div>
                                 <div class="form-group col-md-6">
                                    <input type="checkbox" id="is_default" name="is_billing"
                                    value=""> Use as my default billing address<br>
                                    <input type="checkbox" id="is_default" name="is_shipping"> Use as my default shipping address
                                </div>
                            
                              <!--   <div class="buttons-set">
                                    <p class="back-link"><a href="{{url('/customer/address-list')}}"><small>« </small>Back</a></p>
                                </div> -->
                            </div>
                            <div class="log2-back">
                                 <p class="back-link"><a href="{{url('/customer/address-list')}}"><small>« </small>Back</a></p>
                                <button type="submit" class="btn btn-login2 pull-right"> Save Address </button>
                            </div>
                        </form>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
    
@endsection
