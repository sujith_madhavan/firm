
@php ($page = "return-privacy")
@extends('layouts.app') 
@section('content') 

<div class="login-banner"></div>
<div class="building-header">
    
    <div class="breadcrumb">
       <a href="index.php"><img src="{{asset('frontend/images/home.png')}}"></a>&nbsp;
      
        <i class="fa fa-angle-right" aria-hidden="true"></i>
        <p>&nbsp;Return policy&nbsp;</p>

    </div>







<div class="container">
  
        <div class="std">
            <p style="text-align: center;color: #e75e0d;margin-bottom:40px;"><strong><span style="font-size: x-large;">Return Policy</span> </strong></p>
            
            <hr align="center" noshade="noshade" size="0" width="100%">
            <p> This Returns and Policy sets out the terms under which Firmer accepts and executes returns and cancellations of orders, and shall apply to all Buyers. All capitalized terms not defined here shall have the meanings assigned to them in the Terms and Conditions of Use available
Firmer shall endeavor to facilitate timely delivery of Products and materials as per a Buyer’s order details, indicated dispatch times and terms and conditions of the supply of the Products stated by a Seller. If a Buyer is not satisfied with a purchase, we are here to help.</p>
           <p> Returns </p>
            <p>Returns of Products are accepted from Buyers under the following conditions:</p>
            
            <ul type="disc">
                <li> - If the size or the dimensions of the ordered Product is not as specified in the purchase order and is beyond tolerance limit (as per industry standard).</li>
                <li> - If Incorrect Products are delivered. </li>
                <li> - If a packed Product delivered is found tampered with.</li>
                <li> - If it is found that received Products are defective or malfunctioning.</li>
                <li> - If quality of the received Product does not match with the specifications stated in a purchase order</li>
            </ul>
            <p> NOTE: Issues pertaining to Product quality shall be reconfirmed through third party testing.</p>
            <p> Subject to the above, a Buyer may request for a replacement of the product, In case the Seller does not have a replacement Product, the Seller can agree to a refund to the Buyer and the Buyer shall be obligated to accept the refund in place of a replacement.</p>
            <p> A replacement request should be raised by the Buyer within 24 (twenty-four) hours of delivery, and a replacement will be done within 4-8 days’ time from raising of the replacement request by the Buyer through Firmer and solely depends upon the availability of the Products or as per manufacturers terms.</p>
            <p> Exception:</p>
            <p>Firmer will schedule the process of pickup on acceptance of a Buyer’s “request for return” .
</p>
           <p> A Buyer can mail us on <a href="mailto:info@firmer.in">info@firmer.in</a> with the Buyer’s ‘order-id’, time of delivery of the Products, ‘date of material receipt’ and ‘reasons for replacement’ within the timelines mentioned above. We will get back to you and initiate the returns procedure. </p>
           <p> All shipping, replacement and third party quality testing charges shall be borne and incurred by the Supplier, if any or above conditions are satisfied otherwise by a Buyer.</p>
           <p> Returns are NOT if, </p>
            <ul type="disc">
                <li> - A Product is used and/or is found to have been tampered by a Buyer.</li>
                <li> - A Product is not properly installed or used as per manufacturers guidelines/industry standard which results in damage or malfunctioning.</li>
                <li> - A return is not notified within time as specified above.</li>
                <li> - We cannot exchange a Product for another Product.</li>
                <li> - Partial returns / leftovers of delivered Product are not accepted.</li>
                <li> - If the packages are open or packet is open and subject to vary from actual product.</li>
            </ul>
            <p><strong><span style="text-decoration: underline;">  Shipping</span></strong></p>
            <p> FIRMER will bear the shipping charges for returns in case the material is eligible for returns as per the returns policy.</p>
            <p> Cancellations are accepted When,</p>
           
            <ul type="disc">
                <li> - Ordered Products are NOT in transit / orders have NOT yet been processed by the Sellers.</li>
                <li>- Ordered Products are NOT customized to suit a Buyer. (Example: Custom mixed paints cannot be cancelled)</li>
                <li> - A shipment, once arrived at a Buyer’s doorstep, cannot be cancelled for any reason other than mentioned in the return policy above.</li>
                <li> - Ordered Products are NOT imported by the Sellers based on the Buyer’s order.</li>
                <li> - Customer can cancel the order within 12 hours of order placement, Firmer will refund the amount after deducting processing fees (bank charges, etc.) of the order..</li>
                <li> - If customer cancel the order after FIRMER arranged the products and just before the delivery, 15% of the total order amount will be charged as cancellation fee along with processing fee mentioned above.</li>
                
            </ul>
            <p><strong><span style="text-decoration: underline;"> Partial Cancellations / Modifications</span></strong></p>
            <p> In case a Buyer wants to partially cancel some Products from the order placed, the same needs to be notified to the customer care of customercare@firmer.in immediately and a new order has to be generated for the modified quantities. For example, if the Buyer placed order for 1000 sq. ft. tiles, but wants to modify it to only 750 sq. ft.). This may be done either online on the Website or by contacting customer care.</p>
            <p> NOTE:&nbsp;All such modification / partial cancellations must be initiated before the Products ordered / order is processed.</p>
            <p> Once a Buyer cancels the order (subject to and satisfying the above mentioned conditions) online on the Website/through customer care, the entire amount paid will be refunded to the Buyer with no cancellation charges being applicable.</p>
            
            <p> <strong><span style="text-decoration: underline;"> Refund </span></strong></p>
            <p> <strong><span style="text-decoration: underline;"> Buyer is eligible for Refund when </span></strong></p>
            <p> Firmer is not able to arrange for the ordered product.</p>
                <ul type="disc">
                    <li> The replacement for the same product is not available </li>
                    <li> When Firmer fails to deliver the product within the time mentioned on the website.</li>
                    <li> Products returned by the buyer to  Firmer without raising  the replacement request or proper intimation will be sent back to the customer and we will not be able to process any refund.</li>
                    <li> Product should be returned in their original packaging along with the original price tags, labels, invoices and documentations in proper condition and products / items should be UNUSED</li>
                    <li> The returned item should reach us in the same condition without any damages. Final decision on whether a returned item will be retained by us or sent back to the customer shall be taken by FIRMER ‘Quality Assurance Team’.</li>
                   
            </ul>
            <p>Contact Us</p>
            <p>Email Id:&nbsp;<strong> <a href="mailto:info@firmer.in">info@firmer.in</a></strong></p>
            <p>&nbsp;</p>
        </div>
    </div>

</div>


@endsection
