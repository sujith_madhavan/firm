<?php $page="forgot";?>  
@extends('layouts.app') 
@section('content')
<div class="login-banner"></div>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="login-title text-center">
				<h2>RETRIEVE YOUR PASSWORD HERE</h2>
				<p>Please enter your email address below. You will receive a link to reset your password. </p>
			</div>
		</div>

		<div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">

			<div class="login-border">
				<form id="forgot_password" action="{{ url('/customer/forgot-password') }}" method="post">
                	{{ csrf_field() }}
                	@if ($errors->any())
                        <div class="alert alert-danger" style="display: block;">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
					<div class="form-group">
						<label for="email"> Email Address*</label>
						<input type="email" class="form-control" name="email_id" required>
					</div>
						
					<button type="submit" class="btn btn-login"> Submit </button>
				</form>	
			</div>
		</div>
	</div>
</div>


@endsection
