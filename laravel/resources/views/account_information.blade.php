@php ($page = "account")
@php ($customer_sidebar = "account")
@extends('layouts.app')
@section('styles')
    <link href="{{ asset('backend/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection 
@section('content') 
<div class="dashboard-header">
    <div class="dashboard-banner">

    </div>
    <div class="dashboard-backgrnd">
        <div class="container-fluid">
            <div class="row">
                @include('layouts._customer_sidebar')
                <div class="col-md-9 dashboard-right tabcontent" id="editaddress">
                    <div class="edit-account">
                        <div class="reg-head">
                            <h2><img src="{{asset('frontend/images/inf.png')}}"> Edit Account Information</h2>
                        </div>
                        <div class="log-border">
                            <form id="customer_info" action="{{url('/customer/account-info')}}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" style="display: block;">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="reg-head3">
                                        <h3> Account Information</h3>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name"> First Name* </label>
                                        <input type="text" class="form-control" value="{{ $customer->first_name }}" name="first_name" id="first_name" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name"> Last Name* </label>
                                        <input type="text" class="form-control" value="{{ $customer->last_name }}" name="last_name" id="last_name" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email"> Email Address*</label>
                                        <input type="email" class="form-control" value="{{ $customer->email }}" name="email_id" id="email_id" required>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> Mobile number<span>*</span></label>
                                        <input type="text" maxlength="10" pattern="[9|8|7]\d{9}$" class="form-control" name="mobile" id="mobile"  value="{{ $customer->mobile }}" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>User Type<span>*</span></label><br>
                                        @if($customer->user_type == 'COMPANY')
                                            <input type="radio" name="user_type" value="INDIVIDUAL">Individual
                                            <input type="radio" name="user_type" checked value="COMPANY">Company
                                        @else
                                            <input type="radio" name="user_type" checked value="INDIVIDUAL">Individual
                                            <input type="radio" name="user_type" value="COMPANY">Company
                                        @endif        
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>GST</label>
                                        <input type="text" class="form-control" value="{{ $customer->gst }}" name="gst" id="gst" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="checkbox" id="change_password"> Change password
                                    </div>
                                    <div class="form-group col-md-6">
                                        @if($customer->news_letter)
                                            <input type="checkbox" checked name="news_letter" value="1">Subscribe to newsletter
                                        @else
                                            <input type="checkbox" name="news_letter" value="1">Subscribe to newsletter
                                        @endif    
                                    </div>
                                    <div id="confirm_password" style="display: none">
                                        <div class="form-group col-md-12">
                                            <label> Current Password <span>*</span></label>
                                            <input type="password" class="form-control clear-value" name="current_password" id="current_password"><span id="current_pwd" class="text-danger"></span>
                                        </div>
                                        <div class="reg-head3">
                                            <h3> Change password</h3>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label> New Password <span>*</span></label>
                                            <input type="password" class="form-control clear-value" name="new_password" id="new_password"><span id="err_new_pwd" class="text-danger"></span>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label> Confirm New Password <span>*</span></label>
                                            <input type="password" class="form-control clear-value" name="conf_new_password" id="conf_new_password"><span id="err_confirm_pwd" class="text-danger"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="log1-back">
                                    <button type="submit" class="btn btn-login2 pull-right">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
    <script src="{{ asset('backend/sweetalert2/dist/sweetalert2.min.js') }}"></script> 
    <script type="text/javascript">
        $("#change_password").change(function() {
            if(this.checked) {
                $("#confirm_password").show();
            }
            else{
                $("#confirm_password").hide();
                $('.clear-value').val("");

            }
        });

        $(document).ready(function() {
            $("#customer_info").submit(function(e){ 
                var change_password = $('#change_password').is(':checked'); 
                if(change_password){
                    var current_password = $("#current_password").val();
                    var password = $("#new_password").val();
                    var confirm_password = $("#conf_new_password").val();
                    var length = password.length;
                    //alert(length1);
                    if(current_password == ""){
                        $("#current_pwd").text("* Current Password is required");
                        e.preventDefault();
                    }
                    if(password.length < 6)
                    {
                        $("#err_new_pwd").text("* Password length should be atleast six");
                        e.preventDefault();
                    }
                    if(password != confirm_password)
                    {
                        $("#err_confirm_pwd").text("* Password mismatch");
                        e.preventDefault();
                       
                    }
                }
            });
        });
    </script>
    <script>
        $(window).load(function(){           
            @if (session('status'))
                swal({
                    title: "{{session('message')}}",
                    type:"{{session('status')}}",
                    timer: 2000,
                    showConfirmButton:false,
                    animation:false,
                });
            @endif            
        });
    </script>
@endsection
