<?php 
foreach ($category as $cat) 
{
    if($product[0]->category_id == $cat->id)
    {
        $catname = $cat->name;
    }
}
$page=$catname;?>
@extends('layouts.app') 
@section('content') 
<div class="producthetail-header">
    <div class="building-banner">
        <div class="banner-content">
            <h5><?php 
            foreach ($category as $cat) 
            {
                if($product[0]->category_id == $cat->id)
                {
                    $catname = $cat->name;
                }
            }
            echo $catname ?></h5>
            <h3>IRON AND STEEL <span>{{$product[0]->name}}</span></h3>
        </div>
    </div>
    <div class="breadcrumb">
        <a href="{{url('/')}}"><img src="{{asset('frontend/images/home.png')}}"></a>&nbsp;
        <i class="fa fa-angle-right" aria-hidden="true"></i>
        <a href="{{url('/category')}}/{{$product[0]->category_id}}"><p>&nbsp;<?php 
        foreach ($category as $cat) 
        {
            if($product[0]->category_id == $cat->id)
            {
                $catname = $cat->name;
            }
        }
        echo $catname ?></a>&nbsp;</p>&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>
        <p>&nbsp;{{$product[0]->name}}</p>
    </div>
</div>

<div class="compare-area compare-single-productt">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="single-thumbnail-wrapper">
                    <div class="single-product-tab">
                        <ul class="single-tab-control" role="tablist">
                            <li class="active">
                                <a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">
                                    <img src="{{$image->thumb_image_path}}" alt="Domino" class="img-responsive" />
                                </a>
                            </li>
                            @php ($i = 2)
                            @foreach($productimages as $img)
                            <li class="">
                                <a href="#tab-{{$i}}" aria-controls="tab-{{$i}}" role="tab" data-toggle="tab">
                                    <img src="{{$img->thumb_image_path}}" alt="Domino" class="img-responsive" />
                                </a>
                            </li>

                            @php ($i++) 
                            @endforeach    
                        </ul>
                    </div>
                    <div class="single-cat-main">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab-1">
                                <div class="tab-single-image">
                                    <img src="{{$image->image_path}}" alt="" class="img-responsive" />


                                </div>
                            </div>
                            @php ($j = 2)
                            @foreach($productimages as $images)
                            <div role="tabpanel" class="tab-pane" id="tab-{{$j}}">
                                <div class="tab-single-image">
                                    <img src="{{$images->image_path}}" alt="" class="img-responsive" />

                                </div>
                            </div>
                            
                            @php ($j++) 
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-7 description-head">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 product-detail">
                        <div class="product-inner">
                            <div class="product-head">
                                <h2> {{$product[0]->name}} </h2>
                                <p> Starting from <span>Rs{{$product[0]->price}} /{{$product[0]->price_for}}</span></p>

                            </div>

                            <div class="grade-point bg">
                                <table>
                                    @foreach($productattr as $attr)
                                    <tr>
                                        <th>
                                            @foreach($attribute as $name)
                                            @if($name->id == $attr->attribute_id)
                                            {{$name->name}}
                                            @endif
                                            @endforeach
                                        </th>
                                        <th> :</th>
                                        <th> {{ucwords($attr->detail)}} </th>

                                    </tr>



                                    @endforeach
                                   <!--  <tr>
                                        <th> GRADE </th>
                                        <th> :</th>
                                        <th> FE500 </th>

                                    </tr>

                                    <tr>
                                        <th> DIAMETER </th>
                                        <th> :</th>
                                        <th> 20mm</th>
                                    </tr>
                                    <tr>
                                        <th> BRAND </th>
                                        <th> : </th>
                                        <th> TATA STEEL</th>
                                    </tr> -->
                                </table>

                                <div class="input-group plus-minus">

                                    <span class="input-group-btn">

                                        <button type="button" data-row="0" class="quantity-left-minus btn btn-theme btn-number"  data-type="minus" data-field="">
                                          <span class="glyphicon glyphicon-minus"></span>
                                      </button>
                                  </span>

                                  <input type="text" id="quantity" name="quantity" class="form-control input-number" value="{{$product[0]->stock}}" min="1" max="100">
                                  <span class="input-group-btn">
                                    <button type="button" data-row="0" class="quantity-right-plus btn btn-theme btn-number" data-type="plus" data-field="">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>

                        <div class="product-head1">

                            <p> <span>Customer review (302)</span> 1-302 of 302 reviews </p>

                        </div>
                        <p class="clr-pa pull-right"> write a reviews <span>&raquo;</span> </p>
                        <p class="avg-rating"> Average rating (4.7)</p>

                        <div class="rating">
                            <input type="radio" id="star5" name="rating" value="5" />
                            <label class="star checked" for="star5" title="Awesome" aria-hidden="true"></label>
                            <input type="radio" id="star4" name="rating" value="4" />
                            <label class="star" for="star4" title="Great" aria-hidden="true"></label>
                            <input type="radio" id="star3" name="rating" value="3" />
                            <label class="star" for="star3" title="Very good" aria-hidden="true"></label>
                            <input type="radio" id="star2" name="rating" value="2" />
                            <label class="star" for="star2" title="Good" aria-hidden="true"></label>
                            <input type="radio" id="star1" name="rating" value="1" />
                            <label class="star" for="star1" title="Bad" aria-hidden="true"></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="product-list">
                        <div class="product-bottom">
                            <div class="product-head2">
                                <div class="product-dim">
                                    <h3> Stock Avaliable </h3>

                                    <div class="hover-img">
                                        <p> More Product + <span><a class="arrowup" href="#"><img src="{{asset('frontend/images/arrowup.png')}}"></a></span></p>
                                        <div class="stock-product">
                                            <div class="in-stock">
                                                <h6>{{$product[0]->name}}</h6>
                                                <div class="row">
                                                    <div class="col-xs-6 left-price">
                                                        <p>Price displayed as per <span>Ton</span></p>
                                                    </div>
                                                    <div class="col-xs-6 right-stock">
                                                        <p>Avaliability : In stock</p>
                                                    </div>
                                                </div>
                                                <div class="cus-table">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Product name</th>
                                                                <th>price</th>
                                                                <th>qty</th>
                                                                <th>request for price</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>8mm</th>
                                                                <th></th>
                                                                <th>5</th>
                                                                <th class="request-quote">Request quote <i class="fa fa-angle-double-right"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <th>8mm</th>
                                                                <th></th>
                                                                <th>5</th>
                                                                <th class="request-quote">Request quote <i class="fa fa-angle-double-right"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <th>8mm</th>
                                                                <th></th>
                                                                <th>5</th>
                                                                <th class="request-quote">Request quote <i class="fa fa-angle-double-right"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <th>8mm</th>
                                                                <th></th>
                                                                <th>5</th>
                                                                <th class="request-quote">Request quote <i class="fa fa-angle-double-right"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <th>8mm</th>
                                                                <th></th>
                                                                <th>5</th>
                                                                <th class="request-quote">Request quote <i class="fa fa-angle-double-right"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <th>8mm</th>
                                                                <th></th>
                                                                <th>5</th>
                                                                <th class="request-quote">Request quote <i class="fa fa-angle-double-right"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <th>8mm</th>
                                                                <th></th>
                                                                <th>5</th>
                                                                <th class="request-quote">Request quote <i class="fa fa-angle-double-right"></i></th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="stock-img">
                                    <img src="{{asset('frontend/images/stock.png')}}">
                                </div>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="more-review text-left">
                            <p> We support online 24/7</p>
                            <p> <span> Call </span> : 044 27262822</p>
                            <p> <span> Mail </span>: domail@firmerfoundation.com</p>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 service-enquiry">
                                <div class="service-img">
                                    <img src="{{asset('frontend/images/support.png')}}">
                                </div>
                            </div>
                            <div class="col-xs-6 service-enquiry">
                                <div class="theme-btn1">
                                    <button type="button" class="theme-btn" data-toggle="modal" data-target="#myModal"> Service Enquiry </button>

                                    <div class="modal right fade" id="myModal" role="dialog">
                                        <div class="modal-dialog modal-dialog-slideout">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h2>Request for services</h2>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="" method="">
                                                        <div class="contact pop-up-enq">
                                                            <div class="eqry row">
                                                                <label for="">Name :</label>
                                                                <input type="text" name="name" id="name" minlength="3" maxlength="32" pattern="[A-Za-z]{1,32}" required="required">
                                                                <label for="">Mail - ID :</label>
                                                                <input type="email" name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" required="required">
                                                                <label for="">Mobile No :</label>
                                                                <input type="text" name="phone" maxlength="10" pattern="[9|8|7]\d{9}$" title="10 Numeric characters only" required="required">
                                                                <label for="">Services :</label>
                                                                <select name="" id="cusSelectbox" class="s-hidden scroll form-control">
                                                                    <option value="Select">--Select Services--</option>
                                                                    <option value="A">Electrical</option>
                                                                    <option value="B">Plumbing</option>
                                                                    <option value="C">Interior work</option>
                                                                    <option value="D">Exterior painting</option>
                                                                    <option value="E">Interior painting</option>
                                                                    <option value="D">Civil works</option>
                                                                    <option value="F">Landscaping</option>
                                                                    <option value="G">Tank/ sump cleaning</option>
                                                                </select>

                                                                <label for="">Message :</label>
                                                                <!--                                                                    <input type="text" class="mess-age" name="message" id="e_message" required="required">-->
                                                                <textarea class="mess-age" name="message" id="e_message"></textarea>
                                                                <button class="sub-mit-cont" type="submit" name="submit" id=""><i class="fa fa-paper-plane" aria-hidden="true"></i> SEND </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <div class="review-center">
                                    <p> (2 reviews)</p>
                                    <p> (50 reviews)</p>
                                    <p> (50 reviews)</p>
                                    <p> (200 reviews)</p>
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-6">
                                <div class="rate-star1">
                                    <p> <i class="fa fa-star "></i>
                                        <i class="fa fa-star "></i> (2 star)</p>
                                        <p> <i class="fa fa-star "></i>
                                            <i class="fa fa-star "></i> <i class="fa fa-star "></i> (3 star)</p>
                                            <p> <i class="fa fa-star "></i>
                                                <i class="fa fa-star "></i> <i class="fa fa-star "></i>
                                                <i class="fa fa-star "></i> (4 star)</p>
                                                <p> <i class="fa fa-star "></i>
                                                    <i class="fa fa-star "></i> <i class="fa fa-star "></i>
                                                    <i class="fa fa-star "></i> <i class="fa fa-star "></i> (5 star)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-listbottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 product-info">
                                <ul id="myTab" class="nav nav-tabs nav_tabs">

                                    <li class="active"><a href="#service-one" data-toggle="tab">Description</a></li>
                                    <li><a href="#service-two" data-toggle="tab">Specification</a></li>
                                    <li><a href="#service-three" data-toggle="tab">Review</a></li>

                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane fade in active" id="service-one">

                                        <section class="container product-info1">

                                            <div class="para-as">
                                    <!-- <ul>
                                        <h5> Over view </h5>
                                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</li>
                                        <h5> Product Description</h5>
                                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</li>
                                        <h5> Company Description </h5>
                                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</li>
                                        <h5> Disciaimer </h5>
                                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</li>
                                    </ul> -->
                                    {!! $product[0]->description !!}
                                </div>
                            </section>

                        </div>
                        <div class="tab-pane fade" id="service-two">
                            <section class="container product-info1">
                                <div class="para-as">
                                    <table>
                                        @foreach($balanceattr as $attributes)


                                        <tr>
                                            <th>
                                                @foreach($attribute as $name)
                                                @if($name->id == $attributes->attribute_id)
                                                {{$name->name}}
                                                @endif
                                                @endforeach
                                            </th>
                                            <th> :</th>
                                            <th> {{ucwords($attributes->detail)}} </th>

                                        </tr>                            
                                        @endforeach

                                    </table>
                                    <!-- <ul>
                                        <h5> Over view </h5>
                                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</li>
                                        <h5> Product Description</h5>
                                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</li>
                                    </ul> -->
                                </div>
                            </section>
                        </div>
                        <div class="tab-pane fade" id="service-three">
                           <section class="container product-info1">
                            <div class="review-three">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="review-aa">
                                                <p> Be the first to review this product</p>
                                                <hr>
                                                <h3> WRITE YOUR OWN REVIEW </h3>
                                                <p>How do you rate this product? *</p>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <th> </th>
                                                        <th> 1 STAR</th>
                                                        <th> 2 STARS</th>
                                                        <th> 3 STARS</th>
                                                        <th> 4 STARS</th>
                                                        <th> 5 STARS</th>
                                                    </tr>

                                                    <tr>
                                                        <td> Quality </td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="o1" value="">
                                                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                   <input type="radio" name="o1" value="">
                                                                   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                                               </label>
                                                           </div>
                                                       </td>
                                                       <td>
                                                        <div class="radio">
                                                            <label>
                                                              <input type="radio" name="o1" value="">
                                                              <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                                          </label>
                                                      </div>
                                                  </td>
                                                  <td>
                                                    <div class="radio">
                                                        <label>
                                                         <input type="radio" name="o1" value="">
                                                         <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                                     </label>
                                                 </div>
                                             </td>
                                             <td>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="o1" value="">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                                    </label>
                                                </div>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td> value </td>
                                            <td>
                                                <div class="radio">
                                                    <label>
                                                       <input type="radio" name="2" title="Bad" value="">
                                                       <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                                   </label>
                                               </div>
                                           </td>
                                           <td>
                                            <div class="radio">
                                                <label>
                                                  <input type="radio" name="2" title="Good" value="">
                                                  <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                              </label>
                                          </div>
                                      </td>
                                      <td>
                                        <div class="radio">
                                            <label>
                                              <input type="radio" name="2" title="very Good" value="">
                                              <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                          </label>
                                      </div>
                                  </td>
                                  <td>
                                    <div class="radio">
                                        <label>
                                          <input type="radio" name="2" title="Great" value="">
                                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                      </label>
                                  </div>
                              </td>
                              <td>
                                <div class="radio">
                                    <label>
                                      <input type="radio" name="2" title="Awesome" value="">
                                      <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                                  </label>
                              </div>
                          </td>

                      </tr>

                      <tr>
                        <td> price </td>
                        <td>
                            <div class="radio">
                                <label>
                                   <input type="radio" name="3" title="Bad" value="">
                                   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                               </label>
                           </div>
                       </td>
                       <td>
                        <div class="radio">
                            <label>
                              <input type="radio" name="3" title="Good" value="">
                              <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                          </label>
                      </div>
                  </td>
                  <td>
                    <div class="radio">
                        <label>
                          <input type="radio" name="3" title="very Good" value="">
                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                      </label>
                  </div>
              </td>
              <td>
                <div class="radio">
                    <label>
                      <input type="radio" name="3" title="Great" value="">
                      <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
                  </label>
              </div>
          </td>
          <td>
            <div class="radio">
                <label>
                  <input type="radio" name="3" title="Awesome" value="">
                  <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>          
              </label>
          </div>
      </td>

  </tr>
</table>
</div>

<form>
    <div class="form-group">
        <label for="name"> Nick Name <span>*</span></label>
        <input type="text" class="form-control" placeholder="">
    </div>
    <div class="form-group">
        <label> Summary of your Review <span>*</span></label>
        <input type="text" class="form-control" name="" value="">
    </div>
    <div class="form-group">
        <label> Review <span>*</span></label>
        <textarea name="detail" rows="6" class="form-control"></textarea>

    </div>
</form>

<button type="submit" class="btn btn-login2 pull-right"> Submit Review </button>

</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</div>
</div>
</div>
</div>




@endsection