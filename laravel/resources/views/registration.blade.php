@php ($page = "registration")
@extends('layouts.app') 
@section('styles')
    <style type="text/css">
        .container {
          margin-top: 20px;
        }

        .panel-heading {
          font-size: larger;
        }

        .alert {
          display: none;
        }


        /**
         * Error color for the validation plugin
         */

        .error {
          color: #e74c3c;
        }
    </style>
@endsection
@section('content')
<div class="login-banner"></div>

<div class="container">
    <div class="row">
        <div class="reg-head">
            <h2> PERSONAL INFORMATION</h2>
        </div>

        <div class="log-border">
            <form id="register" action="{{url('/register')}}" method="post">
                {{ csrf_field()}}
                <div class="row"> 
                    @if ($errors->any())
                        <div class="alert alert-danger" style="display: block;">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group col-md-6">
                        <label> First Name*	</label>
                        <input type="text" class="form-control" value="{{ old('first_name') }}" placeholder="" name="first_name" id="first_name" required><span id="err_name" class="text-danger"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Last Name* </label>
                        <input type="text" class="form-control" placeholder="" value="{{ old('last_name')}}" name="last_name" id="last_name" required><span id="err_lname" class="text-danger"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Email Address*</label>
                        
                        <input type="email" class="form-control" placeholder="" name="email_id" id="email_id" value="{{ old('email_id')}}" required><span id="err_email" class="text-danger"></span>
                    </div>

                    <div class="form-group col-md-6">
                        <label> Mobile number<span>*</span></label>
                        <input type="text" maxlength="10" pattern="[9|8|7]\d{9}$" class="form-control" name="mobile" id="mobile"  value="{{ old('mobile') }}" required><span id="err_mobile" class="text-danger"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Password <span>*</span></label>
                        <input type="password" class="form-control" name="password" id="password" required><span id="err_pwd" class="text-danger"></span>
                    </div>
                    <div class="form-group con-top col-md-6">
                        <label>Confirm Password<span>*</span></label>
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required><span id="err_confirmpwd" class="text-danger"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label>User Type<span>*</span></label><br>
                        <input type="radio" name="user_type" checked value="INDIVIDUAL">Individual
                        <input type="radio" name="user_type" value="COMPANY">Company
                    </div>
                    <div class="form-group col-md-6">
                        <label>GST</label>
                        <input type="text" class="form-control" name="gst" id="gst" >
                    </div>
                    <div class="form-group col-md-6">
                        <input type="checkbox" name="news_letter" value="1">Subscribe to newsletter
                    </div>
                </div>

                <div class="log1-back">
                   <span class="login-account">Already have an account?<a  href="{{ url('/login') }}">Login</a></span>
                    <button type="submit" id="submit" class="btn btn-login pull-right"> Register</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection
@section('scripts') 
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#register").submit(function(e){ 
                var password = $("#password").val();
                var confirm_password = $("#password_confirmation").val();
                var length = password.length;
                //alert(length1);
                if(password.length < 6)
                {
                    $('#register').attr('disabled',true);
                    $("#err_pwd").text("* Password length should be atleast six");
                    e.preventDefault();
                }
                if(password != confirm_password)
                {

                    $('#register').attr('disabled',true);
                    $("#err_confirmpwd").text("* Password mismatch");
                    e.preventDefault();
                   
                }
             
            });
        });

        // function chk_pwd()
        // {
        //     //alert();
        //     var password = $("#password").val();
        //     var confirm_password = $("#confirm_password").val();
        //     var length1 = password.length;
        //     //alert(length1);
        //     if(password != confirm_password)
        //     {
        //         //$("#confirm_password").val('');
        //         //$("#confirm_password").focus();
        //         $('#register').attr('disabled',true);
        //         $("#err_confirmpwd").text("* Password mismatch!");
        //         return false;
               
        //     }
        //     else if(length1 < 6)
        //     {
        //         $('#register').attr('disabled',true);
        //         $("#err_confirmpwd").text("* Password length should be atleast 6!");
        //         return false;
        //     }
        //     else
        //     {
        //          $("#err_confirmpwd").text(""); 
        //     }

        // }

      
         // $('document').ready(function(){      
         //    //alert();
         //    $("#first_name").focusout(function(){
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                //$('#register').attr('disabled',true);
         //                 $("#err_name").text("* You have to enter the name!");
         //        }
                
         //        else
         //        {

         //            $(this).css("border-color", "#2eb82e");
         //            //$('#register').attr('disabled',false);
         //            $("#err_name").text("");

         //        }
         //    });
            
         //    $("#email").focusout(function(){
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                $('#register').attr('disabled',true);
         //                 $("#err_email").text("* You have to enter the email!");
         //        }
         //        else
         //        {
         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            $("#err_email").text("");

         //        }
         //    });
         //    $("#last_name").focusout(function(){            
                
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                $('#register').attr('disabled',true);
         //                 $("#err_lname").text("* You have to enter the lastname!");
         //        }
         //        else
         //        {
         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            $("#err_lname").text("");                
         //        }
         //    }); 
         //    $("#mobile").focusout(function(){            
                
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                $('#register').attr('disabled',true);
         //                 $("#err_mobile").text("* You have to enter the mobile no!");
         //        }
         //        else
         //        {
         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            $("#err_mobile").text("");                
         //        }
         //    }); 
         //    $("#password").focusout(function(){
         //        if($(this).val()==''){
         //            $(this).css("border-color", "#FF0000");
         //                $('#register').attr('disabled',true);
         //                 $("#err_pwd").text("* You have to enter the password!");
         //        }
                
         //        else
         //        {

         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            $("#err_pwd").text("");

         //        }
         //    });
         //    $("#confirm_pwd").focusout(function(){
         //        if(($(this).val()=='')){
         //            $(this).css("border-color", "#FF0000");
         //            $('#register').attr('disabled',true);
         //            $("#err_confirmpwd").text("* You have to enter the confirm password!");
         //        }
         //        else
         //        {
         //            $(this).css("border-color", "#2eb82e");
         //            $('#register').attr('disabled',false);
         //            //$("#err_confirmpwd").text("");

         //        }
         //    });

        // });

     </script>
@endsection

