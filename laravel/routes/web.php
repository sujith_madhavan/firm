<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//frontend

//Home page
Route::get('/', 'HomeController@getHome')->name('home');

Route::get('/autoslug', 'HomeController@autoslug');
//Logout
Route::get('/logout', 'HomeController@getLogout')->name('logout');

Route::get('return-policy', function () {
	return view('return_privacy');
});

Route::get('terms-and-condition', function () {
	return view('terms_condition');
});
Route::get('privacy-policy', function () {
	return view('privacy_policy');
});
Route::get('about-us', function () {
	return view('about_us');
});
// Reset Password
Route::get('/customer/reset-password/{token}','HomeController@resetCustomerPassword');
Route::post('/customer/reset-password', 'HomeController@postRestPassword');

//brand list page
Route::get('/brand-logo/{slug}', 'HomeController@getBrandPageList');
// Vendor Registration
Route::get('/vendor/register', 'HomeController@getVendorRegister')->name('vendor.register');
Route::post('/vendor/register', 'HomeController@postVendorRegister');

Route::post('/searchpostal-code', 'HomeController@postsearchAddressCode');


Route::group(['middleware' => ['customer.authorization']], function(){	

	//Registration
	Route::get('/register', 'HomeController@getRegister')->name('register');
	Route::post('/register', 'HomeController@postRegister');

	//Login
	Route::get('/login', 'HomeController@getLogin')->name('login');
	Route::post('/login', 'HomeController@postLogin');

	//Forgot Password
	Route::get('/customer/forgot-password', 'HomeController@getForgotPassword')->name('forgot-password');
	Route::post('/customer/forgot-password', 'HomeController@postForgotPassword');

	//Verify otp
	Route::post('/otp/login', 'HomeController@postOtpLogin');

});	



Route::group(['middleware' => ['auth.customer']], function(){
	//customer dashboard page
	Route::get('/customer/dashboard', 'HomeController@dashboard')->name('customer.dashboard');


	// Customer Account Info
	Route::get('/customer/account-info', 'HomeController@getCustomerAccountInfo');
	Route::post('/customer/account-info', 'HomeController@postCustomerAccountInfo');


	// Customer Address
	Route::get('/customer/address/add', 'HomeController@getCustomerAddress');
	Route::get('/customer/address-list', 'HomeController@getCustomerAddressList');
	Route::get('/customer/address-edit/{id}', 'HomeController@CustomerAddressEdit');
	Route::post('/customer/address-update/{id}/edit', 'HomeController@CustomerAddressUpdate');
	Route::get('/customer/address-delete/{id}', 'HomeController@deleteAddress');

	Route::post('/customer/address/add', 'HomeController@postCustomerAddress');

	// Customer News Letter
	Route::get('/customer/news-letters', 'HomeController@getCustomerNewsLetters');

	// Product Review
	Route::get('/customer/product-review', 'FrontendController@getCustomerProductReviews');
	Route::get('/customer/product-review-detail/{id}', 'FrontendController@getProductReviewDetail');

	Route::get('/customer/myorder', 'FrontendController@getCustomerMyorder');
	Route::get('/customer/order-detail/{id}', 'FrontendController@getOrderDetail');
	Route::get('/customer/print/orders/{order_id}', 'FrontendController@printOrderDetail');

	Route::get('/cart','FrontendController@getCartDetail');
	Route::post('/cart-add','FrontendController@postCartAdd');
	Route::post('/add-to-cart','FrontendController@postAddToCart');
	Route::get('/list-cart-add/{id}', 'FrontendController@getCartAdd');
	Route::get('/cart-delete/{id}', 'FrontendController@deleteCart');
	Route::get('/clear-cart', 'FrontendController@clearCart');
	Route::post('/cart-update', 'FrontendController@cartUpdate');
	
	Route::post('/billing-address', 'FrontendController@billingaddress');
	Route::post('/billing-address-customer', 'FrontendController@billingaddressCustomer');
	Route::post('/shipping-address-customer', 'FrontendController@shippingaddressCustomer');
	Route::post('/shipping-address', 'FrontendController@shippingaddress');
	Route::post('/shipping-billing-address', 'FrontendController@shippingBillingAddress');
	// Coupon
	Route::post('/coupon-code', 'FrontendController@getCouponCode');

	Route::post('/estimate-shipping', 'FrontendController@getEstimateShipping');

	Route::post('/customer/shipping-address', 'FrontendController@getCustomerShippingAddress');
	Route::post('/payment', 'PaymentController@sendPaymentGateway');
	Route::post('/hdfc/response', 'PaymentController@paymentResponse')->name('hdfc.return');
	
});	


//product list(by category) page
Route::get('/category/{slug}', 'FrontendController@getCategoryProducts');
//product detail page
Route::get('/category/product/{slug}', 'FrontendController@getProduct');
Route::post('/product-review', 'FrontendController@reviewProduct');
Route::post('/service-enquiry', 'FrontendController@ServiceEnquirySent');

Route::post('/product-enquiry', 'FrontendController@ProductEnquirySent');

Route::post('/contact-enquiry', 'FrontendController@contactEnquirySent');
// Route::get('/category/{slug}/filters', 'FrontendController@getFilterProducts');
Route::any('/category/{slug}/filters', 'FrontendController@getFilterCategoryProducts');

Route::any('/search-products', 'FrontendController@getSearchedProducts');

Route::get('/product/search', 'FrontendController@getProductNames')->name('product.names');





// End Of Frontend

//backend
Route::group(['middleware' => ['admin.authorization']], function(){	
	Route::get('/admin', function () {
		return view('admin.auth.login');
	});

	Route::get('/admin/login', function () {
		return view('admin.auth.login');
	})->name('user.login');

	Route::post('/admin/login', 'LoginController@checkUser');
	Route::post('/admin/otp/login', 'LoginController@checkUserOtp');
	

	Route::get('/admin/forgot-password', function () {
		return view('admin.auth.forgot_password');
	});
	Route::post('/admin/forgot-password', 'LoginController@forgotPassword');

});

Route::get('/admin/logout', 'LoginController@logoutUser');

Route::group(['middleware' => ['auth']], function(){

	Route::get('/admin/home', function () {
		return view('admin.dashboard');
	});

	Route::group(['middleware' => ['route.authorization']], function(){	
		// Users
		Route::get('/admin/users/add', 'UserController@addUser')->name('admin.users.add');
		Route::post('/admin/users/add', 'UserController@postUser');
		Route::get('/admin/users', 'UserController@getUsers')->name('admin.users');
		Route::get('/admin/users/{user_id}/view', 'UserController@viewUser')->name('admin.users.view');
		Route::get('/admin/users/{user_id}/edit', 'UserController@editUser')->name('admin.users.edit');
		Route::post('/admin/users/{user_id}/edit', 'UserController@updateUser');
		Route::get('/admin/users/{user_id}/status/{status}','UserController@updateUserStatus')->name('admin.users.status');


		// Roles
		Route::get('/admin/roles', 'RoleController@getRoles')->name('admin.roles');
		Route::get('/admin/roles/add', 'RoleController@addRole')->name('admin.roles.add');
		Route::post('/admin/roles/add', 'RoleController@postRole');
		Route::get('/admin/roles/{role_id}/edit', 'RoleController@editRole')->name('admin.roles.edit');
		Route::post('/admin/roles/{role_id}/edit', 'RoleController@updateRole');
		Route::get('/admin/roles/{role_id}/status/{status}','RoleController@updateRoleStatus')->name('admin.roles.status');
		Route::get('/admin/roles/{role_id}/view', 'RoleController@viewRole')->name('admin.roles.view');

		// Blocked Ip
		Route::get('/admin/blocked-ips', 'LoginController@getBlockedIps')->name('admin.blocked-ips');
		Route::get('/admin/blocked-ips/{blocked_ip_id}/delete', 'LoginController@removeBlockedIp')->name('admin.blocked-ips.remove');


		// Menus
		Route::get('/admin/menus', 'MenuController@getMenus')->name('admin.menus');
		Route::post('/admin/menus/add', 'MenuController@postMenu')->name('admin.menus.add');
		Route::post('/admin/menus/{menu_id}/edit', 'MenuController@updateMenu')->name('admin.menus.edit');
		Route::get('/admin/menus/{menu_id}/status/{status}','MenuController@updateMenuStatus')->name('admin.menus.status');
		Route::get('/admin/menus/{menu_id}/delete', 'MenuController@deleteMenu')->name('admin.menus.delete');

		// Category
		Route::get('/admin/categories', 'CategoryController@getCategories')->name('admin.categories');
		Route::get('/admin/categories/add', 'CategoryController@addCategory')->name('admin.categories.add');
		Route::post('/admin/categories/add', 'CategoryController@postCategory');
		Route::get('/admin/categories/{category_id}/edit', 'CategoryController@editCategory')->name('admin.categories.edit');
		Route::post('/admin/categories/{category_id}/edit', 'CategoryController@updateCategory');
		Route::get('/admin/categories/{category_id}/status/{status}','CategoryController@updateCategoryStatus')->name('admin.categories.status');

		// Pincode
		Route::get('/admin/pincodes', 'PincodeController@getPincodes')->name('admin.pincodes');
		Route::post('/admin/pincodes/add', 'PincodeController@postPincode')->name('admin.pincodes.add');
		Route::post('/admin/pincodes/{pincode_id}/edit', 'PincodeController@updatePincode')->name('admin.pincodes.edit');
		Route::get('/admin/pincodes/{pincode_id}/status/{status}','PincodeController@updatePincodeStatus')->name('admin.pincodes.status');
		Route::get('/admin/pincodes/{pincode_id}/delete', 'PincodeController@deletePincode')->name('admin.pincodes.delete');

		// Attribute
		Route::get('/admin/attributes', 'AttributeController@getAttributes')->name('admin.attributes');
		Route::get('/admin/attributes/add', 'AttributeController@addAttribute')->name('admin.attributes.add');
		Route::post('/admin/attributes/add', 'AttributeController@postAttribute');
		Route::get('/admin/attributes/{attribute_id}/edit', 'AttributeController@editAttribute')->name('admin.attributes.edit');
		Route::post('/admin/attributes/{attribute_id}/edit', 'AttributeController@updateAttribute');
		Route::get('/admin/attributes/{attribute_id}/status/{status}','AttributeController@updateAttributeStatus')->name('admin.attributes.status');
		Route::get('/admin/attribute_delete/{attribute_id}', 'AttributeController@deleteAttribute')->name('admin.attribute_delete.edit');

		// Group
		Route::get('/admin/groups', 'GroupController@getGroups')->name('admin.groups');
		Route::get('/admin/groups/add', 'GroupController@addGroup')->name('admin.groups.add');
		Route::post('/admin/groups/add', 'GroupController@postGroup');
		Route::get('/admin/groups/{group_id}/view', 'GroupController@viewGroup')->name('admin.groups.view');
		Route::get('/admin/groups/{group_id}/edit', 'GroupController@editGroup')->name('admin.groups.edit');
		Route::post('/admin/groups/{group_id}/edit', 'GroupController@updateGroup');
		Route::get('/admin/groups/{group_id}/status/{status}','GroupController@updateGroupStatus')->name('admin.groups.status');

		// Customers
		Route::get('/admin/customers', 'CustomerController@getCustomers')->name('admin.customers');
		Route::get('/admin/customers/{customer_id}/status/{status}','CustomerController@updateCustomerStatus')->name('admin.customers.status');
		Route::get('/admin/customers/{customer_id}/view', 'CustomerController@viewCustomer')->name('admin.customers.view');
		Route::post('/admin/customers/password-reset', 'CustomerController@updateCustomerPassword')->name('admin.customers.edit');

		// Service Requests
		Route::get('/admin/service-requests', 'ServiceRequestController@getServiceRequests')->name('admin.service_requests');
		Route::get('/admin/service-requests/{service_request_id}/view', 'ServiceRequestController@viewServiceRequest')->name('admin.service_requests.view');
		Route::get('/admin/service-requests/{service_request_id}/remove', 'ServiceRequestController@deleteServiceRequest')->name('admin.service_requests.delete');

		//Product
		Route::get('/admin/products', 'ProductController@getproduct')->name('admin.products');
		Route::get('/admin/products/add', 'ProductController@addProduct')->name('admin.products.add');
		Route::post('/admin/products/add', 'ProductController@postProduct');
		Route::get('/admin/products/{product_id}/view', 'ProductController@viewProduct')->name('admin.products.view');
		Route::get('/admin/products/{product_id}/edit', 'ProductController@editProduct')->name('admin.products.edit');
		Route::post('/admin/products/{product_id}/edit', 'ProductController@updateProduct');
		Route::get('/admin/products/{product_id}/status/{status}','ProductController@updateProductStatus')->name('admin.products.status');
		Route::post('/admin/getattributes','ProductController@getAttributes');
		Route::get('/admin/products/excel-download', 'ProductController@exportProducts')->name('admin.products.excel');
		Route::post('/admin/products/upload-product', 'ProductController@importProductExcel');
		Route::post('/admin/products/download-sample', 'ProductController@getSampleProduct');
		Route::post('/admin/check-product-details','ProductController@checkProductDetails');
		Route::post('/admin/check-update-product-details','ProductController@checkUpdateProductDetails');

		Route::get('/admin/products/{product_id}/duplicate', 'ProductController@duplicateProduct')->name('admin.products.duplicate');


		Route::get('/admin/products/{product_id}/delete', 'ProductController@deleteProduct')->name('admin.products.delete');

		Route::get('admin/product-image/{id}/delete', 'ProductController@deleteProductImage');


		// Coupon
		Route::get('/admin/coupons', 'CouponController@getCoupons')->name('admin.coupons');
		Route::get('/admin/coupons/add', 'CouponController@addCoupon')->name('admin.coupons.add');
		Route::post('/admin/coupons/add', 'CouponController@postCoupon');
		Route::get('/admin/coupons/{coupon_id}/view', 'CouponController@viewCoupon')->name('admin.coupons.view');
		Route::get('/admin/coupons/{coupon_id}/edit', 'CouponController@editCoupon')->name('admin.coupons.edit');
		Route::post('/admin/coupons/{coupon_id}/edit', 'CouponController@updateCoupon');
		Route::get('/admin/coupons/{coupon_id}/status/{status}','CouponController@updateCouponStatus')->name('admin.coupons.status');


		// Vendor
		Route::get('/admin/vendors', 'VendorController@getVendors')->name('admin.vendors');
		Route::get('/admin/vendors/{vendor_id}/view', 'VendorController@viewVendor')->name('admin.vendors.view');
		Route::get('/admin/vendors/{vendor_id}/status/{status}','VendorController@updateVendorStatus')->name('admin.vendors.status');


		// News Letter
		Route::get('/admin/news-letters', 'NewsLetterController@getNewsLetters')->name('admin.news-letters');
		Route::get('/admin/news-letters/add', 'NewsLetterController@addNewsLetter')->name('admin.news-letters.add');
		Route::post('/admin/news-letters/add', 'NewsLetterController@postNewsLetter');
		Route::get('/admin/news-letters/{news_letter_id}/delete', 'NewsLetterController@deleteNewsLetter')->name('admin.news-letters.delete');


		// Banners
		Route::get('/admin/banners', 'BannerController@getBanners')->name('admin.banners');
		Route::get('/admin/banners/add', 'BannerController@addBanner')->name('admin.banners.add');
		Route::post('/admin/banners/add', 'BannerController@postBanner');
		Route::get('/admin/banners/{banner_id}/edit', 'BannerController@editBanner')->name('admin.banners.edit');
		Route::post('/admin/banners/{banner_id}/edit', 'BannerController@updateBanner');
		Route::get('/admin/banners/{banner_id}/status/{status}','BannerController@updateBannerStatus')->name('admin.banners.status');
		Route::get('/admin/banners/{banner_id}/delete', 'BannerController@deleteBanner')->name('admin.banners.delete');

		// Brand Logos
		Route::get('/admin/brand-logos', 'BrandLogoController@getBrandLogos')->name('admin.brand-logos');
		Route::post('/admin/brand-logos/add', 'BrandLogoController@postBrandLogo')->name('admin.brand-logos.add');
		Route::post('/admin/brand-logos/{brand_logo_id}/edit', 'BrandLogoController@updateBrandLogo')->name('admin.brand-logos.edit');
		Route::get('/admin/brand-logos/{brand_logo_id}/status/{status}','BrandLogoController@updateBrandLogoStatus')->name('admin.brand-logos.status');
		Route::get('/admin/brand-logos/{brand_logo_id}/delete', 'BrandLogoController@deleteBrandLogo')->name('admin.brand-logos.delete');

		// Product Groups
		Route::get('/admin/product-groups', 'ProductGroupController@getProductGroups')->name('admin.product-groups');
		Route::post('/admin/product-groups/add', 'ProductGroupController@postProductGroup')->name('admin.product-groups.add');
		Route::post('/admin/product-groups/{product_group_id}/edit', 'ProductGroupController@updateProductGroup')->name('admin.product-groups.edit');
		Route::get('/admin/product-groups/{product_group_id}/status/{status}','ProductGroupController@updateProductGroupStatus')->name('admin.product-groups.status');

		// Orders
		Route::get('/admin/orders', 'OrderController@getOrders')->name('admin.orders');
		Route::get('/admin/orders/{order_id}/view', 'OrderController@viewOrder')->name('admin.orders.view');
		Route::get('/admin/orders/{order_id}/shipping-detail', 'OrderController@getShippingDetail')->name('admin.orders.edit');
		Route::post('/admin/orders/{order_id}/shipping-detail', 'OrderController@updateShippingDetail');
		Route::post('/admin/orders/{order_id}/order-status', 'OrderController@updateOrderStatus')->name('admin.orders.status');
		Route::get('/admin/customer/print/orders/{order_id}', 'OrderController@printOrderDetail');
		Route::post('/admin/vendor_assign/{order_id}/status', 'OrderController@vendorAssignStatus');

		Route::post('/admin/get_product', 'ProductController@getProductSearch');

			// Product Groups
		Route::get('/admin/product-review', 'ProductReviewController@getProductReview')->name('admin.product-review');
		// Route::post('/admin/product-groups/{product_group_id}/edit', 'ProductGroupController@updateProductGroup')->name('admin.product-groups.edit');
		Route::get('/admin/product-review/{id}/status/{status}','ProductReviewController@updateProductReviewStatus')->name('admin.product-review.status');
		Route::get('/admin/product-review/{id}/delete', 'ProductReviewController@deleteProductReview')->name('admin.product_review.delete');


		// Manual-Billing
		Route::get('/admin/manual-billing', 'ManualBillingController@getManualBilling')->name('admin.manual-billing');
		 Route::get('/admin/manual-billing-add', 'ManualBillingController@addManualBilling')->name('admin.manual-billing.add');
		Route::post('/admin/manual-billing/insert', 'ManualBillingController@postManualBilling');
		Route::get('/admin/manual-billing/{id}/edit', 'ManualBillingController@editManualBilling')->name('admin.manual-billing.edit');
		Route::post('/admin/manual-billing/{id}/update', 'ManualBillingController@updateManualBilling');
		Route::get('/admin/manual-billing/{id}/delete', 'ManualBillingController@deleteManualBilling')->name('admin.manual-billing.delete');
		Route::get('/admin/manual-billing-product/{id}/delete', 'ManualBillingController@deleteManualBillingProduct')->name('admin.manual-billing.delete');

		Route::get('/admin/manual_billing/print/{order_id}', 'ManualBillingController@printOrderDetail');

	});	

});	

